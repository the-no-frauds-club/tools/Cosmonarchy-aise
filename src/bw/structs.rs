#![allow(dead_code)]
#![allow(non_snake_case)]
use libc::c_void;
use bw_dat::UNIT_LIMIT;

#[repr(C, packed)]
#[derive(Debug, Clone, Copy)]
pub struct AiScript {
    pub next: *mut AiScript,//0x4
    pub prev: *mut AiScript,//0x8
    pub pos: u32,//0xc
    pub wait: u32,//0x10
    pub player: u32,
    pub area: Rect32,
    pub center: Point32,
    pub town: *mut AiTown,
    pub flags: u32,
}

unsafe impl Send for AiScript {}

#[repr(C)]
#[derive(Eq, PartialEq, Copy, Clone, Debug)]
pub struct TownReq {
    pub flags_and_count: u8,
    pub priority: u8,
    pub id: u16,
}

#[repr(C, packed)]
pub struct AiTown {
    pub next: *mut AiTown,                    // 0x0
    pub prev: *mut AiTown,                    // 0x4
    //pub free_workers: *mut c_void,   	// 0x8
	
    pub free_workers: *mut WorkerAiArray,     // 0x8
    pub workers: *mut WorkerAi,               // 0xc
    pub free_buildings: *mut BuildingAiArray, // 0x10
    pub buildings: *mut BuildingAi,           // 0x14
    pub player: u8,                           // 0x18 town player
    pub inited: u8,                           // 0x19
    pub worker_count: u8,					  // 0x1a
    pub worker_limit: u8,					  // 0x1b
    pub resource_area: u8,                    // 0x1c
    pub resource_units_not_set: u8,     //0x1d
    pub in_battle: u8,                  //0x1e
    pub unk1f: u8,                      //0x1f
    pub position: Point,                //0x20
    pub main_building: *mut Unit,       //0x24
    pub building_scv: *mut Unit,
    pub mineral: *mut Unit,
    pub gas_buildings: [*mut Unit; 0x3],
    pub town_units: [TownReq; 0x64],
}

#[repr(C, packed)]
pub struct AiTownArray {
    pub towns: [AiTown; 100],
    pub first_free: *mut AiTown,
}

#[repr(C)]
pub struct AiTownList {
//    pub array: *mut AiTown,
	pub array: *mut AiTownArray,
    pub first: *mut AiTown,
}

#[repr(C, packed)]
pub struct WorkerAi {
    pub next: *mut WorkerAi,
    pub prev: *mut WorkerAi,
    pub ai_type: u8,//0x8
    pub target_resource: u8,//0x9
    pub reassign_count: u8,//0xa
    pub wait_timer: u8,//0xb
    pub last_update_second: u32,//0xc
    pub parent: *mut Unit,//0x10
    pub town: *mut AiTown,//0x14
}

#[repr(C, packed)]
pub struct BuildingAi {
    pub next: *mut BuildingAi,
    pub prev: *mut BuildingAi,
    pub ai_type: u8,
    pub train_queue_types: [u8; 0x5],
    pub dce: [u8; 0x2],
    pub parent: *mut Unit, // 0x10
    pub town: *mut AiTown,//0x14
    pub train_queue_values: [*mut c_void; 0x5],//0x18
	
}

#[repr(C, packed)]
pub struct WorkerAiArray {
    pub ais: [WorkerAi; 1000],
    pub first_free: *mut WorkerAi,
}

#[repr(C, packed)]
pub struct BuildingAiArray {
    pub ais: [BuildingAi; 1000],
    pub first_free: *mut BuildingAi,
}

#[repr(C, packed)]
pub struct GuardAiList {
    pub array: *mut GuardAiArray,
    pub first: *mut GuardAi,
}

#[repr(C, packed)]
pub struct GuardAiArray {
    pub ais: [GuardAi; 1000],
    pub first_free: *mut GuardAi,
}

#[repr(C, packed)]
pub struct GuardAi {
    pub next: *mut GuardAi,//0x0
    pub prev: *mut GuardAi,//0x4
    pub ai_type: u8,//0x8
    pub times_died: u8,//0x9
    pub dca: [u8; 0x2],//0xb
    pub parent: *mut Unit,//0xc
    pub unit_id: u16,//0x10
    pub home: Point,//0x12
    pub other_home: Point,//0x16
    pub padding1a: [u8; 0x2],//0x1a
    pub previous_update: u32,//0x1c
}

pub struct File;

#[repr(C, packed)]
pub struct AiRegion {
    pub id: u16,
    pub target_region_id: u16,
    pub player: u8,
    pub state: u8,
    pub unk_val: u8,
    pub unk_count: u8,
    pub flags: u8,
    pub unk: u8,
    pub ground_unit_count: u16,
    pub needed_ground_strength: u16,
    pub needed_air_strength: u16,
    pub local_military_ground_strength: u16,
    pub local_military_air_strength: u16,
    pub all_military_ground_strength: u16,
    pub all_military_air_strength: u16,
    // Are these ordered correctly?
    pub enemy_air_strength: u16,
    pub enemy_ground_strength: u16,
    pub air_target: *mut Unit,
    pub ground_target: *mut Unit,
    pub slowest_military: *mut Unit,
//    pub dc28: [u8; 0x4],
	pub detector: *mut Unit,
    pub military: MilitaryAiList,
}

#[repr(C, packed)]
pub struct MilitaryAiList {
    pub array: *mut MilitaryAiArray,
    pub first: *mut MilitaryAi,
}

#[repr(C, packed)]
pub struct MilitaryAiArray {
    pub ais: [MilitaryAi; 1000],
    pub first_free: *mut MilitaryAi,
}

#[repr(C, packed)]
pub struct MilitaryAi {
    pub next: *mut MilitaryAi,
    pub prev: *mut MilitaryAi,
    pub ai_type: u8,
    pub unk9: u8,
    pub unka: u16,
    pub parent: *mut Unit,
    pub region: *mut AiRegion,
}

#[repr(C, packed)]
pub struct PlayerAiData {
    pub mineral_need: u32,
    pub gas_need: u32,
    pub supply_need: u32,
    pub minerals_available: u32,
    pub gas_available: u32,
    pub supply_available: u32,
    pub requests: [AiSpendingRequest; 0x3f],
    pub request_count: u8,
	pub currently_building_bunkers: u8,
    pub dc211: [u8; 0x6], // builtSomething
    //pub nuke_rate: u16,
    //pub attacks: u8,
    //pub last_nuke_time: u32,
    pub flags: u16,
    pub panic_block: [u8; 0x4],
    //pub max_force: u16,
    pub attack_grouping_region: u16,
    pub train_unit_id: u16,
//    pub dc222: [u8; 0x2],
    pub default_min_strength_for_regions: u8,
    pub defense_priority_base: u8,
	//
    pub previous_building_hit_second: u32,
    pub last_attack_second: u32,
    pub strategic_suicide_mission_cooldown: u8,
    pub spell_cooldown: u8,
    pub attack_failed: u8,
    pub difficulty: u8,
    pub attack_force: [u16; 0x3f],
    pub attack_force_zero_terminator: u16,
    pub ground_vs_ground_build_def: [u16; 0x14],
    pub ground_vs_air_build_def: [u16; 0x14],
    pub air_vs_ground_build_def: [u16; 0x14],
    pub air_vs_air_build_def: [u16; 0x14],
    pub ground_vs_ground_use_def: [u16; 0x14],
    pub ground_vs_air_use_def: [u16; 0x14],
    pub air_vs_ground_use_def: [u16; 0x14],
    pub air_vs_air_use_def: [u16; 0x14],
    pub build_limits: [u8; UNIT_LIMIT as usize], // [u8; 0xe4],
    pub free_medic: *mut Unit,
    pub dc4e8: [u8; 0x10],
}

#[repr(C, packed)]
#[derive(Copy, Clone)]
pub struct AiSpendingRequest {
    pub priority: u8,
    pub ty: u8,
    pub id: u16,
    pub val: *mut c_void,
}

#[repr(C)]
pub struct Supplies {
    pub provided: [u32; 0xc],
    pub used: [u32; 0xc],
    pub max: [u32; 0xc],
}

#[repr(C)]
pub struct ResourceArea {
	pub position: [u16; 0x2],
	pub mineral_field_count: u8,
	pub geyser_count: u8,
	pub is_start_location: bool,
	pub flags: u8,
	pub total_mineral_count: u32,
	pub total_gas_count: u32,
	pub unk10: [u32; 0x8],
}

#[repr(C)]
pub struct ResourceAreaArray {
	pub areas: [ResourceArea; 0xfa],
	pub used_count: u32,
	pub frames_till_update: u32,
}

#[repr(C, packed)]
pub struct Game {
    pub minerals: [u32; 0xc],
    pub gas: [u32; 0xc],
    pub dc60: [u8; 0x84],
    pub map_width_tiles: u16,
    pub map_height_tiles: u16,
    pub dce8: [u8; 0x4],
    pub tileset: u16,
    pub dcee: [u8; 0x5e],
    pub frame_count: u32,
		
    pub dc150: [u8; 0x3c],
    pub _unit_availability: [[u8; 0xe4]; 0xc],
    pub dcc3c: [u8; 0x204a],
    pub player_color_palette: [[u8; 0x8]; 0xc],
    pub player_minimap_color: [u8; 0xc],
    //pub dc2cf2: [u8; 0x362],
	
	pub dc2cf2: [u8; 0x2],
	pub unit_count_score: [u32; 0xc],//2cf4
	pub dc2d24: [u8; 0x270],//2d24
	
	pub factories_built_score: [u32; 0xc],//2f94
	pub dc60_2: [u8; 0x90],//2fc4
	
    pub supplies: [Supplies; 0x3],//3054
    pub dc3204: [u8; 0x30],
    pub _all_units_count: [[u32; 0xc]; 0xe4],
    pub _completed_units_count: [[u32; 0xc]; 0xe4],
    pub _unit_kills: [[u32; 0xc]; 0xe4],
    pub _deaths: [[u32; 0xc]; 0xe4],
    pub tech_availability_sc: [[u8; 0x18]; 0xc],
    pub tech_level_sc: [[u8; 0x18]; 0xc],
    pub dcdf74: [u8; 0x24],
    pub upgrade_limit_sc: [[u8; 0x2e]; 0xc],
    pub upgrade_level_sc: [[u8; 0x2e]; 0xc],
    pub dce3e8: [u8; 0xd8],
    pub player_forces: [u8; 0x8],
    pub force_flags: [u8; 0x4],
    pub force_names: [[u8; 0x1e]; 0x4],
    pub alliances: [[u8; 0xc]; 0xc],
    pub dce5d4: [u8; 0x34],
    pub elapsed_seconds: u32,
    pub dce60c: [u8; 0x564],
    pub locations: [Location; 0xff],
    pub dcff5c: [u8; 0x4],
    pub tech_availability_bw: [[u8; 0x14]; 0xc],
    pub tech_level_bw: [[u8; 0x14]; 0xc],
    pub dc10140: [u8; 0x48],
    pub upgrade_limit_bw: [[u8; 0xf]; 0xc],
    pub upgrade_level_bw: [[u8; 0xf]; 0xc],
}

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct Location {
    pub area: Rect32,
    pub unk: u16,
    pub flags: u16,
}

pub struct GrpSprite;
pub struct ReplayData;

#[repr(C, packed)]
pub struct MegatileVx4ex {
	pub minitiles: [u32; 0x10],//old is u16
}

#[repr(C,packed)]
pub struct TileGroupCv5Ex {
	pub unk0: u16,
	pub flags: u16,
	pub padding: [u8;0x10],
	pub megatiles: [u32;0x10],
}

#[repr(C, packed)]
pub struct Minitile {
	pub colors: [u8; 0x40],
}
#[repr(C, packed)]
pub struct Iscript {
	pub header: u16,
	pub pos: u16,
	pub return_pos: u16,
	pub animation_id: u8,
	pub wait: u8,

}
#[repr(C, packed)]
pub struct Image {
    pub prev: *mut Image,//0x0 
    pub next: *mut Image,//0x4
	pub image_id: u16,//0x8
	pub drawfunc: u8,//0xa
	pub direction: u8,//0xb
	pub flags: u16,//0xc
	pub x_off: u8,//0xe
	pub y_off: u8,//0xf	
	
//	pub iscript_header_offset: u16,
//	pub iscript_offset: u16,
//	pub unk14: u16,
//	pub animation: u8,
//	pub wait: u8,

	pub iscript: Iscript,
	
	

	pub frameset: u16,
	pub frame_index: u16,//0x1a
	
	pub map_position: Point,
	pub screen_position: Point,
	pub grp_bounds: Rect,
	pub grp: *mut GrpSprite,
	pub drawfunc_param: *mut c_void,
	pub render_function: *mut c_void,
	pub update_function: *mut c_void,
	pub parent: *mut Sprite,
}

//pub struct Control;

#[repr(C, packed)]
pub struct GptpSaveState {
	pub len: u32,
	pub ptr: *const u8,
}

#[repr(C, packed)]
pub struct ColorShiftData {
	pub index: u32,
	pub data: *mut libc::c_void,
	pub name: [u8; 0xc],
}
#[repr(C, packed)]
pub struct PaletteRemap {
	pub index: u32,
	pub data: *mut libc::c_void,
}
#[repr(C, packed)]
pub struct Button {
	pub pos: u16,
	pub icon_id: u16,
	pub condition: *mut libc::c_void,
	//pub action: *mut libc::c_void,	
	pub action: unsafe extern "fastcall" fn(u32, u32),
	pub cond_var: u16,
	pub act_var: u16,
	pub enabled_string_id: u16,
	pub disabled_string_id: u16,
}
#[repr(C, packed)]
pub struct Event {
	pub extended_command_type: u32,
	pub ext_param: u32,
	pub value: u32,
	pub event_type: u16,
	pub xpos: u16,
	pub ypos: u16,
}

#[repr(C,packed)]
pub struct DialogEvent {
    pub user: u32,
    pub field_4: u16,
    pub field_6: u16,
    pub virtual_key: u16,
    pub virtual_key_mode: u16,
    pub number: u16,
    pub cursor_x: u16,
    pub cursor_y: u16,
    pub field_12: u16,
}

#[repr(C, packed)]
pub struct Control {
	pub next: *mut Control,
	pub area: Rect,
	pub dcc: [u8; 0x8],
	pub string: *const u8,
	pub flags: u8,
	pub dc19: [u8;0x7],
	pub id: u16,
	pub control_type: u16,
	pub button_icon: u16,
	pub val: *mut libc::c_void,
	// button control val is *Button
	pub event_handler: *mut libc::c_void,
	pub draw: *mut libc::c_void,
	pub dialog: *mut Control,//dialog
	pub control_specific: [u8;0x20],
}
//size = 0x56  

#[repr(C, packed)]
pub struct Player {
	pub id: u32,
	pub storm_id: u32,
	pub ty: u8,
	pub race: u8,
	pub team: u8,
	pub name: [u8;25],
}

#[repr(C, packed)]
pub struct Sprite {
    pub prev: *mut Sprite,
    pub next: *mut Sprite,
    pub sprite_id: u16,
    pub player: u8,
    pub selection_index: u8,
    pub visibility_mask: u8,
    pub elevation_level: u8,
    pub flags: u8,
    pub selection_flash_timer: u8,
    pub index: u16,
    pub width: u8,
    pub height: u8,
    pub position: Point,
    pub main_image: *mut Image,
    pub first_image: *mut Image,
    pub last_image: *mut Image,
}

#[repr(C, packed)]
pub struct LoneSprite {
    pub prev: *mut LoneSprite,
    pub next: *mut LoneSprite,
    pub value: u32,
    pub sprite: *mut Sprite,
}

#[repr(C, packed)]
pub struct Order {
    pub prev: *mut Order,
    pub next: *mut Order,
    pub order_id: u8,
    pub padding: u8,
    pub unit_id: u16,
    pub position: Point,
    pub target: *mut Unit,
}


#[repr(C)]
#[derive(Copy, Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct Rect32 {
    pub left: i32,
    pub top: i32,
    pub right: i32,
    pub bottom: i32,
}

#[repr(C)]
#[derive(Copy, Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct Rect {
    pub left: i16,
    pub top: i16,
    pub right: i16,
    pub bottom: i16,
}

#[repr(C)]
#[derive(Copy, Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct Point {
    pub x: i16,
    pub y: i16,
}

#[repr(C)]
#[derive(Copy, Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct Point32 {
    pub x: i32,
    pub y: i32,
}


#[repr(C, packed)]
pub struct UnitAi               // sizeof 0x2C
{
	pub next: *mut UnitAi,      // 0x00
    pub prev: *mut UnitAi,      // 0x04
    pub ai_type: u8,            // 0x08
    pub unk_9: u8,              // 0x09
    pub unk_A: u8,              // 0xA
    pub timer: u8,              // 0xB
    pub unit: *mut Unit,        // 0xC
    pub region: *mut Region,    // 0x10
    pub town: *mut AiTown,        // 0x14
    pub unk_18: u32,            // 0x18
    pub unk_1C: u32,            // 0x1C
    pub unk_20: u32,            // 0x20
    pub unk_24: u32,            // 0x24
    pub unk_28: u32,            // 0x28
}


#[repr(C, packed)]
pub struct GrpFrame {
	pub frame_count: u16,
	pub width: u16,
	pub height: u16,
	pub frame_headers: *mut GrpFrameHeader,
}
#[repr(C, packed)]
pub struct GrpFrameHeader {
	pub x: u8,
	pub y: u8,
	pub w: u8,
	pub h: u8,
	pub offset: *mut GrpFrame,
}


#[repr(C, packed)]
pub struct Bullet {
	pub prev: *mut Bullet,
	pub next: *mut Bullet,
	pub hitpoints: i32,
	pub sprite: *mut Sprite,
	pub move_target: Point,
	pub move_target_unit: *mut Unit,
	pub next_move_waypoint: Point,
	pub unk_move_waypoint: Point,
	pub flingy_flags: u8,
	pub facing_direction: u8,
	pub flingy_turn_speed: u8,
	pub movement_direction: u8,
	pub flingy_id: u16,
	pub unk_26: u8,
	pub flingy_movement_type: u8,
	pub position: Point,
	pub exact_position: Point32,
	pub flingy_top_speed: u32,
	pub current_speed: i32,
	pub next_speed: i32,
	pub speed: i32,
	pub speed2: i32,
	pub acceleration: u16,
	pub new_direction: u8,
	pub target_direction: u8,
	// Flingy end
	pub player: u8,
	pub order: u8,
	pub order_state: u8,
	pub order_signal: u8,
	pub order_fow_unit: u16,
	pub unused52: u16,
	pub order_timer: u8,
	pub ground_cooldown: u8,
	pub air_cooldown: u8,
	pub spell_cooldown: u8,	
	pub order_target_pos: Point,
	pub target: *mut Unit,
	//Unit/bullet end
	pub weapon_type: u8,
	pub time_remaining: u8,
	pub hit_flags: u8,
	pub remaining_bounces: u8,
	pub source_unit: *mut Unit,
	pub next_bounce_unit: *mut Unit,
	pub cyclic_missile_index: u32,
}

#[repr(C, packed)]
pub struct Flingy {
    pub prev: *mut Unit,
    pub next: *mut Unit,
    pub hitpoints: i32,//0x8
    pub sprite: *mut Sprite,//0xc
    pub move_target: Point,//0x10
    pub move_target_unit: *mut Unit,//0x14
    pub next_move_waypoint: Point,//0x18
    pub unk_move_waypoint: Point,
    pub flingy_flags: u8,
    pub facing_direction: u8,
    pub flingy_turn_speed: u8,
    pub movement_direction: u8,
    pub flingy_id: u16,
    pub unk_26: u8,
    pub flingy_movement_type: u8,
    pub position: Point,
    pub exact_position: Point32,
    pub flingy_top_speed: u32,
    pub current_speed: i32,
    pub next_speed: i32,
    pub speed: i32,
    pub speed2: i32,
    pub acceleration: u16,
    pub new_direction: u8,
    pub target_direction: u8,
}

#[repr(C, packed)]
pub struct UnitPositions {
	pub val: *mut c_void,
	pub key: u32,
}

#[repr(C, packed)]
pub struct Unit {
    pub prev: *mut Unit,
    pub next: *mut Unit,
    pub hitpoints: i32,//0x8
    pub sprite: *mut Sprite,//0xc
    pub move_target: Point,//0x10
    pub move_target_unit: *mut Unit,//0x14
    pub next_move_waypoint: Point,//0x18
    pub unk_move_waypoint: Point,
    pub flingy_flags: u8,
    pub facing_direction: u8,
    pub flingy_turn_speed: u8,
    pub movement_direction: u8,
    pub flingy_id: u16,
    pub unk_26: u8,
    pub flingy_movement_type: u8,
    pub position: Point,
    pub exact_position: Point32,
    pub flingy_top_speed: u32,
    pub current_speed: i32,
    pub next_speed: i32,
    pub speed: i32,
    pub speed2: i32,
    pub acceleration: u16,
    pub new_direction: u8,
    pub target_direction: u8,
    // Flingy end
    pub player: u8,
    pub order: u8,
    pub order_state: u8,
    pub order_signal: u8,
    pub order_fow_unit: u16,
    pub egg_origin: u8,
    pub unused53: u8,
    pub order_timer: u8,
    pub ground_cooldown: u8,
    pub air_cooldown: u8,
    pub spell_cooldown: u8,
	
    pub order_target_pos: Point,
    pub target: *mut Unit,
    // Entity end
    pub shields: i32,
    pub unit_id: u16,
    pub unused66: u16,
    pub next_player_unit: *mut Unit,
    pub prev_player_unit: *mut Unit,
    pub subunit: *mut Unit,
    pub order_queue_begin: *mut Order,
    pub order_queue_end: *mut Order,
    pub previous_attacker: *mut Unit,
    pub related: *mut Unit,
    pub highlight_order_count: u8,
    pub order_wait: u8,
    pub unk86: u8,
    pub attack_notify_timer: u8,
    pub previous_unit_id: u16,
    pub minimap_draw_counter: u8,
    pub minimap_draw_color: u8,
    pub unused8c: u16,
    pub rank: u8,
    pub kills: u8,
    pub last_attacking_player: u8,
    pub secondary_order_wait: u8,
    pub ai_spell_flags: u8,
    pub order_flags: u8,
    pub buttons: u16,
    pub invisibility_effects: u8,
    pub movement_state: u8,
    pub build_queue: [u16; 5],
    pub energy: u16,
    pub current_build_slot: u8,
    pub minor_unique_index: u8,
    pub secondary_order: u8,
    pub building_overlay_state: u8,
    pub build_hp_gain: u16,
    pub build_shield_gain: u16,
    pub remaining_build_time: u16,
    pub previous_hp: u16,
    pub loaded_units: [u16; 8],
    pub unit_specific: [u8; 16],
	
    pub unit_specific2: [u8; 12],
    pub flags: u32,
    pub carried_powerup_flags: u8,
    pub wireframe_seed: u8,
    pub secondary_order_state: u8,
    pub move_target_update_timer: u8,
    pub detection_status: u32,
    pub unke8: u16,
    pub unkea: u16,
    pub currently_building: *mut Unit,
    pub next_invisible: *mut Unit,
    pub prev_invisible: *mut Unit,
//    pub rally_pylon: [u8; 8],
	pub rally_point: Point,
	pub rally_unit: *mut Unit,
    pub path: *mut c_void,
    pub path_frame: u8,
    pub pathing_flags: u8,
    pub _unk106: u8,
    pub _unk107: u8,
    pub collision_points: [u16; 0x4],
    pub death_timer: u16,
    pub defensive_matrix_dmg: u16,
    pub matrix_timer: u8,
    pub stim_timer: u8,
    pub ensnare_timer: u8,
    pub lockdown_timer: u8,
    pub irradiate_timer: u8,
    pub stasis_timer: u8,
    pub plague_timer: u8,
    pub is_under_storm: u8,
    pub irradiated_by: *mut Unit,
    pub irradiate_player: u8,
    pub parasited_by_players: u8,
    pub master_spell_timer: u8,
    pub is_blind: u8,
    pub maelstrom_timer: u8,
    pub _unk125: u8,
    pub acid_spore_count: u8,
    pub acid_spore_timers: [u8; 0x9],
	pub offsetindex_3x3: u16,
	pub _dc132: u8,
	pub _dc133: u8,
//    pub _dc130: [u8; 0x4],
    pub ai: *mut c_void,
//	pub ai: *mut UnitAi,
//    pub _dc138: [u8; 0x18],
	pub air_strength: u16,
	pub ground_strength: u16,
	pub position_search_left: u32,
	pub position_search_right: u32,
	pub position_search_top: u32,
	pub position_search_bottom: u32,
	pub repulse_misc: u8,
	pub repulse_direction: u8,
	pub repulse_chunk_x: u8,
	pub repulse_chunk_y: u8,
}




#[repr(C, packed)]
pub struct Pathing {
    pub region_count: u16,
    pub _dc2: [u8; 0x449fa],
    pub regions: [Region; 5000],
    pub _dc92bfc: [u8; 0x4e24],
}

/*

pub struct ContourData;
#[repr(C, packed)]
pub struct Pathing {
	pub region_count: u16,//0x0
	pub unk1: u16,
	pub unk2: *mut u16,//0x4
	pub split_region_unk: u32,//should be separate struct //0x8
	pub map_tile_regions: [[u16; 0x100]; 0x100],
	pub split_regions: [u32;0x927c],//0x2000c
	pub regions: [Region;0x1388],//0x449FC
	pub many_neighbour_ids: [u16;0x2710],//0x92BFC
	pub contour_data: *mut ContourData,//97A1C
}
*/

#[repr(C, packed)]
pub struct Region {
    pub unk: u16,
    pub group: u16,
    pub _dc4: [u8; 0x14],
    pub area: Rect,
    pub _dc20: [u8; 0x20],
}

#[repr(C, packed)]
pub struct GameData {
    pub save_time: u32,
    pub game_name: char,
    // GameStatstringData:
    pub save_timestamp: u32,
    pub map_width_tiles: u16,
    pub map_height_tiles: u16,
    pub active_human_players: u8,
    pub max_players: u8,
    pub game_speed: u8,
    pub approval_status: u8,
    pub game_type_id: u8,
    pub game_type_unk: u8, // 0x28
    pub game_type_param: u16,
    pub cdkey_hash: u32,
    pub tileset: u16,
    pub is_replay: u8,
    pub active_computer_players: u8,
    pub host_name: u8,
    pub map_title: char,
    //
//  pub game_template: // not sure what data type this would be, pointer to another structure?
}

#[repr(C, packed)]
pub struct GameTemplate {
    pub game_type_id: u8,
    pub game_type_unk: u8,
    pub game_type_param: u16,
    pub victory_conditions: u8, // 0x0 = UMS triggers | 0x4 = ? | 0x5 = ?
    pub starting_resources: u8,
    pub dont_use_ums_unitstats: bool,
    pub fog_mode: u8, // 0x0 = Off | 0x1 = No fog | 0x2 = Workers + town center
    pub starting_units: u8, // 0x0 = UMS | 0x1 = Workers | 0x2 = Workers + town center
    pub starting_positions: u8, // 0x0 = Random
    pub flags: u8, // 0x1 = Allow computers | 0x2 = Allow singleplayer
    pub allow_alliances: bool,
    pub team_game: u8,
    pub allow_cheats: bool,
    pub unk_tournament: bool,
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn test_sizes() {
        use std::mem;
        assert_eq!(mem::size_of::<AiScript>(), 0x34);
        assert_eq!(mem::size_of::<AiRegion>(), 0x34);
        assert_eq!(mem::size_of::<AiTown>(), 0x1cc);
        assert_eq!(mem::size_of::<BuildingAi>(), 0x2c);
        assert_eq!(mem::size_of::<WorkerAi>(), 0x18);
        assert_eq!(mem::size_of::<GuardAi>(), 0x20);
        assert_eq!(mem::size_of::<GuardAiArray>(), 0x7d04);
        assert_eq!(mem::size_of::<MilitaryAi>(), 0x14);
        assert_eq!(mem::size_of::<MilitaryAiArray>(), 0x4e24);
        assert_eq!(mem::size_of::<PlayerAiData>(), 0x4e8);
        assert_eq!(mem::size_of::<Game>(), 0x102f0);
        assert_eq!(mem::size_of::<Unit>(), 0x150);
        assert_eq!(mem::size_of::<Sprite>(), 0x24);
        assert_eq!(mem::size_of::<Image>(), 0x40);		
        assert_eq!(mem::size_of::<Pathing>(), 0x97a20);
        assert_eq!(mem::size_of::<Region>(), 0x40);
		assert_eq!(mem::size_of::<Player>(), 0x24);
		assert_eq!(mem::size_of::<Pathing>(), 0x97a20);
		assert_eq!(mem::size_of::<DialogEvent>(), 0x14); // from IDA
    }
}
