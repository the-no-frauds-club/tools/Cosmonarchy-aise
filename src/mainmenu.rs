use config::{self, Config};
use globals::{
    Globals,
};
use bw_dat::{UnitId};
use std::ptr::null_mut;
use bw;
use game::Game;
use aiscript;
use unit::{Unit};
use std::ptr;
/*
// 004ADF20 = gluCustm_raceDropdown -- https://pastebin.com/raw/pExGxMH5
pub unsafe fn glucustm_race_dropdown_hook(
    bin: *mut bw::Control, // EAX
    orig: &dyn Fn(*mut bw::Control)) {
        let mut binVar = bin;
        *bin.flags |= 0x80000000; // Frozen? Intercepts mouse move?
        let mut raceSlc = bw::singleplayer_race_select;
        let mut string = *raceSlc.0x1 - 1;
        let mut pszRaceStr: u8 = 0;
        let mut listboxAdd: u8 = 0;
        let mut value: u32 = 0;
        while raceSlc < bw::singleplayer_race_select {
            if string != null_mut() {
                if string < bw::network_tbl {
                    pszRaceStr = bw::network_tbl as u8 + bw::network_tbl[string + 1];
                }
                else {
                    pszRaceStr = "";
                }
            }
            else {
                pszRaceStr = 0;
            }
            listboxAdd = bw::listbox_add(pszRaceStr, binVar, 0);
            if listboxAdd == -1 {
                break;
            }
            value = raceSlc.0
            raceSlc += 1;
            ((*binVar.0x42) + 4 * listboxAdd) = value;
        }
        let mut flags = *binVar.flags;
        if flags < 0 {
            *binVar.flags = flags & 0x7FFFFFFF;
            bw::list_update(binVar);
        }
        if *binVar.next + 0x46 > 3 {
            let mut eleven: u32 = 11;
            (*binVar.next + 0x2A)(binVar, eleven as *mut Control);
            bw::set_selected_dialog(3, binVar);
        }
    }
*/
/*
// 004AD850 = InitSinglePlayerCustom(), eax Dialog *custom_game -- https://pastebin.com/raw/hmKHU5xv
    pub unsafe fn init_singleplayer_custom_hook(
    bin: *mut bw::Control,
    orig: &dyn Fn(*mut bw::Control)->player_struct)->player_struct {
        
    }
*/
/*
// 004EF100 = InitGameSomething?() -- https://pastebin.com/raw/DCJ9awNc
    pub unsafe fn init_game_something_hook(

*/