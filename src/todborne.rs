#![allow(non_snake_case)]
#![allow(dead_code)]

//use std::ffi::CString;
use std::ptr::null_mut;
//use std::slice;

use bw;
use game::Game;
use globals::{
    self, NewGameState,
	CommTowerMenu,OverlordFaction,ActionButton,HydraEffect,SpriteEffect,Cullen
};
//use idle_orders::Rate;
//use rng::Rng;
//use swap_retain::SwapRetain;
use unit::{HashableUnit, Unit, SerializableSprite};
//use config::{self, Config};
use std::collections::hash_map::Entry;

impl NewGameState {
	pub fn class_unlocked(&mut self, eq_class: u16)->bool {
		if let Some(ref mut cullen) = self.cullen {
			let u = cullen.unlocked_classes.iter_mut().find(|x| **x==eq_class).is_some();
			return u;
		}
		return false;
	}
	pub fn unlock_class(&mut self, eq_class: u16){
	//unlock_class
		if let Some(ref mut cullen) = self.cullen {
			if cullen.unlocked_classes.iter_mut().find(|x| **x==eq_class).is_none(){
				cullen.unlocked_classes.push(eq_class);
			}		
		}
		else {
			self.cullen = Some(Cullen{unlocked_classes: vec![eq_class], equipped_class: 20});
		}

	}
	pub fn equip_class(&mut self, eq_class: u16){
		if let Some(ref mut cullen) = self.cullen {
			cullen.equipped_class = eq_class;
		}		
		
	}
	pub unsafe fn get_moderation_stacks(&mut self, target: Unit)->u32 {	
		
		let result = match self.new_units.entry(HashableUnit(target)){
			Entry::Vacant(_e) => {return 0;},
			Entry::Occupied(entry) => entry.into_mut().moderation.len(),
		};
		result as u32
	}

	pub unsafe fn get_spatula_status(&mut self, target: Unit)->u32 {	
		
		let result = match self.new_units.entry(HashableUnit(target)){
			Entry::Vacant(_e) => {return 0;},
			Entry::Occupied(entry) => entry.into_mut(),
		};
		result.steeled_spatula as u32
	}	
	pub unsafe fn death_by_dough_status(&mut self, target: Unit)->u32 {	
		
		let result = match self.new_units.entry(HashableUnit(target)){
			Entry::Vacant(_e) => {return 0;},
			Entry::Occupied(entry) => entry.into_mut(),
		};
		if result.death_by_dough.is_none(){
			return 0;
		}
		//bw_print!("Status: {}",result.death_by_dough.unwrap());
		let answer = match result.death_by_dough.unwrap() {
			5=>1,
			_=>0,
		};
		answer
	}	
	
	pub unsafe fn set_void(&mut self, target: Unit, safety: u32){
		bw_print!("set void timer");
		let u_str = self.default_unit_struct(target);
		let mut result = match self.new_units.entry(HashableUnit(target)) {
		   Entry::Vacant(entry) => entry.insert(u_str),
		   Entry::Occupied(entry) => entry.into_mut(),
		};
		result.void_timer = 19;
		result.safe_void = safety!=0;
		
	}
	pub unsafe fn set_goodboy_timer(&mut self, target: Unit){
		let u_str = self.default_unit_struct(target);
		let mut result = match self.new_units.entry(HashableUnit(target)) {
		   Entry::Vacant(entry) => entry.insert(u_str),
		   Entry::Occupied(entry) => entry.into_mut(),
		};
		result.goodboy_timer = 5*24;	
	}
	pub unsafe fn set_frosted_timer(&mut self, target: Unit){
		let u_str = self.default_unit_struct(target);
		let mut result = match self.new_units.entry(HashableUnit(target)) {
		   Entry::Vacant(entry) => entry.insert(u_str),
		   Entry::Occupied(entry) => entry.into_mut(),
		};
		result.frosted_timer = 5*24;
	}
		
	pub unsafe fn add_n_moderation_stacks(&mut self, target: Unit, stack_count: u32, overload: u32){	
		let u_str = self.default_unit_struct(target);
		let mut result = match self.new_units.entry(HashableUnit(target)) {
		   Entry::Vacant(entry) => entry.insert(u_str),
		   Entry::Occupied(entry) => entry.into_mut(),
		};		
		//bw_print!("add {} stacks",stack_count);
		let mut count = stack_count;
		if overload !=0 {
			bw_print!("1SetCanoverloadToTrue");
			result.can_overload=true;
		}
		while result.moderation.len()<20 && count>0 {
			result.moderation.push(48);
			count-=1;
		}
		
	}	
	pub unsafe fn switch_spatula(&mut self, source: Unit){
		let u_str = self.default_unit_struct(source);
		let mut result = match self.new_units.entry(HashableUnit(source)) {
		   Entry::Vacant(entry) => entry.insert(u_str),
		   Entry::Occupied(entry) => entry.into_mut(),
		};
		result.steeled_spatula = !result.steeled_spatula;
	}
	pub fn iterate_death_by_dough(&mut self, source: Unit){
		let u_str = self.default_unit_struct(source);
		let mut result = match self.new_units.entry(HashableUnit(source)) {
		   Entry::Vacant(entry) => entry.insert(u_str),
		   Entry::Occupied(entry) => entry.into_mut(),
		};
		if let Some(ref mut dbd) = result.death_by_dough {
			*dbd += 1;
			//bw_print!("Iterate: {}",dbd);
			if *dbd > 5 {
				*dbd = 0;
			}
		}
		else {
			result.death_by_dough = Some(0);
		}
	}	
	
	pub fn is_deathwishing(&mut self, unit_ptr: *mut bw::Unit)->u32{
		let result = match self.new_units.entry(HashableUnit(Unit::from_ptr(unit_ptr).unwrap())){
			Entry::Vacant(_e) => {return 0;},
			Entry::Occupied(entry) => entry.into_mut().deathwish,
		};
		let answer = match result {
			false=>0,
			true=>1,
		};
		answer	
	}

	pub fn has_goodboy_timer(&mut self,unit_ptr: *mut bw::Unit)->u32 {
		let result = match self.new_units.entry(HashableUnit(Unit::from_ptr(unit_ptr).unwrap())){
			Entry::Vacant(_e) => {return 0;},
			Entry::Occupied(entry) => entry.into_mut().goodboy_timer,
		};
		let answer = match result {
			0=>0,
			_=>1,
		};
		answer		
	}
	pub fn frosted_status(&mut self,unit_ptr: *mut bw::Unit)->u32 {
		let result = match self.new_units.entry(HashableUnit(Unit::from_ptr(unit_ptr).unwrap())){
			Entry::Vacant(_e) => {return 0;},
			Entry::Occupied(entry) => entry.into_mut().frosted_timer,
		};
		
		let answer = match result {
			0=>0,
			_=>1,
		};
		answer		
	}	
	
	pub unsafe fn add_moderation_stack(&mut self, target: Unit, up_lev: u32)->u16{			
		let u_str = self.default_unit_struct(target);
		let mut result = match self.new_units.entry(HashableUnit(target)) {
		   Entry::Vacant(entry) => entry.insert(u_str),
		   Entry::Occupied(entry) => entry.into_mut(),
		};
		let stacks = result.moderation.len();
		if up_lev >= 2 {
			bw_print!("Set CanOverload to true");
			result.can_overload=true;
		}
		for s in &mut result.moderation {
			*s = 48;
		}
		if stacks < 20 {
			result.moderation.push(48);
		}	
		
		stacks as u16
	}

	pub unsafe fn add_bern_effect(&mut self, source: Unit, sprite: *mut bw::Sprite, global_extra: u16, origin: bw::Point){	
		if sprite==null_mut(){
			bw_print!("null sprite");
			return;
		}
		let global_timer = match global_extra {
//			0=>24*12,
			0=>224,
			_=>global_extra,
		};
		let sef = SpriteEffect{sprite: SerializableSprite(sprite), timer: 48, 
			global_timer: global_timer, respawn_timer: 112, effect_id: HydraEffect::FeelingTheBern, 
			position: (*sprite).position, origin: origin,
			source: source};
		self.sprite_effects.push(sef);		
	}		

	pub fn is_blowing_up_todborne(&mut self,unit_ptr: *mut bw::Unit)->u32 {
		
		for u in &mut self.persist_unit_structs {
			if u.unit.0==unit_ptr {
				if u.prepare_to_explode_todborne {
					return 1;
				}
			}
		}
		0
	}		
	/*
	pub unsafe fn remove_fire(&mut self, x: i16, y: i16){
		let i = x/512;
		let j = y/512;
		let mut result = match self.activist_hashmap.entry((i,j)) {
		   Entry::Vacant(entry) => entry.insert(Vec::new()),
		   Entry::Occupied(entry) => entry.into_mut(),
		};	
		result.swap_retain(|(n,m)| !(n==&x && m==&y));
	}*/
	pub unsafe fn add_fire(&mut self, x: i16, y: i16){
	
//		let i = x/512;
//		let j = y/512;
		let i = x/32;
		let j = y/32;
		let result = match self.activist_hashmap.entry((i,j)) {
		   Entry::Vacant(entry) => entry.insert(Vec::new()),
		   Entry::Occupied(entry) => entry.into_mut(),
		};	
//		bw_print!("Add fire: block {} {}, pos {} {}",i,j,x,y);
		result.push((x,y));	
	}
	pub unsafe fn get_fires_in_block(&mut self, x: i16, y: i16)->u32{
		let result = match self.activist_hashmap.entry((x/32,y/32)) {
		   Entry::Vacant(entry) => entry.insert(Vec::new()),
		   Entry::Occupied(entry) => entry.into_mut(),
		};	
		return result.len() as u32;
	}
	
	pub unsafe fn get_fires_in_range(&mut self, x: i16, y: i16, range: u32)->u32{
		let step = (range as i16/512).saturating_add(1);
		let mut count = 0;
		let search_block_x = x/512;
		let search_block_y = y/512;		
		
		bw_print!("Search for fire in block {} {}, pos {} {}",search_block_x,search_block_y,x,y);
		for i in -step..= step {
			for j in -step..= step {
				let result = match self.activist_hashmap.entry((i+search_block_x,j+search_block_y)) {
				   Entry::Vacant(entry) => entry.insert(Vec::new()),
				   Entry::Occupied(entry) => entry.into_mut(),
				};	
				for (vx,vy) in result {
					if bw::distance(bw::Point{x: x,y: y}, bw::Point{x: *vx, y: *vy}) <= range {
						count += 1;
					}
				}
			}
		}
		count as u32
	}
	pub unsafe fn feeling_the_bern(&mut self, source: Unit, sprite: *mut bw::Sprite, x: u32, y: u32, global_extra: u16){
		//bw_print!("Feel");
		let mut can = true;	
		/*
		let fires_in_range = self.get_fires_in_range(x as i16,y as i16,32);
		//bw_print!("Fires: {}",fires_in_range);
		if fires_in_range >= 1 {
			can = false;
		}	*/
		if self.get_fires_in_block(x as i16,y as i16)>0 {
			can=false;
		}
		//bw_print!("Fires in {} {}: {}",x/32,y/32,can);
		if can {
			let mut flame = bw::create_lone_sprite(316, x, 11, y);
			//bw_print!("Create sprite {:p}",flame);
			if flame!=null_mut() {
				(*((*flame).sprite)).elevation_level = (*sprite).elevation_level + 1;
				//let position = (*sprite).position;
				bw::update_ls_visibility(flame);
				self.add_bern_effect(source,(*flame).sprite,global_extra,bw::Point{x: x as i16, y: y as i16});
				self.add_fire(x as i16, y as i16);
				//bw_print!("Add fire at {} {}",x/32,y/32);
				//self.activist_effect_rtree.insert([position.x as f64, position.y as f64]);
			}		
		}
	}
	pub fn prepare_to_explode_todborne(&mut self, fanboy: Unit){
		for u in &mut self.persist_unit_structs {
			if u.unit==fanboy {
				u.prepare_to_explode = true;
				return;
			}
		}	
		let mut u_str = self.default_unit_struct(fanboy);
		u_str.prepare_to_explode = true;
		//bw_print!("Set expl2");
		self.persist_unit_structs.push(u_str);
	}
								
}