use aiscript::{self,UnitMatch,LocalModifier,PlayerMatch,Position};
use bw;
use bw_dat::{UnitId,OrderId};
use config;
use game::Game;
use globals::{self,Globals,NewGameState,HydraEffect,LongEffect,RenameStatus};
use rng::Rng;
use idle_tactics::{self,IdleTactics,PatrolState,HunterKillerAi,HunterKillerFlock};
use fxhash::FxHashMap;
use std::collections::hash_map::Entry;
use unit_search::UnitSearch;
use unit::{self,Unit};
use std::cmp;
use std::ptr::null_mut;
#[derive(Debug, Serialize, Deserialize, Clone, Eq, PartialEq)]
pub enum ToolboxId {
	UnitSetup,
	ComposeAttack,
	BuildingBoost,
	WorkerQueue,
	Strain,
	SetCustomExpand,
	CustomExpand,
	CritterRoute,
	HunterKiller,
	SpreadStrainPeons,
	OverlordNameSetup,
	WaitRes,
	ForceGas,
	SetFaction,
}

#[derive(Debug, Hash, Serialize, Deserialize, Copy, Clone, Eq, PartialEq)]
pub enum FuncUnitType {
	Infantry,
	GroundSpecialist,
	AirSpecialist,
	LightMech,
	HeavyMech,
	Starfighter,
	Cruiser,
	Special,
}


fn parse_function_header(string: &str)->Option<ToolboxId>{
	let result = match string.to_lowercase().as_ref(){
		"unit_setup"=>ToolboxId::UnitSetup,
		"compose_attack"=>ToolboxId::ComposeAttack,	
		"building_boost"=>ToolboxId::BuildingBoost,
		"worker_queue"=>ToolboxId::WorkerQueue,
		"strain"=>ToolboxId::Strain,
		"set_custom_expand"=>ToolboxId::SetCustomExpand,
		"custom_expand"=>ToolboxId::CustomExpand,
		"hunter_killer"=>ToolboxId::HunterKiller,
		"spread_workers"=>ToolboxId::SpreadStrainPeons,
		"critter_route"=>ToolboxId::CritterRoute,
		"overlord_name_setup"=>ToolboxId::OverlordNameSetup,
		"wait_res"=>ToolboxId::WaitRes,
		"force_gas"=>ToolboxId::ForceGas,
		"set_faction"=>ToolboxId::SetFaction,
		_=>{
			bw_print!("Unknown function header: {}",string);
			return None;
		},
	};
	Some(result)
}

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub struct UnitSetup {
	pub unit_id: UnitId,
	pub factory_ids: aiscript::UnitMatch,
	pub unit_type: FuncUnitType,
	pub tier: f64,//
	pub player: u8,//
}
#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub struct Route {
	pub max: u32,
	pub delay: u32,
	pub id: UnitId,
	pub route: Vec<aiscript::Position>,
	pub index: u32,
	pub state: u8,
	pub player: u8,
	pub current_unit: Option<Unit>,
}

#[derive(Clone,PartialEq)]
pub enum AttackType {
	GroundInfantryAttack,
	GroundMechAttack,
	GroundCombinedArms,
	AirCruiserAttack,
	AirFighterAttack,
	AirMixedAttack,
	MixedAttack
}

fn parse_attack_type(string: &str)->Option<AttackType>{
	let result = match string.to_lowercase().as_ref(){
		"ground_infantry"=>AttackType::GroundInfantryAttack,
		"ground_mech"=>AttackType::GroundMechAttack,
		"ground_combined"=>AttackType::GroundCombinedArms,
		"air_cruiser"=>AttackType::AirCruiserAttack,
		"air_fighter"=>AttackType::AirFighterAttack,
		"air_mixed"=>AttackType::AirMixedAttack,
		"mixed"=>AttackType::MixedAttack,
		_=>{
			return None;
		},
	};
	Some(result)
}

fn splash_danger(splash_area: u32, damage: u32)->u32{
	let mut factor = (splash_area*splash_area*damage) as f64;
	factor = factor.sqrt();
	factor as u32
}
pub unsafe fn determine_attack_type(player: u8, game: Game, globals: &mut Globals, search: &UnitSearch)->AttackType {
	let mut attack_types = vec![AttackType::GroundInfantryAttack,
	AttackType::GroundMechAttack,
	AttackType::GroundCombinedArms,
	AttackType::AirCruiserAttack,
	AttackType::AirFighterAttack,
	AttackType::AirMixedAttack,
	AttackType::MixedAttack];
	let splash_danger = search.search_iter(&aiscript::anywhere_rect())
						      .filter(|x| !game.allied(x.player(),player))
							  .filter(|x| x.id().ground_weapon().is_some())
							  .fold(0,|acc,x| acc+splash_danger(x.id().ground_weapon().unwrap().outer_splash(),
												  x.id().ground_weapon().unwrap().damage()));
	let config = config::config();											
	let antiair_danger = search.search_iter(&aiscript::anywhere_rect())
						      .filter(|x| !game.allied(x.player(),player))
							  .filter(|x| x.id().air_weapon().is_some())
							  .fold(0,|acc,x| acc+x.id().air_weapon().unwrap().damage());
							  
	let antiground_danger = search.search_iter(&aiscript::anywhere_rect())
						      .filter(|x| !game.allied(x.player(),player))
							  .filter(|x| x.id().ground_weapon().is_some())
							  .fold(0,|acc,x| acc+x.id().ground_weapon().unwrap().damage());						  
	let fighter_danger = search.search_iter(&aiscript::anywhere_rect())
						      .filter(|x| !game.allied(x.player(),player))
							  .filter(|x| x.id().air_weapon().is_some() && x.is_air() && x.id().has_large_overlay())
							  .fold(0,|acc,x| acc+x.id().air_weapon().unwrap().damage());
	let spell_splash_danger = search.search_iter(&aiscript::anywhere_rect())
						      .filter(|x| !game.allied(x.player(),player))
							  .filter(|x| globals.idle_tactics.splash_factors.get(&(x.id().0 as u32)).is_some())
							  .fold(0.0,|acc,x| acc+globals.idle_tactics.splash_factors.get(&(x.id().0 as u32)).unwrap());
	let mut foes=0;
	for i in 0..8 {
		let ai = bw::player_ai(i as u32);
		if !game.allied(player,i) && ((*ai).flags & 0x2 !=0 || bw::player_list[i as usize].ty==2){
			foes+=1;
		}
	}
	if splash_danger > 17000*foes || spell_splash_danger>(9000*foes) as f64 {//500*18=9000
		attack_types.retain(|x| *x!=AttackType::GroundInfantryAttack);
	}
	if fighter_danger > 750*foes {
		attack_types.retain(|x| *x!=AttackType::AirCruiserAttack);
	}
	if antiair_danger > 2000*foes {
		attack_types.retain(|x| *x!=AttackType::AirCruiserAttack && *x !=AttackType::AirFighterAttack);
	}
	if antiair_danger > 3000*foes {
		attack_types.retain(|x| *x!=AttackType::AirMixedAttack);
	}	
	if antiground_danger > 5000*foes {
		attack_types.retain(|x| *x!=AttackType::GroundMechAttack);
	}
	if antiground_danger > 8000*foes {
		attack_types.retain(|x| *x!=AttackType::GroundCombinedArms);
	}
	let i = globals.rng.synced_rand(0..attack_types.len() as u32);
	attack_types[i as usize].clone()
}

pub unsafe fn determine_unit_types(ty: AttackType)->Vec<FuncUnitType>{
	match ty {
		AttackType::GroundInfantryAttack=>vec![FuncUnitType::Infantry,FuncUnitType::GroundSpecialist,FuncUnitType::Special],
		AttackType::GroundMechAttack=>vec![FuncUnitType::LightMech,FuncUnitType::HeavyMech,
										  FuncUnitType::GroundSpecialist,FuncUnitType::Special],
		AttackType::GroundCombinedArms=>vec![FuncUnitType::Infantry,FuncUnitType::GroundSpecialist,
					FuncUnitType::AirSpecialist,FuncUnitType::LightMech,FuncUnitType::HeavyMech,FuncUnitType::Special],
		AttackType::AirCruiserAttack=>vec![FuncUnitType::Cruiser,FuncUnitType::AirSpecialist,FuncUnitType::Special],
		AttackType::AirFighterAttack=>vec![FuncUnitType::Starfighter,FuncUnitType::AirSpecialist,FuncUnitType::Special],
		AttackType::AirMixedAttack=>vec![FuncUnitType::Starfighter,FuncUnitType::Cruiser,FuncUnitType::AirSpecialist,
										FuncUnitType::Special],
		AttackType::MixedAttack=>vec![FuncUnitType::Infantry,FuncUnitType::GroundSpecialist,FuncUnitType::AirSpecialist,
									FuncUnitType::LightMech,FuncUnitType::HeavyMech,FuncUnitType::Starfighter,
									FuncUnitType::Cruiser,FuncUnitType::Special],
		_=>vec![],
	}
}

#[derive(Debug, Hash, Clone, Eq, PartialEq)]
pub struct UnitFuncHash {
	pub id: UnitId,
	pub factory_ids: UnitMatch,
	pub ty: FuncUnitType,
}

fn mass_production(ty: FuncUnitType)->bool {
	match ty {	
		FuncUnitType::GroundSpecialist|FuncUnitType::AirSpecialist|FuncUnitType::Special=>false,
		_=>true,
	}
}
fn type_factor(ty: FuncUnitType)->f64 {
	match ty {
		FuncUnitType::Infantry=>5.0,
		FuncUnitType::LightMech=>4.0,
		FuncUnitType::HeavyMech=>2.0,
		FuncUnitType::Starfighter=>3.8,
		FuncUnitType::Cruiser=>1.5,
		_=>1.0,
	}
}
pub unsafe fn critter_route_hook(
	idle_tactics: &mut IdleTactics,
    ngs: &mut NewGameState,
    picked_unit: Unit,
    search: &UnitSearch,
	game: Game,
) {
	let mut i = 0;
	for route in &mut idle_tactics.critter_routes {
		
		if route.state == 0 {//Wait For Spawn
			route.delay = route.delay.saturating_sub(1);
			if route.delay == 0 {
				route.delay = route.max;
			}
		}
		match route.state {
			0=>{
				route.delay = route.delay.saturating_sub(1);
				if route.delay == 0 {
					route.delay = route.max;				
					route.index = 0;
					if let Some (dummy) = Unit::from_ptr(bw::first_player_unit[route.player as usize]){
						let ef = LongEffect{effect_id: HydraEffect::CreateUnit, timer: 1, 
							position: route.route[route.index as usize].center, 
							player: route.player, energy: 0, unit_id: route.id.0,
							unit: dummy, misc: i, target: dummy};
						ngs.long_term_effects.push(ef);
					}
				}
			},
			1=>{
				if route.current_unit==None {
					route.state = 0;
					continue;
				}
				if bw::distance(route.current_unit.unwrap().position(), route.route[route.index as usize+1].center) <= 32 {
					if route.index as usize + 1 == route.route.len()-1{
						ngs.add_single_effect_misc(route.current_unit.unwrap().0, HydraEffect::RemoveUnit,0);
						route.current_unit = None;
						route.state = 0;
					}
					else {
						route.index += 1;
					}
				}
				else {
					ngs.add_single_effect_point_spec(route.current_unit.unwrap().0,HydraEffect::IssueOrder,
						route.route[route.index as usize+1].center,6);//Issue Move order
				}
				
			},
			_=>(),
		}
		/*
		//pseudocode:
		if(state==WaitForSpawn){
			decrement();
			if(delay==0){
				delay = delay.max;
				state=Move;
				index=0;
				spawnunit();
			}
		}
		else if(state==Move){
			if(intargetregion()){
				if(lastindex){
					remove()
				}
				else {
					index++;
				}
			}
			else {
				moveto()
			}
		}*/
		i+=1;
		
	}
}
use std::ffi::CString;
pub unsafe fn add_unitname(globals: &mut Globals, unit: u16, string: String, players: PlayerMatch){
	let byte_string = string.as_bytes();
	let rename_status = RenameStatus {
        area: Position::from_rect32(&bw::location(63).area).area,
        unit_id: UnitId(unit),
        name: CString::new(byte_string).unwrap(),
		
        players,
    };
	globals.renamed_units.try_add(rename_status);
}
pub unsafe fn function(script: *mut bw::AiScript, globals: &mut Globals, search: &UnitSearch, cmd: Vec<&str>, old_pos: u32){
	let game = Game::get();
	let mut cmd = cmd.clone();
	let header = cmd[1];
	cmd.drain(0..2);
	if let Some(code) = parse_function_header(header){
		match code {
			ToolboxId::UnitSetup=>{
				//bw_print!("setup");
				let unit_id = config::parse_unit_id(cmd[0]);
				let factory_ids = idle_tactics::parse_unit_match(cmd[1].to_string());
				let unit_type = match cmd[2].to_lowercase().as_ref(){
					"infantry"=>FuncUnitType::Infantry,
					"ground_spec"=>FuncUnitType::GroundSpecialist,
					"air_spec"=>FuncUnitType::AirSpecialist,
					"light_mech"=>FuncUnitType::LightMech,
					"heavy_mech"=>FuncUnitType::HeavyMech,
					"fighter"|"starfighter"=>FuncUnitType::Starfighter,
					"cruiser"=>FuncUnitType::Cruiser,
					_=>FuncUnitType::Special,
				};
				let tier = cmd[3].parse::<f64>().unwrap();
				if cmd.len()>4 {
					globals.idle_tactics.splash_factors.insert(unit_id.0 as u32,cmd[4].parse::<f64>().unwrap());
				}
				if unit_id.0==228 {
					bw_print!("Unable to parse unit id {:?}",cmd[0]);
					return;
				}
				let setup = UnitSetup { player: (*script).player as u8, unit_id, factory_ids, unit_type: unit_type.clone(), tier };
				if !globals.idle_tactics.toolbox_setup.iter().any(|x| x==&setup){
					globals.idle_tactics.toolbox_setup.push(setup);
				}
				
			},
			ToolboxId::ComposeAttack=>{
				let current_tier = cmd[0].parse::<f64>().unwrap();
				let strength = cmd[1].parse::<f64>().unwrap();
				let explicit_type = match (cmd.len()>2){
					true=>parse_attack_type(cmd[2]),
					false=>None,
				};
				
				let mut list: FxHashMap<UnitFuncHash,u32> = FxHashMap::default();
				for id in globals.idle_tactics.toolbox_setup.iter()
						  .filter(|x| x.player==(*script).player as u8 && game.unit_available(x.player,x.unit_id)
									  && x.tier<=current_tier)
				{
					let mut factories = 0;
					for f_id in &id.factory_ids.units {
						let production_count = game.safe_unit_completed_count(id.player,*f_id);
						if production_count == 0 {
							continue;
						}
						factories += production_count;
					}
					if factories == 0 {
						continue;
					}
					let e = UnitFuncHash { id: id.unit_id, ty: id.unit_type, factory_ids: id.factory_ids.clone() };
					let result = match list.entry(e) {
					   Entry::Vacant(entry) => { entry.insert(0) },
					   Entry::Occupied(entry) => entry.into_mut(),
					};
					*result += factories;
				}
				let attack_type = match explicit_type {
					Some(s)=>s,
					None=>determine_attack_type((*script).player as u8,game,globals,search),
				};
				let unit_types = determine_unit_types(attack_type);
				//bw_print!("List initial len: {}",list.len());
				list.retain(|k,_| unit_types.iter().any(|&x| x==k.ty));	
				//bw_print!("List retain len: {}",list.len());
				for (k,v) in list.iter(){
					if mass_production(k.ty){
						let index = type_factor(k.ty)*(strength.sqrt());
						//bw_print!("Add queues {} and units {} (id {:?}",strength.sqrt(),v*(index as u32),k.id);
						aiscript::try_add_queue(script,255,strength.sqrt() as u8,k.id,
							k.factory_ids.clone(),LocalModifier::Global,Position::from_rect32(&bw::location(63).area),80,globals);						
						aiscript::add_to_attack_force(globals, (*script).player as u8, k.id, v*(index as u32));
					}
					else {
						let quantity = strength as u32;
						//bw_print!("Add queues 1 and units {} (id {:?}, rel",quantity,k.id);
						aiscript::try_add_queue(script,255,1,k.id,
								k.factory_ids.clone(),LocalModifier::Global,Position::from_rect32(&bw::location(63).area),80,globals);
						aiscript::add_to_attack_force(globals, (*script).player as u8, k.id, quantity);
						//debug!("Add queue and attack: {}",quantity);
					}
				}
				//...
				//get queues and attacks (relative and non-relative - must be specified 
				//						  in unit setup with tier and type - specialist unit or mass unit)	
			},
			ToolboxId::BuildingBoost=>{
				//get current demands - i.e. air production, air defense, ground defense, mass infantry, etc.
				//read resources and worker status
				//add build stuff
				
			},
			ToolboxId::WorkerQueue=>{
				let town_id = cmd[0].trim().parse().unwrap();
				let critical_mineral_count = cmd[1].trim().parse().unwrap();
				let worker_unit_id = config::parse_unit_id(cmd[2]);
				let townhall_unit_id = idle_tactics::parse_unit_match(cmd[3].to_string());
				
				let town = aiscript::town_from_id(script, globals, town_id);
				let resarea = &bw::resource_areas;
				if let Some(town) = town {
					let resarea_id = (*town.0).resource_area;
					let field_count = resarea.areas[resarea_id as usize].mineral_field_count;
					let mineral_count = resarea.areas[resarea_id as usize].total_mineral_count;
					let mut worker_count = (*town.0).worker_count;		
				    for ai in town.buildings() {
						let building = Unit::from_ptr((*ai).parent);
						if let Some(building)=building {
							if building.current_train_id()==worker_unit_id && townhall_unit_id.matches(&building){
								worker_count+=1;
							}
						}
					}
					let workers_per_minerals = match (mineral_count>=critical_mineral_count){
						true=>2.5,
						false=>1.0,
					};
					let workers_required = (field_count as f64 * workers_per_minerals) as u32;
					if worker_count < workers_required as u8 {
						aiscript::try_add_queue(script,town_id,1,worker_unit_id,townhall_unit_id,
						LocalModifier::Local,Position::from_rect32(&bw::location(63).area),80,globals);
					}
				}
				//self-explanatory
			},
			ToolboxId::ForceGas=>{
				let town_id = cmd[0].trim().parse().unwrap();
				let town = aiscript::town_from_id(script, globals, town_id);
				if let Some(town) = town {
					let mut gas_workers=0;
					if (*town.0).gas_buildings[0] == null_mut(){
						return;
					}
					if (*(*town.0).gas_buildings[0]).unit_id == 188 {
						return;
					}
					for ai in town.workers(){
						let worker = Unit::from_ptr((*ai).parent);
						if let Some(worker) = worker {
							match worker.order().0{
								81..=84=>{
									gas_workers+=1;
								},
								_=>(),
							}
						}
					}
					if gas_workers >= 3 {
						return;
					}
					for ai in town.workers(){
						let worker = Unit::from_ptr((*ai).parent);
						if let Some(worker) = worker {
							match worker.order().0{
								81..=84=>(),
								30|33|34|35=>(),//repair
								_=>{
									worker.issue_order(OrderId(81),worker.position(),Unit::from_ptr((*town.0).gas_buildings[0]));
									gas_workers+=1;
									if gas_workers==3 {
										return;
									}
								},
							}
						}
					}
				
				}
			},
			ToolboxId::SetFaction=>{
				let player: u8 = cmd[0].trim().parse().unwrap();
				let faction: u16 = cmd[1].trim().parse().unwrap();
				let result = match globals.ngs.factions.entry(faction) {
				   Entry::Vacant(_) => { return; },
				   Entry::Occupied(entry) => entry.into_mut(),
				};	
				globals.ngs.diplomacy[player as usize].faction = result.faction_id;
			},
			ToolboxId::Strain=>{
				//read unit id filter
				//read variable value (integer)
				//iterate town buildings and filter by id, save count
				//if researching/upgrading/morphing/training, save count2
				//divide count2 as f64 by count as f64, multiply by 100, convert to u32, set to variable
				//reads how busy buildings currently are with training/upgrading/etc. and records the value to u32 variable
			},
			ToolboxId::SetCustomExpand=>{
				//sets create_script-like expansion settings that can be used by CustomExpand later
			},
			ToolboxId::CustomExpand=>{
				//runs expand with preset create_script settings
			
			},
			ToolboxId::CritterRoute=>{
//				bw_print!("critter route");
				let id = config::parse_unit_id(cmd[0].trim());
				let frequency = cmd[1].trim().parse().unwrap();
				let player = cmd[2].trim().parse().unwrap();
				let mut points = Vec::new();
				for i in 3..cmd.len(){
					let point = idle_tactics::parse_position(cmd[i].trim());
					points.push(point);
				}
				let route = Route { max: frequency, delay: frequency, id: id, route: points, state: 0, index: 0, 
							player: player, current_unit: None};
							
				globals.idle_tactics.critter_routes.push(route);
				
			},
			ToolboxId::HunterKiller=> {
				//hunter_killer(count id [max])
				let mut count = cmd[0].trim().parse().unwrap();
				let unit_id = config::parse_unit_id(cmd[1].trim());
				let max: u32 = match cmd.len() {
					2=>150,
					_=>cmd[2].parse().unwrap(),
				};
				let mut current_count = 0;
				for f in globals.idle_tactics.hunter_killer_ai.iter(){
					for u in f.flock.iter(){
						if u.player==(*script).player as u8 && u.unit.id()==unit_id {
							current_count += 1;
						}
					}
				}
				let free = max.saturating_sub(current_count as u32);
				count = cmp::min(count,free);
				if count==0 {
					return;
				}													
				let targets = search
					.search_iter(&aiscript::anywhere_rect())
					.filter(|u| u.player()==(*script).player as u8 &&  u.id()==unit_id && u.order().0!=0 
							&& (*u.0).sprite!=null_mut() &&
								u.is_completed() && !u.is_disabled())
					.take(count as usize)
					.collect::<Vec<_>>();		
				let mut flock = Vec::new();
				let length = targets.len();
				for target in targets {
					let h_ai = HunterKillerAi {
						player: (*script).player as u8,
						unit: target,
						delay: 4,
						max_delay: 4,
						state: PatrolState::Default,
					};
					flock.push(h_ai);
					if (*target.0).ai != null_mut(){
						bw::remove_unit_ai(target.0, 0);
					}	
				}
				let leader = flock[0].clone();
				if flock.len()>0 {
					globals.idle_tactics.hunter_killer_ai.push(HunterKillerFlock { flock: flock, leader: leader, 
																initial_count: length as u32});
				}
			},
			ToolboxId::SpreadStrainPeons=>{
				//calculate amount of unnecessary workers
				//read other towns
				//move to other towns
				
			},
			ToolboxId::WaitRes=>{
				let minerals: u32 = cmd[0].trim().parse().unwrap();
				let gas: u32 = cmd[1].trim().parse().unwrap();
				let current_min = game.minerals((*script).player as u8);
				let current_gas = game.gas((*script).player as u8);
				if !(current_min>=minerals && current_gas>=gas){
					(*script).pos = old_pos;
					(*script).wait = 30;
				}
			},
			
			ToolboxId::OverlordNameSetup=>{
				let string = cmd[0].trim();			
				let players = idle_tactics::value_to_player_match(cmd[1].trim().parse().	unwrap(),(*script).player as u8,game);
				let prefix = match string.to_lowercase().as_ref() {
					"kmc"|"combine"=>{
						"Kel-Morian"					
					},
					"pirate"|"pirates"=>{
						"Pirate"			
					},
					"scavenger"|"scavengers"=>{
						"Scavenger"
					},
					"miner"|"miners"=>{
						"Miner"
					},
					"mercenaries"|"mercenary"|"merc"|"mercs"=>{
						"Mercenary"
					},
					_=>{
						"Terran"
					},
				};
				let combine = prefix.to_string()+&(" ").to_string();	
				//merc units
				
				add_unitname(globals,68,combine.clone()+"Pyro",players);
				add_unitname(globals,84,combine.clone()+"Shrike",players);
				add_unitname(globals,98,combine.clone()+"Swiss",players);
				add_unitname(globals,99,combine.clone()+"Monitor",players);
				add_unitname(globals,103,combine.clone()+"Bullfrog",players);
				add_unitname(globals,161,combine.clone()+"Southpaw",players);
				//scavenger units
				add_unitname(globals,56,combine.clone()+"Carrion",players);			
				add_unitname(globals,69,combine.clone()+"Hawk",players);			
				add_unitname(globals,79,combine.clone()+"Mustang",players);			
				add_unitname(globals,102,combine.clone()+"Desperado",players);	
				add_unitname(globals,193,combine.clone()+"Behemoth",players);	
				//miner units
				add_unitname(globals,43,combine.clone()+"Stingray",players);
				add_unitname(globals,50,combine.clone()+"Militia",players);
				add_unitname(globals,74,combine.clone()+"Prospector",players);
				add_unitname(globals,77,combine.clone()+"Mole",players);
				//pirate units
				add_unitname(globals,46,combine.clone()+"Hellfire",players);
				add_unitname(globals,53,combine.clone()+"Cobra",players);
				add_unitname(globals,61,combine.clone()+"Dagger",players);	
				add_unitname(globals,54,combine.clone()+"Hound",players);	
				add_unitname(globals,181,combine.clone()+"Gigashark",players);	
				//kmc units
				add_unitname(globals,4,combine.clone()+"Goliath",players);
				add_unitname(globals,48,combine.clone()+"Paladin",players);
				add_unitname(globals,6,combine.clone()+"Mammoth",players);
				add_unitname(globals,23,combine.clone()+"Mammoth",players);//upgraded
				add_unitname(globals,216,combine.clone()+"Charon",players);
				//
				//generic units
				add_unitname(globals,16,combine.clone()+"Black Ops",players);
				add_unitname(globals,17,combine.clone()+"Runabout",players);
				add_unitname(globals,37,combine.clone()+"Brigand",players);
				add_unitname(globals,38,combine.clone()+"Grizzly",players);
				add_unitname(globals,76,combine.clone()+"Marksman",players);
				add_unitname(globals,41,combine.clone()+"Engineer",players);
				add_unitname(globals,47,combine.clone()+"Vermin",players);
				add_unitname(globals,52,combine.clone()+"Grenadier",players);
				add_unitname(globals,55,combine.clone()+"Privateer",players);
				add_unitname(globals,57,combine.clone()+"Cargoship",players);
				add_unitname(globals,58,combine.clone()+"Gunship",players);
				add_unitname(globals,62,combine.clone()+"Overlord",players);
				add_unitname(globals,97,combine.clone()+"Camp",players);
				add_unitname(globals,100,combine.clone()+"Whirlwind",players);
				add_unitname(globals,110,combine.clone()+"Refinery",players);//until gas pump is implemented
				add_unitname(globals,131,combine.clone()+"Tactical Center",players);
				add_unitname(globals,133,combine.clone()+"Guildhall",players);
				add_unitname(globals,136,combine.clone()+"Refit Bay",players);
				add_unitname(globals,137,combine.clone()+"Defense Control",players);
				add_unitname(globals,138,combine.clone()+"Merc Compound ",players);
				add_unitname(globals,139,combine.clone()+"Training Center",players);
				add_unitname(globals,140,combine.clone()+"Starbase",players);
				add_unitname(globals,142,combine.clone()+"Vehicle Depot",players);
				add_unitname(globals,145,combine.clone()+"Foundry",players);
				add_unitname(globals,150,combine.clone()+"Radar Tower",players);
				add_unitname(globals,153,combine.clone()+"Gas Pump",players);
				add_unitname(globals,174,combine.clone()+"Scrap Processor",players);
				add_unitname(globals,184,combine.clone()+"Autocannon",players);
				add_unitname(globals,185,combine.clone()+"Flak Cannon",players);
				add_unitname(globals,200,combine.clone()+"Mining Station",players);
				add_unitname(globals,201,combine.clone()+"Research Center",players);	
			},
			_=>(),
		}
	}
}