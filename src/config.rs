//use std::ffi::CString;
//use std::slice;
use std::ptr::null_mut;
use std::sync::{Arc, Mutex};
use failure::{Context, Error};
//use parking_lot::Mutex;
use bw_dat::{UnitId,TechId,OrderId};
use data_extender::{self,FlingyValues, SpriteValues, ImageValues, DataExtender, FlingyExtender, SpriteExtender};
use samase;
use bw;
use idle_tactics;
use fxhash::{FxHashMap, FxHashSet};
use std::collections::hash_map::Entry;
use aiscript::{
    self
};
use globals::{
    self, Globals
};
use game::Game;
#[repr(C)]
#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct ReqItem {
	comparison: u8,
	id: u16,
	setter_type: u8,
	value: u32,
}

#[repr(C)]
#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct ReqOpcode {
	pub core_root: bool,
	pub current_comparison_index: u16,	
	pub current_level_index: u16,	
	pub current_value_index: u32,	
	pub id: u16,
	pub item: ReqItem,
}

#[repr(C)]
#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct UpgReqData {
	pub opcode: ReqOpcode,
	pub list: Vec<UpgReqData>,//nested	
}


impl UpgReqData {
	fn comparison(&mut self, id: OpcodeId)->Option<OpcodeComparison>{
		let can = match id {				
			OpcodeId::CurrentUnitIs|
			OpcodeId::RequiresTech|
			OpcodeId::IsBusy|
			OpcodeId::IsConstructingAddon|
			OpcodeId::IsResearchingTech|
			OpcodeId::IsUpgrading|
			OpcodeId::HasAddonOfType|
			OpcodeId::HasUnitOfType|
			OpcodeId::Landed|
			OpcodeId::HasAddon|
			OpcodeId::HasNuke|
			OpcodeId::Burrowed|
			OpcodeId::AIControlled|
			OpcodeId::CanMove|
			OpcodeId::CanAttack|
			OpcodeId::IsWorker|
			OpcodeId::IsTransport|
			OpcodeId::IsPowerUp|
			OpcodeId::IsSubunit|
			OpcodeId::HasSpidermines|
			OpcodeId::IsHero|
			OpcodeId::HasRallyOrRightClick|
			OpcodeId::RequiresUpgradeLevel|
			OpcodeId::RequiresFaction=>true,
			_=>false,
		};
		if !can {
			return None;
		}
		else {
			let result = match self.opcode.current_comparison_index {			
				0=>OpcodeComparison::Equal,
				1=>OpcodeComparison::Not,
				2=>OpcodeComparison::GreaterThan,
				3=>OpcodeComparison::GreaterThanOrEqual,
				4=>OpcodeComparison::LessThan,
				5=>OpcodeComparison::LessThanOrEqual,
				6=>OpcodeComparison::AtLeast,
				7=>OpcodeComparison::AtMost,
				
				_=>OpcodeComparison::Equal,
			};
			return Some(result);
		}
	}
	fn value(&mut self, id: OpcodeId)->Option<i32>{
		let result = match id {				
			OpcodeId::CurrentUnitIs|
			OpcodeId::RequiresTech|
			OpcodeId::HasAddonOfType|
			OpcodeId::HasUnitOfType|
			OpcodeId::RequiresUpgradeLevel|
			OpcodeId::LevelReq|
			OpcodeId::RequiresFaction=>Some(self.opcode.current_value_index as i32),
			_=>None,
		};
		result
	}	
	fn level(&mut self, id: OpcodeId)->Option<u16>{
		let result = match id {		
			OpcodeId::RequiresUpgradeLevel|
			OpcodeId::LevelReq=>Some(self.opcode.current_level_index),
			_=>None,
		};
		result
	}		
	pub fn process(&mut self)->GTP_OpcodeData {
		let enum_opcode = match self.opcode.id {
			0=>OpcodeId::And,
			1=>OpcodeId::Or,
			2=>OpcodeId::LevelReq,
			//
			3=>OpcodeId::CurrentUnitIs,
			4=>OpcodeId::RequiresUpgradeLevel,
			5=>OpcodeId::RequiresTech,
			6=>OpcodeId::IsBusy,
			7=>OpcodeId::IsConstructingAddon,
			8=>OpcodeId::IsResearchingTech,
			9=>OpcodeId::IsUpgrading,
			10=>OpcodeId::HasAddonOfType,
			11=>OpcodeId::HasUnitOfType,
			12=>OpcodeId::Landed,
			//
			13=>OpcodeId::HasAddon,
			14=>OpcodeId::HasNuke,
			15=>OpcodeId::Burrowed,
			16=>OpcodeId::AIControlled,
			17=>OpcodeId::CanMove,
			18=>OpcodeId::CanAttack,
			19=>OpcodeId::IsWorker,
			20=>OpcodeId::IsTransport,
			21=>OpcodeId::IsPowerUp,
			22=>OpcodeId::IsSubunit,
			23=>OpcodeId::HasSpidermines,
			24=>OpcodeId::IsHero,
			25=>OpcodeId::HasRallyOrRightClick,	
			26=>OpcodeId::RequiresFaction,
			_=>OpcodeId::AlwaysTrue,
		};
		
		let opc = GTP_Opcode { opcode_id: enum_opcode.clone(), comparison: self.comparison(enum_opcode.clone()), 
								value: self.value(enum_opcode.clone()), level: self.level(enum_opcode.clone()) };
		let mut proc_list = Vec::new();
		for a in &mut self.list.clone() {
			let processed = a.process().clone();
			proc_list.push(processed);
		}
		let result = GTP_OpcodeData { list: proc_list.clone(), opcode: opc };
		result	
	
	}
}


#[repr(C)]
#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct UpgReqElem {
	pub reqs: Vec<UpgReqData>,
	pub upgrade_id: u32,
	pub used: bool,
}

impl UpgReqElem {
	pub fn process(&mut self)->GTP_OpcodeData {
		//root
		let opc = GTP_Opcode { opcode_id: OpcodeId::AlwaysTrue, comparison: None, value: None, level: None };
		let mut proc_list = Vec::new();
		for a in &mut self.reqs {
			let processed = a.process().clone();
			proc_list.push(processed);
		}
		let result = GTP_OpcodeData { list: proc_list.clone(), opcode: opc };
		result
	}
}


#[repr(C)]
#[derive(Debug, Clone, Default, Serialize, Deserialize)]
pub struct GraftingData {
	pub upgrade_reqs: Vec<UpgReqElem>,
	
}
//
//
/*
	pub list: Vec<GTP_OpcodeData>,
	pub opcode: GTP_Opcode,
}
*/
//


#[repr(C)]
#[derive(Copy, Debug, Clone, Eq, PartialEq)]
pub enum OpcodeId {
	And,
	Or,
	LevelReq,
	//
	CurrentUnitIs,
	RequiresUpgradeLevel,
	RequiresTech,
	//
	IsBusy,
	IsConstructingAddon,
	IsResearchingTech,
	IsUpgrading,
	HasAddonOfType,
	HasUnitOfType,
	Landed,
	HasAddon,
	HasNuke,
	Burrowed,
	AIControlled,
	CanMove,
	CanAttack,
	IsWorker,
	IsTransport,
	IsPowerUp,
	IsSubunit,
	HasSpidermines,
	IsHero,
	HasRallyOrRightClick,	
	RequiresFaction,
	
	AlwaysTrue,
}

impl Default for OpcodeId {
    fn default() -> OpcodeId {
        OpcodeId::AlwaysTrue
    }
}

#[repr(C)]
#[derive(Copy, Debug, Clone)]
pub enum OpcodeComparison {
	Equal,//or true
	Not,//or false 
	GreaterThan,
	GreaterThanOrEqual,
	LessThan,
	LessThanOrEqual,
	AtLeast,
	AtMost,
}

impl Default for OpcodeComparison {
    fn default() -> OpcodeComparison {
        OpcodeComparison::Not
    }
}

use unit::{Unit};

#[repr(C)]
#[derive(Copy, Debug, Default, Clone)]
pub struct GTP_Opcode {
	pub opcode_id: OpcodeId,
	pub comparison: Option<OpcodeComparison>,
	pub value: Option<i32>,
	pub level: Option<u16>,
}

impl GTP_Opcode {
	pub fn opcode_value_bool_match(self, original: i32)->bool{
		if self.value.is_none(){
		//	bw_print!("Value is none");
			return false;
		}		
		//bw_print!("Compare (bool): {} {}",self.value.unwrap(),original);
		self.bool_compare(self.value.unwrap(),original)
	}
	
	pub fn opcode_value_match(self, original: i32)->bool{
		if self.value.is_none(){
			return false;
		}
		if self.comparison.is_none(){
			return false;
		}
		self.compare(original,self.value.unwrap())	
	}
	pub fn opcode_level_match(self, original: i32)->bool{
		if self.level.is_none(){
			return false;
		}
		if self.comparison.is_none(){
			return false;
		}
		self.compare(original,self.level.unwrap() as i32)
	}	
	pub fn bool_compare(self, first: i32, second: i32)->bool{
		if self.comparison.is_none(){
			return false;
		}
		match self.comparison.unwrap() {
			OpcodeComparison::Equal=>{
				return first==second;
			},
			_=>{
				return first!=second;
			},
		};
	}
	pub fn bool_simple_eval(self, val: bool)->bool {
		if self.comparison.is_none(){
			return false;
		}
		let result = match self.comparison.unwrap() {
			OpcodeComparison::Equal=>val,
			_=>!val,
		};
		result		
	}
	pub fn compare(self, first: i32, second: i32)->bool{
		let result = match self.comparison.unwrap() {
			OpcodeComparison::Equal=>{
				first == second
			},
			OpcodeComparison::GreaterThan=>{
				first > second
			},
			OpcodeComparison::GreaterThanOrEqual|OpcodeComparison::AtLeast=>{
				first >= second
			},
			OpcodeComparison::LessThan=>{
				first < second
			},
			OpcodeComparison::LessThanOrEqual|OpcodeComparison::AtMost=>{
				first <= second
			},
			_=>{
				first != second
			},
		};
		result
	}
	pub fn evaluate_unit(self, unit: Unit, player: u8, game: Game, globals: &mut Globals)->bool{		
		//bw_print!("Evaluate...{:?}", self.opcode_id);
		unsafe {
		if unit.player()>=8 {
			*bw::last_error = 0x13;
			return false;
		}
		match self.opcode_id {
			OpcodeId::And|OpcodeId::Or|OpcodeId::AlwaysTrue|OpcodeId::LevelReq=>{
				//bw_print!("And/Or/True/LevelReq...");
				return true;	
			},
			OpcodeId::CurrentUnitIs=>{
				
				let answer = self.opcode_value_bool_match(unit.id().0 as i32);
				//bw_print!("Current unit is...[result = {}]",answer);
				if !answer {
					*bw::last_error = 0x19;
				}
				return answer;
			},
			OpcodeId::RequiresUpgradeLevel=>{
				
				let current_level = aiscript::get_new_upgrade_level(game,globals,self.value.unwrap() as u16,player);			
				let answer = self.opcode_level_match(current_level as i32);
				if !answer {
					*bw::last_error = 0x8;
					*bw::unk_unused = 1;
				}
				return answer;
				
			},
			OpcodeId::RequiresTech=>{
				
				let is_researched = game.tech_researched(unit.player(),TechId(self.value.unwrap() as u16));
				let answer = self.bool_simple_eval(is_researched);
				if !answer {
					*bw::last_error = 0x8;
					*bw::unk_unused = 1;
				}
				return answer;
			},
			OpcodeId::IsBusy=>{
				//must be restored back to old, or, rather, new opcode must be added
				let mut result = true;
				if unit.tech_in_progress().0 == 0x2c && unit.upgrade_in_progress_raw()==0x3d 
				/*|| bw::is_training_or_building_addon(unit.0)!=0*/ && unit.current_train_id().0==0xe4 
				&& !unit.is_building_addon(){
					result = false;
				}
				let answer = self.bool_simple_eval(result);
				if !answer {
					*bw::last_error = 0x5;
				}
				return answer;
			},
			OpcodeId::IsConstructingAddon=>{			
				let answer = self.bool_simple_eval(unit.is_building_addon());
				if !answer {
					*bw::last_error = 0x5;
				}
				return answer;		
			},
			OpcodeId::IsResearchingTech=>{
				let answer = self.bool_simple_eval(unit.tech_in_progress().0!=0x2c);//MUST BE DEFINED
				if !answer {
					*bw::last_error = 0x5;
				}
				return answer;
			},
			OpcodeId::IsUpgrading=>{
//				bw_print!("UpgResult: {}",unit.upgrade_in_progress_raw()!=0x3d);
				let answer = self.bool_simple_eval(unit.upgrade_in_progress_raw()!=0x0);//MUST BE DEFINED
				if !answer {
					*bw::last_error = 0x5;
				}
				
				return answer;
			},
			OpcodeId::HasAddonOfType=>{
				let result;
				if let Some(addon) = unit.addon(){
					result = self.opcode_value_match(addon.id().0 as i32);
				}
				else {
					result = self.bool_simple_eval(false);
				}
				if !result {
					*bw::last_error = 0x8;
					*bw::unk_unused = 1;
				}
				return result;
			},
			OpcodeId::HasUnitOfType=>{
				let count = game.completed_count(unit.player(),UnitId(self.value.unwrap() as u16));
				let answer = self.bool_simple_eval(count > 0);
				if !answer {
					*bw::last_error = 0x8;
					*bw::unk_unused = 1;
				}
				return answer;
			},
			OpcodeId::Landed=>{
				let answer = self.bool_simple_eval(unit.is_landed_building());
				if !answer {
					if unit.is_landed_building(){
						*bw::last_error = 0x7;
					}
					else {
						*bw::last_error = 0x6;
					}
				}
				return answer;
			},
			OpcodeId::HasAddon=>{
				let answer = self.bool_simple_eval(unit.addon().is_some());
				if !answer {
					*bw::last_error = 0x8;
					*bw::unk_unused = 1;
				}
				return answer;
			},
			OpcodeId::HasNuke=>{
				let answer = self.bool_simple_eval(unit.has_nuke());
				if !answer {
					*bw::last_error = 0x8;
					*bw::unk_unused = 1;
				}
				return answer;
			},
			OpcodeId::Burrowed=>{
				let answer = self.bool_simple_eval(unit.burrowed());
				if !answer {
					*bw::last_error = 0x5;
				}
				return answer;
			},
			OpcodeId::AIControlled=>{
				let answer = self.bool_simple_eval(unit.unit_ai().is_some());
				if !answer {
					*bw::last_error = 0x8;
					*bw::unk_unused = 0;
				}
				return answer;
			},
			OpcodeId::CanMove=>{
				let mut result = false;
				if unit.is_landed_building() && unit.id().rclick_action()!=0 && !(unit.id()==bw_dat::unit::LURKER && unit.is_burrowed()){
					let action = bw::get_rclick_action(unit.0);
					if action != 0x3 {
						result = true;
					}
				}
				let answer = self.bool_simple_eval(result);
				if !answer {
					*bw::last_error = 0x13;
				}
				return answer;
			},
			OpcodeId::CanAttack=>{
				let answer = self.bool_simple_eval(bw::has_way_of_attacking(unit.0)!=0);
				if !answer {
					*bw::last_error = 0x13;
				}
				return answer;
			},
			OpcodeId::IsWorker=>{
				let answer = self.bool_simple_eval(unit.id().is_worker());
				if !answer {
					*bw::last_error = 0x13;
				}
				return answer;
			},
			OpcodeId::IsTransport=>{
				let answer = self.bool_simple_eval(unit.is_transport(game));
				if !answer {
					*bw::last_error = 0x13;
				}
				return answer;
			},
			OpcodeId::IsPowerUp=>{
				let answer = self.bool_simple_eval(unit.id().flags() & 0x8 != 0);
				if !answer {
					*bw::last_error = 0x13;
				}
				return answer;
			},
			OpcodeId::IsSubunit=>{
				let answer = self.bool_simple_eval(unit.id().flags() & 0x10 != 0);
				if !answer {
					*bw::last_error = 0x13;
				}
				return answer;
			},
			OpcodeId::HasSpidermines=>{
				let answer = self.bool_simple_eval(unit.spider_mines(game) != 0);
				if !answer {
					*bw::last_error = 0x8;
					*bw::unk_unused = 0;
				}
				return answer;
			},
			OpcodeId::IsHero=>{
				let answer = self.bool_simple_eval(unit.id().is_hero());
				if !answer {
					*bw::last_error = 0x13;
				}
				return answer;
			},
			OpcodeId::HasRallyOrRightClick=>{
				if unit.id().0==0x67{//lurker
					if unit.burrowed(){
						return self.bool_simple_eval(false);
					}
				}
				let rclick = unit.id().rclick_action();
				let answer = self.bool_simple_eval(rclick & 0x2 == 0);		
				if !answer {
					*bw::last_error = 0x13;
				}
				return answer;
			},	
			OpcodeId::RequiresFaction=>{
				
				let faction = globals.ngs.diplomacy[unit.player() as usize].faction;
				let answer = self.opcode_value_match(faction as i32);
				if !answer {
					*bw::last_error = 0x13;
				}
				return answer;
			},
		}
		//bw_print!("Return true");
		true
		}
	}
}

#[repr(C)]
#[derive(Debug, Default, Clone)]
pub struct GTP_OpcodeData {
	pub list: Vec<GTP_OpcodeData>,
	pub opcode: GTP_Opcode,
}

impl GTP_OpcodeData {
	pub fn evaluate_unit(self, upgrade: u16, unit: Unit, player: u8, _evaluator: Option<GTP_Opcode>, game: Game, globals: &mut Globals)->bool{
		unsafe{
		//bw_print!("Evaluate (GTP...)");
		let result = self.opcode.evaluate_unit(unit, player,game,globals); 
		
		if !result{
			//bw_print!("Opcode is not evaluated, RETURN");
			return false;
		}
		 
		//bw_print!("Continue evaluation {:?}",self.opcode.opcode_id);
	//	bw_print!("Evaluate (GTP, continue...) {:?} {}",self.opcode.opcode_id,self.list.len());
		if self.opcode.opcode_id == OpcodeId::LevelReq {
		//if opcode_match()		
		//	bw_print!("Evaluate (LevelReq)");
			let current_level = aiscript::get_new_upgrade_level(game,globals,upgrade,player);
			if let Some(level) = self.opcode.level {
				if current_level != level.saturating_sub(1) as u8 {
					return true;
				}
			}
			else {
//				debug!("EVALUATION BUG: LevelReq without level");
				return false;
			}
		}	
		let evaluator = self.clone().evaluator();
//		bw_print!("Get evaluator {:?}",self.opcode.opcode_id);
		match self.opcode.opcode_id {
		
			OpcodeId::And|OpcodeId::LevelReq|OpcodeId::AlwaysTrue=>{
				/*let mut result = true;
				for n in self.list.iter(){
					let answer = n.clone().evaluate_unit(upgrade,unit,player,evaluator,game, globals);
					bw_print!("Eval: result = {}, id = {:?}",answer,n.opcode.opcode_id);
					if !answer {
						result = false;
						break;
					}
					
				}*/
				let result = self.list.iter().any(|x| !(x.clone().evaluate_unit(upgrade,unit,player,evaluator,game, globals)));
				//bw_print!("And/LevelReq Evaluation {} {} ", result,self.list.len());
				return !result;
			},
			OpcodeId::Or=>{
				let result = self.list.iter().any(|x| x.clone().evaluate_unit(upgrade,unit,player,evaluator,game, globals));
				//bw_print!("Or Evaluation {} {}", result,self.list.len());
				return result;
			},
			_=>{
				return true;
			},
		}
//		let result = self.list.iter().any(|x| !x.clone().evaluate_unit(unit,player,self.evaluator()));
//		!result
		}
	}
	pub fn evaluator(self)->Option<GTP_Opcode>{
		let result = match self.opcode.opcode_id {
			OpcodeId::And|OpcodeId::Or|OpcodeId::LevelReq=>{Some(self.opcode.clone())},
			_=>None,
		};
		if result.is_none(){
//			debug!("EVALUATION BUG: Wrong tree, wrong evaluator");
		}
		result	
	}
	pub fn new()->GTP_OpcodeData {
		let data = GTP_OpcodeData {
			list: Vec::new(),
			opcode: Default::default(),
		};
		data
	}

}

pub struct GraftingDataProcessed {
	pub upgrades: FxHashMap<u32,GTP_OpcodeData>,
	pub unit_names: FxHashMap<u32,String>,
}

impl GraftingDataProcessed {
	pub fn evaluate_unit(&self, unit: Unit, player: u8, upgrade_id: u16, game: Game, globals: &mut Globals)->bool{
		/*let result = match &mut self.upgrades.entry(upgrade_id as u32) {
		   Entry::Vacant(_e) => {return false;},//
		   Entry::Occupied(entry) => entry.into_mut(),
		};*/
//		bw_print!("Struct size: {}",self.upgrades.len());

		for (k,v) in self.upgrades.iter() {
			if *k == upgrade_id as u32 {
				//bw_print!("Found comparison: {}",upgrade_id);
				return v.clone().evaluate_unit(upgrade_id, unit,player,None, game, globals);
			}
		}
		//result.evaluate_unit(upgrade_id, unit,player,None, game, globals)
		false
		//true
	}
	fn new()->GraftingDataProcessed {
		unsafe {
		let mut unit_names: FxHashMap<u32,String> = Default::default();
		let (data, len) = bw::unitdef_txt();
		let slice = std::slice::from_raw_parts(data, len);
		for line in slice.split(|&x| x == b'\n') 
		{
			let line = String::from_utf8_lossy(line);
			let line = line.trim();
			if line.starts_with("#") || line.starts_with(";") || line.starts_with("@") || line.is_empty() {
				continue;
			}
			if !(line.starts_with("military") || line.starts_with("building")){
				continue;
			}
			
			let command_list: Vec<&str> = line.split('=').collect();
					
			if command_list.len()==2 {
				let header = command_list[0];
				let header_break: Vec<&str> = header.trim().split(' ').collect();				
				let value: u32 = command_list[1].trim().parse().unwrap();				
				if header_break.len()==2 {
					unit_names.insert(value,header_break[1].to_string());
				}
			}
		}
		let data = GraftingDataProcessed {
			upgrades: FxHashMap::default(),
			unit_names: unit_names,
		};
		data	
		}

	}
	
	fn process(data: GraftingData)->GraftingDataProcessed {
		let mut proc = GraftingDataProcessed::new();
		for u in &mut data.upgrade_reqs.clone() {
			if u.used {
				let convert = u.process();
				/*
				let	mut result = match proc.upgrades.entry(u.upgrade_id) {
				   Entry::Vacant(entry) => entry.insert(GTP_OpcodeData::new()),
				   Entry::Occupied(entry) => entry.into_mut(),
				};
				result = &mut convert.clone();*/
				proc.upgrades.insert(u.upgrade_id,convert);
				
//				result.list = convert.list.clone();
//				result.opcode = convert.opcode.clone();	
			}
		}
		proc
	}
}


#[derive(Debug, Clone, Eq, PartialEq)]
pub struct CfgConvUnit {
	pub from: UnitId,
	pub to: UnitId,//later replace by new value once unit extender is implemented
	pub count: u32,
}
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct CfgConvBuilding {
	pub head: UnitId,
	pub listing: Vec<UnitId>,//later replace by new value once unit extender is implemented
	pub is_not_building: bool,
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct EscId {
	pub from: UnitId,
	pub to: UnitId,
	pub upg_id: u32,
}

#[derive(Debug, Clone, Default)]
pub struct EscalationList {
	pub esc_list: Vec<EscId>,
}

impl EscalationList {
	pub fn add(&mut self, from: UnitId, to: UnitId, upg_id: u32){
		let value = EscId{from,to,upg_id};
        if !self.esc_list.iter().any(|i| i == &value) {
            let insert_pos = self
                .esc_list
                .binary_search_by(|a| value.from.0.cmp(&a.from.0))
                .unwrap_or_else(|e| e);
            self.esc_list.insert(insert_pos, value)
        }	
	}
    pub fn remove(&mut self, from: UnitId, to: UnitId, upg_id: u32) {
		let value = EscId{from,to,upg_id};
        if let Some(pos) = self.esc_list.iter().position(|i| i == &value) {
            self.esc_list.remove(pos);
        }
    }
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct CfgBounce {
	pub bullet_id: u32,
	pub bounces: u32,
	pub unit_id: UnitId,
}

#[derive(Debug, Clone, Default)]
pub struct CfgConvList {
	pub id_list: Vec<CfgConvUnit>,
	pub equivalent_list: Vec<CfgConvBuilding>,
	pub single_list: Vec<UnitId>,
	pub bounce_list: Vec<CfgBounce>,
	pub integer_list: Vec<u32>,
	pub modified: bool,
}

impl CfgConvList {
	fn new()->CfgConvList {
		use bw_dat::unit::*;
		let mut zerg_list: CfgConvList = Default::default();
		// zerg_list.id_list.push(CfgConvUnit{from: MUTALISK, to: GUARDIAN, count: 1});
		zerg_list.id_list.push(CfgConvUnit{from: MUTALISK, to: DEVOURER, count: 1});
		// zerg_list.id_list.push(CfgConvUnit{from: HYDRALISK, to: LURKER, count: 1});
		// zerg_list.id_list.push(CfgConvUnit{from: HIGH_TEMPLAR, to: ARCHON, count: 2});		
		// zerg_list.id_list.push(CfgConvUnit{from: DARK_TEMPLAR, to: DARK_ARCHON, count: 2});
		zerg_list
	}
	fn new_eggs()->CfgConvList {
		use bw_dat::unit::*;
		let mut egg_list: CfgConvList = Default::default();
		egg_list.single_list.push(EGG);
		egg_list.single_list.push(COCOON);
		// egg_list.single_list.push(LURKER_EGG);
		egg_list	
	}
	fn zerg_morphs()->CfgConvList {
		use bw_dat::unit::*;
		let mut morph_list: CfgConvList = Default::default();
		morph_list.single_list.push(SPORE_COLONY);
		morph_list.single_list.push(SUNKEN_COLONY);
		morph_list.single_list.push(GREATER_SPIRE);
		morph_list.single_list.push(HIVE);
		morph_list.single_list.push(LAIR);	
		morph_list
		
	}
	fn def_bullets()->CfgConvList {
		use bw_dat::unit::*;
		let mut bullet_list: CfgConvList = Default::default();
		for i in 0x8d..=0xab {
			bullet_list.integer_list.push(i);
		}
		for i in 0xc9..=0xce {
			bullet_list.integer_list.push(i);
		}
		bullet_list
		
	}	
	fn train_morphs()->CfgConvList {
		use bw_dat::unit::*;
		let mut lister: CfgConvList = Default::default();
		//lister.equivalent_list.push(CfgConvBuilding{head: MUTALISK, listing: vec![MUTALISK,GUARDIAN], 
		//							is_not_building: true});
		lister.equivalent_list.push(CfgConvBuilding{head: MUTALISK, listing: vec![MUTALISK,DEVOURER], 
									is_not_building: true});
		//lister.equivalent_list.push(CfgConvBuilding{head: HYDRALISK, listing: vec![HYDRALISK,LURKER], 
		//							is_not_building: true});									
		lister
	}
	fn new_builds()->CfgConvList {
		use bw_dat::unit::*;
		let mut lister: CfgConvList = Default::default();
		
		lister.equivalent_list.push(CfgConvBuilding{head: SIEGE_TANK_TANK, listing: vec![SIEGE_TANK_TANK,SIEGE_TANK_SIEGE], 
									is_not_building: true});
		lister.equivalent_list.push(CfgConvBuilding{head: SIEGE_TANK_SIEGE, listing: vec![SIEGE_TANK_TANK,SIEGE_TANK_SIEGE], 
									is_not_building: true});
		lister.equivalent_list.push(CfgConvBuilding{head: SPIRE, listing: vec![SPIRE,GREATER_SPIRE], is_not_building: false});
		lister.equivalent_list.push(CfgConvBuilding{head: CREEP_COLONY, listing: vec![CREEP_COLONY,SUNKEN_COLONY,SPORE_COLONY], 
									is_not_building: false});
		lister.equivalent_list.push(CfgConvBuilding{head: HATCHERY, listing: vec![HATCHERY,LAIR,HIVE], is_not_building: false});
		lister.equivalent_list.push(CfgConvBuilding{head: LAIR, listing: vec![LAIR,HIVE], is_not_building: false});

		
		lister
	}	
	
	pub fn get_req(&mut self, morphs_to: UnitId)->(UnitId,u32){
		use bw_dat::unit::*;
		for i in self.id_list.clone() {
			if i.to == morphs_to {
				return (i.from,i.count).to_owned();
			}
		}
		return (NONE,0);
	}
	
	pub fn add_build(&mut self, cfgbuild: CfgConvBuilding){
        if !self.equivalent_list.iter().any(|i| i == &cfgbuild) {
            let insert_pos = self
                .equivalent_list
                .binary_search_by(|a| cfgbuild.head.0.cmp(&a.head.0))
                .unwrap_or_else(|e| e);
            self.equivalent_list.insert(insert_pos, cfgbuild)
        }
		
	}
    pub fn remove_build(&mut self, cfgbuild: CfgConvBuilding) {
        if let Some(pos) = self.equivalent_list.iter().position(|i| i == &cfgbuild) {
            self.equivalent_list.remove(pos);
        }
    }	
	pub fn add_single(&mut self, value: UnitId){
        if !self.single_list.iter().any(|i| i == &value) {
            let insert_pos = self
                .single_list
                .binary_search_by(|a| value.0.cmp(&a.0))
                .unwrap_or_else(|e| e);
            self.single_list.insert(insert_pos, value)
        }		
	}
	pub fn remove_single(&mut self, value: UnitId) {
        if let Some(pos) = self.single_list.iter().position(|i| i == &value) {
            self.single_list.remove(pos);
        }
    }
	pub fn add_integer(&mut self, value: u32){
		self.modified = true;
        if !self.integer_list.iter().any(|i| i == &value) {
            let insert_pos = self
                .integer_list
                .binary_search_by(|a| value.cmp(&a))
                .unwrap_or_else(|e| e);
            self.integer_list.insert(insert_pos, value)
        }		
	}
	pub fn remove_integer(&mut self, value: u32) {
		self.modified = true;
        if let Some(pos) = self.integer_list.iter().position(|i| i == &value) {
            self.integer_list.remove(pos);
        }
    }	
	
	pub fn add(&mut self, from: UnitId, to: UnitId, count: u32){
		let value = CfgConvUnit{from,to,count};
        if !self.id_list.iter().any(|i| i == &value) {
            let insert_pos = self
                .id_list
                .binary_search_by(|a| value.from.0.cmp(&a.from.0))
                .unwrap_or_else(|e| e);
            self.id_list.insert(insert_pos, value)
        }	
	}
    pub fn remove(&mut self, from: UnitId, to: UnitId, count: u32) {
		let value = CfgConvUnit{from,to,count};
        if let Some(pos) = self.id_list.iter().position(|i| i == &value) {
            self.id_list.remove(pos);
        }
    }
	pub fn add_n(&mut self, unit_id: UnitId, first: u32, second: u32){
		let value = CfgBounce{bullet_id: first, bounces: second, unit_id: unit_id};
        if !self.bounce_list.iter().any(|i| i == &value) {
            let insert_pos = self
                .bounce_list
                .binary_search_by(|a| value.bullet_id.cmp(&a.bullet_id))
                .unwrap_or_else(|e| e);
            self.bounce_list.insert(insert_pos, value)
        }	
	}
    pub fn remove_n(&mut self,  unit_id: UnitId, first: u32, second: u32) {
		let value = CfgBounce{bullet_id: first, bounces: second, unit_id: unit_id};
        if let Some(pos) = self.bounce_list.iter().position(|i| i == &value) {
            self.bounce_list.remove(pos);
        }
    }
}

#[derive(Debug, Clone, Default, Eq, PartialEq)]
pub struct CfgRelic {
	pub upg_id: u32,
}

#[derive(Debug, Clone, Default, Eq, PartialEq)]
pub struct CfgRelicList {
	pub list: Vec<CfgRelic>,
}
impl CfgRelicList {
	pub fn add(&mut self, value: CfgRelic){
        if !self.list.iter().any(|i| i == &value) {
            let insert_pos = self
                .list
                .binary_search_by(|a| value.upg_id.cmp(&a.upg_id))
                .unwrap_or_else(|e| e);
            self.list.insert(insert_pos, value)
        }		
	}
	pub fn remove(&mut self, value: CfgRelic) {
        if let Some(pos) = self.list.iter().position(|i| i == &value) {
            self.list.remove(pos);
        }
    }
}

pub enum DataType {
	Units,
	Weapons,
	Flingy,
	Sprites,
	Images,
}
#[derive(Debug,PartialEq,Clone)]
pub enum UnitsDatEntry {
	Flingy,
	ArmorUpgrade,
	Construction,
	SndReady,
	SndYesFirst,
	SndYesLast,
	SndWhatFirst,
	SndWhatLast,
	SndPissedFirst,
	SndPissedLast,
	Portrait,
}
//COMPATIBILITY MUST BE ADDED IN DATA_EXTENDER/HOOKS.RS IF THE NEW OPCODES ARE ADDED

pub enum WeaponsDatEntry {
	Dmg_Upgrade,
	Flingy,
}

pub struct UnitDataSetting {
	pub entry: UnitsDatEntry,
	pub value: u32,
	pub index: usize,
}

pub struct WeaponDataSetting {
	pub entry: WeaponsDatEntry,
	pub value: u32,
	pub index: usize,
}
pub struct FlingyDataSetting {
	pub entry: FlingyValues,
	pub value: u32,
	pub index: usize,
}

pub struct SpriteDataSetting {
	pub entry: SpriteValues,//changed entry
	pub value: u32,//new value of changed value
	pub index: usize,//id of changed value
}

pub struct ImageDataSetting {
	pub entry: ImageValues,
	pub value: u32,
	pub index: usize,
}

pub struct Config {
    pub test: u8,
	pub extended_upgrades: bool,
	pub extended_data: bool,
	pub escalations_enabled: bool,
	pub extended_megatiles: bool,
	pub shaman_gimmick: bool,
	pub parasite_timers: bool,
	pub full_toilet: bool,
	pub mapcrap_void3: bool,
	pub remove_creeper: bool,
	pub legionnaire_timer: bool,
	pub hierophant_timer: bool,
	pub new_energy_ui: bool,
	pub creeper_link: bool,
	
	pub hydra_malediction: bool,
	pub hydra_indomitable_musculature: bool,
	pub hydra_signify_cowardice: bool,
	pub hydra_signify_malice: bool,
	pub relocator_reaver_pathfinding: bool,
	pub reliquary_hero_loading: bool,
	pub relics_enabled: bool,
	pub hydra_misc: bool,
	pub computer_switch: bool,
	pub hydra_caustic_fission: bool,
	pub unit_morph_prerequisites: CfgConvList,
	pub equivalent_prerequisites: CfgConvList,
	pub bullet_bounces: CfgConvList,
	pub eggs: CfgConvList,
	pub zerg_buildmorphs: CfgConvList,
	pub morph_train_ai: CfgConvList,
	pub selectable: CfgConvList,
	pub unselectable: CfgConvList,
	pub escalations: EscalationList,
	pub relics: CfgRelicList,
	pub bullet_hardcode: CfgConvList,
	//todborne
	pub moderator_behavior: bool,
	pub addon_autoplacing: bool,
	//misc
	pub comm_tower_struct: bool,
	pub high_yield: bool,
	pub ai_nydus_move: bool,
	pub cullen_resource_micro: bool,
	pub debug_upgrade_req: bool,
	pub cullen_equipment: bool,
	pub allow_any_race_build: bool,
	pub extended_tooltips: bool,
	pub extended_attacks: bool,
	pub overlord: bool,
	pub gptp_id_ext: bool,
	pub gptp_globals: bool,
	pub reaver_autobuild: bool,
	pub excavation_settings: ExcSets,
	pub no_miss_chance: bool,
	pub listening_system: bool,
	pub switchable_type_default: Option<u32>,
	pub switchables: Vec<u32>,
	pub units_dat_rewrite: Vec<UnitDataSetting>,
	pub weapons_dat_rewrite: Vec<WeaponDataSetting>,
	pub flingy_dat_rewrite: Vec<FlingyDataSetting>,
	pub sprite_dat_rewrite: Vec<SpriteDataSetting>,
	pub image_dat_rewrite: Vec<ImageDataSetting>,
	
	pub unit_ids: FxHashMap<String,UnitId>,
	pub upgrade_ids: FxHashMap<String,u16>,
}

impl Config {
	fn new()->Config {
		let cfg = Config {
				test: 14,
				extended_upgrades: false,
				extended_data: false,
				extended_megatiles: false,
				escalations_enabled: false,
				shaman_gimmick: false,
				parasite_timers: false,
				full_toilet: false,
				remove_creeper: false,
				hierophant_timer: false,
				
				legionnaire_timer: false,
				new_energy_ui: false,
				creeper_link: false,
				hydra_malediction: false,
				hydra_indomitable_musculature: false,
				hydra_signify_cowardice: false,
				hydra_signify_malice: false,
				relocator_reaver_pathfinding: false,
				reliquary_hero_loading: false,
				relics_enabled: false,
				computer_switch: false,
				hydra_misc: false,
				hydra_caustic_fission: false,
				unit_morph_prerequisites: CfgConvList::new(),
				equivalent_prerequisites: CfgConvList::new_builds(),
				eggs: CfgConvList::new_eggs(),
				zerg_buildmorphs: CfgConvList::zerg_morphs(),
				bullet_hardcode: CfgConvList::def_bullets(),
				morph_train_ai: CfgConvList::train_morphs(),
				bullet_bounces: Default::default(),
				selectable: Default::default(),
				unselectable: Default::default(),
				escalations: Default::default(),
				relics: Default::default(),
				ai_nydus_move: false,
				overlord: false,
				gptp_id_ext: false,
				gptp_globals: false,
				//todborne
				
				moderator_behavior: false,
				comm_tower_struct: false,
				addon_autoplacing: false,
				high_yield: false,
				cullen_resource_micro: false,
				debug_upgrade_req: false,
				cullen_equipment: false,
				allow_any_race_build: false,
				extended_tooltips: false,
				mapcrap_void3: false,
				reaver_autobuild: false,
				no_miss_chance: false,
				listening_system: false,
				extended_attacks: false,
				
				switchable_type_default: None,
				switchables: Vec::new(),
				excavation_settings: Default::default(),
				units_dat_rewrite: Vec::new(),
				weapons_dat_rewrite: Vec::new(),
				flingy_dat_rewrite: Vec::new(),
				sprite_dat_rewrite: Vec::new(),
				image_dat_rewrite: Vec::new(),
				
				unit_ids: FxHashMap::default(),
				upgrade_ids: FxHashMap::default(),
		};	
		cfg
	}
	pub unsafe fn parse_uid(&self, unit_id_str: &str)->UnitId{
		for (string,id) in self.unit_ids.iter(){
			if unit_id_str==string {
				return id.clone();
			}
		}
		parse_unit_num(unit_id_str)
	}
	pub unsafe fn id_setup(&mut self){
		let (data, len) = bw::unitdef_txt();
		let slice = std::slice::from_raw_parts(data, len);
		for line in slice.split(|&x| x == b'\n') 
		{
			let line = String::from_utf8_lossy(line);
			let line = line.trim();
			if (line.starts_with("#") && !line.starts_with("#upgrade_ext")) || line.starts_with(";") || line.starts_with("@") || line.is_empty() {
				continue;
			}
			let command_list: Vec<&str> = line.split('=').collect();
			if command_list.len()==2 {
				if line.starts_with("military") || line.starts_with("building") || line.starts_with("upgrade") ||
					line.starts_with("#upgrade_ext"){
					let header = command_list[0];
					let header_break: Vec<&str> = header.trim().split(' ').collect();	
					if header_break.len()==2 {
						if line.starts_with("military") || line.starts_with("building") {
							self.unit_ids.insert(header_break[1].to_string(),parse_unit_num(command_list[1].trim()));	
						}
						else if line.starts_with("upgrade") {
							//upgrade
//							self.upgrade_ids.insert(header_break[1].to_string(),command_list[1].trim().parse().unwrap());
						}
					}
				}
			}			
			
		}
	}
}

use parking_lot::{RwLock};
lazy_static! {
   static ref CONFIG: Mutex<Option<Arc<Config>>> = Mutex::new(None);
}

lazy_static! {
    static ref GRAFT_DATA: Mutex<Option<Arc<GraftingDataProcessed>>> = Mutex::new(None);
//	static ref GRAFT2_DATA: RwLock<GraftingDataProcessed> = RwLock::new(GraftingDataProcessed::new());
	
	
}

pub fn set_config(config: Config) {
    *CONFIG.lock().unwrap() = Some(Arc::new(config));
}
pub fn set_grafting_data(data: GraftingDataProcessed){
	*GRAFT_DATA.lock().unwrap() = Some(Arc::new(data));


//	let mut w = GRAFT2_DATA.write();
	//*w = data;
}

pub fn process_grafting_data(data: GraftingData)->Result<GraftingDataProcessed,String>{
	let n: GraftingDataProcessed = GraftingDataProcessed::process(data);
	Ok(n)
}

pub fn default_config(){
	let config = Config::new();
    *CONFIG.lock().unwrap() = Some(Arc::new(config));
	
}
pub fn set_graftdata_default(){
	let data = GraftingDataProcessed::new();
   *GRAFT_DATA.lock().unwrap() = Some(Arc::new(data));
   
	//let mut w = GRAFT2_DATA.write();
	//*w = data;
}
fn one_liner(line: &str, cfg_str: &str)->bool{
	if line.starts_with(cfg_str) {
		let command_list: Vec<&str> = line.split('=').collect();
		if command_list.len() == 2 && command_list[1].trim() == "true" {	
			return true;
		}
	}	
	false
}



pub unsafe fn parse_unit_num(upgrade: &str)->UnitId {
	if upgrade.parse::<u32>().is_ok() {
	
		return UnitId(upgrade.parse().unwrap());
	}
/*	let msg = format!("can't parse string {}",upgrade);
	windows::message_box("Aise panic", &msg);*/
	UnitId(0xe4)
}

pub unsafe fn parse_unit_id(unit_id_str: &str)->UnitId{
	//bw_print!("Load unitdef for upgrades");	
	let (data, len) = bw::unitdef_txt();
    
	let slice = std::slice::from_raw_parts(data, len);
	for line in slice.split(|&x| x == b'\n') 
	{
		let line = String::from_utf8_lossy(line);
		let line = line.trim();
		if line.starts_with("#") || line.starts_with(";") || line.starts_with("@") || line.is_empty() {
			continue;
		}
		if !(line.starts_with("military") || line.starts_with("building")){
			continue;
		}
		let command_list: Vec<&str> = line.split('=').collect();
		if command_list.len()==2 {
			let header = command_list[0];
			let header_break: Vec<&str> = header.trim().split(' ').collect();	
			let value: UnitId = parse_unit_num(command_list[1].trim());
			if header_break.len()==2 {
				if header_break[1] == unit_id_str {
					return value;
				}
			}
		}
	}
	parse_unit_num(unit_id_str)
}


#[derive(Debug, Clone, Eq, PartialEq, Copy)]
pub enum CfgOpcode {
	None,Add,Remove
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct UnitMLSet {
	pub head: UnitId,
	pub listing: Vec<UnitId>,
	pub flag: bool,
	pub value: u32,
	pub value2: u32,
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct UnitMultilister {
	pub sets: Vec<UnitMLSet>,
	pub cfg_opc: CfgOpcode,
}

#[derive(Debug, Clone, Eq, PartialEq)]
enum Listers {
	UnitPrerequisite,
	EquivalentId,
	EscalationId,
	SingleUnitParse,
	SingleUpgradeParse,
	SingleIntegerParse,
	NumberSet,
	None
}

#[derive(Debug, Default, Clone, Eq, PartialEq)]
pub struct ExcSets {
	pub sets: [Vec<ExcSetting>;0x8],
}
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct ExcSetting {
	pub min: u32,
	pub max: u32,
	pub tileset: u8,
}
use windows;
pub unsafe fn parse_upgrade_range(line: &str)->Result<Vec<u32>,Error>{
	let command_list: Vec<&str> = line.split('=').collect();	
	if command_list.len() != 2 {
		return Err(Context::new(format!("command list length == {}",command_list.len())).into());
	}
	let mut result = Vec::new();
	let mut second_line = command_list[1].trim().to_string();
	second_line.drain(second_line.len()-1..second_line.len());
	second_line.drain(0..1);
	let upg_list: Vec<&str> = second_line.split(',').collect();
	for u in upg_list {
		let upgrade = idle_tactics::parse_upg_id(u.trim());
		
		result.push(upgrade);
	}
	Ok(result)
}

pub unsafe fn parse_data_rewrite_range(line: &str, cfg: &mut Config){
	let command_list: Vec<&str> = line.split('=').collect();
	if command_list.len() != 2 {
		return;
	}
	let mut second_line = command_list[1].trim().to_string();
	second_line.drain(second_line.len()-1..second_line.len());
	second_line.drain(0..1);
	let data_info: Vec<&str> = second_line.split(',').collect();	
	if data_info.len() != 4 {
		return;
	}
	let data_type = data_info[0].to_string();
	let data_type = match data_type.to_lowercase().as_ref(){
		"weapon"=>DataType::Weapons,
		"unit"=>DataType::Units,
		"flingy"=>DataType::Flingy,
		"sprite"=>DataType::Sprites,
		"image"=>DataType::Images,
		//
		_=>DataType::Units,
	};
	match data_type {
		DataType::Units=>{
			let units_dat_type = match data_info[1].trim().to_string().to_lowercase().as_ref(){
				"armorupgrade"=>UnitsDatEntry::ArmorUpgrade,
				"flingy"=>UnitsDatEntry::Flingy,
				"construction"=>UnitsDatEntry::Construction,				
				"snd_ready"=>UnitsDatEntry::SndReady,
				"snd_yes_first"=>UnitsDatEntry::SndYesFirst,
				"snd_yes_last"=>UnitsDatEntry::SndYesLast,
				"snd_what_first"=>UnitsDatEntry::SndWhatFirst,
				"snd_what_last"=>UnitsDatEntry::SndWhatLast,
				"snd_pss_first"=>UnitsDatEntry::SndPissedFirst,
				"snd_pss_last"=>UnitsDatEntry::SndPissedLast,
				"portrait"=>UnitsDatEntry::Portrait,
				_=>UnitsDatEntry::ArmorUpgrade,
			};
			let index = parse_unit_id(data_info[2].trim()).0 as usize;
			if index == 228 {
				let msg = format!("can't parse string {}",data_info[2]);
				windows::message_box("Aise panic", &msg);
			}
			let value = match units_dat_type {
				UnitsDatEntry::ArmorUpgrade=>idle_tactics::parse_upg_id(data_info[3].trim()),
				_=>data_info[3].trim().parse().unwrap(),
			};
			let data = UnitDataSetting { entry: units_dat_type, 
										index,
										value };	
			cfg.units_dat_rewrite.push(data);
		},
		DataType::Weapons=>{
			let weapon_dat_type = match data_info[1].trim().to_string().to_lowercase().as_ref(){
				"upgrade"=>WeaponsDatEntry::Dmg_Upgrade,
				"flingy"=>WeaponsDatEntry::Flingy,
				_=>WeaponsDatEntry::Dmg_Upgrade,
			};			
			let index = data_info[2].trim().parse::<u32>().unwrap() as usize;
			let value = match weapon_dat_type {
				WeaponsDatEntry::Dmg_Upgrade=>idle_tactics::parse_upg_id(data_info[3].trim()),
				_=>data_info[3].trim().parse().unwrap(),
			};			
			let data = WeaponDataSetting { entry: weapon_dat_type, 	
										index,
										value };
			debug!("Save weapon entry");
			cfg.weapons_dat_rewrite.push(data);
		},
		DataType::Flingy=>{
			let flingy_dat_rewrite = data_extender::flingy_identifier(&data_info[1].trim().to_string().to_lowercase());
			let data = FlingyDataSetting { entry: flingy_dat_rewrite.unwrap(), 	
										index: data_info[2].trim().parse().unwrap(),
										value: data_info[3].trim().parse().unwrap() };
			cfg.flingy_dat_rewrite.push(data);
		},
		DataType::Sprites=>{
			let sprites_dat_rewrite = data_extender::sprite_identifier(&data_info[1].trim().to_string().to_lowercase());
			let data = SpriteDataSetting { entry: sprites_dat_rewrite.unwrap(), 	
										index: data_info[2].trim().parse().unwrap(),
										value: data_info[3].trim().parse().unwrap() };
			cfg.sprite_dat_rewrite.push(data);
		},
		DataType::Images=>{
			let image_dat_rewrite = data_extender::image_identifier(&data_info[1].trim().to_string().to_lowercase());
			let data = ImageDataSetting { entry: image_dat_rewrite.unwrap(), 	
										index: data_info[2].trim().parse().unwrap(),
										value: data_info[3].trim().parse().unwrap() };
			cfg.image_dat_rewrite.push(data);
		},	
		_=>{},
	}
}

pub unsafe fn parse_excavation_range(line: &str)->Result<ExcSetting,Error>{
	let command_list: Vec<&str> = line.split('=').collect();		
	if command_list.len() != 2 {
		return Err(Context::new(format!("command list length == {}",command_list.len())).into());
	}
	let mut second_line = command_list[1].trim().to_string();
	second_line.drain(second_line.len()-1..second_line.len());
	second_line.retain(|x| x!=')' || x!='(');
	
	
	let exc_list: Vec<&str> = second_line.split(',').collect();	
	if exc_list.len() != 3 {
		return Err(Context::new(format!("excavation list length == {}",command_list.len())).into());
	}
	let mut tileset_name = exc_list[0].to_string();
	tileset_name.drain(0..1);
	let tileset = match tileset_name.to_lowercase().as_ref(){
		"badlands"=>0,
		"space"|"space_platform"|"platform"=>1,
		"installation"=>2,
		"ashworld"=>3,
		"jungle"=>4,
		"desert"=>5,
		"ice"=>6,
		"twilight"=>7,
		_=>0,
	};
	
	//let f = format!("{} {}, tileset -- {}/{}",exc_list[1],exc_list[2],tileset,tileset_name);
	//windows::message_box("Aise panic", &f);
	let exc = ExcSetting {
		tileset: tileset,
		min: exc_list[1].parse().unwrap(),
		max: exc_list[2].parse().unwrap(),
//		min: 0,
//		max: 0,
	};
	
	Ok(exc)
}
pub unsafe fn parse_multilister(line: &str)->Result<UnitMultilister,Error>{
	let mut result = UnitMultilister{ sets: Vec::new(), cfg_opc: CfgOpcode::None, };
	let command_list: Vec<&str> = line.split('=').collect();		
	if command_list.len() != 2 {
		//return Ok(cfg);
		return Err(Context::new(format!("command list length == {}",command_list.len())).into());
	}
	let mut cmd = Listers::None;
	if line.starts_with("unit_morph_prerequisites") {
		cmd=Listers::UnitPrerequisite;
	}
	if line.starts_with("equivalent_ids") || line.starts_with("morph_train_ai") {
		cmd=Listers::EquivalentId;
	}	
	if line.starts_with("escalation") && !line.starts_with("escalations_enabled"){
		cmd = Listers::EscalationId;
	}
	if line.starts_with("egg") || line.starts_with("unit_selectable") || line.starts_with("zerg_buildmorph")  {
		cmd=Listers::SingleUnitParse;
	}	
	if line.starts_with("relic_setup"){
		cmd=Listers::SingleUpgradeParse;
	}		
	if line.starts_with("bullet_hardcode"){
		cmd=Listers::SingleIntegerParse;
	}			
	if line.starts_with("bounce_set"){
		cmd=Listers::NumberSet;
	}
	
	let op;
	let second_line = command_list[1].trim();
	if second_line.starts_with("Add"){
		op = CfgOpcode::Add;
	}
	else if second_line.starts_with("Remove"){
		op = CfgOpcode::Remove;
	}
	else {		
		//return Ok(cfg);
		return Err(Context::new(format!("{} - incorrect second line: {}",command_list[0],second_line)).into());
	}
	
	let unit_list: &str = match op {
		CfgOpcode::Add=> (&second_line.trim()[4..second_line.len() - 1]).into(),
		CfgOpcode::Remove=> (&second_line.trim()[7..second_line.len() - 1]).into(),
		_=> second_line,
	};
	result.cfg_opc = op;
	let conversion_list: Vec<&str> = unit_list.split(';').collect();
	
	
	for unit_value in conversion_list {
		let mut set = UnitMLSet{ head: UnitId(0xe4), listing: Vec::new(), flag: false, value: 0, value2: 0};
		
		let unit_line = unit_value.clone();
		let arg_list: Vec<&str> = unit_line.split(',').collect();

		match cmd {
			Listers::UnitPrerequisite=>{
				if arg_list.len()==3 {
					set.value = arg_list[2].trim().parse().unwrap();
					set.listing.push(parse_unit_id(arg_list[0].trim()));
					set.listing.push(parse_unit_id(arg_list[1].trim()));
				}
				else {
					return Err(Context::new(format!("unit prereq incorrect length: {}, ",arg_list.len())).into());
					
				}
			},
			Listers::EscalationId=>{
				if arg_list.len()==3 {
//					set.value = arg_list[0].trim().parse().unwrap();
					set.value = idle_tactics::parse_upg_id(arg_list[0].trim());
					set.listing.push(parse_unit_id(arg_list[1].trim()));
					set.listing.push(parse_unit_id(arg_list[2].trim()));/*
					return Err(Context::new(format!("escalation incorrect length: {}, list {:?}, res {} {}, u {}",arg_list.len(),arg_list,
							parse_unit_id(arg_list[1].trim()).0,parse_unit_id(arg_list[2].trim()).0, 			
							idle_tactics::parse_upg_id(arg_list[0].trim()))).into());*/
				}
				else {
					return Err(Context::new(format!("escalation incorrect length: {}",arg_list.len())).into());
					
				}
			},			
			Listers::EquivalentId=>{
				if arg_list.len()>=1 {
					set.head = parse_unit_id(arg_list[0].trim());
					
				}
				else {
					return Err(Context::new(format!("equivalent argument list < 1, {:?}",arg_list)).into());
					
				}
				
				if arg_list.len()>=2 {
					for i in arg_list {
						if i.trim()=="non_building" {
							set.flag = true;
							
						}
						else {
							set.listing.push(parse_unit_id(i.trim()));
						}
					}
					//return Err(Context::new(format!("first lister - okay")).into());
					
				}
				else {
					return Err(Context::new(format!("equivalent argument list empty head or no head, {:?}",arg_list)).into());				
				}
			},
			Listers::SingleUnitParse=>{
				if arg_list.len()>=1 {
					set.head = parse_unit_id(arg_list[0].trim());
					set.flag = match op {
						CfgOpcode::Add => true,
						CfgOpcode::Remove => false,
						_=>{return Err(Context::new(format!("wrong single unit opcode")).into());	},
					}
				}
			},
			Listers::SingleUpgradeParse=>{
				if arg_list.len()>=1 {
					set.value = idle_tactics::parse_upg_id(arg_list[0].trim());
					set.flag = match op {
						CfgOpcode::Add => true,
						CfgOpcode::Remove => false,
						_=>{return Err(Context::new(format!("wrong single unit opcode")).into());	},
					}
				}
			},
			Listers::SingleIntegerParse=>{
				if arg_list.len()>=1 {
					set.value = idle_tactics::parse_upg_id(arg_list[0].trim());
				}			
			},
			Listers::NumberSet=>{
				if arg_list.len()==3 {
					set.value = arg_list[1].trim().parse().unwrap();
					set.value2 = arg_list[2].trim().parse().unwrap();
					set.head =  parse_unit_id(arg_list[0].trim());
				}
				else {
					return Err(Context::new(format!("unit numberset bounce incorrect length: {}, ",arg_list.len())).into());
					
				}
			},
			_=>{},
		}
		result.sets.push(set);
	}

	Ok(result)
}

//was mutable previously



pub fn read_config(data: &[u8]) -> Result<Config, Error> {
	unsafe {
	let mut cfg = Config::new();
	
	cfg.id_setup();
	for line in data.split(|&x| x == b'\n') 
	{
		let line = String::from_utf8_lossy(line);
		let line = line.trim();
		if line.starts_with("#") || line.starts_with(";") || line.is_empty() {
			continue;
		}
		if one_liner(line,"extended_upgrades"){
			cfg.extended_upgrades=true;
		}
		if one_liner(line,"extended_data"){
			cfg.extended_data=true;
		}
		if one_liner(line,"extended_attacks"){
			cfg.extended_attacks=true;
		}		
		if one_liner(line,"escalations_enabled"){
			cfg.escalations_enabled=true;
		}	
		if one_liner(line,"extended_megatiles"){
			cfg.extended_megatiles=true;
		}		
		if one_liner(line,"hydra_shaman_ai"){
			cfg.shaman_gimmick=true;
		}				
		if one_liner(line,"parasite_timers"){
			cfg.parasite_timers=true;
		}	
	
		if one_liner(line,"full_toilet"){
			cfg.full_toilet=true;
		}	
		if one_liner(line,"mapcrap_void3"){
			cfg.mapcrap_void3=true;
		}	
		if one_liner(line,"remove_creeper"){
			cfg.remove_creeper=true;
		}		
		if one_liner(line,"hierophant_timer"){
			cfg.hierophant_timer=true;
		}			
		if one_liner(line,"moderator_behavior") {
			cfg.moderator_behavior=true;
		}			
		if one_liner(line,"addon_autoplacing") {
			cfg.addon_autoplacing = true;
		}
		if one_liner(line,"legionnaire_timer"){
			cfg.legionnaire_timer=true;
		}
		if one_liner(line,"new_energy_ui"){
			cfg.new_energy_ui=true;
		}
		if one_liner(line,"comm_tower"){
			cfg.comm_tower_struct=true;
		}
		if one_liner(line,"high_yield"){
			cfg.high_yield=true;
		}
		if one_liner(line,"creeper_link"){
			cfg.creeper_link=true;
		}		
		if one_liner(line,"hydra_indomitable_musculature"){
			cfg.hydra_indomitable_musculature=true;
		}
		if one_liner(line,"hydra_malediction"){
			cfg.hydra_malediction=true;
		}
		if one_liner(line,"hydra_signify_cowardice"){
			cfg.hydra_signify_cowardice=true;
		}
		if one_liner(line,"hydra_signify_malice"){
			cfg.hydra_signify_malice=true;
		}
		if one_liner(line,"relocator_reaver_pathfinding"){
			cfg.relocator_reaver_pathfinding=true;
		}
		if one_liner(line,"reliquary_hero_loading"){
			cfg.reliquary_hero_loading=true;
		}
		if one_liner(line,"relics_enabled"){
			cfg.relics_enabled=true;
		}
		if one_liner(line,"hydra_command_opcodes") || one_liner(line,"hydra_misc"){
			cfg.hydra_misc=true;
		}
		if one_liner(line,"computer_switch"){
			cfg.computer_switch=true;
		}		
		if one_liner(line,"hydra_caustic_fission"){
			cfg.hydra_caustic_fission=true;
		}
		if one_liner(line,"ai_nydus_move"){
			cfg.ai_nydus_move=true;
		}		
		if one_liner(line,"cullen_resource_micro"){
			cfg.cullen_resource_micro=true;
		}
		if one_liner(line,"debug_upgrade_req"){
			cfg.debug_upgrade_req=true;
		}	
		if one_liner(line,"overlord"){
			cfg.overlord=true;
		}	
		if one_liner(line,"gptp_globals"){
			cfg.gptp_globals=true;
		}	
		if one_liner(line,"gptp_id_ext"){
			cfg.gptp_id_ext = true;
		}
		if one_liner(line,"cullen_equipment"){
			cfg.cullen_equipment=true;
		}
		if one_liner(line,"allow_any_race_build"){
			cfg.allow_any_race_build = true;
		}
		if one_liner(line,"extended_tooltips"){
			cfg.extended_tooltips = true;
		}
		if one_liner(line,"reaver_autobuild"){
			cfg.reaver_autobuild = true;
		}
		if one_liner(line,"no_miss_chance"){
			cfg.no_miss_chance = true;
		}
		if one_liner(line,"listening_system"){
			cfg.listening_system = true;
		}
		if line.starts_with("excavate_settings"){
			let parse_lister = parse_excavation_range(line);
			let lister = match parse_lister {
				Ok(o)=>o,
				Err(e)=>{
					return Err(e);
				},
			};
			cfg.excavation_settings.sets[lister.tileset as usize].push(lister);
		}
		if line.starts_with("unit_morph_prerequisites") {
			let parse_lister = parse_multilister(line);
			let lister = match parse_lister {
				Ok(o)=>o,
				Err(e)=>{
					return Err(e);
				},
			};
			match lister.cfg_opc {
				CfgOpcode::Add=>{
					for s in lister.sets {
						cfg.unit_morph_prerequisites.add(s.listing[0],s.listing[1],s.value);
					}				
				},
				CfgOpcode::Remove=>{
					for s in lister.sets {
						cfg.unit_morph_prerequisites.remove(s.listing[0],s.listing[1],s.value);
					}
				},
				CfgOpcode::None=>{
					return Err(Context::new(format!("multilister parsed incorrectly, no opcode")).into());
				},
			}
		}
		if line.starts_with("bounce_set") {
			let parse_lister = parse_multilister(line);
			let lister = match parse_lister {
				Ok(o)=>o,
				Err(e)=>{
					return Err(e);
				},
			};
			match lister.cfg_opc {
				CfgOpcode::Add=>{
					for s in lister.sets {
						cfg.bullet_bounces.add_n(s.head,s.value,s.value2);
					}				
				},
				CfgOpcode::Remove=>{
					for s in lister.sets {
						cfg.bullet_bounces.remove_n(s.head,s.value,s.value2);
					}
				},
				CfgOpcode::None=>{
					return Err(Context::new(format!("multilister bullet bounce parsed incorrectly, no opcode")).into());
				},
			}
		}
		//unit escalations
		if line.starts_with("escalation") && !line.starts_with("escalations_enabled"){
			let parse_lister = parse_multilister(line);
			let lister = match parse_lister {
				Ok(o)=>o,
				Err(e)=>{
					return Err(e);
				},
			};
			match lister.cfg_opc {
				CfgOpcode::Add=>{
					for s in lister.sets {
						cfg.escalations.add(s.listing[0],s.listing[1],s.value);
					}				
				},
				CfgOpcode::Remove=>{
					for s in lister.sets {
						cfg.escalations.remove(s.listing[0],s.listing[1],s.value);
					}
				},
				CfgOpcode::None=>{
					return Err(Context::new(format!("escalations parsed incorrectly, no opcode")).into());
				},
			}		
		}
		//upgrade escalations (overlord)
		if line.starts_with("switchable_type_default"){
			let list: Vec<&str> = line.split('=').collect();
			if list.len()==2 {
				let upg = list[1].trim();
				let upgrade = idle_tactics::parse_upg_id(upg);
				cfg.switchable_type_default = Some(upgrade);
			}
		}
		if line.starts_with("data_rewrite"){
			parse_data_rewrite_range(line,&mut cfg);
		}
		if line.starts_with("switchable_type_upgrades"){
			let parse_lister = parse_upgrade_range(line);
			let mut lister = match parse_lister {
				Ok(o)=>o,
				Err(e)=>{
					return Err(e);
				},
			};
			cfg.switchables.append(&mut lister);
		}
		if line.starts_with("equivalent_ids") {
			
			let parse_lister = parse_multilister(line);
			let lister = match parse_lister {
				Ok(o)=>o,
				Err(e)=>{
					return Err(e);
				},
			};
			for s in lister.sets {
				let mut cbuild = CfgConvBuilding{head: s.listing[0],  listing: Vec::new(), is_not_building: s.flag};
				for i in s.listing {
					cbuild.listing.push(i);
				}	
				match lister.cfg_opc {
					CfgOpcode::Add => {
						cfg.equivalent_prerequisites.add_build(cbuild);
					},
					CfgOpcode::Remove => {
						cfg.equivalent_prerequisites.remove_build(cbuild);
					},				
					_=>{
						return Err(Context::new(format!("multilister equivalent parsed incorrectly, no opcode")).into());
					},
				}
			}
			
		}
		if line.starts_with("morph_train_ai") {		
			let parse_lister = parse_multilister(line);
			let lister = match parse_lister {
				Ok(o)=>o,
				Err(e)=>{
					return Err(e);
				},
			};
			for s in lister.sets {
				let mut cbuild = CfgConvBuilding{head: s.listing[0],  listing: Vec::new(), is_not_building: s.flag};
				for i in s.listing {
					cbuild.listing.push(i);
				}	
				match lister.cfg_opc {
					CfgOpcode::Add => {
						cfg.morph_train_ai.add_build(cbuild);
					},
					CfgOpcode::Remove => {
						cfg.morph_train_ai.remove_build(cbuild);
					},				
					_=>{
						return Err(Context::new(format!("multilister morph_train_ai parsed incorrectly, no opcode")).into());
					},
				}
			}
		}
		if line.starts_with("egg"){
			let parse_lister = parse_multilister(line);
			let lister = match parse_lister {
				Ok(o)=>o,
				Err(e)=>{
					return Err(e);
				},
			};
			for s in lister.sets {
				match lister.cfg_opc {
					CfgOpcode::Add => {
						cfg.eggs.add_single(s.head);
					},
					CfgOpcode::Remove => {
						cfg.eggs.remove_single(s.head);
					},				
					_=>{
						return Err(Context::new(format!("multilister egg parsed incorrectly, no opcode")).into());
					},				
				}
			}
		}	
		if line.starts_with("zerg_buildmorph"){
			let parse_lister = parse_multilister(line);
			let lister = match parse_lister {
				Ok(o)=>o,
				Err(e)=>{
					return Err(e);
				},
			};
			for s in lister.sets {
				match lister.cfg_opc {
					CfgOpcode::Add => {
						cfg.zerg_buildmorphs.add_single(s.head);
					},
					CfgOpcode::Remove => {
						cfg.zerg_buildmorphs.remove_single(s.head);
					},				
					_=>{
						return Err(Context::new(format!("multilister zerg buildmorph parsed incorrectly, no opcode")).into());
					},				
				}
			}		
		}
		if line.starts_with("bullet_hardcode"){
			let parse_lister = parse_multilister(line);
			let lister = match parse_lister {
				Ok(o)=>o,
				Err(e)=>{
					return Err(e);
				},
			};
			for s in lister.sets {
				match lister.cfg_opc {
					CfgOpcode::Add => {
						cfg.bullet_hardcode.add_integer(s.value);
					},
					CfgOpcode::Remove => {
						cfg.bullet_hardcode.remove_integer(s.value);
					},				
					_=>{
						return Err(Context::new(format!("multilister bullet hardcode parsed incorrectly, no opcode")).into());
					},				
				}
			}		
		}
		if line.starts_with("relic_setup"){
			let parse_lister = parse_multilister(line);
			let lister = match parse_lister {
				Ok(o)=>o,
				Err(e)=>{
					return Err(e);
				},
			};
			for s in lister.sets {
				match lister.cfg_opc {
					CfgOpcode::Add => {
						cfg.relics.add(CfgRelic{upg_id: s.value});
					},
					CfgOpcode::Remove => {
						cfg.relics.remove(CfgRelic{upg_id: s.value});
					},				
					_=>{
						return Err(Context::new(format!("multilister relic parsed incorrectly, no opcode")).into());
					},				
				}
			}
		}		
		if line.starts_with("unit_selectable"){
			let parse_lister = parse_multilister(line);
			let lister = match parse_lister {
				Ok(o)=>o,
				Err(e)=>{
					return Err(e);
				},
			};
			for s in lister.sets {
				match lister.cfg_opc {
					CfgOpcode::Add => {
					
						cfg.selectable.add_single(s.head);
					},
					CfgOpcode::Remove => {
						cfg.unselectable.add_single(s.head);
					},				
					_=>{
						return Err(Context::new(format!("multilister selectable parsed incorrectly, no opcode")).into());
					},				
				}
			}
		}
			
		/*
		if line.starts_with("unit_morph_prerequisites") {
			let mut command_list: Vec<&str> = line.split('=').collect();		
			if command_list.len() != 2 {
				//return Ok(cfg);
				return Err(Context::new(format!("command list length == {}",command_list.len())).into());
			}
			let mut op = CfgOpcode::None;
			let second_line = command_list[1].trim();
			if second_line.starts_with("Add"){
				op = CfgOpcode::Add;
			}
			else if second_line.starts_with("Remove"){
				op = CfgOpcode::Remove;
			}
			else {		
				//return Ok(cfg);
				return Err(Context::new(format!("unit morph - incorrect second line: {}",second_line)).into());
				
			}
			
			let unit_list: &str = match op {
				CfgOpcode::Add=> (&second_line[4..second_line.len() - 1]).trim().into(),
				CfgOpcode::Remove=> (&second_line[7..second_line.len() - 1]).trim().into(),
				_=> second_line,
			};
			let conversion_list: Vec<&str> = unit_list.split(';').collect();
			for unit_value in conversion_list {
				let unit_line = unit_value.clone();
				let arg_list: Vec<&str> = unit_line.split(',').collect();
				if arg_list.len()==3 {			
					match op {
						CfgOpcode::Add=>{
							cfg.unit_morph_prerequisites.add(parse_unit_id(arg_list[0].trim()),
															parse_unit_id(arg_list[1].trim()),
															arg_list[2].trim().parse().unwrap());
							},
						CfgOpcode::Remove=>{
							cfg.unit_morph_prerequisites.remove(parse_unit_id(arg_list[0].trim()),
															parse_unit_id(arg_list[1].trim()),
															arg_list[2].trim().parse().unwrap());
							},	
						_=>{},
					}					
			

				}
				else {
					return Err(Context::new(format!("unit morph incorrect parsed line: {}, length: {}",unit_line,arg_list.len())).into());
				}
			}
		}	*/	
	}
	
	return Ok(cfg);
	}
}

pub fn grafting_data() -> Arc<GraftingDataProcessed> {
//pub fn grafting_data() -> GraftingDataProcessed {
    GRAFT_DATA.lock().unwrap().as_ref().unwrap().clone()

//	let read = GRAFT2_DATA.read();
//	*read
}
pub fn config() -> Arc<Config> {
    CONFIG.lock().unwrap().as_ref().unwrap().clone()
}