//for simple extenders (i.e. same byte length)
#![allow(non_snake_case)]
#![allow(dead_code)]
use samase;
use config::{UnitsDatEntry};
use failure::{Context,Error};
use parking_lot::{Mutex, RwLock};
use globals::{self,Globals};
use bw;
use windows;
use winapi::um::processthreadsapi::{GetCurrentProcess, TerminateProcess};

lazy_static! {
   static ref FAKE_UNIT_EXTENDER: RwLock<UnitExtender> = RwLock::new(Default::default());
   static ref FLINGY_EXTENDER: RwLock<FlingyExtender> = RwLock::new(Default::default());
   static ref SPRITE_EXTENDER: RwLock<SpriteExtender> = RwLock::new(Default::default());
   static ref IMAGE_EXTENDER: RwLock<ImageExtender> = RwLock::new(Default::default());
   static ref SFX_EXTENDER: RwLock<SFXExtender> = RwLock::new(Default::default());
   static ref CCMU_EXTENDER: RwLock<CcmuExtender> = RwLock::new(Default::default());
}

pub unsafe fn init_flingy_ext(exe: &mut whack::ModulePatcher){
	FLINGY_EXTENDER.write().extend_data_conventional(exe);
}

pub unsafe fn init_sprite_ext(exe: &mut whack::ModulePatcher){
	SPRITE_EXTENDER.write().extend_data_conventional(exe);
}
pub unsafe fn init_image_ext_new(exe: &mut whack::ModulePatcher){
	IMAGE_EXTENDER.write().init_code(exe);
}
pub unsafe fn init_unit_ext(exe: &mut whack::ModulePatcher){
	FAKE_UNIT_EXTENDER.write().extend_data_conventional(exe);
}
pub unsafe fn init_new_sprite_ext(exe: &mut whack::ModulePatcher){
	SPRITE_EXTENDER.write().extend_data_from_dat_file(exe);
}
pub unsafe fn flingy_identifier(string: &str)->Option<FlingyValues>{
	let mut r = FLINGY_EXTENDER.write();
	let identifier = r.identifier(string);
	identifier
}
pub unsafe fn sprite_identifier(string: &str)->Option<SpriteValues>{
	let mut r = SPRITE_EXTENDER.write();
	let identifier = r.identifier(string);
	identifier
}
pub unsafe fn image_identifier(string: &str)->Option<ImageValues>{
	let mut r = IMAGE_EXTENDER.write();
	let identifier = r.identifier(string);
	identifier
}

pub unsafe fn fakeid_flingy_ptr()->u32 {
	let mut w = FAKE_UNIT_EXTENDER.write();
	if w.flingy_ext.len()==0 {
		return 0;
	}
	let array = &mut w.flingy_ext[0] as *mut u32 as u32;
	array
}

pub unsafe fn set_image_entries(exe: &mut whack::ModulePatcher){
	IMAGE_EXTENDER.write().extend_data_conventional(exe);
}

pub unsafe fn flingy_ext_debug(){
//	FLINGY_EXTENDER.write().extender_debug();
}

pub unsafe fn get_flingy_data(entry: u32, index: usize)->u32{
	let r = FLINGY_EXTENDER.read();
	let result = match entry {
		0=>r.top_speed[index].into(),
		1=>r.acceleration[index].into(),
		2=>r.turn_radius[index].into(),
		3=>r.movement_type[index].into(),
		4=>r.sprite_id[index].into(),
		5=>r.halt_distance[index].into(),
		6=>r.iscript_mask[index].into(),
		_=>0,
	};
	result
}
pub unsafe fn get_unit_data(entry: u32, index: usize)->u32{
	let r = FAKE_UNIT_EXTENDER.read();
	let result = match entry {
		0=>r.flingy_ext[index].into(),
		29=>r.snd_ready[index].into(),
		30=>r.snd_wht_start[index].into(),
		31=>r.snd_wht_end[index].into(),
		32=>r.snd_pss_start[index].into(),
		33=>r.snd_pss_end[index].into(),
		34=>r.snd_yes_start[index].into(),
		35=>r.snd_yes_end[index].into(),
		_=>0,
	};
	result
}
pub unsafe fn set_flingy_data(entry: u32, index: usize, value: u32){
	let mut r = FLINGY_EXTENDER.write();
	match entry {
		0=>r.top_speed[index] = value,
		1=>r.acceleration[index] = value as u16,
		2=>r.turn_radius[index] = value as u8,
		3=>r.movement_type[index] = value as u8,
		4=>r.sprite_id[index] = value as u16,
		5=>r.halt_distance[index] = value,
		6=>r.iscript_mask[index] = value as u8,
		_=>{},
	}	
}
pub unsafe fn set_flingy_data_e(entry: FlingyValues, index: usize, value: u32){
	let mut r = FLINGY_EXTENDER.write();
	match entry {
		FlingyValues::TopSpeed=>r.top_speed[index] = value,
		FlingyValues::Acceleration=>r.acceleration[index] = value as u16,
		FlingyValues::TurnRadius=>r.turn_radius[index] = value as u8,
		FlingyValues::MovementType=>r.movement_type[index] = value as u8,
		FlingyValues::SpriteId=>r.sprite_id[index] = value as u16,
		FlingyValues::HaltDistance=>r.halt_distance[index] = value,
		FlingyValues::IscriptMask=>r.iscript_mask[index] = value as u8,
		_=>{},
	}	
}
pub unsafe fn set_unit_data_e(entry: UnitsDatEntry, index: usize, value: u32){
	let mut r = FAKE_UNIT_EXTENDER.write();
	match entry {
		UnitsDatEntry::Flingy=>r.flingy_ext[index] = value as u32,
		UnitsDatEntry::SndReady=>r.snd_ready[index] = value as u16,
		UnitsDatEntry::SndYesFirst=>r.snd_yes_start[index] = value as u16,
		UnitsDatEntry::SndYesLast=>r.snd_yes_end[index] = value as u16,
		UnitsDatEntry::SndWhatFirst=>r.snd_wht_start[index] = value as u16,
		UnitsDatEntry::SndWhatLast=>r.snd_wht_end[index] = value as u16,
		UnitsDatEntry::SndPissedFirst=>r.snd_pss_start[index] = value as u16,
		UnitsDatEntry::SndPissedLast=>r.snd_pss_end[index] = value as u16,
		_=>{},
	}	
}

pub unsafe fn set_sprite_data_e(entry: SpriteValues, index: usize, value: u32){
	let mut r = SPRITE_EXTENDER.write();
	match entry {
		SpriteValues::IncludeInVision=>r.include_in_vision[index]=value as u8,
		SpriteValues::StartAsVisible=>r.start_as_visible[index]=value as u8,
		SpriteValues::ImageId=>r.image_id[index]=value as u16,
		SpriteValues::SelectionCircle=>r.selection_circle[index]=value as u8,
		SpriteValues::ImagePos=>r.imagepos[index]=value as u8,
		SpriteValues::HpBar=>r.hp_bar[index]=value as u8,
		_=>{},
	}	
}
pub unsafe fn set_image_data_e(entry: ImageValues, index: usize, value: u32){
	let mut r = IMAGE_EXTENDER.write();
	match entry {
		ImageValues::GrpId => r.grp_id[index]=value as u32, 
		ImageValues::GraphicTurns => r.graphic_turns[index]=value as u8, 
		ImageValues::Clickable => r.clickable[index]=value as u8, 
		ImageValues::UseFullIscript => r.use_full_iscript[index]=value as u8, 
		ImageValues::DrawIfCloaked => r.draw_if_cloaked[index]=value as u8, 
		ImageValues::DrawFunc => r.draw_func[index]=value as u8, 
		ImageValues::DrawRemap => r.draw_remap[index]=value as u8, 
		ImageValues::IscriptId => r.iscript_id[index]=value as u32, 
		ImageValues::OverlayShield => r.overlay_shield[index]=value as u32, 
		ImageValues::OverlayAttack => r.overlay_attack[index]=value as u32, 
		ImageValues::OverlayDamage => r.overlay_damage[index]=value as u32, 
		ImageValues::OverlaySpecial => r.overlay_special[index]=value as u32, 
		ImageValues::OverlayLanding => r.overlay_landing[index]=value as u32, 
		ImageValues::OverlayLiftoff => r.overlay_liftoff[index]=value as u32, 
		_=>(),
	}
}

pub unsafe fn set_sprite_data(entry: u32, index: usize, value: u32) {
	let mut r = SPRITE_EXTENDER.write();
	match entry {
		0=>r.include_in_vision[index]=value as u8,
		1=>r.start_as_visible[index]=value as u8,
		2=>r.image_id[index]=value as u16,
		3=>r.selection_circle[index]=value as u8,
		4=>r.imagepos[index]=value as u8,
		5=>r.hp_bar[index]=value as u8,
		_=>(),	
	}
}
pub unsafe fn get_sprite_data(entry: u32, index: usize)->u32 {
	let r = SPRITE_EXTENDER.read();
	match entry {
		4|5|6=>{
			if index < 105 {
				return 0;
			}
		},
		_=>{},
	}
	let result = match entry {
		0=>r.include_in_vision[index].into(),
		1=>r.start_as_visible[index].into(),
		2=>r.image_id[index].into(),
		3=>r.selection_circle[index-105].into(),
		4=>r.imagepos[index-105].into(),
		5=>r.hp_bar[index-105].into(),
		_=>0,
	};
	result
}

pub unsafe fn get_image_data(entry: u32, index: usize)->u32{
	let r = IMAGE_EXTENDER.read();
	let length = r.length as usize;
	let result = match entry {
		0 => r.grp_id[index].into(), 
		1 => r.graphic_turns[index].into(), 
		2 => r.clickable[index].into(), 
		3 => r.use_full_iscript[index].into(), 
		4 => r.draw_if_cloaked[index].into(), 
		5 => r.draw_func[index].into(), 
		6 => r.draw_remap[index].into(), 
		7 => r.iscript_id[index].into(), 
		8 => r.overlay_shield[index].into(), 
		9 => r.overlay_attack[index].into(), 
		10 => r.overlay_damage[index].into(), 
		11 => r.overlay_special[index].into(), 
		12 => r.overlay_landing[index].into(), 
		13 => r.overlay_liftoff[index].into(), 
		14 => r.overlays[index+length*5].into(),
		15 => r.overlays[index].into(), //attack overlays
		16 => r.overlays[index+length].into(), //damage overlays
		17 => r.overlays[index+length*2].into(), //special overlays
		18 => r.overlays[index+length*3].into(), //landing overlays
		19 => r.overlays[index+length*4].into(), //liftoff overlays
		20 => r.image_grp_ptr[index].into(),
		21 => r.building_overlay_state_max[index].into(),
		_=>0,
	};
	result
}

pub trait DataExtender {
    type Id: Copy;
	type SizeLimit;
	unsafe fn uplink<T>(exe: &mut whack::ModulePatcher, new_array: *mut T, address_list: Vec<usize>){
		//debug!("Uplink list...");
		for a in address_list {
			//debug!("address: {:x}, val {:p}",a,new_array);	
			exe.replace_val(a, new_array as u32);
		}	
	}
	unsafe fn uplink_length_dattable(exe: &mut whack::ModulePatcher, length: u32, size_address: Vec<usize>){
		//debug!("Uplink length list...");
		for i in size_address {
			//debug!("address: 0x{:x} {}",i,length);
			exe.replace_val(i, length);
		}
		
	}	
	unsafe fn init_code(&mut self, exe: &mut whack::ModulePatcher){}
	fn get(&mut self, index: usize, identifier: Self::Id)->u32;
	fn identifier(&mut self, line: &str)->Option<Self::Id>;
	fn push_val<T: num_traits::cast::ToPrimitive>(&mut self, identifier: Self::Id, value: T);
	fn set<T: num_traits::cast::ToPrimitive>(&mut self, index: usize, identifier: Self::Id, value: T);
	unsafe fn parse_entry_value(&mut self, text: String, identifier: Self::Id)->u32;
	unsafe fn parse(&mut self, filename: &str)->Result<(),String>{
		//debug!("Parse stuff");
		let (data, len) = match samase::read_file(filename) {
            Some(s) => s,
            None => { return Err(format!("Unable to read file {}",filename)); },
        };
		let slice = std::slice::from_raw_parts(data, len);
		for line in slice.split(|&x| x == b'\n') 
		{
            let line = String::from_utf8_lossy(line);
            let line = line.trim();
            if line.starts_with("#") || line.starts_with(";") || line.is_empty() {
                continue;
            }
			let command_list: Vec<&str> = line.split(':').collect();
			if command_list.len()==2 {
				let name = command_list[0];
				//debug!("Split line: {} {}",command_list[0],command_list[1]);
				
				if let Some(id) = self.identifier(&name.to_lowercase()){
					let value: u32 = self.parse_entry_value(command_list[1].to_string(),id);
					self.push_val::<u32>(id,value);
				}
			}
		}
		Ok(())	
	}
	unsafe fn parse_set(&mut self, filename: &str, default_max_limit: u32, struct_entries: u32)->Result<(),String>{
		let (data, len) = match samase::read_file(filename) {
            Some(s) => s,
            None => { return Err(format!("Unable to read file {}",filename)); },
        };
		let slice = std::slice::from_raw_parts(data, len);
		let mut entry_count = 0;
		let mut current_entry = default_max_limit;
		for line in slice.split(|&x| x == b'\n') 
		{
            let line = String::from_utf8_lossy(line);
            let line = line.trim();
            if line.starts_with("#") || line.starts_with(";") || line.is_empty() {
                continue;
            }
			let command_list: Vec<&str> = line.split(':').collect();
			if command_list.len()==2 {
				let name = command_list[0];
				if let Some(id) = self.identifier(&name.to_lowercase()){
					let value: u32 = self.parse_entry_value(command_list[1].to_string(),id);
					self.set::<u32>(current_entry as usize,id,value);
				}
			}
			entry_count+=1;
			if entry_count==struct_entries {
				entry_count=0;
				current_entry += 1;
			}
		}
		Ok(())	
	}
	unsafe fn get_file_entry_count(&mut self, filename: &str, struct_entries: u32)->Option<u32>{
		debug!("Parse stuff [2]");
		let (data, len) = match samase::read_file(filename) {
            Some(s) => s,
            None => { return None; },
        };
		let mut count = 0;
		let slice = std::slice::from_raw_parts(data, len);
		for line in slice.split(|&x| x == b'\n') 
		{
            let line = String::from_utf8_lossy(line);
            let line = line.trim();
            if line.starts_with("#") || line.starts_with(";") || line.is_empty() {
                continue;
            }
			let command_list: Vec<&str> = line.split(':').collect();
			if command_list.len()==2 {
				let name = command_list[0];
				if let Some(id) = self.identifier(&name.to_lowercase()){
					count+=1;
				}
			}
		}
		count /= struct_entries;
		Some(count)	
	}	

	fn get_address_list(identifier: Self::Id)->Vec<usize>;
	fn get_size_list()->Vec<usize>;
	unsafe fn extend_data_conventional(&mut self, exe: &mut whack::ModulePatcher);
	fn clear(&mut self);
	unsafe fn init_raw<T: num_traits::cast::ToPrimitive>(&mut self, new_size: T, repatch_array: bool);
	unsafe fn extra_code(&mut self, exe: &mut whack::ModulePatcher);
	

	unsafe fn copy_array_from<T: num_traits::cast::FromPrimitive+Clone+Copy>(old_offset: *mut T, new_vec: &mut Vec<T>, length: usize){
		new_vec.resize(length, T::from_u32(0).unwrap());
		for i in 0..length {
			new_vec[i as usize] = *old_offset.offset(i as isize);
		}
	}
	unsafe fn copy_array_from_changesize<T: num_traits::cast::FromPrimitive+Clone+Copy>(old_offset: *mut T, new_vec: &mut Vec<T>,
			old_length: usize, new_length: usize){
//		new_vec.resize(new_length, T::from_u32(0).unwrap());//no longer required because of new default
		for i in 0..old_length {
			new_vec[i as usize] = *old_offset.offset(i as isize);
		}//for stupid arrays like sounds in units.dat
	}
	
}
//attack extender in current version is not used in w/ actual data extender, only functions/type declarations
#[derive(Default, Serialize, Deserialize)]
pub struct AttackExtender {
	pub unit_ids: Vec<u16>,
}
//used to get bytes from pointer for wrappers

#[allow(dead_code)]
fn split_bytes(val: u32)->[u8;4]{
	let result: [u8;4] = [
		(val & 0xff) as u8,
		((val & 0xff00)>>8) as u8,
		((val & 0xff0000)>>16) as u8,
		((val & 0xff000000)>>24) as u8];
	result
}



#[derive(Copy,Clone)]
pub enum SpriteValues {
	IncludeInVision,
	StartAsVisible,
	ImageId,
	SelectionCircle,
	ImagePos,
	HpBar,
}

//
#[derive(Default, Serialize, Deserialize)]
pub struct SpriteExtender {
	pub include_in_vision: Vec<u8>,//vision sync
	pub start_as_visible: Vec<u8>,
	pub image_id: Vec<u16>,
	pub selection_circle: Vec<u8>,
	pub imagepos: Vec<u8>,
	pub hp_bar: Vec<u8>,
	
	//not used or present in data files, but set in data tables
	pub unknown: Vec<u8>,
}

impl SpriteExtender {
	
	unsafe fn extend_data_from_dat_file(&mut self, exe: &mut whack::ModulePatcher){
		let (data, len) = match samase::read_file("arr\\sprites.dat"){
			Some(s) => s,
			None => { return; },
		};
		debug!("Length is {}",len);
		//selcircle, imagepos and hpbar are not set for 0-129 entries
		//equation formula for calculation of number of entries is following:
		// x*4 + (x-130)*3 = n, where n is length of file in bytes
		let count = (len + 390) / 7;
		self.include_in_vision.resize(count,0);
		self.start_as_visible.resize(count,0);
		self.image_id.resize(count,0);
		self.selection_circle.resize(count-130,0);
		self.imagepos.resize(count-130,0);
		self.hp_bar.resize(count-130,0);		
		self.unknown.resize(count,0);
		exe.replace_val(0x00513FC0, count);
		exe.replace_val(0x00513FCC, count-130);
		exe.replace_val(0x00513FD8, count);
		exe.replace_val(0x00513FE4, count);
		exe.replace_val(0x00513FF0, count-130);
		exe.replace_val(0x00513FFC, count-130);
	  exe.replace_val(0x00463A8B, &mut self.image_id[0] as *mut u16);
	  exe.replace_val(0x004683D9, &mut self.image_id[0] as *mut u16);
	  exe.replace_val(0x0048D769, &mut self.image_id[0] as *mut u16);
	  exe.replace_val(0x0048DAA3, &mut self.image_id[0] as *mut u16);
	  exe.replace_val(0x00499081, &mut self.image_id[0] as *mut u16);
	  exe.replace_val(0x004D7A5C, &mut self.image_id[0] as *mut u16);
	  exe.replace_val(0x004D7ABF, &mut self.image_id[0] as *mut u16);
	  exe.replace_val(0x004E4CC9, &mut self.image_id[0] as *mut u16);
	  exe.replace_val(0x004E4FDF, &mut self.image_id[0] as *mut u16);
	  exe.replace_val(0x004E501C, &mut self.image_id[0] as *mut u16);
	  exe.replace_val(0x004E6644, &mut self.image_id[0] as *mut u16);
	  exe.replace_val(0x00513FB8, &mut self.image_id[0] as *mut u16);
  exe.replace_val(0x0047A873, ((&mut self.hp_bar[0] as *mut u8 as *mut u32 as u32)-130) as *mut u8);
  exe.replace_val(0x0047A90F, ((&mut self.hp_bar[0] as *mut u8 as *mut u32 as u32)-130) as *mut u8);
  exe.replace_val(0x0047AA6E, ((&mut self.hp_bar[0] as *mut u8 as *mut u32 as u32)-130) as *mut u8);
  exe.replace_val(0x004D6045, ((&mut self.hp_bar[0] as *mut u8 as *mut u32 as u32)-130) as *mut u8);
  exe.replace_val(0x00513FC4, &mut self.hp_bar[0] as *mut u8);
  exe.replace_val(0x00513FD0, &mut self.unknown[0] as *mut u8);
  exe.replace_val(0x0049906D, &mut self.start_as_visible[0] as *mut u8);
  exe.replace_val(0x00513FDC, &mut self.start_as_visible[0] as *mut u8);
  exe.replace_val(0x004D6021, ((&mut self.selection_circle[0] as *mut u8 as *mut u32 as u32)-130) as *mut u8);
  exe.replace_val(0x004D681E, ((&mut self.selection_circle[0] as *mut u8 as *mut u32 as u32)-130) as *mut u8);
  exe.replace_val(0x00513FE8, &mut self.selection_circle[0] as *mut u8);
  exe.replace_val(0x004D6034, ((&mut self.imagepos[0] as *mut u8 as *mut u32 as u32)-130) as *mut u8);
  exe.replace_val(0x004D6842, ((&mut self.imagepos[0] as *mut u8 as *mut u32 as u32)-130) as *mut u8);
  exe.replace_val(0x00513FF4, &mut self.imagepos[0] as *mut u8);
  exe.replace_val(0x0042D517, &mut self.include_in_vision[0] as *mut u8);
  exe.replace_val(0x00497159, &mut self.include_in_vision[0] as *mut u8);
  exe.replace_val(0x00497180, &mut self.include_in_vision[0] as *mut u8);
  exe.replace_val(0x004971A7, &mut self.include_in_vision[0] as *mut u8);
  exe.replace_val(0x004971CE, &mut self.include_in_vision[0] as *mut u8);
  exe.replace_val(0x004971F5, &mut self.include_in_vision[0] as *mut u8);
  exe.replace_val(0x0049721C, &mut self.include_in_vision[0] as *mut u8);
  exe.replace_val(0x00497C66, &mut self.include_in_vision[0] as *mut u8);
	}
}

impl DataExtender for SpriteExtender {
	type Id = SpriteValues;
	type SizeLimit = u32;
	unsafe fn extra_code(&mut self, exe: &mut whack::ModulePatcher){}
	fn clear(&mut self){
		self.include_in_vision.clear();
		self.start_as_visible.clear();
		self.image_id.clear();
		self.selection_circle.clear();
		self.imagepos.clear();
		self.hp_bar.clear();
	}

	unsafe fn init_code(&mut self, exe: &mut whack::ModulePatcher){}
	unsafe fn init_raw<T: num_traits::cast::ToPrimitive>(&mut self, new_size: T, repatch_array: bool){}
	fn get(&mut self, index: usize, identifier: Self::Id)->u32{
		let result = match identifier {
			SpriteValues::IncludeInVision => self.include_in_vision[index] as u32,
			SpriteValues::StartAsVisible => self.start_as_visible[index] as u32,
			SpriteValues::ImageId => self.image_id[index] as u32,
			SpriteValues::SelectionCircle => self.selection_circle[index] as u32,
			SpriteValues::ImagePos => self.imagepos[index] as u32,
			SpriteValues::HpBar => self.hp_bar[index] as u32,
		};
		result	
	}	
	fn identifier(&mut self, line: &str)->Option<Self::Id>{
		let result = match line.trim().as_ref(){
			"include_in_vision"=>SpriteValues::IncludeInVision,
			"start_as_visible"=>SpriteValues::StartAsVisible,
			"image_id"=>SpriteValues::ImageId,
			"selection_circle"=>SpriteValues::SelectionCircle,
			"image_pos"=>SpriteValues::ImagePos,
			"hp_bar"=>SpriteValues::HpBar,
			_=>{
				return None;
			},
		};
		Some(result)
	}
	fn push_val<T: num_traits::cast::ToPrimitive>(&mut self, identifier: Self::Id, value: T){
		match identifier {
			SpriteValues::IncludeInVision=>self.include_in_vision.push(T::to_u8(&value).unwrap()),
			SpriteValues::StartAsVisible=>self.start_as_visible.push(T::to_u8(&value).unwrap()),
			SpriteValues::ImageId=>self.image_id.push(T::to_u16(&value).unwrap()),
			SpriteValues::SelectionCircle=>self.selection_circle.push(T::to_u8(&value).unwrap()),
			SpriteValues::ImagePos=>self.imagepos.push(T::to_u8(&value).unwrap()),
			SpriteValues::HpBar=>self.hp_bar.push(T::to_u8(&value).unwrap()),
		}
	}
	fn set<T: num_traits::cast::ToPrimitive>(&mut self, index: usize, identifier: Self::Id, value: T){
		match identifier {
			SpriteValues::IncludeInVision=>self.include_in_vision[index] = T::to_u8(&value).unwrap(),
			SpriteValues::StartAsVisible=>self.start_as_visible[index] = T::to_u8(&value).unwrap(),
			SpriteValues::ImageId=>self.image_id[index] = T::to_u16(&value).unwrap(),
			SpriteValues::SelectionCircle=>self.selection_circle[index] = T::to_u8(&value).unwrap(),
			SpriteValues::ImagePos=>self.imagepos[index] = T::to_u8(&value).unwrap(),
			SpriteValues::HpBar=>self.hp_bar[index] = T::to_u8(&value).unwrap(),	
		}
	}
	unsafe fn parse_entry_value(&mut self, text: String, identifier: Self::Id)->u32{//placeholder for the future
		text.trim().parse().unwrap()
	}
	fn get_address_list(identifier: Self::Id)->Vec<usize>{
		let list = match identifier {

			
			SpriteValues::IncludeInVision=>vec![0x0042D515+2,0x00497157+2,0x0049717E+2,
												0x004971A5+2,0x004971CC+2,
												0x004971F3+2,0x0049721A+2,0x00497C64+2],//+
			SpriteValues::StartAsVisible=>vec![0x0049906B+2],//+	
			
			SpriteValues::ImageId=>vec![0x00463A87+4,0x004683D5+4,0x0048D765+4,0x0048DA9F+4,
			0x0049907D+4,0x004D7A58+4,
									0x004D7ABB+4,0x004E4CC5+4,0x004E4FDB+4,0x004E5018+4,
									0x004E6640+4],
			SpriteValues::SelectionCircle=>vec![0x004D601E+3,0x004D681A+4],
			SpriteValues::ImagePos=>vec![0x004D6032+2,0x004D6840+2],
			SpriteValues::HpBar=>vec![0x0047A871+2,0x0047A90D+2,0x0047AA6B+3,0x004D6043+2],
			_=>vec![],

		};
		list			
	}
	unsafe fn extend_data_conventional(&mut self, exe: &mut whack::ModulePatcher){
		self.clear();
		//sync
		debug!("Init sprite stuff");
		let sprite_list_start = 130;
		Self::copy_array_from::<u8>(&mut bw::sprites_dat_vision[0] as *mut u8, &mut self.include_in_vision, 0x205);
		Self::copy_array_from::<u8>(&mut bw::sprites_dat_as_visible[0] as *mut u8, &mut self.start_as_visible, 0x205);
		Self::copy_array_from::<u16>(&mut bw::sprites_dat_image_id[0] as *mut u16, &mut self.image_id, 0x205);
		Self::copy_array_from::<u8>(&mut bw::sprites_dat_selection_circle_off[0] as *mut u8, 
									&mut self.selection_circle, 0x205 - sprite_list_start);
		Self::copy_array_from::<u8>(&mut bw::sprites_dat_selection_imagepos_off[0] as *mut u8, 
									&mut self.imagepos, 0x205 - sprite_list_start);
		Self::copy_array_from::<u8>(&mut bw::sprites_dat_healthbar_off[0] as *mut u8, 
									&mut self.hp_bar, 0x205 - sprite_list_start);
		debug!("Copy arrays");
		let _ = match self.parse("samase\\sprites_ext.txt"){
			Ok(())=>{
				//everything's okay
			},
			Err(e)=>{
				windows::message_box("Aise panic", &e);
				TerminateProcess(GetCurrentProcess(), 0x4230daef);
			},
		};
		debug!("S: {} {} {} {} {} {}",self.include_in_vision.len(),self.start_as_visible.len(),self.image_id.len(),
			self.selection_circle.len(), self.imagepos.len(), self.hp_bar.len());
		let length = self.image_id.len();
		Self::uplink::<u8>(exe, &mut self.include_in_vision[0] as *mut u8, Self::get_address_list(SpriteValues::IncludeInVision));
 		Self::uplink::<u8>(exe, &mut self.start_as_visible[0] as *mut u8, Self::get_address_list(SpriteValues::StartAsVisible));
		Self::uplink::<u16>(exe, &mut self.image_id[0] as *mut u16, Self::get_address_list(SpriteValues::ImageId));
		
		Self::uplink::<u8>(exe, 
		((&mut self.selection_circle[0] as *mut u8 as *mut u32 as u32)-130) as *mut u8, 
				Self::get_address_list(SpriteValues::SelectionCircle));
		Self::uplink::<u8>(exe, 
		((&mut self.imagepos[0] as *mut u8 as *mut u32 as u32)-130) as *mut u8, 
				Self::get_address_list(SpriteValues::ImagePos));//selection circle offset
		Self::uplink::<u8>(exe, 
		((&mut self.hp_bar[0] as *mut u8 as *mut u32 as u32)-130) as *mut u8, 
				Self::get_address_list(SpriteValues::HpBar));
		Self::uplink_length_dattable(exe, length.clone() as u32, Self::get_size_list());
		self.extra_code(exe);
		debug!("End sprite stuff");
	}	
	fn get_size_list()->Vec<usize>{
		return vec![
		
		
		];
	}
}

//
//
//
//
#[derive(Default, Serialize, Deserialize)]
pub struct SFXExtender {
	pub length: u32,
	
	pub volume: Vec<u8>,
	pub flags: Vec<u8>,
	pub race: Vec<u16>,
	pub stype: Vec<u8>,
	pub soundfile: Vec<u32>,
	//ptr-related
	//suffering starts
	pub ptr: Vec<u32>,//x16
//	pub sound_data: Vec<SoundData>,//x16
}		
/*006D1270 = SoundData sound_data[?]x16
SoundData: (0x10)
0x0     dword wav_data_size
0x4     dword 0 - tick? (x4 + GetTickCount ~~ frame?) (0 - (GetTickCount() - 0x51))
0x8     dword duration_msecs (006D1278)
0xc     IDirectSoundBuffer *buffer
*/


#[derive(Copy,Clone)]
pub enum SFXValues {
	Volume,
	Flags,
	Race,
	Type,
	Soundfile,
	SoundDataPtr,
}
/*
	pub volume: Vec<u8>,
	pub flags: Vec<u8>,
	pub race: Vec<u16>,
	pub stype: Vec<u8>,
	pub soundfile: Vec<u32>,
	//ptr-related
	//suffering starts
	pub ptr: Vec<u32>,//x16
//	pub sound_data: Vec<SoundData>,


*/

impl DataExtender for SFXExtender {
	type Id = SFXValues;
	type SizeLimit = u32;
	unsafe fn extra_code(&mut self, exe: &mut whack::ModulePatcher){
		//
	
		//
	
		//
	}
	fn clear(&mut self){
		self.volume.clear();
		self.flags.clear();
		self.race.clear();
		self.stype.clear();
		self.soundfile.clear();
	}
	unsafe fn init_code(&mut self, exe: &mut whack::ModulePatcher){
		if let Some(count) = self.get_file_entry_count("samase\\sfx_ext.txt",5){//14 is number of data default entries
			self.length = 1144+count;
			let sum = self.length as usize;
			self.volume.resize(sum,0);
			self.flags.resize(sum,0);
			self.race.resize(sum,0);
			self.stype.resize(sum,0);
			self.soundfile.resize(sum,0);
			self.ptr.resize(sum*4,0);
			self.extra_code(exe);
			Self::uplink_length_dattable(exe, sum as u32, Self::get_size_list());
			Self::uplink::<u8>(exe, &mut self.volume[0] as *mut u8, Self::get_address_list(SFXValues::Volume));
			Self::uplink::<u8>(exe, &mut self.flags[0] as *mut u8, Self::get_address_list(SFXValues::Flags));
			Self::uplink::<u16>(exe, &mut self.race[0] as *mut u16, Self::get_address_list(SFXValues::Race));
			Self::uplink::<u8>(exe, &mut self.stype[0] as *mut u8, Self::get_address_list(SFXValues::Type));
			Self::uplink::<u32>(exe, &mut self.soundfile[0] as *mut u32, Self::get_address_list(SFXValues::Soundfile));
			//
		}
	}
	unsafe fn extend_data_conventional(&mut self, exe: &mut whack::ModulePatcher){}
	unsafe fn init_raw<T: num_traits::cast::ToPrimitive>(&mut self, new_size: T, repatch_array: bool){}
	fn get(&mut self, index: usize, identifier: Self::Id)->u32{
		let result = match identifier {
			SFXValues::Volume => self.volume[index] as u32,
			SFXValues::Flags => self.flags[index] as u32,
			SFXValues::Race => self.race[index] as u32,
			SFXValues::Type => self.stype[index] as u32,
			SFXValues::Soundfile => self.soundfile[index] as u32,
			SFXValues::SoundDataPtr => self.ptr[index] as u32,
		};
		result	
	}	
	fn identifier(&mut self, line: &str)->Option<Self::Id>{
		let result = match line.trim().as_ref(){
			"volume"=>SFXValues::Volume,
			"flags"=>SFXValues::Flags,
			"race"=>SFXValues::Race,
			"type"=>SFXValues::Type,
			"soundfile"=>SFXValues::Soundfile,
			_=>{
				return None;
			},
		};
		Some(result)
	}
	fn push_val<T: num_traits::cast::ToPrimitive>(&mut self, identifier: Self::Id, value: T){
		match identifier {
			SFXValues::Volume=>self.volume.push(T::to_u8(&value).unwrap()),
			SFXValues::Flags=>self.flags.push(T::to_u8(&value).unwrap()),
			SFXValues::Race=>self.race.push(T::to_u16(&value).unwrap()),
			SFXValues::Type=>self.stype.push(T::to_u8(&value).unwrap()),
			SFXValues::Soundfile=>self.soundfile.push(T::to_u32(&value).unwrap()),
			_=>{},
		}
	}
	fn set<T: num_traits::cast::ToPrimitive>(&mut self, index: usize, identifier: Self::Id, value: T){
		let length=self.length as usize;
		match identifier {
			SFXValues::Volume=>self.volume[index] = T::to_u8(&value).unwrap(),
			SFXValues::Flags=>self.flags[index] = T::to_u8(&value).unwrap(),
			SFXValues::Race=>self.race[index] = T::to_u16(&value).unwrap(),
			SFXValues::Type=>self.stype[index] = T::to_u8(&value).unwrap(),
			SFXValues::Soundfile=>self.soundfile[index] = T::to_u32(&value).unwrap(),
			_=>{},
		}
	}
	unsafe fn parse_entry_value(&mut self, text: String, identifier: Self::Id)->u32{//placeholder for the future
		text.trim().parse().unwrap()
	}
	fn get_address_list(identifier: Self::Id)->Vec<usize>{
		let list = match identifier {
			SFXValues::Volume=>vec![],
			SFXValues::Flags=>vec![],
			SFXValues::Race=>vec![],
			SFXValues::Type=>vec![],
			SFXValues::Soundfile=>vec![],			
			_=>vec![],

		};
		list	
	}
	fn get_size_list()->Vec<usize>{
		return vec![];
	}
}
//
//
//

#[derive(Copy,Clone)]
pub enum ImageValues {
	GrpId,
	IscriptId,
	GraphicTurns,
	DrawIfCloaked,
	Clickable,
	UseFullIscript,
	DrawFunc,
	DrawRemap,
	OverlayAttack,
	OverlayDamage,
	OverlaySpecial,
	OverlayLanding,
	OverlayLiftoff,
	OverlayShield,
	
	BuildingOverlayStateMax,
	OverlayShieldPtr,
	OverlayAttackPtr,
	OverlayDamagePtr,
	OverlaySpecialPtr,
	OverlayLandingPtr,
	OverlayLiftoffPtr,
	ImageGrpPtr,
}

#[derive(Default)]
pub struct ImageExtender {
	pub length: u32,
	
	pub grp_id: Vec<u32>,
	pub iscript_id: Vec<u32>,
	pub graphic_turns: Vec<u8>,
	pub draw_if_cloaked: Vec<u8>,
	pub clickable: Vec<u8>,
	pub use_full_iscript: Vec<u8>,
	pub draw_func: Vec<u8>,
	pub draw_remap: Vec<u8>,
	
	pub overlay_shield: Vec<u32>,
	
	pub overlay_attack: Vec<u32>,
	pub overlay_damage: Vec<u32>,
	pub overlay_special: Vec<u32>,
	pub overlay_landing: Vec<u32>,
	pub overlay_liftoff: Vec<u32>,
	//extra
	pub building_overlay_state_max: Vec<u8>,
	//ptrs
/*	pub overlay_attack_ptr: Vec<u32>,
	pub overlay_damage_ptr: Vec<u32>,
	pub overlay_special_ptr: Vec<u32>,
	pub overlay_landing_ptr: Vec<u32>,
	pub overlay_liftoff_ptr: Vec<u32>,*/
	pub overlays: Vec<u32>,//why we have to suffer
//	pub overlay_shield_ptr: Vec<u32>,
	pub image_grp_ptr: Vec<u32>,
}

impl ImageExtender {
	pub fn new()->ImageExtender {
		ImageExtender {
			length: 0,
			grp_id: Vec::new(),
			graphic_turns: Vec::new(),
			draw_if_cloaked: Vec::new(),
			clickable: Vec::new(),
			use_full_iscript: Vec::new(),
			draw_func: Vec::new(),
			draw_remap: Vec::new(),
			iscript_id: Vec::new(),
			overlay_attack: Vec::new(),
			overlay_damage: Vec::new(),
			overlay_special: Vec::new(),
			overlay_landing: Vec::new(),
			overlay_liftoff: Vec::new(),
			overlay_shield: Vec::new(),
			building_overlay_state_max: Vec::new(),
			overlays: Vec::new(),
			/*overlay_shield_ptr: Vec::new(),
			overlay_attack_ptr: Vec::new(),
			overlay_damage_ptr: Vec::new(),
			overlay_special_ptr: Vec::new(),
			overlay_landing_ptr: Vec::new(),
			overlay_liftoff_ptr: Vec::new(),*/
			image_grp_ptr: Vec::new(),
		}
	}
}



	

impl DataExtender for ImageExtender {
	type Id = ImageValues;
	type SizeLimit = u32;
	fn clear(&mut self){
		self.grp_id.clear();
		self.iscript_id.clear();
		self.graphic_turns.clear();
		self.draw_if_cloaked.clear();
		self.clickable.clear();
		self.use_full_iscript.clear();
		self.draw_func.clear();
		self.draw_remap.clear();
		self.overlay_attack.clear();
		self.overlay_damage.clear();
		self.overlay_special.clear();
		self.overlay_landing.clear();
		self.overlay_liftoff.clear();
		self.overlay_shield.clear();
//		
		self.overlays.clear();
/*		self.overlay_attack_ptr.clear();
		self.overlay_damage_ptr.clear();
		self.overlay_special_ptr.clear();
		self.overlay_landing_ptr.clear();
		self.overlay_liftoff_ptr.clear();
		self.overlay_shield_ptr.clear();*/
		self.image_grp_ptr.clear();
		self.building_overlay_state_max.clear();
	}
	unsafe fn init_raw<T: num_traits::cast::ToPrimitive>(&mut self, new_size: T, repatch_array: bool){}
	unsafe fn init_code(&mut self, exe: &mut whack::ModulePatcher){
		if let Some(count) = self.get_file_entry_count("samase\\images_ext.txt",14){//14 is number of data default entries
			self.length = 999+count;
			let sum = self.length as usize;
			debug!("Sum is: {}",sum);
			self.grp_id.resize(sum,0);
			self.iscript_id.resize(sum,0);
			self.graphic_turns.resize(sum,0);
			self.draw_if_cloaked.resize(sum,0);
			self.clickable.resize(sum,0);
			self.use_full_iscript.resize(sum,0);
			self.draw_func.resize(sum,0);
			self.draw_remap.resize(sum,0);
			self.overlay_attack.resize(sum,0);
			self.overlay_damage.resize(sum,0);
			self.overlay_special.resize(sum,0);
			self.overlay_landing.resize(sum,0);
			self.overlay_liftoff.resize(sum,0);
			self.overlay_shield.resize(sum,0);
			
			
			self.overlays.resize(sum*6,0);
			
			/*
			self.overlay_attack_ptr.resize(sum,0);
			self.overlay_damage_ptr.resize(sum,0);
			self.overlay_special_ptr.resize(sum,0);
			self.overlay_landing_ptr.resize(sum,0);
			self.overlay_liftoff_ptr.resize(sum,0);
			
			self.overlay_shield_ptr.resize(sum,0);
			*/
			
			self.image_grp_ptr.resize(sum,0);
			self.building_overlay_state_max.resize(sum,0);
			
			
			/*
			//extra uplinks/memory patches
			debug!("Resize done");*/
			self.extra_code(exe);
			
			debug!("Extra code done, uplink image length table, length is {}",self.length);
			Self::uplink_length_dattable(exe, sum as u32, Self::get_size_list());

			debug!("Dattable done");
			
			
			Self::uplink::<u32>(exe, &mut self.grp_id[0] as *mut u32, Self::get_address_list(ImageValues::GrpId));		
			Self::uplink::<u8>(exe, &mut self.graphic_turns[0] as *mut u8, Self::get_address_list(ImageValues::GraphicTurns));			
			Self::uplink::<u8>(exe, &mut self.draw_if_cloaked[0] as *mut u8, Self::get_address_list(ImageValues::DrawIfCloaked));
			Self::uplink::<u8>(exe, &mut self.clickable[0] as *mut u8, Self::get_address_list(ImageValues::Clickable));
			Self::uplink::<u8>(exe, &mut self.use_full_iscript[0] as *mut u8, Self::get_address_list(ImageValues::UseFullIscript));
			Self::uplink::<u8>(exe, &mut self.draw_func[0] as *mut u8, Self::get_address_list(ImageValues::DrawFunc));
			Self::uplink::<u8>(exe, &mut self.draw_remap[0] as *mut u8, Self::get_address_list(ImageValues::DrawRemap));	
			Self::uplink::<u32>(exe, &mut self.iscript_id[0] as *mut u32, Self::get_address_list(ImageValues::IscriptId));
			Self::uplink::<u32>(exe, &mut self.overlay_attack[0] as *mut u32, Self::get_address_list(ImageValues::OverlayAttack));
			Self::uplink::<u32>(exe, &mut self.overlay_damage[0] as *mut u32, Self::get_address_list(ImageValues::OverlayDamage));
			Self::uplink::<u32>(exe, &mut self.overlay_special[0] as *mut u32, Self::get_address_list(ImageValues::OverlaySpecial));
			Self::uplink::<u32>(exe, &mut self.overlay_landing[0] as *mut u32, Self::get_address_list(ImageValues::OverlayLanding));
			Self::uplink::<u32>(exe, &mut self.overlay_liftoff[0] as *mut u32, Self::get_address_list(ImageValues::OverlayLiftoff));
			Self::uplink::<u32>(exe, &mut self.overlay_shield[0] as *mut u32, Self::get_address_list(ImageValues::OverlayShield));
			/*
			Self::uplink::<u32>(exe, &mut self.overlay_attack_ptr[0] as *mut u32, Self::get_address_list(ImageValues::OverlayAttackPtr));
			Self::uplink::<u32>(exe, &mut self.overlay_damage_ptr[0] as *mut u32, Self::get_address_list(ImageValues::OverlayDamagePtr));
			Self::uplink::<u32>(exe, &mut self.overlay_special_ptr[0] as *mut u32, Self::get_address_list(ImageValues::OverlaySpecialPtr));
			Self::uplink::<u32>(exe, &mut self.overlay_landing_ptr[0] as *mut u32, Self::get_address_list(ImageValues::OverlayLandingPtr));
			Self::uplink::<u32>(exe, &mut self.overlay_liftoff_ptr[0] as *mut u32, Self::get_address_list(ImageValues::OverlayLiftoffPtr));
			Self::uplink::<u32>(exe, &mut self.overlay_shield_ptr[0] as *mut u32, Self::get_address_list(ImageValues::OverlayShieldPtr));

			*/
			Self::uplink::<u32>(exe, &mut self.overlays[0] as *mut u32, Self::get_address_list(ImageValues::OverlayAttackPtr));
			Self::uplink::<u32>(exe, &mut self.overlays[sum] as *mut u32, Self::get_address_list(ImageValues::OverlayDamagePtr));
			Self::uplink::<u32>(exe, &mut self.overlays[sum*2] as *mut u32, Self::get_address_list(ImageValues::OverlaySpecialPtr));
			Self::uplink::<u32>(exe, &mut self.overlays[sum*3] as *mut u32, Self::get_address_list(ImageValues::OverlayLandingPtr));
			Self::uplink::<u32>(exe, &mut self.overlays[sum*4] as *mut u32, Self::get_address_list(ImageValues::OverlayLiftoffPtr));
			Self::uplink::<u32>(exe, &mut self.overlays[sum*5] as *mut u32, Self::get_address_list(ImageValues::OverlayShieldPtr));
			//
			Self::uplink::<u32>(exe, &mut self.image_grp_ptr[0] as *mut u32, Self::get_address_list(ImageValues::ImageGrpPtr));			
			Self::uplink::<u8>(exe, &mut self.building_overlay_state_max[0] as *mut u8, Self::get_address_list(ImageValues::BuildingOverlayStateMax));			
		
			debug!("All uplinks done");
			
		}
	}
	fn get(&mut self, index: usize, identifier: Self::Id)->u32{
		let length = self.length as usize;
		let result = match identifier {
			ImageValues::GrpId => self.grp_id[index] as u32,
			ImageValues::IscriptId => self.iscript_id[index] as u32,
			ImageValues::GraphicTurns => self.graphic_turns[index] as u32,
			ImageValues::DrawIfCloaked => self.draw_if_cloaked[index] as u32,
			ImageValues::Clickable => self.clickable[index] as u32,
			ImageValues::UseFullIscript => self.use_full_iscript[index] as u32,
			ImageValues::DrawFunc => self.draw_func[index] as u32,
			ImageValues::DrawRemap => self.draw_remap[index] as u32,
			ImageValues::OverlayAttack => self.overlay_attack[index] as u32,
			ImageValues::OverlayDamage => self.overlay_damage[index] as u32,
			ImageValues::OverlaySpecial => self.overlay_special[index] as u32,
			ImageValues::OverlayLanding => self.overlay_landing[index] as u32,
			ImageValues::OverlayLiftoff => self.overlay_liftoff[index] as u32,
			ImageValues::OverlayShield => self.overlay_shield[index] as u32,
/*			ImageValues::OverlayAttackPtr => self.overlay_attack_ptr[index] as u32,
			ImageValues::OverlayDamagePtr => self.overlay_damage_ptr[index] as u32,
			ImageValues::OverlaySpecialPtr => self.overlay_special_ptr[index] as u32,
			ImageValues::OverlayLandingPtr => self.overlay_landing_ptr[index] as u32,
			ImageValues::OverlayLiftoffPtr => self.overlay_liftoff_ptr[index] as u32,*/
			ImageValues::OverlayAttackPtr => self.overlays[index] as u32,
			ImageValues::OverlayDamagePtr => self.overlays[index+length] as u32,
			ImageValues::OverlaySpecialPtr => self.overlays[index+length*2] as u32,
			ImageValues::OverlayLandingPtr => self.overlays[index+length*3] as u32,
			ImageValues::OverlayLiftoffPtr => self.overlays[index+length*4] as u32,
			ImageValues::OverlayShieldPtr => self.overlays[index+length*5] as u32,
//			ImageValues::OverlayShieldPtr => self.overlay_shield_ptr[index] as u32,	
			ImageValues::ImageGrpPtr => self.image_grp_ptr[index] as u32,	
			ImageValues::BuildingOverlayStateMax => self.building_overlay_state_max[index] as u32,	
			
		};
		result	
	}	
	fn identifier(&mut self, line: &str)->Option<Self::Id>{
		let result = match line.trim().as_ref(){
			"grp_id"=>ImageValues::GrpId,
			"iscript_id"=>ImageValues::IscriptId,
			"graphic_turns"=>ImageValues::GraphicTurns,
			"draw_if_cloaked"=>ImageValues::DrawIfCloaked,
			"clickable"=>ImageValues::Clickable,
			"use_full_iscript"=>ImageValues::UseFullIscript,
			"draw_func"=>ImageValues::DrawFunc,
			"draw_remap"=>ImageValues::DrawRemap,
			"overlay_attack"=>ImageValues::OverlayAttack,
			"overlay_damage"=>ImageValues::OverlayDamage,
			"overlay_special"=>ImageValues::OverlaySpecial,
			"overlay_landing"=>ImageValues::OverlayLanding,
			"overlay_liftoff"=>ImageValues::OverlayLiftoff,
			"overlay_shield"=>ImageValues::OverlayShield,
			_=>{
				return None;
			},
		};
		Some(result)
	}
	fn push_val<T: num_traits::cast::ToPrimitive>(&mut self, identifier: Self::Id, value: T){
		match identifier {
			ImageValues::GrpId=>self.grp_id.push(T::to_u32(&value).unwrap()),
			ImageValues::GraphicTurns=>self.graphic_turns.push(T::to_u8(&value).unwrap()),
			ImageValues::DrawIfCloaked=>self.draw_if_cloaked.push(T::to_u8(&value).unwrap()),
			ImageValues::Clickable=>self.clickable.push(T::to_u8(&value).unwrap()),
			ImageValues::UseFullIscript=>self.use_full_iscript.push(T::to_u8(&value).unwrap()),
			ImageValues::DrawFunc=>self.draw_func.push(T::to_u8(&value).unwrap()),
			ImageValues::DrawRemap=>self.draw_remap.push(T::to_u8(&value).unwrap()),
			ImageValues::IscriptId=>self.iscript_id.push(T::to_u32(&value).unwrap()),
			ImageValues::OverlayAttack=>self.overlay_attack.push(T::to_u32(&value).unwrap()),
			ImageValues::OverlayDamage=>self.overlay_damage.push(T::to_u32(&value).unwrap()),
			ImageValues::OverlaySpecial=>self.overlay_special.push(T::to_u32(&value).unwrap()),
			ImageValues::OverlayLanding=>self.overlay_landing.push(T::to_u32(&value).unwrap()),
			ImageValues::OverlayLiftoff=>self.overlay_liftoff.push(T::to_u32(&value).unwrap()),
			ImageValues::OverlayShield=>self.overlay_shield.push(T::to_u32(&value).unwrap()),

/*			ImageValues::OverlayAttackPtr=>self.overlay_attack_ptr.push(T::to_u32(&value).unwrap()),	
			ImageValues::OverlayDamagePtr=>self.overlay_damage_ptr.push(T::to_u32(&value).unwrap()),	
			ImageValues::OverlaySpecialPtr=>self.overlay_special_ptr.push(T::to_u32(&value).unwrap()),	
			ImageValues::OverlayLandingPtr=>self.overlay_landing_ptr.push(T::to_u32(&value).unwrap()),	
			ImageValues::OverlayLiftoffPtr=>self.overlay_liftoff_ptr.push(T::to_u32(&value).unwrap()),	
			ImageValues::OverlayShieldPtr=>self.overlay_shield_ptr.push(T::to_u32(&value).unwrap()),		*/
			ImageValues::ImageGrpPtr=>self.image_grp_ptr.push(T::to_u32(&value).unwrap()),
			ImageValues::BuildingOverlayStateMax=>self.building_overlay_state_max.push(T::to_u8(&value).unwrap()),
			_=>{
				debug!("Unable to push value");
			},
		}
	}
	fn set<T: num_traits::cast::ToPrimitive>(&mut self, index: usize, identifier: Self::Id, value: T){
		let length=self.length as usize;
		match identifier {
			ImageValues::GrpId=>self.grp_id[index] = T::to_u32(&value).unwrap(),
			ImageValues::GraphicTurns=>self.graphic_turns[index] = T::to_u8(&value).unwrap(),
			ImageValues::DrawIfCloaked=>self.draw_if_cloaked[index] = T::to_u8(&value).unwrap(),
			ImageValues::Clickable=>self.clickable[index] = T::to_u8(&value).unwrap(),
			ImageValues::UseFullIscript=>self.use_full_iscript[index] = T::to_u8(&value).unwrap(),	
			ImageValues::DrawFunc=>self.draw_func[index] = T::to_u8(&value).unwrap(),
			ImageValues::DrawRemap=>self.draw_remap[index] = T::to_u8(&value).unwrap(),	
			ImageValues::IscriptId=>self.iscript_id[index] = T::to_u32(&value).unwrap(),
			ImageValues::OverlayAttack=>self.overlay_attack[index] = T::to_u32(&value).unwrap(),	
			ImageValues::OverlayDamage=>self.overlay_damage[index] = T::to_u32(&value).unwrap(),	
			ImageValues::OverlaySpecial=>self.overlay_special[index] = T::to_u32(&value).unwrap(),	
			ImageValues::OverlayLanding=>self.overlay_landing[index] = T::to_u32(&value).unwrap(),	
			ImageValues::OverlayLiftoff=>self.overlay_liftoff[index] = T::to_u32(&value).unwrap(),	
			ImageValues::OverlayShield=>self.overlay_shield[index] = T::to_u32(&value).unwrap(),	
			
/*			ImageValues::OverlayAttackPtr=>self.overlay_attack_ptr[index] = T::to_u32(&value).unwrap(),	
			ImageValues::OverlayDamagePtr=>self.overlay_damage_ptr[index] = T::to_u32(&value).unwrap(),	
			ImageValues::OverlaySpecialPtr=>self.overlay_special_ptr[index] = T::to_u32(&value).unwrap(),	
			ImageValues::OverlayLandingPtr=>self.overlay_landing_ptr[index] = T::to_u32(&value).unwrap(),	
			ImageValues::OverlayLiftoffPtr=>self.overlay_liftoff_ptr[index] = T::to_u32(&value).unwrap(),	
			
			ImageValues::OverlayShieldPtr=>self.overlay_shield_ptr[index] = T::to_u32(&value).unwrap(),		*/
			ImageValues::OverlayAttackPtr=>self.overlays[index] = T::to_u32(&value).unwrap(),	
			ImageValues::OverlayDamagePtr=>self.overlays[index+length] = T::to_u32(&value).unwrap(),	
			ImageValues::OverlaySpecialPtr=>self.overlays[index+length*2] = T::to_u32(&value).unwrap(),	
			ImageValues::OverlayLandingPtr=>self.overlays[index+length*3] = T::to_u32(&value).unwrap(),	
			ImageValues::OverlayLiftoffPtr=>self.overlays[index+length*4] = T::to_u32(&value).unwrap(),			
			ImageValues::OverlayShieldPtr=>self.overlays[index+length*5] = T::to_u32(&value).unwrap(),		
			
			ImageValues::ImageGrpPtr=>self.image_grp_ptr[index] = T::to_u32(&value).unwrap(),
			ImageValues::BuildingOverlayStateMax=>self.building_overlay_state_max[index] = T::to_u8(&value).unwrap(),
			
		}
	}
	unsafe fn parse_entry_value(&mut self, text: String, identifier: Self::Id)->u32{//placeholder for the future
		text.trim().parse().unwrap()
	}
	fn get_address_list(identifier: Self::Id)->Vec<usize>{
		let list = match identifier {
			//0x005***** is data table info, only for image extender-type extenders initialized at game init
			ImageValues::GrpId=>vec![0x004D7262+1,/**/0x00514010],
			ImageValues::IscriptId=>vec![0x00498FFF+3, 0x004D6698+3, 0x004D671C+3, 0x004D67A8+3, 0x004D689C+3, 0x004D691E+3, 
										0x004D853F+3,/**/0x00514064],
			ImageValues::GraphicTurns=>vec![0x004D5A67+2,/**/0x0051401C],
			ImageValues::DrawIfCloaked=>vec![0x004976E4+2, 0x00498184+2, 0x004D6EC7+2, 0x004D7A60+2, 0x004D7AC3+2,
											/**/0x00514040],
			ImageValues::Clickable=>vec![0x004D5A7C+2,/**/0x00514028],
            ImageValues::UseFullIscript=>vec![0x00498FDB+2, 0x004D6701+2,/**/0x00514034],
            ImageValues::DrawFunc=>vec![0x00498FCA+2, 0x004D5AD0+2, 0x004D5AE0+2, 0x004D66B8+2,/**/0x0051404C],
			//also known as RLE Func
			
            ImageValues::DrawRemap=>vec![0x004D5AE9+3,/**/0x00514058],    
			ImageValues::OverlayAttack=>vec![0x004D728A+1,/**/0x0051407C],
			ImageValues::OverlayDamage=>vec![0x00467384+3,0x004798EF+3,0x004993D1+3,0x00499591+3,0x004D72B7+1,
											/**/0x00514088],
			ImageValues::OverlaySpecial=>vec![0x004D72DA+1,/**/0x00514094],
			ImageValues::OverlayLanding=>vec![0x004997EA+3, 0x004D72FD+1,/**/0x005140A0],
			ImageValues::OverlayShield=>vec![0x004D7343+1,/**/0x00514070],
			ImageValues::OverlayLiftoff=>vec![0x0049982A+3, 0x004D7320+1,/**/0x005140AC],
			ImageValues::OverlayAttackPtr=>vec![0x00401E0F+3, 0x00477FFD+3, 0x00478071+3, 0x0049968F+3, 0x004D50F5+1,
												0x004D5788+3, 0x004D728F+1],
			ImageValues::OverlayDamagePtr=>vec![0x0049941A+3, 0x004994C2+3, 0x00499603+3, 0x004D5C39+3, 0x004D72BC+1],	
            ImageValues::OverlaySpecialPtr=>vec![0x00498BE5+3, 0x0049F73E+3, 0x004D5A0A+3, 0x004D72DF+1],		
			ImageValues::OverlayLandingPtr=>vec![0x0049826A+3, 0x0049829A+3, 0x004D7302+1, 0x004D7325+1],
			ImageValues::OverlayLiftoffPtr=>vec![0x004D7325+1],
			ImageValues::OverlayShieldPtr=>vec![0x0049938E+3, 0x004D511D+1, 0x004D5748+3, 0x004E614F+3, 0x004D7348+1
												],											
			ImageValues::ImageGrpPtr=>vec![0x0048D76D+3,0x0048DAA7+3,0x004D50E1+1,0x004D5A57+3,0x004D6360+3,
										0x004D682F+3,0x004D701B+3,0x004D7267+1],											
			ImageValues::BuildingOverlayStateMax=>vec![0x00496ED7+2, 0x004994AA+3, 0x0049EF37+2, 0x004D5C33+2, 0x004D5C77+2,
													0x004E60B5+3,0x004E669A+2],
			_=>vec![],

		};
		list			
	}

	unsafe fn extend_data_conventional(&mut self, exe: &mut whack::ModulePatcher){
		let _ = match self.parse_set("samase\\images_ext.txt",999,14){
			Ok(())=>{
				//everything's okay
			},
			Err(e)=>{
				windows::message_box("Aise panic", &e);
				TerminateProcess(GetCurrentProcess(), 0x4230daef);
			},
		};
	}	
	unsafe fn extra_code(&mut self, exe: &mut whack::ModulePatcher){
		//extra uplinks
		let length = self.length as usize;
		exe.replace_val(0x00493367+2,(&mut self.image_grp_ptr[0] as *mut u32) as u32 + 0x920);//image 584 (psi field 1)
		exe.replace_val(0x004D5415+2,(&mut self.image_grp_ptr[0] as *mut u32) as u32 + 0x348);
		exe.replace_val(0x004D5636+3,(&mut self.image_grp_ptr[0] as *mut u32) as u32 - 0x4);
		exe.replace_val(0x004D6025+3,(&mut self.image_grp_ptr[0] as *mut u32) as u32 + 0x8c4);//image 561 (sel circle 22px)
		exe.replace_val(0x004D5110+2,(&mut self.overlays[length*4] as *mut u32) as u32 + 4*(self.grp_id.len() as u32));
//		exe.replace_val(0x004D5110+2,(&mut self.overlay_liftoff_ptr[0] as *mut u32) as u32 + 4*(self.grp_id.len() as u32));

		debug!("Replace val check: {}",self.grp_id.len());
		exe.replace_val(0x004D510A+2, self.grp_id.len()*4);
		exe.replace_val(0x004D7183+1, self.grp_id.len()*24+8);
		exe.replace_val(0x0047AE48+2, self.grp_id.len()-0x0F4);
		exe.replace_val(0x0047AF48+2, self.grp_id.len()-0x0F4);
		exe.replace_val(0x004D6356+1, self.grp_id.len()-0x0F4);	
		let offset_n4_8 = -(self.grp_id.len() as i32)*4-8;
		let offset_n24_8 = -(self.grp_id.len() as i32)*24-8;
		exe.replace_val(0x004D724E+2, offset_n4_8 as usize);
		exe.replace_val(0x004D7255+2, offset_n24_8 as usize);
		exe.replace_val(0x004D7276+2, offset_n4_8 as usize);
		exe.replace_val(0x004D727D+2, offset_n24_8 as usize);	
		exe.replace_val(0x004D72A3+2, offset_n4_8 as usize);
		exe.replace_val(0x004D72AA+2, offset_n24_8 as usize);	
		exe.replace_val(0x004D72C6+2, offset_n4_8 as usize);
		exe.replace_val(0x004D72CD+2, offset_n24_8 as usize);	
		exe.replace_val(0x004D72E9+2, offset_n4_8 as usize);
		exe.replace_val(0x004D72F0+2, offset_n24_8 as usize);	
		exe.replace_val(0x004D730C+2, offset_n4_8 as usize);
		exe.replace_val(0x004D7313+2, offset_n24_8 as usize);
		exe.replace_val(0x004D732F+2, offset_n4_8 as usize);
		exe.replace_val(0x004D7336+2, offset_n24_8 as usize);
		exe.replace_val(0x004EA60D+2, self.grp_id.len() as u16);	
	}
	fn get_size_list()->Vec<usize>{
	
	
	
		return vec![0x00401DFA+2, 0x00499676+2, 0x004D50DC+1, 0x004D5100+1, 0x004D5118+1, 0x004D5773+2, 0x004D5C23+1,
				   0x004D72D4+1, 0x004D72F7+1, 0x004D731A+1, 0x004D725C+1, 0x004D7284+1, 0x004D72B1+1, 0x004D733D+1,
				   0x004F397D+1, 
				   //only for images.dat, not required for new extender:
/*				   0x00514018, 0x00514024, 0x00514030, 0x0051403C, 0x00514048, 0x00514054, 0x00514060, 0x0051406C,
				   0x00514078, 0x00514084, 0x00514090, 0x0051409C, 0x005140A8, 0x005140B4*/];			   
	}
}
//
//
//
#[derive(Copy,Clone)]
pub enum UnitValues {
	Flingy,
	SndWhtStart,
	SndWhtEnd,
	SndYesStart,
	SndYesEnd,
	SndPssStart,
	SndPssEnd,
	SndReady,
}


//#[derive(Default)]
pub struct UnitExtender {
	pub length: u32,
	
	pub flingy_ext: Vec<u32>,
	pub snd_wht_start: Vec<u16>,
	pub snd_wht_end: Vec<u16>,
	pub snd_yes_start: Vec<u16>,
	pub snd_yes_end: Vec<u16>,
	pub snd_pss_start: Vec<u16>,
	pub snd_pss_end: Vec<u16>,
	pub snd_ready: Vec<u16>,
}

impl Default for UnitExtender {
	fn default() -> UnitExtender {
        UnitExtender {
			length: 228,
			flingy_ext: vec![0;228],
			snd_wht_start: vec![0;228],
			snd_wht_end: vec![0;228],
			snd_yes_start: vec![0;228],
			snd_yes_end: vec![0;228],
			snd_pss_start: vec![0;228],
			snd_pss_end: vec![0;228],
			snd_ready: vec![0;228],//custom default is required for pointer to first member to exist	*/			
        }
    }
}

impl UnitExtender {
	fn new()->UnitExtender {
		UnitExtender {
			length: 228,
			flingy_ext: Vec::new(),//required for ptr
			snd_wht_start: Vec::new(),
			snd_wht_end: Vec::new(),
			snd_yes_start: Vec::new(),
			snd_yes_end: Vec::new(),
			snd_pss_start: Vec::new(),
			snd_pss_end: Vec::new(),
			snd_ready: Vec::new(),

		}
	}
}
impl DataExtender for UnitExtender {
	type Id = UnitValues;
	type SizeLimit = u32;
	fn clear(&mut self){
		self.flingy_ext.clear();
		self.snd_wht_start.clear();
		self.snd_wht_end.clear();
		self.snd_yes_start.clear();
		self.snd_yes_end.clear();
		self.snd_pss_start.clear();
		self.snd_pss_end.clear();
		self.snd_ready.clear();
	}
	
	unsafe fn init_raw<T: num_traits::cast::ToPrimitive>(&mut self, new_size: T, repatch_array: bool){}
	unsafe fn init_code(&mut self, exe: &mut whack::ModulePatcher){	}
	fn get(&mut self, index: usize, identifier: Self::Id)->u32{
		let length = self.length as usize;
		let result = match identifier {
			UnitValues::Flingy => self.flingy_ext[index] as u32,	
			UnitValues::SndWhtStart => self.snd_wht_start[index] as u32,	
			UnitValues::SndWhtEnd => self.snd_wht_end[index] as u32,	
			UnitValues::SndYesStart => self.snd_yes_start[index] as u32,	
			UnitValues::SndYesEnd => self.snd_yes_end[index] as u32,	
			UnitValues::SndPssStart => self.snd_pss_start[index] as u32,	
			UnitValues::SndPssEnd => self.snd_pss_end[index] as u32,	
			UnitValues::SndReady => self.snd_ready[index] as u32,	
		};
		result
	}
	fn identifier(&mut self,line: &str)->Option<Self::Id>{
		None//not used for fake extender, as there's no units_dat.txt
	}
	fn push_val<T: num_traits::cast::ToPrimitive>(&mut self, identifier: Self::Id, value: T){
		//not used for fake extender, length is always 228
	}	
	fn set<T: num_traits::cast::ToPrimitive>(&mut self, index: usize, identifier: Self::Id, value: T){
		match identifier {
			UnitValues::Flingy => self.flingy_ext[index] = T::to_u32(&value).unwrap(),	
			UnitValues::SndWhtStart => self.snd_wht_start[index] = T::to_u16(&value).unwrap(),	
			UnitValues::SndWhtEnd => self.snd_wht_end[index] = T::to_u16(&value).unwrap(),	
			UnitValues::SndYesStart => self.snd_yes_start[index] = T::to_u16(&value).unwrap(),	
			UnitValues::SndYesEnd => self.snd_yes_end[index] = T::to_u16(&value).unwrap(),	
			UnitValues::SndPssStart => self.snd_pss_start[index] = T::to_u16(&value).unwrap(),	
			UnitValues::SndPssEnd => self.snd_pss_end[index] = T::to_u16(&value).unwrap(),	
			UnitValues::SndReady => self.snd_ready[index] = T::to_u16(&value).unwrap(),	
		};
	}
	unsafe fn parse_entry_value(&mut self, text: String, identifier: Self::Id)->u32{//placeholder for the future
		text.trim().parse().unwrap()
	}
	fn get_address_list(identifier: Self::Id)->Vec<usize>{
		match identifier {
			UnitValues::Flingy => vec![],//patched in gptp, not used
			
			UnitValues::SndReady => vec![0x0048F478+4],		
			UnitValues::SndPssStart => vec![0x0048EA26+4],	
			UnitValues::SndPssEnd => vec![0x0048EA33+4],//also 0042FF21 but not related to sounds at all (?)
			UnitValues::SndWhtStart => vec![0x0047BE12+4, 0x0048EA4F+4],
			UnitValues::SndWhtEnd => vec![0x0047BF36+4,0x0048EA58+4],
			UnitValues::SndYesStart => vec![0x0048EAB5+4],
			UnitValues::SndYesEnd => vec![0x0048EAC4+4],
			_=>vec![],
		}
	}
	unsafe fn extend_data_conventional(&mut self, exe: &mut whack::ModulePatcher){
		//self.clear();
		let length = 228;
//		debug!("Extend data: units");
//		self.flingy_ext.resize(length, 0);
/*		for i in 0..length {
			self.flingy_ext[i] = bw::units_dat_flingy_old[i] as u32;
		}*/
		bw::gptp_flingyid_copy();
		Self::copy_array_from_changesize::<u16>(&mut bw::units_dat_snd_ready[0] as *mut u16, &mut self.snd_ready, 106, 228);
		Self::copy_array_from_changesize::<u16>(&mut bw::units_dat_snd_annoyed_begin[0] as *mut u16, &mut self.snd_pss_start, 106, 228);
		Self::copy_array_from_changesize::<u16>(&mut bw::units_dat_snd_annoyed_end[0] as *mut u16, &mut self.snd_pss_end, 106, 228);
		Self::copy_array_from_changesize::<u16>(&mut bw::units_dat_snd_what_begin[0] as *mut u16, &mut self.snd_wht_start, 228, 228);
		Self::copy_array_from_changesize::<u16>(&mut bw::units_dat_snd_what_end[0] as *mut u16, &mut self.snd_wht_end, 228, 228);
		Self::copy_array_from_changesize::<u16>(&mut bw::units_dat_snd_yes_begin[0] as *mut u16, &mut self.snd_yes_start, 106, 228);
		Self::copy_array_from_changesize::<u16>(&mut bw::units_dat_snd_yes_end[0] as *mut u16, &mut self.snd_yes_end, 106, 228);
		Self::uplink::<u16>(exe, &mut self.snd_ready[0] as *mut u16, Self::get_address_list(UnitValues::SndReady));
		Self::uplink::<u16>(exe, &mut self.snd_pss_start[0] as *mut u16, Self::get_address_list(UnitValues::SndPssStart));
 		Self::uplink::<u16>(exe, &mut self.snd_pss_end[0] as *mut u16, Self::get_address_list(UnitValues::SndPssEnd));
 		Self::uplink::<u16>(exe, &mut self.snd_wht_start[0] as *mut u16, Self::get_address_list(UnitValues::SndWhtStart));
 		Self::uplink::<u16>(exe, &mut self.snd_wht_end[0] as *mut u16, Self::get_address_list(UnitValues::SndWhtEnd));
 		Self::uplink::<u16>(exe, &mut self.snd_yes_start[0] as *mut u16, Self::get_address_list(UnitValues::SndYesStart));
 		Self::uplink::<u16>(exe, &mut self.snd_yes_end[0] as *mut u16, Self::get_address_list(UnitValues::SndYesEnd));
 		/*let bytes = split_bytes(&mut self.flingy_ext[0] as *mut u32 as u32);
		let eax_eax = [0x8b, 0x04, 0x85,bytes[0],bytes[1],bytes[2],bytes[3],0xc3];
		globals::simple_wrapper(exe,&eax_eax,0x00454317,2);
		let ecx_edi = [0x8b,0x0c,0xbd,bytes[0],bytes[1],bytes[2],bytes[3],0xc3];
		globals::simple_wrapper(exe,&ecx_edi,0x00463A71,2);	
		let edx_ecx = [0x8b,0x14,0x8d,bytes[0],bytes[1],bytes[2],bytes[3],0xc3];
		//globals::simple_wrapper(exe,&edx_ecx,0x004683C6,2);
		let edx_eax = [0x8b, 0x14, 0x85,bytes[0],bytes[1],bytes[2],bytes[3],0xc3];
		//globals::simple_wrapper(exe,&edx_eax,0x0047B854,2);
		//globals::simple_wrapper(exe,&edx_eax,0x0047B8A4,2);
		//wrappers 3-5 if gptp speed hooks are enabled
		let ecx_eax = [0x8b, 0x0c, 0x85, bytes[0],bytes[1],bytes[2],bytes[3],0xc3];
		globals::simple_wrapper(exe,&ecx_eax,0x0047B8F4,2);
		globals::simple_wrapper(exe,&ecx_eax,0x0048D756,2);
		globals::simple_wrapper(exe,&ecx_eax,0x0048DA8A,2);
		globals::simple_wrapper(exe,&edx_eax,0x00497148,2);
		let eax_ebx = [0x8b, 0x04, 0x9d, bytes[0],bytes[1],bytes[2],bytes[3],0xc3];
		globals::simple_wrapper(exe,&eax_ebx,0x0049ED0B,2);
		let eax_edx = [0x8b, 0x04, 0x95, bytes[0],bytes[1],bytes[2],bytes[3],0xc3];
		globals::simple_wrapper(exe,&eax_edx,0x004E9A10,2);*/
		
		//Self::uplink::<u8>(exe, &mut self.flingy_ext[0] as *mut u16, Self::get_address_list(UnitValues::Flingy));
	}
	unsafe fn extra_code(&mut self, exe: &mut whack::ModulePatcher){}
	fn get_size_list()->Vec<usize>{
		vec![]
	}
}

//
//
//
#[derive(Copy,Clone)]
pub enum FlingyValues {
	TopSpeed,
	Acceleration,
	TurnRadius,
	MovementType,
	SpriteId,
	HaltDistance,
	IscriptMask,
}

#[derive(Default, Serialize, Deserialize)]
pub struct FlingyExtender {
	pub top_speed: Vec<u32>,
	pub acceleration: Vec<u16>,
	pub turn_radius: Vec<u8>,
	pub movement_type: Vec<u8>,
	pub sprite_id: Vec<u16>,
	pub halt_distance: Vec<u32>,
	pub iscript_mask: Vec<u8>,
}

impl FlingyExtender {
	pub fn new()->FlingyExtender {
		FlingyExtender {
			top_speed: Vec::new(),
			acceleration: Vec::new(),
			turn_radius: Vec::new(),
			movement_type: Vec::new(),
			sprite_id: Vec::new(),
			halt_distance: Vec::new(),
			iscript_mask: Vec::new(),
		}
	}
}

impl DataExtender for FlingyExtender {
	type Id = FlingyValues;
	type SizeLimit = u32;//old - u8
	unsafe fn extra_code(&mut self, exe: &mut whack::ModulePatcher){}
	unsafe fn init_raw<T: num_traits::cast::ToPrimitive>(&mut self, new_size: T, repatch_array: bool){}
	fn get(&mut self, index: usize, identifier: Self::Id)->u32{
		let result = match identifier {
			FlingyValues::TopSpeed => self.top_speed[index],
			FlingyValues::Acceleration => self.acceleration[index] as u32,
			FlingyValues::TurnRadius => self.turn_radius[index] as u32,
			FlingyValues::MovementType => self.movement_type[index] as u32,
			FlingyValues::SpriteId => self.sprite_id[index] as u32,
			FlingyValues::HaltDistance => self.halt_distance[index],
			FlingyValues::IscriptMask => self.iscript_mask[index] as u32,
		};
		result	
	}
	unsafe fn init_code(&mut self, exe: &mut whack::ModulePatcher){}
	unsafe fn parse_entry_value(&mut self, text: String, identifier: Self::Id)->u32{//placeholder for the future
		text.trim().parse().unwrap()
	}
	fn get_address_list(identifier: Self::Id)->Vec<usize>{
		let list = match identifier {
			FlingyValues::TopSpeed=>vec![0x00454334+3,0x0047B8FB+3,0x00494FA3+3,0x0049637E+3, /* 0x00515A44*/],
			FlingyValues::Acceleration=>vec![0x0047B8AB+4,0x00494FB0+4,0x00496388+4,/* 0x00515A50*/],
			FlingyValues::TurnRadius=>vec![0x0047B85B+3,0x00496397+2, /*0x00515A68*/],
			FlingyValues::MovementType=>vec![0x0045431E+2,0x004963A0+2, /*0x00515A80*/],
			FlingyValues::SpriteId=>vec![0x00463A78+4,0x004683CD+4,0x0048D75D+4,0x0048DA97+4,0x00496411+4,0x0049714F+4,0x00497176+4,
							0x0049719D+4,0x004971C4+4,0x004971EB+4,0x00497212+4,0x004E9A17+4,/*0x00515A38*/],
			FlingyValues::HaltDistance=>vec![0x00494FBA+3, /*0x00515A5C*/],
//			FlingyValues::IscriptMask=>vec![0x00515A74],//not used in bw, memory ref only
			_=>vec![],
		};
		list			
	}
	fn get_size_list()->Vec<usize>{
		//flingy data header offset +0x8 (count)
		return vec![];
//		return vec![0x00515A38+8, 0x00515A44+8, 0x00515A50+8, 0x00515A5C+8, 0x00515A68+8, 0x00515A74+8, 0x00515A80+8];//unused
	}
	fn clear(&mut self){
		self.top_speed.clear();
		self.acceleration.clear();
		self.turn_radius.clear();
		self.movement_type.clear();
		self.sprite_id.clear();
		self.halt_distance.clear();
		self.iscript_mask.clear();
	}
	unsafe fn extend_data_conventional(&mut self, exe: &mut whack::ModulePatcher){
		self.clear();
		Self::copy_array_from::<u32>(&mut bw::flingy_dat_top_speed[0] as *mut u32, &mut self.top_speed, 0xd1);
		Self::copy_array_from::<u16>(&mut bw::flingy_dat_acceleration[0] as *mut u16, &mut self.acceleration, 0xd1);
		Self::copy_array_from::<u8>(&mut bw::flingy_dat_turn_radius[0] as *mut u8, &mut self.turn_radius, 0xd1);
		Self::copy_array_from::<u8>(&mut bw::flingy_dat_movement_type[0] as *mut u8, &mut self.movement_type, 0xd1);
		Self::copy_array_from::<u16>(&mut bw::flingy_dat_sprite_id[0] as *mut u16, &mut self.sprite_id, 0xd1);
		Self::copy_array_from::<u32>(&mut bw::flingy_dat_halt_distance[0] as *mut u32, &mut self.halt_distance, 0xd1);
		Self::copy_array_from::<u8>(&mut bw::flingy_dat_iscript_mask[0] as *mut u8, &mut self.iscript_mask, 0xd1);	
		
		let _ = match self.parse("samase\\flingy_ext.txt"){
			Ok(())=>{
				//everything's okay
			},
			Err(e)=>{
				windows::message_box("Aise panic", &e);
				TerminateProcess(GetCurrentProcess(), 0x4230daef);
			},
		};
		debug!("F: {} {} {} {} {} {} {}",self.top_speed.len(),self.acceleration.len(),self.movement_type.len(),self.turn_radius.len(), 
								self.sprite_id.len(), self.halt_distance.len(),self.iscript_mask.len());
		let length = self.top_speed.len();
		Self::uplink::<u32>(exe, &mut self.top_speed[0] as *mut u32, Self::get_address_list(FlingyValues::TopSpeed));
 		Self::uplink::<u16>(exe, &mut self.acceleration[0] as *mut u16, Self::get_address_list(FlingyValues::Acceleration));
		Self::uplink::<u8>(exe, &mut self.turn_radius[0] as *mut u8, Self::get_address_list(FlingyValues::TurnRadius));
		Self::uplink::<u8>(exe, &mut self.movement_type[0] as *mut u8, Self::get_address_list(FlingyValues::MovementType));
		Self::uplink::<u16>(exe, &mut self.sprite_id[0] as *mut u16, Self::get_address_list(FlingyValues::SpriteId));
		Self::uplink::<u32>(exe, &mut self.halt_distance[0] as *mut u32, Self::get_address_list(FlingyValues::HaltDistance));
		Self::uplink::<u8>(exe, &mut self.iscript_mask[0] as *mut u8, Self::get_address_list(FlingyValues::IscriptMask));
		Self::uplink_length_dattable(exe, length.clone() as u32, Self::get_size_list());
		self.extra_code(exe);
	}	
	fn identifier(&mut self, line: &str)->Option<Self::Id>{
		let result = match line.trim().as_ref(){
			"top_speed"=>FlingyValues::TopSpeed,
			"acceleration"=>FlingyValues::Acceleration,
			"turn_radius"=>FlingyValues::TurnRadius,
			"movement_type"=>FlingyValues::MovementType,
			"sprite_id"=>FlingyValues::SpriteId,
			"halt_distance"=>FlingyValues::HaltDistance,
			"iscript_mask"=>FlingyValues::IscriptMask,
			_=>{
				return None;
			},
		};
		Some(result)
	}
	fn set<T: num_traits::cast::ToPrimitive>(&mut self, index: usize, identifier: Self::Id, value: T){
		match identifier {
			FlingyValues::TopSpeed=>self.top_speed[index] = T::to_u32(&value).unwrap(),
			FlingyValues::Acceleration=>self.acceleration[index] = T::to_u16(&value).unwrap(),
			FlingyValues::TurnRadius=>self.turn_radius[index] = T::to_u8(&value).unwrap(),
			FlingyValues::MovementType=>self.movement_type[index] = T::to_u8(&value).unwrap(),
			FlingyValues::SpriteId=>self.sprite_id[index] = T::to_u16(&value).unwrap(),
			FlingyValues::HaltDistance=>self.halt_distance[index] = T::to_u32(&value).unwrap(),
			FlingyValues::IscriptMask=>self.iscript_mask[index] = T::to_u8(&value).unwrap(),		
			_=>{},
		}
	}
	fn push_val<T: num_traits::cast::ToPrimitive>(&mut self, identifier: Self::Id, value: T){
		match identifier {
			FlingyValues::TopSpeed=>self.top_speed.push(T::to_u32(&value).unwrap()),
			FlingyValues::Acceleration=>self.acceleration.push(T::to_u16(&value).unwrap()),
			FlingyValues::TurnRadius=>self.turn_radius.push(T::to_u8(&value).unwrap()),
			FlingyValues::MovementType=>self.movement_type.push(T::to_u8(&value).unwrap()),
			FlingyValues::SpriteId=>self.sprite_id.push(T::to_u16(&value).unwrap()),
			FlingyValues::HaltDistance=>self.halt_distance.push(T::to_u32(&value).unwrap()),
			FlingyValues::IscriptMask=>self.iscript_mask.push(T::to_u8(&value).unwrap()),		
			_=>{},
		}
	}
}

#[derive(Default, Serialize, Deserialize)]
pub struct CcmuExtender {
	pub bytes: Vec<u8>,
	//59cb58 - offset 0
	//59cca8 - offset 336
	//6283E8 - offset (1700*336)+336
	pub length: u32,
}

impl DataExtender for CcmuExtender {
	type Id = u8;
	type SizeLimit = u32;
	unsafe fn extra_code(&mut self, exe: &mut whack::ModulePatcher){}
	fn clear(&mut self){}
	unsafe fn init_code(&mut self, exe: &mut whack::ModulePatcher){}
	unsafe fn init_raw<T: num_traits::cast::ToPrimitive>(&mut self, new_size: T, repatch_array: bool){}
	fn get(&mut self, index: usize, identifier: Self::Id)->u32{0}//later add unit recovery but probably not required at all	
	fn identifier(&mut self, line: &str)->Option<Self::Id>{None}
	fn push_val<T: num_traits::cast::ToPrimitive>(&mut self, identifier: Self::Id, value: T){}
	fn set<T: num_traits::cast::ToPrimitive>(&mut self, index: usize, identifier: Self::Id, value: T){}
	unsafe fn parse_entry_value(&mut self, text: String, identifier: Self::Id)->u32{0}
	fn get_address_list(identifier: Self::Id)->Vec<usize>{
		/*let list = {
			//last is start of dat array in memory to patch *data entry
			SpriteValues::IncludeInVision=>vec![0x0042D515+2,0x00497157+2,0x0049717E+2,0x004971A5+2,0x004971CC+2,
												0x004971F3+2,0x0049721A+2,0x00497C64+2],//+
			SpriteValues::StartAsVisible=>vec![0x00665C48+2],//+			
			SpriteValues::ImageId=>vec![0x00463A87+4,0x004683D5+4,0x0048D765+4,0x0048DA9F+4,0x0049907D+4,0x004D7A58+4,
									0x004D7ABB+4,0x004E4CC5+4,0x004E4FDB+4,0x004E5018+4,0x004E6640+4],
			SpriteValues::SelectionCircle=>vec![0x004D601E+3,0x004D681A+4],
			SpriteValues::ImagePos=>vec![0x004D6032+2,0x004D6840+2],
			SpriteValues::HpBar=>vec![0x0047A871+2,0x0047A90D+2,0x0047AA6B+3,0x004D6043+2],
			_=>vec![],
		
		};
		list	*/		
		vec![]
	}
	unsafe fn extend_data_conventional(&mut self, exe: &mut whack::ModulePatcher){
		//patch all entries
		//add gptp support (59cca8/59cb58 use)
		//add aise function to recover array offsets
	}	
	fn get_size_list()->Vec<usize>{
		return vec![];
	}
}