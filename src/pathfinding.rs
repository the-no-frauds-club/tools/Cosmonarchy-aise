use std::ffi::CString;
use std::ptr::null_mut;
use std::slice;
use bincode;
use bw_dat::{UnitId, UpgradeId};
use samase;
use idle_tactics;
use ai::GuardState;
use aiscript::{
    self, AiMode, AttackTimeoutState, MaxWorkers, PlayerMatch, Town, TownId, UnitMatch, WriteModifier,
};
use block_alloc::BlockAllocSet;
use bw;
use ai_spending;
use game::Game;
use idle_orders::IdleOrders;
use idle_tactics::IdleTactics;
//use idle_orders::Rate;
use fxhash::FxHashMap;
use recurse_checked_mutex::{Mutex, MutexGuard};
use rng::Rng;
//use rstar::RTree;
//use rstar::{RTreeObject, AABB};
use swap_retain::SwapRetain;
use unit::{self, HashableUnit, Unit, SerializableSprite};
use config::{self};
use unit_search::UnitSearch;

#[derive(Clone, Copy, PartialEq)]
pub struct InfluenceNode {
	pub value: f64,
	pub impassable: bool,
	pub distance: u32,
}
bitflags! {
    pub struct InfluenceFlags: u8 {
        const GROUND_UNIT = 0x1;
		const AIR_UNIT = 0x2;
    }
}


impl InfluenceNode {
	fn new(distance: u32)->InfluenceNode {
		InfluenceNode {
			value: 0.0,
			impassable: false,
			distance: distance,
		}
	}
}


pub enum InfluenceShape {
	Circle,
//	Rectangle,
}
pub enum InfluenceType {
	ForEnemy,
}

pub enum InfluenceFind {
	FindMaxBestRangeSpot,
	FindMaxBestRangeSpotReaver,
}
use std::collections::HashSet;


use std::collections::BTreeMap;

pub struct InfluenceZone {
	pub flags: InfluenceFlags,
	pub source: bw::Point,
	pub target: bw::Point,
	pub range: u32,
	//
	pub grid: BTreeMap<(i32,i32),InfluenceNode>,
}
fn distf(p1: bw::Point, p2: bw::Point)->f64{
	let result = (((p2.x-p1.x) as f64).powf(2.0)+((p2.y-p1.y) as f64).powf(2.0)).sqrt();
	result
}
use std::cmp;
use std::collections::hash_map::Entry;
impl InfluenceZone {
	pub fn new() -> InfluenceZone {
		InfluenceZone {
			grid: BTreeMap::default(),
			flags: InfluenceFlags::empty(),
			
			source: bw::Point{x: 0, y: 0},
			target: bw::Point{x: 0, y: 0},
			range: 0,
		}
	}
	/*
	pub fn add_neighbors(&mut self, x: i32, y: i32){
		
		for i in -1..=1 {
			for j in -1..=1 {
				if !(i==0 && j==0){
					let distance = bw::distance(self.target, bw::Point{ x: x as i16 + i, y: y as i16 + j });
			
				}
			}
		}
	}*/
	
    pub fn find_best_node<F>(&self, mut filter: F) -> Option<(bw::Point)>
    where
        F: FnMut(InfluenceNode) -> bool,
    {
		let mut node = None;
//		let mut value = ::std::f64::MAX;
		let mut value = ::std::f64::MIN;
		for (k,v) in self.grid.iter()
		{
			let point = bw::Point{ x: (((*k).0)*32) as i16, y: (((*k).1)*32) as i16};
			//if v.value < value && filter(*v) {
			if v.value > value && filter(*v) {
				value = v.value;
				node = Some(point);
			}
		}		
//        node.map(|x| x)
		node
    }	
	
	
	pub fn generate(&mut self, source: bw::Point, position: bw::Point, source_unit: Unit, _target_unit: Unit, 
					range: u32, cell_size: u32, _shape: InfluenceShape, 
					_ty: InfluenceType, _find: InfluenceFind, flags: InfluenceFlags, search: &UnitSearch)->Option<bw::Point>{
		unsafe {
		assert!(cell_size != 0);
		self.source = source;
		self.target = position;
		self.flags = flags;
		self.range = range;
		let zone_size = (range/cell_size);
		let game = Game::get();
		let map_width = (*game.0).map_width_tiles;
		let map_height = (*game.0).map_height_tiles;
		let closest_to = idle_tactics::get_point_on_circle_closest(source, position, range);
		let zone_size = zone_size as i32;
		//bw_print!("Closest to: {:?}, zone_size {}",closest_to,zone_size);
		for i in -zone_size..=zone_size {
			let y_span = ((zone_size as f64)*((-i as f64/zone_size as f64).acos()).sin()) as i32;
			for j in -y_span..=y_span {
				let mut node = InfluenceNode::new(bw::distance(bw::Point{x: 0, y: 0}, bw::Point{x: i as i16, y: j as i16}));
				let mut x = i as i16+(position.x/cell_size as i16);
				let mut y = j as i16+(position.y/cell_size as i16);
				if x < 0 {
					x=0;
				}
				else if x >= map_width as i16 {
					x = map_width as i16 - 1;
				}
				if y < 0 {
					y=0;
				}
				else if y >= map_height as i16 {
					y = map_height as i16 - 1;
				}
				
				let tile_flag =
                    (*bw::tile_flags).offset(x as isize + (map_width * (y as u16)) as isize);
				let current_pos = bw::Point{x: x*cell_size as i16, y: y*cell_size as i16};
				
				node.distance = bw::distance(current_pos,position);
				
				let placement = source_unit.id().placement();
				if flags == InfluenceFlags::GROUND_UNIT {
					if !aiscript::check_placement(game, search, source_unit, x, y, source_unit.id(), false, current_pos){
						node.impassable = true;
					}
					else {
						if *tile_flag & 0x20000000 !=0 || *tile_flag & 0x40000 !=0 || *tile_flag & 0x08000000 !=0 {
							node.impassable = true;
						}
						if bw::can_walk_here(current_pos.x as u32,current_pos.y as u32, placement.width as u32, placement.height as u32)==0 {
							node.impassable = true;
						}				
					}
					if bw::pathing_distance(position.x as u32, position.y as u32, source_unit.0) > range*2 {
					node.impassable = true;
					}
				}
				/*if source_unit.id()==UnitId(0x53){//reaver
					//if bw::pathing_distance(position.x,position.y,
					//add later, bw pathing distance unfortunately requires unit
					//must respect relocators
				}*/
				
				//
				//
				//	pathing distance was added recently, might have to remove it
				//
				//
				//previous was checking from max (worst) to min (best)
				//current_pos is pos of cell
				//position is target cell
				//closest_to is cell on circle closest to source
				
				node.value = distf(current_pos, position)+(distf(closest_to,position)-distf(closest_to,current_pos));
				//node.value = distf(current_pos, position);
				//node.value = 30000.0-distf(position, closest_to);
				//from target position to current point
				
				
//				node.value = bw::distance(closest_to, current_pos) as f64 / 20.0;
				if !node.impassable {
					self.grid.insert((x as i32,y as i32),node);
				}
				if node.impassable {
					node.value = 30000.0;
				}
			}
		}
		
	//	bw_print!("Try find the best");
		return self.find_best_node(|x| !x.impassable);


		/*
		if value < 32766.0 {
			return Some(bw::Point{x: (node.0*32) as i16, y: (node.1*32) as i16});
		}
		return None;*/
		//add later influence area full support
		//
		//
		
		}
		/*
		let points = (zone_size as f64)*std::f64::consts::PI;
		for i in 0..(points as u32) {
			let x = position.x as i32+((zone_size as f64)*(((i as f64/points)*std::f64::consts::PI).sin())) as i32;
			let y = position.y as i32+((zone_size as f64)*(((i as f64/points)*std::f64::consts::PI).cos())) as i32;			
			self.add_default_node(x, y, bw::distance32(bw::Point32{ x, y },bw::Point32{ x: position.x as i32, y: position.y as i32}));
		}*/
		//self.setup();
	}/*
	fn add_default_node(&mut self, x: i32, y: i32, distance: u32){
		let node = InfluenceNode::new(distance);
		self.grid.insert((x,y),node);	
	}*/
}