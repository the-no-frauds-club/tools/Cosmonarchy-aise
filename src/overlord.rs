#![allow(non_snake_case)]
#![allow(dead_code)]

//use std::ffi::CString;
use std::ptr::null_mut;
//use std::slice;
use bw_dat::{UnitId, UpgradeId, TechId};
use bw;
use byteorder::{ReadBytesExt,WriteBytesExt, LE};
use game::Game;
use globals::{
    self, Globals, NewGameState,
	CommTowerMenu,OverlordFaction,ActionButton, HydraEffect, LongEffect, SpriteStruct, Upgrades,
};
//use idle_orders::Rate;
//use rng::Rng;
//use swap_retain::SwapRetain;
use unit::{HashableUnit, Unit, SerializableSprite};
use config::{self, Config};
use std::collections::hash_map::Entry;
use unit_search::{self, UnitSearch};

pub unsafe fn get_menu(buttonset: u16)->CommTowerMenu {
	let result = match buttonset {
		93 => CommTowerMenu::Proposal,
		94 => CommTowerMenu::PickTargetForListening,
		90 => CommTowerMenu::PickTargetForProposal,
		190 => CommTowerMenu::General,//psi disruptor/commtower id
		218 => CommTowerMenu::Listening,
		91 => CommTowerMenu::PickTargetForOrders,		
		95 => CommTowerMenu::Orders,
		//message details, pending messages - add later
		_=>{
//			bw_print!("Set previous: Incorrect buttonset {}",buttonset);
			return CommTowerMenu::General;
		},		
	};
	result
}	

pub enum UpgradeParameter {
	MineralCost,
	MineralCostFactor,
	GasCost,
	GasCostFactor,
	TimeCost,
	TimeCostFactor,
	RestrictionFlags,
	Icon,
	Label,
	Race,
	RepeatCount,
	BroodwarFlag,
}
pub unsafe fn get_new_upgrade_stats(
	upgrades: &mut Upgrades,
	upgrade: u16,
	stat: UpgradeParameter,)->u32 {
		let config = config::config();
		if upgrade >=45 && config.extended_upgrades {
			let id = UpgradeId(upgrade);
			let result = match stat {
				UpgradeParameter::MineralCost=>id.mineral_cost(),
				UpgradeParameter::MineralCostFactor=>id.mineral_factor(),
				UpgradeParameter::GasCost=>id.gas_cost(),
				UpgradeParameter::GasCostFactor=>id.gas_factor(),
				UpgradeParameter::TimeCost=>id.time(),
				UpgradeParameter::TimeCostFactor=>id.time_factor(),
				UpgradeParameter::RestrictionFlags=>id.restriction_flags(),
				UpgradeParameter::Icon=>id.icon(),
				UpgradeParameter::Label=>id.label(),
				UpgradeParameter::Race=>id.race(),
				UpgradeParameter::RepeatCount=>id.repeat_count(),
				UpgradeParameter::BroodwarFlag=>id.broodwar_flag(),	
				_=>0,
			};		
			return result;
		}
		else {
			let upgrade = upgrade as u32;
			let result = match stat {
				UpgradeParameter::MineralCost=>upgrades.mineral_cost(upgrade),			
				UpgradeParameter::MineralCostFactor=>upgrades.mineral_cost_extra(upgrade),
				UpgradeParameter::GasCost=>upgrades.gas_cost(upgrade),
				UpgradeParameter::GasCostFactor=>upgrades.gas_cost_extra(upgrade),
				UpgradeParameter::TimeCost=>upgrades.time_cost(upgrade),
				UpgradeParameter::TimeCostFactor=>upgrades.time_factor(upgrade),
				UpgradeParameter::RestrictionFlags=>upgrades.restriction(upgrade),
				UpgradeParameter::Icon=>upgrades._icon(upgrade),
				UpgradeParameter::Label=>upgrades._label(upgrade),
				UpgradeParameter::Race=>upgrades._race(upgrade),
				UpgradeParameter::RepeatCount=>upgrades.repeat_count(upgrade),
				UpgradeParameter::BroodwarFlag=>upgrades.bw_flag(upgrade),
				_=>0,
			};				
			return result;
		}
	}
impl Globals {
	pub fn set_sucking_data(&mut self, attacker: Unit,target: *mut bw::Unit, opcode: u32, index: u32){
		let target = Unit::from_ptr(target).unwrap();
		if attacker.player()==target.player(){
			return;
		}
		use std::cmp;
		let game = Game::get();
		unsafe {
		let mut mineral_suck = 0;
		let mut gas_suck = 0;		
		match opcode {

			0x1=>{//tech
				let tech = TechId(index as u16);	
				let mineral_cost = tech.mineral_cost();
				let gas_cost = tech.gas_cost();
				mineral_suck = mineral_cost/20;
				gas_suck = gas_cost/20;						
			},
			0x2=>{//upgrade
				let upgrade = index;
				let mineral_cost = get_new_upgrade_stats(&mut self.upgrades, upgrade as u16, UpgradeParameter::MineralCost);
				let gas_cost = get_new_upgrade_stats(&mut self.upgrades, upgrade as u16, UpgradeParameter::GasCost);
				let mineral_factor = get_new_upgrade_stats(&mut self.upgrades, upgrade as u16, UpgradeParameter::MineralCostFactor);
				let gas_factor = get_new_upgrade_stats(&mut self.upgrades, upgrade as u16, UpgradeParameter::GasCostFactor);							
				let mineral_cost = mineral_cost+(mineral_factor*target.upgrade_level() as u32);
				let gas_cost = gas_cost+(gas_factor*target.upgrade_level() as u32);				
				mineral_suck = mineral_cost/20;
				gas_suck = gas_cost/20;			
			},
			0x3=>{//train--warp--build
				debug!("Set unit id: {}",index);
				let unit = UnitId(index as u16);
				let mineral_cost = unit.mineral_cost();
				let gas_cost = unit.gas_cost();
				mineral_suck = mineral_cost/20;
				gas_suck = gas_cost/20;						
			},
			0x4=>{//refinery
				let res = target.resources().saturating_sub(5) as u32;				
				(&mut (*target.0).unit_specific2[..]).write_u16::<LE>(res as u16).unwrap();
				let res = cmp::max(2,cmp::min(5,res));
				(*game.0).gas[attacker.player() as usize] = (*game.0).gas[attacker.player() as usize].saturating_add(res);
				return;
			},
			_=>{},
		}
		mineral_suck = cmp::min(mineral_suck,(*game.0).minerals[target.player() as usize]);
		gas_suck = cmp::min(gas_suck,(*game.0).minerals[target.player() as usize]);		
		(*game.0).minerals[attacker.player() as usize] = (*game.0).minerals[attacker.player() as usize].saturating_add(mineral_suck);
		(*game.0).gas[attacker.player() as usize] = (*game.0).gas[attacker.player() as usize].saturating_add(gas_suck);
		(*game.0).minerals[target.player() as usize] = (*game.0).minerals[target.player() as usize].saturating_sub(mineral_suck);
		(*game.0).gas[target.player() as usize] = (*game.0).gas[target.player() as usize].saturating_sub(gas_suck);
		
		
		
		}
	}

}
impl NewGameState {
	//
	pub unsafe fn register_button(&mut self, player: u8, button: *mut bw::Button){
		if self.factions.len()==0{
			self.diplomacy[player as usize].registered_button_string=1372;
		}
		let mut player_button_list = Vec::new();
		for i in 0..8 {
			if i!=player {
				let string = self.factions.values().find(|x| self.diplomacy[i as usize].faction == x.faction_id)
						.map(|x| x.string_id.clone());
				if let Some(string) = string {
					player_button_list.push(string);
				}				
			}
		}
		self.diplomacy[player as usize].registered_button_string = player_button_list[(*button).pos.saturating_sub(1) as usize];
	}
	pub unsafe fn get_registered_string_id(&mut self, player: u32)->u32{
		//bw_print!("String: {}",self.diplomacy[player as usize].registered_button_string);
		return self.diplomacy[player as usize].registered_button_string as u32;
	}
	pub unsafe fn enemy_faction(&mut self, source:u8,target:u8)->bool {
		let source_faction = self.diplomacy[source as usize].faction;
		let target_faction = self.diplomacy[target as usize].faction;
		if source_faction==OverlordFaction::KelMorianCombine || source_faction==OverlordFaction::TalQiratGuild {
			let result = match target_faction {
				OverlordFaction::AlphaSquadron|OverlordFaction::GammaSquadron|OverlordFaction::NovaSquadron|
				OverlordFaction::EpsilonSquadron|OverlordFaction::OmegaSquadron => true,
				_=>false,
			};
			return result;
		}
		return false;
	}	
	pub unsafe fn opposing_forces(&mut self, source:u8,target:u8)->bool {
		if self.enemy_faction(source,target) || self.enemy_faction(target,source){
			return true;
		}
		false
	}
	pub unsafe fn normalize_disposition(&mut self, source: u8, target: u8){
		if self.diplomacy[source as usize].disposition[target as usize] > 500 {
			self.diplomacy[source as usize].disposition[target as usize]=500;
		}
	}
	pub unsafe fn get_faction_icon(&mut self, pos: u32)->u32 {
		if self.factions.len()==0{
			return 0;
		}
		let mut player_button_list = Vec::new();
		let mut debug_list = Vec::new();
		let player = *bw::active_player_id;
		for i in 0..8 {
			if i!=player {
				let icon = self.factions.values().find(|x| self.diplomacy[i as usize].faction == x.faction_id)
						.map(|x| x.icon_id.clone()).unwrap();	
				player_button_list.push(icon);
				debug_list.push(self.diplomacy[i as usize].faction);
			}
		}
		debug!("Icon list");
		for i in player_button_list.clone() {
			debug!("{} {}",i,pos);
		}
		if player_button_list.len()>0 {
			return player_button_list[pos.saturating_sub(1) as usize] as u32;
		}
		
		return 93;
	}
	pub unsafe fn get_faction_selection_button_state(&mut self, pos: u32)->i32{
		match *bw::current_button_set {
			90|91|218=>(),
			_=>{
				return 1;
			},
		}
		
		let player = *bw::active_player_id as u8;
		
		
		if pos > 7 {
			return 1;
		}
		
		let unit = bw::player_selections[player as usize][0];
		if unit == null_mut(){
			return 1;
		}
		if self.factions.len()==0{
			return 1;
		}		
		let mut list = Vec::new();
		for i in 0..8 {
			if i!=player {
				list.push(i);
			}
		}	
		let target = list[(pos-1) as usize];	
		match bw::player_list[target as usize].ty {
			6|0|1=>{},
			_=>{
				return 0;
			},
		}		
		let game = Game::get();
		
		if bw::victory_status[target as usize]==2 {
			return 0;//later replace to -1 and have different strings 
		}
		let game=Game::get();			
		let start_x = bw::start_positions[target as usize][0];
		if start_x == 0 {
			return 0;
		}
		let ai = bw::player_ai(target as u32);
		if (*ai).flags & 0x2 == 0 {
			return 0;
		}
		let unit = Unit::from_ptr(unit).unwrap();
		let u_str = self.default_unit_struct(unit);
		let result = match self.new_units.entry(HashableUnit(unit)) {
		   Entry::Vacant(entry) => entry.insert(u_str),
		   Entry::Occupied(entry) => entry.into_mut(),
		};	
		if let Some(ref mut tower) = result.comm_tower {
			if tower.current_menu == CommTowerMenu::PickTargetForProposal {
				return 1;
			}
			if tower.current_menu == CommTowerMenu::Listening {
				return 1;
			}
//			bw_print!("Current menu {:?}",tower.current_menu);
		}	
		
		
		//
		if !game.allied(player,target) {
			return -1;
		}
		if !self.diplomacy[player as usize].alliance_status[target as usize]{
			return -1;
		}
		return 1;
	}
	pub unsafe fn get_button_state(&mut self, pos: u32)->i32 {
		//-1 greyed
		//0 hidden
		//1 enabled
		let player = *bw::active_player_id as u8;
		let unit = bw::player_selections[player as usize][0];
		if unit == null_mut(){
			return 0;
		}
		if self.factions.len()==0{
			return 0;
		}	
		let command = match pos {
			1=>ActionButton::SendMinerals,
			2=>ActionButton::SendGas,
			3=>ActionButton::Ally,
			4=>ActionButton::CeaseFire,
			5=>ActionButton::Surrender,
			_=>{return 1;},
		};				
		let target = self.get_target_player_id(unit) as u8;
		let game=Game::get();
		
		match command {
			ActionButton::Ally=>{
				if game.allied(player,target) && self.diplomacy[player as usize].alliance_status[target as usize]{
					return -1;
				}
				if !game.allied(player,target){
					return -1;
				}
				return 1;
			},
			ActionButton::CeaseFire=>{
				if game.allied(player,target){
					return -1;
				}
				return 1;
			},
			_=>{
				return 1;
			},
		}
	}
	pub unsafe fn adjust_disposition(&mut self, source: u8, target: u8, value: u32){
		let mut converted_value = value as f64;
		let debug_old = self.diplomacy[source as usize].disposition[target as usize];
		let hostility_index: f64 = match self.diplomacy[target as usize].faction {
			OverlordFaction::Pirates=>0.4,
			OverlordFaction::Scavengers=>0.5,
			_=>1.0,
		};
		/*if self.opposing_forces(source,target){
			converted_value*=0.4;
		}*/
		converted_value *= hostility_index;
		
		self.diplomacy[source as usize].disposition[target as usize] += converted_value as u32;
		let debug_new = self.diplomacy[source as usize].disposition[target as usize];
		/*if cfg!(debug_assertions) {
			bw_print!("Disposition: {} -> {}",debug_old,debug_new);
		}*/
		self.diplomacy[target as usize].disposition[source as usize] += converted_value as u32;
		self.normalize_disposition(source,target);
		self.normalize_disposition(target,source);
	}
	pub unsafe fn subtract_disposition(&mut self, source: u8, target: u8, value: u32){
		let mut converted_value = value as f64;
		let hostility_index: f64 = match self.diplomacy[target as usize].faction {
			OverlordFaction::Pirates=>0.3,
			OverlordFaction::Scavengers=>0.5,
			_=>1.0,
		};
		if self.opposing_forces(source,target){
			converted_value*=0.1;
		}
		converted_value *= hostility_index;
	
		self.diplomacy[source as usize].disposition[target as usize]=self.diplomacy[source as usize].disposition[target as usize].saturating_sub(converted_value as u32);
		self.diplomacy[target as usize].disposition[source as usize]=self.diplomacy[target as usize].disposition[source as usize].saturating_sub(converted_value as u32);
		self.normalize_disposition(source,target);
		self.normalize_disposition(target,source);
	}	
	
	
	pub unsafe fn set_button_n(&mut self, unit: *mut bw::Unit, button: *mut bw::Button){
		let result = self.unit_by_key(Unit::from_ptr(unit).unwrap());	
		if let Some(ref mut tower) = result.comm_tower {
			//bw_print!("Set id option to {}",(*button).pos);
			tower.id_option = (*button).pos;
		}	
	}
	pub unsafe fn unit_by_key(&mut self, unit: Unit)->&mut globals::UnitStruct {
		let u_str = self.default_unit_struct(unit);
		let result = match self.new_units.entry(HashableUnit(unit)) {
		   Entry::Vacant(entry) => entry.insert(u_str),
		   Entry::Occupied(entry) => entry.into_mut(),
		};
		result
	}
	pub unsafe fn set_default_unit_struct(&mut self, unit: Unit){
		let u_str = self.default_unit_struct(unit);
		match self.new_units.entry(HashableUnit(unit)) {
		   Entry::Vacant(entry) => {
				entry.insert(u_str);
		   },
		   Entry::Occupied(_e) => {},
		};
	}
	
	pub unsafe fn get_resource_stack_size(&mut self,unit: *mut bw::Unit)->u32{
		let result = self.unit_by_key(Unit::from_ptr(unit).unwrap());
		if let Some(unit) = &result.comm_tower {
			return unit.resource_setup;
		}
		return 1;	
	}
	pub unsafe fn get_scrap(&mut self,unit: Unit)->u32{
		for u in &mut self.persist_unit_structs {
			if u.unit==unit{
				return u.scrap as u32;
			}
		}
		return 0;
	}
	

	
	pub unsafe fn meskalloid(&mut self,unit: Unit)->Option<u8>{
		let result = match self.new_units.entry(HashableUnit(unit)) {
		   Entry::Vacant(_e) => { return None; },
		   Entry::Occupied(entry) => entry.into_mut(),
		};
		
		if result.meskalloid.0 > 0 {
			return Some(result.meskalloid.1);
		}
		return None;
	}
	pub unsafe fn mineral_opcode(&mut self, ptr: *mut bw::Sprite)->u32{
		if ptr==null_mut(){
			return 0;
		}
		let result = match self.custom_sprites.entry(SerializableSprite(ptr)){
		   Entry::Vacant(_e) => {
				return 0;
		   },
		   Entry::Occupied(entry) => entry.into_mut(),		
		};
		result.special_mineral	
	}
	pub unsafe fn basilisk_gimmick(&mut self, ptr: *mut bw::Sprite)->u32{
		if ptr==null_mut(){
			return 0;
		}
		let result = match self.custom_sprites.entry(SerializableSprite(ptr)){
		   Entry::Vacant(_e) => {
				return 0;
		   },
		   Entry::Occupied(entry) => entry.into_mut(),		
		};
		result.basilisk_gimmick	as u32
	}
	pub fn get_mineral_chunk_status(&mut self, ptr: *mut bw::Sprite)->u32{
		if ptr==null_mut(){
			return 0;
		}
		let result = match self.custom_sprites.entry(SerializableSprite(ptr)){
		   Entry::Vacant(_e) => {
				return 0;
		   },
		   Entry::Occupied(entry) => entry.into_mut(),		
		};
		result.chunk_res_id	as u32
	}	
	pub unsafe fn is_meskalloid(&mut self, ptr: *mut bw::Sprite)->u32{
		if ptr==null_mut(){
			return 0;
		}
		let result = match self.custom_sprites.entry(SerializableSprite(ptr)){
		   Entry::Vacant(_e) => {
				return 0;
		   },
		   Entry::Occupied(entry) => entry.into_mut(),		
		};
		result.meskalloid as u32
	}	
	pub fn set_meskalloid_sprite(&mut self, ptr: *mut bw::Sprite){
		let l_str = self.default_sprite_struct(ptr);
		let result = match self.custom_sprites.entry(SerializableSprite(ptr)){
		   Entry::Vacant(entry) => entry.insert(l_str),
		   Entry::Occupied(entry) => entry.into_mut(),		
		};
		result.meskalloid = true;
	}
	pub fn set_special_mineral(&mut self, ptr: *mut bw::Sprite, opcode: u32){
		let l_str = self.default_sprite_struct(ptr);
		let result = match self.custom_sprites.entry(SerializableSprite(ptr)){
		   Entry::Vacant(entry) => entry.insert(l_str),
		   Entry::Occupied(entry) => entry.into_mut(),		
		};
		result.special_mineral = opcode;
	}	
	pub fn set_basilisk_gimmick(&mut self, ptr: *mut bw::Sprite, val: u32){
		let l_str = self.default_sprite_struct(ptr);
		let result = match self.custom_sprites.entry(SerializableSprite(ptr)){
		   Entry::Vacant(entry) => entry.insert(l_str),
		   Entry::Occupied(entry) => entry.into_mut(),		
		};
		result.basilisk_gimmick = val as u8;
	}	
	pub fn set_mineral_chunk_status(&mut self, ptr: *mut bw::Sprite, val: u32){
		let l_str = self.default_sprite_struct(ptr);
		let result = match self.custom_sprites.entry(SerializableSprite(ptr)){
		   Entry::Vacant(entry) => entry.insert(l_str),
		   Entry::Occupied(entry) => entry.into_mut(),		
		};
		result.chunk_res_id = val;
	}	
	pub fn get_waste_status(&mut self, vermin_ptr: *mut bw::Unit)->u32 {
		let result = match self.new_units.entry(HashableUnit(Unit::from_ptr(vermin_ptr).unwrap())) {
		   Entry::Vacant(_e) => { return 0; },
		   Entry::Occupied(entry) => entry.into_mut(),
		};
		return result.expelling_waste.is_some() as u32;
	}
	
	
	pub unsafe fn set_meskalloid_timer(&mut self, target: Unit, attacker: u32){
		let u_str = self.default_unit_struct(target);
		let mut result = match self.new_units.entry(HashableUnit(target)) {
		   Entry::Vacant(entry) => entry.insert(u_str),
		   Entry::Occupied(entry) => entry.into_mut(),
		};
		result.meskalloid = (96, attacker as u8);	
	}
	pub fn place_overlord_pool(&mut self, source: Unit, x: u32, y: u32, player: u32){
		let lef = LongEffect{effect_id: HydraEffect::VerminPoolLoop, timer: 48, energy: 0, 
			player: player as u8, position: bw::Point{x: x as i16,y: y as i16}, unit_id: 0, unit: source, misc: 0, 
			target: source};
		self.long_term_effects.push(lef);
		
	}	
	pub fn set_expel_status(&mut self, vermin: Unit, status: u32){
		let u_str = self.default_unit_struct(vermin);
		let result = match self.new_units.entry(HashableUnit(vermin)) {
		   Entry::Vacant(entry) => entry.insert(u_str),
		   Entry::Occupied(entry) => entry.into_mut(),
		};
		match status {
			0=>{				
				result.expelling_waste = None;
				result.vermin = None;
			},
			1=>{
				result.expelling_waste = Some(true);
				result.vermin = Some(0);
			},
			_=>{},
		}
	}		
	pub fn add_expel_variable(&mut self, vermin: Unit){
		for u in &mut self.unit_structs {
			if u.unit==vermin {
				u.expelling_waste = Some(false);
				return;
			}
		}	
		let mut u_str = self.default_unit_struct(vermin);
		u_str.expelling_waste = Some(false);
		
		self.unit_structs.push(u_str);
	}
	pub unsafe fn get_current_faction(&mut self,player: u32)->u32{
		if player > 7 {
			return 1;
		}
		let source_faction = self.diplomacy[player as usize].faction;
		source_faction as u32
	}
	pub unsafe fn get_current_disposition(&mut self, player1: u32, player2: u32)->u32{
		self.diplomacy[player1 as usize].disposition[player2 as usize] as u32
	}
	pub unsafe fn fill_scrap_depots(&mut self,player: u8){
		let game = Game::get();
		for unitstruct in &mut self.persist_unit_structs {
			if unitstruct.unit.player()==player && unitstruct.unit.id().0==174 {
				if unitstruct.scrap < 6 {
					//bw_print!("Add scrap to depot at: {} {}",unitstruct.unit.position().x,unitstruct.unit.position().y);
					unitstruct.scrap += 1;
					let race = 1;
					let mut supplies = (*game.0).supplies[race as usize].provided[player as usize] as i32;
					supplies = supplies.wrapping_add(2);
					(*game.0).supplies[race as usize].provided[player as usize] = supplies as u32;		
					return;
				}
			}
		}
	
	}
	
	pub unsafe fn add_to_stack(&mut self, unit: *mut bw::Unit, amount: i32){
		let result = self.unit_by_key(Unit::from_ptr(unit).unwrap());
		if let Some(ref mut tower) = result.comm_tower {
			let mut setup = tower.resource_setup as i32;
			setup = setup.saturating_add(amount);
			if setup <= 0 {
				setup = 100;
			}
			tower.resource_setup = setup as u32;					
		}
	}
	pub unsafe fn refresh_attack_to_orders(&mut self, unit: Unit, attack_to_issued: bw::Point){
		/*let mut result = self.unit_by_key(unit);
		if let Some(ref mut tower) = result.comm_tower {
			
		}*/
		let result = self.unit_by_key(unit);
		if let Some(tower) = &result.comm_tower {
//			self.diplomacy[tower.id_option as usize].attack_to_preference = Some(attack_to_issued);
			self.diplomacy[tower.id_option as usize].set_attack_preference(Some(attack_to_issued));
//			bw_print!("Set attack to {:?}",attack_to_issued);
		}
		
	}
	
	pub unsafe fn get_target_player_id(&mut self, unit: *mut bw::Unit)->u16{
		let result = self.unit_by_key(Unit::from_ptr(unit).unwrap());
		if let Some(unit) = &result.comm_tower {
			return unit.id_option;
		}
		return 1;
	}
	pub unsafe fn get_listener_menu(&mut self, unit: *mut bw::Unit)->CommTowerMenu{
		let unit = match Unit::from_ptr(unit) {
			Some(s) => s,
			None => {
				return CommTowerMenu::General;
			},
		};
		let u_str = self.default_unit_struct(unit);
		let result = match self.new_units.entry(HashableUnit(unit)) {
		   Entry::Vacant(entry) => entry.insert(u_str),
		   Entry::Occupied(entry) => entry.into_mut(),
		};	
		
		if let Some(unit) = &result.comm_tower {
			//bw_print!("Current menu: {:?}",unit.current_menu);
			let previous = unit.suboption;
			let answer = match previous {
			
				CommTowerMenu::Listening=>CommTowerMenu::PickTargetForListening,
				CommTowerMenu::Orders=>CommTowerMenu::PickTargetForOrders,
				CommTowerMenu::Proposal=>CommTowerMenu::PickTargetForProposal,
				_=>previous,
			};
			return answer;
		}
		return CommTowerMenu::General;
	}
	pub unsafe fn set_current_menu(&mut self, unit: *mut bw::Unit, buttonset: u16){
		let unit = Unit::from_ptr(unit).unwrap();
		let u_str = self.default_unit_struct(unit);
		let result = match self.new_units.entry(HashableUnit(unit)) {
		   Entry::Vacant(entry) => entry.insert(u_str),
		   Entry::Occupied(entry) => entry.into_mut(),
		};	
		let subset = get_menu(buttonset);
		if let Some(ref mut tower) = result.comm_tower {
			tower.current_menu = subset;
		}	
	}
	pub unsafe fn set_previous(&mut self, unit: *mut bw::Unit, buttonset: u16){	
		let unit = match Unit::from_ptr(unit) {
			Some(s) => s,
			None => {
				return;
			},
		};
		let u_str = self.default_unit_struct(unit);
		let result = match self.new_units.entry(HashableUnit(unit)) {
		   Entry::Vacant(entry) => entry.insert(u_str),
		   Entry::Occupied(entry) => entry.into_mut(),
		};	
		let subset = get_menu(buttonset);
		if let Some(ref mut tower) = result.comm_tower {
			tower.suboption = subset;
		}
	}

	pub unsafe fn get_current_commtower_menu(&mut self, unit: *mut bw::Unit)->CommTowerMenu{
		let unit = match Unit::from_ptr(unit) {
			Some(s) => s,
			None => {
				return CommTowerMenu::General;
			},
		};
		let u_str = self.default_unit_struct(unit);
		let result = match self.new_units.entry(HashableUnit(unit)) {
		   Entry::Vacant(entry) => entry.insert(u_str),
		   Entry::Occupied(entry) => entry.into_mut(),
		};
		if let Some(unit) = &result.comm_tower {
			return unit.current_menu;
		}
		return CommTowerMenu::General;
	}
	pub unsafe fn comm_tower_setup(&mut self, unit: *mut bw::Unit) {	
		let unit = match Unit::from_ptr(unit) {
			Some(s) => s,
			None => {
				return;
			},
		};
		let u_str = self.default_unit_struct(unit);
		match self.new_units.entry(HashableUnit(unit)) {
		   Entry::Vacant(entry) => {entry.insert(u_str);},
		   _=>{},
		}
	}
}

pub unsafe fn diplomacy_ai(
	globals: &mut Globals,
    search: &UnitSearch,
	config: &Config,
	game: Game,
) {
	for i in 0..8 {
		match bw::player_list[i].ty {
			1|5=>{
				let ai = bw::player_ai(i as u32);
				if (*ai).flags & 0x2 !=0 {
//					let faction = globals.ngs.diplomacy;
					//attack_to preference - has no effect yet
					//pub action_timers: Vec<(DiplomacyAction,DiplomacyTimer)>,
					//pub order_settings: Vec<OrderSetting>,
					//
					//
					// check out current conditions
					//		if has enough resources
					//			if ally badly needs resources and has inferior access to resources
					//				send correspondent resources dependent on ore/gas
					//					if both players are not humans, push offer to listening queue
					//		if requires ally
					//			offer to neutral player with best disposition
					//				player will need to offer ally on its own, ai agrees or disagrees
					//				after short delay
					//      if under heavy attack and disposition isn't too bad					
					//			offer cease fire
					//		if alliance or peace is not in interests and faction is aggressive by nature
					//			break alliance/cease fire, open fire
					//		any such action will push a delay preventing further similar actions for 1440
					//		if almost lost
					//			surrender and there active humans/allies on the map - surrender
					//		each attack is accompanied with message but only if attackmessage timer isn't out 
					//
					//		OTHER FUNCTIONS:
					//
					//		each expansion claiming is accompanied with message (timer-based)
					//			if it is ally, may ping it 
					//		each techbuilding constructing  
					//		suggestion to attack weak spot (accompanied with message and ping)
				}
			},
			_=>{},
		}
	}
}