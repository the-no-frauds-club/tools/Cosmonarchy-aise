#![allow(non_snake_case)]
use std::ffi::CString;
use std::fmt;
use std::fs::{self, File};
use std::mem;
use std::path::{Component, Path, PathBuf};
use std::ptr::null_mut;
use std::slice;
use std::sync::atomic::{AtomicUsize, Ordering};
use std::sync::Arc;
use bincode;
use byteorder::{WriteBytesExt, LE};
use directories::UserDirs;
use parking_lot::Mutex;
use serde::{self, Deserialize, Deserializer, Serialize, Serializer};
use hooks;
use bw_dat::{TechId, UnitId, UpgradeId, RaceFlags, UNIT_LIMIT};
use crate::ai::{self, has_resources};
use crate::feature_disabled;
use block_alloc::BlockAllocSet;
use bw;
use windows;
use winapi::um::processthreadsapi::{GetCurrentProcess, TerminateProcess};
use game::Game;
use data_extender;
use globals::{
    self, BankKey, BaseLayout, BunkerCondition, BunkerDecl, Globals, LiftLand, LiftLandBuilding,
    LiftLandStage, Queues, NewGameState, RenameStatus, RevealState, RevealType, UnitQueue, UnitReplace, BuildMax, HydraEffect, LongEffect,
	ActionButton,ListeningButton, SpriteEffect, Upgrades, OverlordFaction /*, TrainState, TrainSequence*/
};
use list::ListIter;
use order::{self, OrderId};
use rng::Rng;
use samase;
use swap_retain::SwapRetain;
use unit::{self, HashableUnit, Unit, SerializableSprite};
use unit_search::UnitSearch;
use config::{self, Config, DataType};
use parking_lot::{RwLock};
use aiscript;

pub fn init_save_mapping() {}

pub fn clear_save_mapping() {}

pub fn init_load_mapping() {}

pub fn clear_load_mapping() {}

// Cache unit search accesses from script commands of same frame, as the script commands
// won't change unit positioning (Except create_unit, but oh well).
// CACHED_UNIT_SEARCH_FRAME has to be set to !0 on loading and game init to surely
// invalidate the cache.
// Arc for simplicity, but obviously getting the search from thread other than main is
// pretty much always racy.

lazy_static! {
    static ref CACHED_UNIT_SEARCH: Mutex<Option<Arc<UnitSearch>>> = Mutex::new(None);
	static ref FILE_READER: RwLock<FileReader> = RwLock::new(Default::default());
}

#[derive(Default)]
struct FileReader {
	pub state: Vec<u8>,
}

impl FileReader {
	pub fn clear(&mut self){
		self.state.clear();
	}
	pub unsafe fn setup(&mut self, ptr: *mut u8, len: usize){
		self.state = Vec::from_raw_parts(ptr,len,len);
	}
}
pub unsafe fn read_save_state_ptr()->u32 {
	let mut w = FILE_READER.write();
	if w.state.len()==0 {
		return 0;
	}
	else {
		let ptr = &mut w.state[0] as *mut u8 as *mut u32 as u32;
		ptr		
	}
}
pub unsafe fn create_file_state(string: &str)->Option<(*mut u8,u32)>{
	let (data, len) = match samase::read_file(string){
		Some(s)=>s,
		None=>{
			return None;
		},
	};
	let mut w = FILE_READER.write();
	w.clear();
	w.setup(data,len);
	Some((&mut w.state[0] as *mut u8,w.state.len() as u32))	
}

static CACHED_UNIT_SEARCH_FRAME: AtomicUsize = AtomicUsize::new(0);
pub fn invalidate_cached_unit_search() {
    CACHED_UNIT_SEARCH_FRAME.store(!0, Ordering::Relaxed);
}

pub unsafe fn aiscript_unit_search(game: Game) -> Arc<UnitSearch> {
    let frame = game.frame_count() as usize;
    if CACHED_UNIT_SEARCH_FRAME.load(Ordering::Relaxed) != frame {
        let search = Arc::new(UnitSearch::from_bw());
        *CACHED_UNIT_SEARCH.lock() = Some(search.clone());
        CACHED_UNIT_SEARCH_FRAME.store(frame, Ordering::Relaxed);
        search
    } else {
        let guard = CACHED_UNIT_SEARCH.lock();
        guard.as_ref().unwrap().clone()
    }
}


pub unsafe fn attack_to_pos(player: u8, grouping: bw::Point, target: bw::Point) {										  
    let grouping_region = match bw::get_region(grouping) {
        Some(s) => s,
        None => {
            bw_print!(
                "Aiscript attackto (player {}): invalid grouping coordinates {:?}",
                player,
                grouping,
            );
            return;
        }
    };
    let target_region = match bw::get_region(target) {
        Some(s) => s,
        None => {
            bw_print!(
                "Aiscript attackto (player {}): invalid target coordinates {:?}",
                player,
                target,
            );
            return;
        }
    };
    let ai_data = bw::player_ai(player.into());
    (*ai_data).last_attack_second = bw::elapsed_seconds();
    (*ai_data).attack_grouping_region = grouping_region + 1;
    let region = ai_region(player.into(), grouping_region);
    bw::change_ai_region_state(region, 8);
    (*region).target_region_id = target_region; // Yes, 0-based
}

pub unsafe extern fn attack_to(script: *mut bw::AiScript) {
    let mut read = ScriptData::new(script);
    let mut grouping = read.read_position();
    let mut target = read.read_position();
    if feature_disabled("attack_to") {
        return;
    }
	let config = config::config();
	if config.overlord {
		let globals = Globals::get("attack_to");
		let order_prepare = globals.ngs.diplomacy[(*script).player as usize]
													.order_setting.attack_wave_prepare;
		let order_targ = globals.ngs.diplomacy[(*script).player as usize]
							.order_setting.attack_wave_target;	
		if let Some(prepare) = order_prepare {
			grouping.center = prepare;
		}
		if let Some(targ) = order_targ {
			target.center = targ;
		}
	}
    attack_to_pos((*script).player as u8, grouping.center, target.center);
}

unsafe fn simple_change(script: *mut bw::AiScript, player: u16)->u16 {
	let result = match player {
		13=>(*script).player as u16,
		_=>player,	
	};
	result
}

pub unsafe extern fn attack_to_deaths(script: *mut bw::AiScript) {
    let mut read = ScriptData::new(script);
    let grouping_x_unit = read.read_u16();
    let grouping_x_player = simple_change(script,read.read_u16());
    let grouping_y_unit = read.read_u16();
    let grouping_y_player = simple_change(script,read.read_u16());
    let target_x_unit = read.read_u16();
    let target_x_player = read.read_u16();
    let target_y_unit = read.read_u16();
    let target_y_player = read.read_u16();
    if feature_disabled("attack_to") {
        return;
    }
    let game = Game::get();
    let group_x = (*Game::deaths())[grouping_x_unit as usize][grouping_x_player as usize];
    let group_y = (*Game::deaths())[grouping_y_unit as usize][grouping_y_player as usize];
    let target_x = (*Game::deaths())[target_x_unit as usize][target_x_player as usize];
    let target_y = (*Game::deaths())[target_y_unit as usize][target_y_player as usize];
    let grouping = bw::Point {
        x: group_x as i16,
        y: group_y as i16,					  	 
    };
    let target = bw::Point {
        x: target_x as i16,
        y: target_y as i16,	 
    };
    attack_to_pos((*script).player as u8, grouping, target);														 											   
}

#[derive(Copy, Clone, Debug, Serialize, Deserialize)]
pub struct AttackTimeoutState {
    value: Option<u32>,
    original_start_second: Option<(u32, u32)>,
}

impl AttackTimeoutState {
    pub fn new() -> AttackTimeoutState {
        AttackTimeoutState {
            value: None,
            original_start_second: None,
        }
    }
}

pub unsafe extern fn attack_timeout(script: *mut bw::AiScript) {
    let mut read = ScriptData::new(script);
    let timeout = read.read_u32();
    if feature_disabled("attack_timeout") {
        return;
    }
    let mut globals = Globals::get("ais attack_timeout");
    globals.attack_timeouts[(*script).player as usize].value = Some(timeout);
}

pub unsafe fn attack_timeouts_frame_hook(globals: &mut Globals, game: Game) {
    let seconds = (*game.0).elapsed_seconds;
    for i in 0..8 {
        let player_ai = bw::player_ai(i as u32);
        let last_attack_second = (*player_ai).last_attack_second;
        if last_attack_second == 0 {
            globals.attack_timeouts[i].value = None;
        }
        let new = if last_attack_second == 0 {
            Some(!400)
        } else {
            if let Some(timeout) = globals.attack_timeouts[i].value {
                if last_attack_second.saturating_add(timeout) < seconds {
                    Some(!400)
                } else {
                    // Don't want to accidentally set it to 0
                    Some(seconds.max(1))
                }
            } else {
                None
            }
        };
        if let Some(new) = new {
            (*player_ai).last_attack_second = new;
            globals.attack_timeouts[i].original_start_second = Some((last_attack_second, new));
        }
    }
}

pub unsafe fn attack_timeouts_frame_hook_after(globals: &mut Globals) {
    // Revert old value for teippi debug, only if it wasn't changed during frame step
    for i in 0..8 {
        if let Some((previous, new)) = globals.attack_timeouts[i].original_start_second.take() {
            let player_ai = bw::player_ai(i as u32);
            if (*player_ai).last_attack_second == new {
                (*player_ai).last_attack_second = previous;
            }
        }
    }
}




pub unsafe extern fn issue_order(script: *mut bw::AiScript) {
    // issue_order(order, count, unit_id, srcx, srcy, src_range, tgtx, tgty, tgt_range,
    //      tgt_param, flags)
    // Flag 0x1 = Target enemies,
    //      0x2 = Target own,
    //      0x4 = Target allies,
    //      0x8 = Target single unit
    //      0x10 = Target each unit once
    //		0x20 = Do not issue if unit is busy
    let mut read = ScriptData::new(script);
    let order = OrderId(read.read_u8());
    let limit = read.read_u16();
    let unit_id = read.read_unit_match();
    let mut src = read.read_position();
    let radius = read.read_u16();
    src.extend_area(radius as i16);
    let mut target = read.read_position();
    let tgt_radius = read.read_u16();
    target.extend_area(tgt_radius as i16);
    let target_misc = read.read_unit_match();
    let flags = read.read_u16();
    if feature_disabled("issue_order") {
        return;
    }
    if flags & 0xffe0 != 0 {
        bw_print!("Aiscript issue_order: Unknown flags 0x{:x}", flags);
        return;
    }
	
    let game = Game::get();
    let search = aiscript_unit_search(game);
    let units = search
        .search_iter(&src.area)
        .filter(|u| u.player() as u32 == (*script).player && unit_id.matches(u))
        .filter(|u| u.can_issue_order(order))
        .take(limit as usize);
    let targets = if flags & 0x7 != 0 {
        let mut acceptable_players = [false; 12];
        for i in 0..12 {
            if i == (*script).player {
                acceptable_players[i as usize] = flags & 0x2 != 0;
            } else {
                if !game.allied((*script).player as u8, i as u8) {
                    acceptable_players[i as usize] = flags & 0x1 != 0;
                } else {
                    acceptable_players[i as usize] = flags & 0x4 != 0;
                }
            }
        }

        let limit = match flags & 0x8 != 0 {
            true => 1,
            false => !0,
        };
        let targets = search
            .search_iter(&target.area)
            .filter(|u| acceptable_players[u.player() as usize] && target_misc.matches(u))
            .take(limit)
            .collect::<Vec<_>>();
	
        Some(targets)
    } else {
        None
    };
	
    if targets.as_ref().map(|x| x.is_empty()).unwrap_or(false) {
        return;
    }
	
    let mut target_pos = 0;
    for unit in units {
        if order.is_secondary() {
            // Not sure how to handle cases where a train overrides another train in queue.
			unit.issue_secondary_order(order);
        } else {
            if let Some(ref targets) = targets {
                if target_pos == targets.len() {
                    if flags & 0x10 != 0 {
                        break;
                    }
                    target_pos = 0;
                }
                unit.issue_order_unit(order, targets[target_pos]);
                target_pos += 1;
            } else {
                unit.issue_order_ground(order, target.center);
            }
        }
        match order {
            order::id::PLACE_ADDON | order::id::BUILD_ADDON => {
                let unit_id = target_misc.get_one();
                (&mut (*unit.0).unit_specific[4..])
                    .write_u16::<LE>(unit_id.0)
                    .unwrap();
            }
            order::id::DRONE_BUILD |
            order::id::SCV_BUILD |
            order::id::PROBE_BUILD |
            order::id::UNIT_MORPH |
            order::id::BUILDING_MORPH |
            order::id::TRAIN |
            order::id::TRAIN_FIGHTER |
            order::id::BUILD_NYDUS_EXIT => {
                let unit_id = target_misc.get_one();
                (*unit.0).build_queue[(*unit.0).current_build_slot as usize] = unit_id.0;
            }
            _ => (),
        }
    }
}
pub unsafe extern fn if_attacking(script: *mut bw::AiScript) {
    let mut read = ScriptData::new(script);
    let dest = read.read_jump_pos();
    if feature_disabled("if_attacking") {
        return;
    }
    let ai = bw::player_ai((*script).player);
    if (*ai).attack_grouping_region != 0 {
        (*script).pos = dest;
    }
}

pub unsafe extern fn unstart_campaign(script: *mut bw::AiScript) {
    if feature_disabled("unstart_campaign") {
        return;
    }
    let ai = bw::player_ai((*script).player);
    (*ai).flags &= !0x20;
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct TownId {
    town: Town,
    id: u8,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct MaxWorkers {
    town: Town,
    count: u8,
}

fn towns() -> Vec<Town> {
    let mut result = Vec::with_capacity(32);
    for unit in unit::active_units() {
        let town = if let Some(ai) = unit.building_ai() {
            unsafe { Town::from_ptr((*ai).town) }
        } else if let Some(ai) = unit.worker_ai() {
            unsafe { Town::from_ptr((*ai).town) }
        } else {
            None
        };
        if let Some(town) = town {
            if !result.iter().any(|&x| x == town) {
                result.push(town);
            }
        }
    }
    let mut script = bw::first_ai_script();
    while !script.is_null() {
        let town = unsafe { Town::from_ptr((*script).town) };
        if let Some(town) = town {
            if !result.iter().any(|&x| x == town) {
                result.push(town);
            }
        }
        script = unsafe { (*script).next };
    }
    result
}

pub fn update_towns(globals: &mut Globals) {
    let old = mem::replace(&mut globals.towns, towns());
    for old in old {
        if !globals.towns.iter().any(|&x| x == old) {
            globals.max_workers.swap_retain(|x| x.town != old);
            globals.town_ids.swap_retain(|x| x.town != old);
            globals
                .lift_lands
                .structures
                .swap_retain(|x| x.town_src != old && x.town_tgt != old);
            globals.queues.queue.swap_retain(|x| x.town != Some(old));
			
        }
    }
}

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub struct Town(pub *mut bw::AiTown);

impl Town {
	//originally private
    pub fn from_ptr(ptr: *mut bw::AiTown) -> Option<Town> {
        if ptr == null_mut() {
            None
        } else {
            Some(Town(ptr))
        }
    }


    pub fn player(self) -> u8 {
        unsafe { (*self.0).player }
    }
	pub fn has_worker(self, unit: Unit) -> bool {
		unsafe { self.workers().any(|x| (*x).parent == unit.0) }
	}
    pub fn has_building(self, unit: Unit) -> bool {
		unsafe {
			let mut building = (*self.0).buildings;
			while building != null_mut(){
				if (*building).parent == unit.0 {
					return true;
				}
				building = (*building).next;
			}
			return false;
		}
	/*
        unsafe { self.buildings().any(|x| (*x).parent == unit.0) }*/
    }

/*	pub fn has_building_id(self, unit_id: UnitId) -> bool {
		unsafe {
			let mut building = (*self.0).buildings;
			while building != null_mut(){
				if (*building).parent != null_mut() && (*(*building).parent).unit_id == unit_id.0 {
					return true;
				}
				building = (*building).next;
			}
			return false;
		}	
	}*/

    pub fn buildings(self) -> impl Iterator<Item = *mut bw::BuildingAi> {
        unsafe { ListIter((*self.0).buildings) }
    }
     pub fn workers(self) -> impl Iterator<Item = *mut bw::WorkerAi> {
        unsafe { ListIter((*self.0).workers) }
    }
	 pub fn requests(self) -> impl Iterator<Item = bw::TownReq> {
        unsafe {
            (*self.0)
                .town_units
                .iter()
                .cloned()
                .take_while(|x| x.flags_and_count != 0)
        }
    }
}

unsafe impl Send for Town {}

impl Serialize for Town {
    fn serialize<S: Serializer>(&self, serializer: S) -> Result<S::Ok, S::Error> {
        use serde::ser::Error;

        //let array = bw::town_array_start();
        let array = bw::town_array() as *mut bw::AiTown;

		if array.is_null() {
            Err(S::Error::custom("Saving is not supported"))
        } else {
            let index = (self.0 as usize - array as usize) / mem::size_of::<bw::AiTown>();
            (index as u32).serialize(serializer)
        }
    }
	
}

impl<'de> Deserialize<'de> for Town {
    fn deserialize<S: Deserializer<'de>>(deserializer: S) -> Result<Self, S::Error> {
        use serde::de::Error;

        let id = u32::deserialize(deserializer)?;
        //let array = bw::town_array_start();
        let array = bw::town_array() as *mut bw::AiTown;

		if array.is_null() {
            Err(S::Error::custom("Saving is not supported"))
        } else {
            //unsafe { Ok(Town(bw::town_array_start().offset(id as isize))) }
			unsafe { Ok(Town(array.offset(id as isize))) }
		}
    }
}

pub unsafe extern fn set_free_id_town(globals: &mut Globals, raw_town: *mut bw::AiTown){
    if feature_disabled("set_free_id") {
        return;
    }	
    let town = match Town::from_ptr(raw_town) {
        Some(s) => s,
        None => {
            bw_print!("Used `set_free_id` without town");
            return;
        }
    };
    if globals.town_ids.iter().any(|x| town == x.town) {
        return;
    }
	let mut count = 254 as u8;
	loop {
		let find_id = globals
				.town_ids
				.iter()
				.any(|x| count == x.id);
		if !find_id {
			globals.town_ids.swap_retain(|x| x.town != town);
			globals.town_ids.push(TownId {
				town,
				id: count,
			});			
			//bw_print!("Free id {} is set",count);
			return;
		}
		if count <= 0 {
			//bw_print!("No free id left");
			break;
		}
		count-=1;
	}
}

pub unsafe extern fn set_free_id(script: *mut bw::AiScript){
	let mut globals = Globals::get("ais set_free_id");
    set_free_id_town(&mut globals, (*script).town);
}

pub unsafe extern fn set_town_id(script: *mut bw::AiScript) {
    let mut read = ScriptData::new(script);
    let id = read.read_u8();
    if feature_disabled("set_town_id") {
        return;
    }
    if id == 255 {
        bw_print!("Unsupported id {} in set_id", id);
        return;
    }
    let town = match Town::from_ptr((*script).town) {
        Some(s) => s,
        None => {
            bw_print!("Used `set_id {}` without town", id);
            return;
        }
    };
    let mut globals = Globals::get("ais set_id");
    globals.town_ids.swap_retain(|x| x.town != town);
    globals.town_ids.push(TownId {
        town,
        id,
    });
}



pub unsafe extern fn remove_build(script: *mut bw::AiScript) {
    let mut read = ScriptData::new(script);
    let amount = read.read_u8();
    let mut unit_id = read.read_unit_match();
    let id = read.read_u8();
    if feature_disabled("remove_build") {
        return;
    }
    let mut globals = Globals::get("ais remove_build");
    let town = town_from_id(script, &mut globals, id);
	
	
    if let Some(town) = town {
		//bw_print!("Current town data: pointer {:p}, current script: {:p}",town.0,(*script).town);
        remove_build_from_town(town, &mut unit_id, amount);
    }
	else {
		//bw_print!("Current town data: no pointer found, current script: {:p}",(*script).town);
	}
}

unsafe fn remove_build_from_town(town: Town, unit_id: &mut UnitMatch, amount: u8) {
    let town = town.0;
    let builds = (*town)
        .town_units
        .iter()
        .cloned()
        .take_while(|x| x.flags_and_count != 0)
        .collect::<Vec<_>>();

    let mut write_units = 0;
    for mut elem in builds {
        let mut write = true;
        let flags_and_count = elem.flags_and_count;
        if flags_and_count != 0 {
            if flags_and_count & 0x4 != 0 {
                //tech - implement later
            } else if flags_and_count & 0x2 != 0 {
                //upgrade - implement later
            } else {
                for unit in unit_id.iter_flatten_groups() {
                    if unit.0 == elem.id {
                        let mut count = flags_and_count / 8;
                        count = count.saturating_sub(amount);
                        if count != 0 {
                            elem.flags_and_count = (flags_and_count & 0x7) | (count << 3);
                        } else {
                            write = false;
                        }
                    }
                }
            }
        }
        if write {
            (*town).town_units[write_units] = elem;
            write_units += 1;
        }
    }
    for i in write_units..64 {
        if (*town).town_units[i].flags_and_count != 0 {
            (*town).town_units[i].flags_and_count = 0;
            (*town).town_units[i].id = 0;
            (*town).town_units[i].priority = 0;
        }
    }
}

pub unsafe extern fn max_workers(script: *mut bw::AiScript) {
    let mut read = ScriptData::new(script);
    let count = read.read_u8();
    if feature_disabled("max_workers") {
        return;
    }
    let town = match Town::from_ptr((*script).town) {
        Some(s) => s,
        None => {
            bw_print!("Used `max_workers {}` without town", count);
            return;
        }
    };
    let mut globals = Globals::get("ais max_workers");
    globals.max_workers.swap_retain(|x| x.town != town);
    if count != 255 {
        globals.max_workers.push(MaxWorkers {
            town,
            count,
        });
    }
}

pub extern fn max_workers_for(globals: &mut Globals, town: *mut bw::AiTown) -> Option<u8> {
    globals
        .max_workers
        .iter()
        .find(|x| x.town.0 == town)
        .map(|x| x.count)
}

pub unsafe extern fn under_attack(script: *mut bw::AiScript) {
    // 0 = Never, 1 = Default, 2 = Always
    let mut read = ScriptData::new(script);
    let mode = read.read_u8();
    if feature_disabled("under_attack") {
        return;
    }
    let player = (*script).player as usize;
    let mut globals = Globals::get("ais under_attack");
    globals.under_attack_mode[player] = match mode {
        0 => Some(false),
        1 => None,
        2 => Some(true),
        _ => {
            bw_print!("Invalid `under_attack` mode: {}", mode);
            return;
        }
    };
}

pub unsafe fn ngs_event_loop(hydra: &mut NewGameState, events: &mut Vec<LongEffect>, sprite_events: &mut Vec<SpriteEffect>){
	
	hydra.long_term_effects.swap_retain(|x| x.timer>0);
	
	
	for ef in &mut hydra.sprite_effects{
		ef.timer = ef.timer.saturating_sub(1);
		ef.global_timer = ef.global_timer.saturating_sub(1);
		ef.respawn_timer = ef.respawn_timer.saturating_sub(1);
		match ef.effect_id {

			HydraEffect::FeelingTheBern=>{
				let event = ef.to_owned();
				sprite_events.push(event);
				let sprite = ef.sprite.0;
				let position = (*sprite).position;				
				if ef.respawn_timer == 0 {	
					let mut count = 0;
					let x = position.x;
					let y = position.y;
					let x_block = x/32;
					let y_block = y/32;
					for i in x_block.saturating_sub(1) ..= x_block+1{
						for j in y_block.saturating_sub(1)..= y_block+1 {
							let result = match hydra.activist_hashmap.entry((i,j)) {
							   Entry::Vacant(entry) => entry.insert(Vec::new()),
							   Entry::Occupied(entry) => entry.into_mut(),
							};
							count += result.len();
						}
					}
					
					if count >= 1 {
						let sef = SpriteEffect{sprite: SerializableSprite(sprite), timer: 0, 
							global_timer: ef.global_timer, respawn_timer: 0, effect_id: HydraEffect::Activist_Respawn, position: position,
							source: ef.source, origin: ef.origin};
						sprite_events.push(sef);				
					}				
					let result = match hydra.activist_hashmap.entry((ef.origin.x/32,ef.origin.y/32)) {
					   Entry::Vacant(entry) => entry.insert(Vec::new()),
					   Entry::Occupied(entry) => entry.into_mut(),
					};	
					result.clear();
				}
				if ef.global_timer==0 {
					let result = match hydra.activist_hashmap.entry((ef.origin.x/32,ef.origin.y/32)) {
					   Entry::Vacant(entry) => entry.insert(Vec::new()),
					   Entry::Occupied(entry) => entry.into_mut(),
					};	
					result.clear();
						
				}
			},			
			_=>{},
		}
	}	
	hydra.sprite_effects.swap_retain(|x| x.respawn_timer>0);
	hydra.sprite_effects.swap_retain(|x| x.global_timer>0);
	for ef in &mut hydra.long_term_effects {
		ef.timer-=1;
		if ef.timer==0 {
			let event = ef.to_owned();
			events.push(event);
		}
		match ef.effect_id {
			HydraEffect::ScreamerAcidPool=>{
				let pool = bw::create_lone_sprite(369,ef.unit.position().x as u32,0,ef.unit.position().y as u32);
				if pool != null_mut() {
					let sprite = (*pool).sprite;
					(*sprite).elevation_level = 1;
					bw::update_ls_visibility(pool);
				}
				let ef = LongEffect{effect_id: HydraEffect::PoolHit, timer: 1, 
					position: ef.position, player: ef.player, energy: 0, unit_id: 0,
					unit: ef.unit, misc: 0, target: ef.unit};
				events.push(ef.to_owned());		
			},
			HydraEffect::VerminPoolLoop=>{
				/*if ef.unit.0 != null_mut() && *bw::elapsed_frames%8==0{
					bw::create_bullet(ef.position.x as u32, ef.position.y as u32,
																ef.unit.player() as u32,0,ef.unit.0,125);																	
				}
				*/
				let ef = LongEffect{effect_id: HydraEffect::PoolHit, timer: 1, 
					position: ef.position, player: ef.player, energy: 0, unit_id: 0,
					unit: ef.unit, misc: 0, target: ef.unit};
				events.push(ef.to_owned());	
			},
			_=>{},
		}
	}

	
}

#[derive(Copy, Clone)]
pub enum PhaseLink {
	On,
	Off,
}
#[derive(Copy, Clone)]
pub enum ExemplarMode {
	On,
	Off,
}

#[derive(Copy,Clone)]
pub enum CustomStim {
	AdrenalineRush,
	Bullet,
	RabidDog,
	Razor,
	DeadEye,
	AbsoluteZero,
	Reckoning,
}

pub unsafe fn custom_stim(game: Game, unit: *mut bw::Unit){
	let stim;
	let mut globals = Globals::get("custom_stim");
	//bw_print!("try stim");
	if get_new_upgrade_level_upg(game, &mut globals.upgrades, 91, (*unit).player)!=0 {
		stim = CustomStim::AdrenalineRush;
	}
	else if get_new_upgrade_level_upg(game, &mut globals.upgrades, 84, (*unit).player)!=0 {
		stim = CustomStim::Bullet;
	}
	else if get_new_upgrade_level_upg(game, &mut globals.upgrades, 85, (*unit).player)!=0 {
		stim = CustomStim::RabidDog;
	}
	else if get_new_upgrade_level_upg(game, &mut globals.upgrades, 86, (*unit).player)!=0 {
		stim = CustomStim::Razor;
	}
	else if get_new_upgrade_level_upg(game, &mut globals.upgrades, 87, (*unit).player)!=0 {
		stim = CustomStim::DeadEye;
	}	
	else if get_new_upgrade_level_upg(game, &mut globals.upgrades, 88, (*unit).player)!=0 {
		stim = CustomStim::AbsoluteZero;
	}	
	else if get_new_upgrade_level_upg(game, &mut globals.upgrades, 89, (*unit).player)!=0 {
		stim = CustomStim::Reckoning;
	}		
	else {
		stim = CustomStim::AdrenalineRush;
	}
	let mut stimmed=false;
	match stim {
		CustomStim::AdrenalineRush=>{
			//ported from GPTP, old "hydralisk" stimpack code
			if (*unit).unused8c == 0 {
				(*unit).hitpoints += 256*30;
				(*unit).unused8c = 24*40;//40 seconds, timer/health decremented in gptp
				//bw_print!("adrenaline");
				stimmed=true;
			}
			
		},
		CustomStim::Bullet=>{			
			if globals.ngs.get_generic_timer(unit,20006)==0 {
				//bw_print!("bullet");
				stimmed=true;
				globals.ngs.set_generic_timer(Unit::from_ptr(unit).unwrap(),20006,4*24);
			}
			
		},
		CustomStim::RabidDog=>{
			if globals.ngs.get_generic_timer(unit,20007)==0 {
				globals.ngs.set_generic_timer(Unit::from_ptr(unit).unwrap(),20007,12*24);
				//bw_print!("rabid dog");
				stimmed=true;
			}
			
		},
		CustomStim::Razor=>{
			//bw_print!("razor");
			if globals.ngs.get_generic_timer(unit,20010)==0 {
				globals.ngs.set_generic_timer(Unit::from_ptr(unit).unwrap(),20010,5*24);
				stimmed=true;
			}
			
		},
		CustomStim::DeadEye=>{
			//bw_print!("deadeye");
			if globals.ngs.get_generic_timer(unit,20011)==0 {
				globals.ngs.set_generic_timer(Unit::from_ptr(unit).unwrap(),20011,12*24);
				stimmed=true;
				
			}
			
		},
		CustomStim::AbsoluteZero=>{
			//bw_print!("absolute zero");
			if globals.ngs.get_generic_timer(unit,20012)==0 {
				globals.ngs.set_generic_timer(Unit::from_ptr(unit).unwrap(),20012,8*24	);
				stimmed=true;
			}
		
		},
		CustomStim::Reckoning=>{
			//bw_print!("reckoning");
			if globals.ngs.get_generic_timer(unit,20013)==0{
				globals.ngs.set_generic_value(Unit::from_ptr(unit).unwrap(),20013,1);
				stimmed=true;
			}		
		},
		_=>{},
	}
	if stimmed {
		let sound = globals.rng.synced_rand(0..2)+278;
		bw::playsound_unit(1,0,sound,unit);			
	}
	drop(globals);
}
pub unsafe fn ngs_events(events: Vec<LongEffect>, sprite_events: Vec<SpriteEffect>){
	for ef in sprite_events {
		match ef.effect_id {
			
			HydraEffect::FeelingTheBern=>{
				bw::create_bullet((*(ef.sprite.0)).position.x as u32, (*(ef.sprite.0)).position.y as u32,
															ef.source.player() as u32,0,ef.source.0,124);
			},
			HydraEffect::Activist_Respawn=>{
				let mut globals=Globals::get("ngs_events_activist");
				globals.ngs.feeling_the_bern(ef.source,ef.sprite.0,ef.origin.x as u32,ef.origin.y as u32,ef.global_timer);
				drop(globals);
			},
			_=>{
			
			},
		}
	}
	let game = Game::get();
	for ef in events {
		match ef.effect_id {
		HydraEffect::ActivateMedstims=>{
			//bw_print!("Medstim on");
			medstim_on(ef.unit.0);
						
		},
		HydraEffect::DeactivateMedstims=>{
			//bw_print!("Medstim off");
			medstim_off(ef.unit.0);
		},
		HydraEffect::SafetyOff=>{
			bw::send_aise_command_func(1,gptpid_to_u32(GptpId::SetCyprianStatus),50000,ef.unit.0,0,null_mut(),0);//2=SetCyprianStatus						
		},
		HydraEffect::SafetyOn=>{
			bw::send_aise_command_func(0,gptpid_to_u32(GptpId::SetCyprianStatus),50000,ef.unit.0,0,null_mut(),0);
		},
		HydraEffect::ApertureOn=>{
			aperture(ef.unit.0, DeployWarpPrism::On);
		},
		HydraEffect::ApertureOff=>{
			aperture(ef.unit.0, DeployWarpPrism::Off);
		},		
		HydraEffect::PhaseLinkOn=>{
			phaselink(ef.unit.0, PhaseLink::On);
		},
		HydraEffect::PhaseLinkOff=>{
			phaselink(ef.unit.0, PhaseLink::Off);
		},
		HydraEffect::Observance=>{
			observance(ef.unit.0);
		},
		HydraEffect::KhaydarinControlOn=>{
			khaydarin_control(ef.unit.0, ExemplarMode::On);
		},
		HydraEffect::KhaydarinControlOff=>{
			khaydarin_control(ef.unit.0, ExemplarMode::Off);
		},			
		HydraEffect::ExpelWasteOn=>{
			expelwaste(ef.unit.0, ExpelWaste::Start);
		},
		HydraEffect::ExpelWasteOff=>{
			expelwaste(ef.unit.0, ExpelWaste::Stop);
		},
		HydraEffect::TurbomodeOn=>{
			turbomode(ef.unit.0, Turbomode::Start);
		},
		HydraEffect::TurbomodeOff=>{
			turbomode(ef.unit.0, Turbomode::Stop);
		},		
		HydraEffect::ReverseThrustOn=>{
			generic_mode(ef.unit.0, 13, 1);
		},
		HydraEffect::ReverseThrustOff=>{
			generic_mode(ef.unit.0, 13, 0);
		},
		HydraEffect::SublimeShepherdOn=>{
			generic_mode(ef.unit.0, 14, 1);
		},
		HydraEffect::SublimeShepherdOff=>{
			generic_mode(ef.unit.0, 14, 0);
		},
		HydraEffect::TempoChangeOn=>{
			generic_mode(ef.unit.0, 45, 1);
		},
		HydraEffect::TempoChangeOff=>{
			generic_mode(ef.unit.0, 45, 0);
		},				
		HydraEffect::DisruptionWebOn=>{
			generic_mode(ef.unit.0, 15, 1);
		},
		HydraEffect::DisruptionWebOff=>{
			generic_mode(ef.unit.0, 15, 0);
		},	
		HydraEffect::AltitudeUp=>{
			generic_mode(ef.unit.0, 20000, 1);
		},
		HydraEffect::AltitudeDown=>{
			generic_mode(ef.unit.0, 20000, 0);
		},
		HydraEffect::MissilesOn=>{
			generic_mode(ef.unit.0, 20001, 1);
		},
		HydraEffect::MissilesOff=>{
			generic_mode(ef.unit.0, 20001, 0);
		},
		HydraEffect::BasiliskOn=>{
			antiair_mode_action(ef.unit.0);
//			generic_mode(ef.unit.0, 20002, 1);
		},
		HydraEffect::BasiliskOff=>{
			antiair_mode_action(ef.unit.0);
//			generic_mode(ef.unit.0, 20002, 0);
		},
		HydraEffect::AggressiveMiningOn=>{
			generic_mode(ef.unit.0, 1, 6);
		},
		HydraEffect::AggressiveMiningOff=>{
			generic_mode(ef.unit.0, 0, 6);
		},
		HydraEffect::CustomStim=>{
			custom_stim(game, ef.unit.0);
		},
		HydraEffect::ActivatePlanetcracker=>{
		
			let mut globals=Globals::get("ngs_events_planetcracker");
			planetcracker_action(ef.unit.0,&mut globals);
			drop(globals);
		},
		HydraEffect::BeginUpgrade=>{
			if bw::begin_upgrade(ef.misc as u32, ef.unit.0)!=0 {
				bw::issue_order_targ_nothing(ef.unit.0,bw_dat::order::UPGRADE.0 as u32);	
			}
		},
		HydraEffect::Impale=>{
			let mut globals=Globals::get("ngs_events_impale");
			let attacker = globals.ngs.get_generic_unit(ef.unit.0, 47);
			drop(globals);
			if let Some(attacker) = attacker {
				bw::create_bullet((*ef.unit.0).position.x as u32,(*ef.unit.0).position.y as u32,
						(*attacker.0).player as u32,0,attacker.0,123);
			}
		},
		HydraEffect::BlindJudgeResurrection=>{
			//bw_print!("Apply event!");
			let mut resurrected_ptr = bw::create_unit(ef.position.y as u32,ef.player as u32,ef.position.x as u32,ef.unit_id as u32) as *mut bw::Unit;
			if resurrected_ptr==null_mut(){
				//add later error msg
			}
			else {
				bw::finish_unit_pre(resurrected_ptr);
				bw::finish_unit(resurrected_ptr);
				(*resurrected_ptr).death_timer = 360;
				let unit_id=UnitId(ef.unit_id);
				(*resurrected_ptr).energy = ef.energy as u16;
				bw::set_hp(resurrected_ptr, unit_id.hitpoints() as u32/2);
				
			}
		},
		//todborne
		HydraEffect::UpdateSpeed=>{
			bw::update_speed(ef.unit.0);
		},
		HydraEffect::RemoveUnit=>{
//			bw_print!("Delete");
			(*ef.unit.0).order_flags |= 0x4;
			bw::kill_unit(ef.unit.0);	
		},
		HydraEffect::CreateUnit=>{//only for critter routes
			let mut globals = Globals::get("createunit_hydraef");
			let check = globals.idle_tactics.critter_routes[ef.misc as usize].state;
			drop(globals);
			if check != 1 {
				let unit = bw::create_unit(ef.position.y as u32, ef.player as u32,
										ef.position.x as u32, 
										ef.unit_id as u32) as *mut bw::Unit;					
				if unit != null_mut(){
					bw::finish_unit_pre(unit);
					bw::finish_unit(unit);
					let mut globals = Globals::get("createunit_hydraef");
					//smh
					globals.idle_tactics.critter_routes[ef.misc as usize].current_unit = Unit::from_ptr(unit);
					globals.idle_tactics.critter_routes[ef.misc as usize].state = 1;
					//bw_print!("Set state of route {} to 1",ef.misc);
	//				bw_print!("Spawn!");
				}
			}
		},
		HydraEffect::AppendOrder=>{
			bw::append_order_targeting_ground(ef.unit.0, ef.misc as u8, ef.position.x as u16, ef.position.y as u16, 0);
		},
		HydraEffect::IssueOrder=>{
			ef.unit.issue_order_ground(OrderId(ef.misc as u8), ef.position);
		},
		HydraEffect::AppendOrderUnit=>{
			bw::append_order_simple(ef.unit.0,ef.misc as u32,0,ef.target.0);
		},
		HydraEffect::UpdateMineralAnimation=>{
			bw::update_mineral_amount_animation(ef.unit.0);
		},
		HydraEffect::NextQueuedOrder=>{
			bw::do_next_queued_order(ef.unit.0);
		},
		HydraEffect::Stop=>{
			bw::issue_order_targ_nothing(ef.unit.0,0x1);
		},
		HydraEffect::KillUnit=>{
			bw::kill_unit(ef.unit.0);	
		},
		HydraEffect::PlanetcrackerBuildup=>{
			let mut globals=Globals::get("ngs_events_buildup");
			//bw_print!("buildup effect");
			globals.ngs.set_generic_value(ef.unit, 2, 1);
			globals.ngs.set_planetcracker_timer(ef.unit, 10*24);
			drop(globals);			
			
				debug!("play iscript anim buildup, unit id {}",(*ef.unit.0).unit_id);
			bw::play_iscript_anim(0x0D, 1, (*ef.unit.0).sprite);
			
			
		},
		HydraEffect::PlanetcrackerBurst=>{
			bw::create_bullet(ef.unit.position().x as u32, ef.unit.position().y as u32,
															ef.unit.player() as u32,0,ef.unit.0,123);
		},
		HydraEffect::VerminPoolEffect=>{
			let pool = bw::create_lone_sprite(369,ef.unit.position().x as u32,0,ef.unit.position().y as u32);
			if pool != null_mut() {
				let sprite = (*pool).sprite;
				(*sprite).elevation_level = 1;
				bw::update_ls_visibility(pool);
			}
			
			let mut globals=Globals::get("vermin_pool_effect");
			globals.ngs.place_overlord_pool(ef.unit,ef.unit.position().x as u32,ef.unit.position().y as u32,
											ef.unit.player() as u32);
			drop(globals);
		},
		HydraEffect::PoolHit=>{
//			bw_print!("Poolhit: {} {}",ef.unit.position().x,ef.unit.position().y);
			if ef.unit.0 != null_mut() && *bw::elapsed_frames%8==0{
				bw::create_bullet(ef.position.x as u32, ef.position.y as u32,
															ef.unit.player() as u32,0,ef.unit.0,125);		
			}
		},
		HydraEffect::VerminPoolHit=>{
			if ef.unit.0 != null_mut() && *bw::elapsed_frames%8==0{
					bw::create_bullet(ef.position.x as u32, ef.position.y as u32,
																ef.unit.player() as u32,0,ef.unit.0,125);																	
			}
		},
		HydraEffect::PlanetcrackerOff=>{
			let mut globals=Globals::get("ngs_events_crackoff");
			//bw_print!("[1] current generic value is {}",globals.ngs.get_generic_value(ef.unit.0,1));
			globals.ngs.set_generic_value(ef.unit, 1, 0);	
			globals.ngs.set_generic_value(ef.unit, 2, 0);
			//bw_print!("[2] current generic value is {}",globals.ngs.get_generic_value(ef.unit.0,1));
			drop(globals);		
			
			(*ef.unit.0).flags &= !0x1000;
			
			debug!("play iscript anim offcracker, unit id {}",(*ef.unit.0).unit_id);
			bw::play_iscript_anim(0x0A, 1, (*ef.unit.0).sprite);
			bw::removeoverlay(ef.unit.0, 942, 942);
		},
		HydraEffect::WarpFlash=>{
								//const u32* u32_006D1200 = (u32*)0x006D1200;

						//current_image->animation = IscriptAnimation::WarpIn;
						//current_image->iscriptOffset = *(u16*)(*u32_006D1200 + current_image->iscriptHeaderOffset + 0x32);
						//current_image->wait = 0;
						//current_image->unknown14 = 0;
						//iscript_OpcodeCases(current_image,(u32)&current_image->iscriptHeaderOffset,0,0);
			
			let sprite = (*ef.unit.0).sprite;
			let mut current_image = (*sprite).first_image;
			//bw_print!("Flash!");
			while current_image != null_mut(){
				if (*current_image).flags & 0x4 != 0{
					let iscript = 0x006d1200 as *mut u32;
					//bw_print!("Iscript!");
					(*current_image).iscript.animation_id = 0x15;
					
//					(*current_image).iscript.pos = *(*iscript+(*current_image).iscr + 0x32) as *mut u16;
					
					(*current_image).iscript.pos = *((*iscript).wrapping_add((*current_image).iscript.header as u32 + 0x32) as *mut u32) as u16;
					(*current_image).iscript.wait = 0;
					(*current_image).iscript.return_pos = 0;
					bw::progress_iscript_frame(&mut ((*current_image).iscript) as *mut bw::Iscript,0,0,current_image);
					
					//bw::play_iscript_anim(0x15, 1, (*ef.unit.0).sprite);
				}
				current_image = (*current_image).next;
			}		
		},
		
		HydraEffect::FeelingTheBern=>{
			//not handled here
		},
		HydraEffect::MindControl=>{
			bw::give_unit(ef.unit.0, ef.misc as u32);
			let mut overlay = 0;
			if ef.unit.id().has_medium_overlay(){
				overlay = 1;
			}
			else if ef.unit.id().has_large_overlay(){
				overlay = 2;
			}
			bw::add_overlay_above_main(0,0,0,(*ef.unit.0).sprite, 973+overlay);
		},
		HydraEffect::SeverityOverload=>{
			//bw_print!("Overload!");
			let area = bw::Rect {
					left: (ef.unit.position().x).saturating_sub(96),
					top: (ef.unit.position().y).saturating_sub(96),
					right: (ef.unit.position().x).saturating_add(96),
					bottom: (ef.unit.position().y).saturating_add(96),
				};	
			let unit_search = UnitSearch::from_bw();
			let units = unit_search
				.search_iter(&area)
				.filter(|u| *u!=ef.unit);
			//let mut debug_count=0;
			for u in units {
				//debug_count+=1;
				bw::send_aise_command_func(ef.misc as u32,gptpid_to_u32(GptpId::AddNSeverityStacks),50000,u.0, 0,null_mut(),0);
			}	
			//bw_print!("Apply effect to {} units",debug_count);
				
		},
		_=>{},
		}	
	}
}

#[derive(Debug, Serialize, Deserialize, Clone, Eq, PartialEq)]
pub enum GameCmd {
	None,
    ShamanAction,
	CyprianAction,
	RelicAction,
	Aperture,
	PhaseLink,
	PlanetCracker,
	CreateUnit,
	KhaydarinControl,
	CarrierPrebuilt,
	OverlordDamageField,
	Turbomode,
	CustomStim,
	
	GenericSwitchAbility,
	Observance,
	BombardmentMode,

	CullenBostonCreme,
	CullenDadwish,
	CullenMorningGlory,
	CullenRecall,
}
fn cmd_to_opcode(cmd: u32)->GameCmd {
	let result = match cmd {
		0x01 => GameCmd::ShamanAction,
		0x02 => GameCmd::CyprianAction,
		0x03 => GameCmd::RelicAction,
		0x04 => GameCmd::Aperture,
		0x05 => GameCmd::PhaseLink,
		0x06 => GameCmd::PlanetCracker,		
		0x07 => GameCmd::CreateUnit,
		0x08 => GameCmd::KhaydarinControl,
		0x09 => GameCmd::CarrierPrebuilt,
		0x0A => GameCmd::OverlordDamageField,
		0x0B => GameCmd::Turbomode,
		0x0C => GameCmd::CustomStim,
		0x0D => GameCmd::GenericSwitchAbility,
		0x0E => GameCmd::Observance,
		0x0F => GameCmd::BombardmentMode,
		0xFC => GameCmd::CullenBostonCreme,
		0xFD => GameCmd::CullenDadwish,		
		0xFE => GameCmd::CullenMorningGlory,
		0xFF => GameCmd::CullenRecall,
		_=> GameCmd::None,
	};
	result
}

unsafe fn medstim_on(unit: *mut bw::Unit){
	(*unit)._dc132 |= 0x8;
	(*unit)._dc133 = 24;
	bw::update_speed(unit);
//	bw::reduce_energy(bw_dat::tech::RESTORATION.energy_cost()<<8, unit);
}

unsafe fn medstim_off(unit: *mut bw::Unit){
	(*unit)._dc132 &= !0x8;
	bw::update_speed(unit);
}

unsafe fn is_ai(player: u8)->bool{
	if bw::player_list[player as usize].ty == 1 {
		return true;
	}
	false
}
unsafe fn player_res(action: ActionButton, game: Game, player: u8)->u32 {
	let result = match action {
		ActionButton::SendMinerals=>{
			game.minerals(player)
		},
		ActionButton::SendGas=>{
			game.gas(player)
		},
		_=>0,
	};
	result
} 

pub fn unit_strength(id: UnitId)->f64{
/*	let mut strength = (id.supply_cost() as f64)*5.0+((id.mineral_cost() as f64).sqrt())+((id.gas_cost() as f64).sqrt())+
					(id.hitpoints() as f64).sqrt()+
					((id.shields() as f64).sqrt() as f64)+(id.armor() as f64)*4.0;
	if let Some(weapon) = id.ground_weapon(){
		strength += (weapon.damage() as f64).sqrt();
	}
	if let Some(weapon) = id.air_weapon(){
		strength += (weapon.damage() as f64).sqrt();
	}	
	strength*/
	id.build_score() as f64
}

unsafe fn cease_fire_conditions_acceptable(game: Game, source: u8, target: u8)->bool {
	if game.allied(source,target){
		return false;
	}
	let mut manpower_source = 0.0 as f64;
	let mut manpower_target = 0.0 as f64;	
	for i in 0..UNIT_LIMIT {
		let id = UnitId(i);
		let strength = unit_strength(id);
		if !id.is_building(){
			manpower_source += (game.unit_count(source,id) as f64)*strength as f64;
			manpower_target += (game.unit_count(target,id) as f64)*strength as f64;	
		}
			
	}
	let delta = manpower_source/manpower_target;
	if delta < 1.5 {
		return false;
	}
	true
}

unsafe fn forced_to_surrender(game: Game, globals: &mut Globals, source: u8, target: u8)->bool {
	let mut kills_by_source = 0;
	let mut kills_by_target = 0; 
	let mut manpower_source = 0.0;
	let mut manpower_target = 0.0;
	let mut factories_source = 0;
	let mut factories_target = 0;
	let mut has_town_halls = false;
	for i in 0..UNIT_LIMIT {
		let id = UnitId(i);
		kills_by_source += globals.kills_table.count_kills(source, target, i);
		kills_by_target += globals.kills_table.count_kills(target, source, i);
		
		if id.is_town_hall() && game.unit_count(target,id)>0{
			has_town_halls=true;
		}
		if id.is_factory(){
			factories_source += game.unit_count(source,id);
			factories_target += game.unit_count(target,id);
		}
		else if !id.is_building() {
			let strength = unit_strength(id);	
			manpower_source += (game.unit_count(source,id) as f64)*strength;
			manpower_target += (game.unit_count(target,id) as f64)*strength;			
		}	
	}
	let kills_diff = kills_by_source.saturating_sub(kills_by_target);
	if manpower_source > manpower_target*5.0 && kills_diff>100{
		return true;
	}
	if factories_source > factories_target*10 && manpower_target < 800.0{
		return true;
	}
	if !has_town_halls && factories_target<8{
		return true;
	}
	if kills_by_source > 200 && factories_target<20{
		return true;
	}
	false
}

unsafe fn send_resources(action: ActionButton, amount: u32, game: Game, source: u8, target: u8){
	let resources_source = match action {
		ActionButton::SendMinerals => (*game.0).minerals.get_mut(source as usize),
		ActionButton::SendGas => (*game.0).gas.get_mut(source as usize),
		_=>{return;},
	};
	let resources_target = match action {
		ActionButton::SendMinerals => (*game.0).minerals.get_mut(target as usize),
		ActionButton::SendGas => (*game.0).gas.get_mut(target as usize),
		_=>{return;},
	};
	if let Some(resources) = resources_source {
		*resources = (*resources).saturating_sub(amount);
	}		
	if let Some(resources) = resources_target {
		*resources = (*resources).saturating_add(amount);
	}	
}

unsafe fn ai_offer_event(game: Game, globals: &mut Globals, source: u8, target: u8, action: ActionButton)->bool{
	let mut disposition = globals.ngs.diplomacy[source as usize].disposition[target as usize];
	let config=config::config();
	if config.overlord {
		let merchantmen = (*Game::all_units_count())[72][source as usize];//carrier-merchantman count
		disposition = disposition.saturating_add(merchantmen);
	}
	let result = match action {
		ActionButton::SendMinerals|ActionButton::SendGas=>{
			if disposition < 20 {
				false
			}
			else {
				true
			}
		},
		ActionButton::Ally=>{
			if disposition < 150 {
				false
			}
			else {
				true
			}
		},
		ActionButton::CeaseFire=>{
			if disposition > 85 && cease_fire_conditions_acceptable(game,source,target) {
				true
			}
			else {
				false
			}
		},
		ActionButton::Surrender=>{
			if forced_to_surrender(game,globals,source,target){
				true
			}
			else {
				false
			}
		},
		_=>false,
	};
	result
}
	
/*
					1=>ActionButton::SendMinerals,
					2=>ActionButton::SendGas,
					3=>ActionButton::Ally,
					4=>ActionButton::CeaseFire,
					5=>ActionButton::Surrender,
*/	
use std::ffi::CStr;
//extern crate hex;


unsafe fn accept_msg(color: u8, name: CString, gtc: u32){
	let string = format!("{}{} \x02\x04{}\0",
						color as char,name.to_str().unwrap(),"accept your proposition!");										
	let string = string.as_bytes();										
	let cstr = CStr::from_bytes_with_nul(string).unwrap();
	bw::print_text_raw(0x04,gtc,0,cstr.as_ptr() as *const i8);
}
unsafe fn desire_msg(color: u8, name:CString, gtc: u32, alliance: bool, value: u32){
	
	let mut string = match alliance {
		true=>format!("{}{} \x02\x04{} {} {}\0",
						color as char,name.to_str().unwrap(),"want",value,
						"minerals/gas more for alliance"),
		false=>format!("{}{} \x02\x04{} {} {}\0",
						color as char,name.to_str().unwrap(),"want",value,
						"minerals/gas more for cease-fire"),
	};
	if value == 0 {
		match alliance {
			true=>{
				string=format!("{}{} \x02\x04{}\0",
						color as char,name.to_str().unwrap(),"are ready to accept alliance.");
			},
			false=>{
				string=format!("{}{} \x02\x04{}\0",
						color as char,name.to_str().unwrap(),"are ready to accept cease-fire.");
			},
		}
	}
	let string = string.as_bytes();										
	let cstr = CStr::from_bytes_with_nul(string).unwrap();
	bw::print_text_raw(0x04,gtc,0,cstr.as_ptr() as *const i8);
}

unsafe fn hostility_msg(color: u8, name: CString, gtc: u32){
	let string = format!("{}{} \x02\x04{}\0",
						color as char,name.to_str().unwrap(),"have turned hostile!");//.as_bytes();*/										
	let string = string.as_bytes();										
	let cstr = CStr::from_bytes_with_nul(string).unwrap();
	bw::print_text_raw(0x04,gtc,0,cstr.as_ptr() as *const i8);
}
unsafe fn reject_msg(color: u8, name: CString, gtc: u32){
	let string = format!("{}{} \x02\x04{}\0",
						color as char,name.to_str().unwrap(),"reject your proposition!");//.as_bytes();*/	
	let string = string.as_bytes();										
	let cstr = CStr::from_bytes_with_nul(string).unwrap();
	bw::print_text_raw(0x04,gtc,0,cstr.as_ptr() as *const i8);
}
unsafe fn reject_msg_ceasefire(color: u8, name: CString, gtc: u32){
	let string = format!("{}{} \x02\x04{}\0",
						color as char,name.to_str().unwrap(),"refuse to talk with weak enemy!");//.as_bytes();*/	
	let string = string.as_bytes();										
	let cstr = CStr::from_bytes_with_nul(string).unwrap();
	bw::print_text_raw(0x04,gtc,0,cstr.as_ptr() as *const i8);
}
use globals::CommTowerMenu;
pub unsafe fn parse_commtower_back(menu: CommTowerMenu)->u32{
	let result = match menu {
		CommTowerMenu::Proposal=>93,
		CommTowerMenu::PickTargetForListening=>90,
		CommTowerMenu::General=>190,
		CommTowerMenu::Listening=>218,
		CommTowerMenu::PickTargetForOrders=>91,
		CommTowerMenu::Orders=>95,
		CommTowerMenu::PendingMessages|CommTowerMenu::MessageDetails=>0,
		CommTowerMenu::PickTargetForProposal=>90,
	};
	result
}
	
pub unsafe fn commtower_rally_point_refresh(globals: &mut Globals, unit: Unit){
	let position;
	if !((*unit.0).rally_point.x == 0 && (*unit.0).rally_point.y == 0) {
		position = (*unit.0).rally_point;	
		(*unit.0).rally_point.x = 0;
		(*unit.0).rally_point.y = 0;
	}
	else if (*unit.0).rally_unit != null_mut(){
		position = (*(*unit.0).rally_unit).position;
		(*unit.0).rally_unit = null_mut();
	}
	else {
		return;
	}
	globals.ngs.refresh_attack_to_orders(unit,position);
}
	
unsafe fn gamecmd_commtower(data: *const u8){
	use std::cmp;
	let player = *data.offset(1 as isize) as u8;
	let mut globals = Globals::get("commtower");
	let button_number = globals.ngs.last_button as u8;
	
//	let button_number = *data.offset(2 as isize) as u8;
	//1-9
	let mut player_button_list = Vec::new();
	for i in 0..8 {
		if i!=player {
			player_button_list.push(i);
		}
	}
	let game = Game::get();
	let gtc = *bw::next_frame_tick+7000;
	let unit = bw::player_selections[player as usize][0];
	
	if unit == null_mut(){
		return;
	}
	
	
	
	globals.ngs.comm_tower_setup(unit);
	//let buttons = (*unit).buttons;
	let buttons = *bw::current_button_set;
	//let current_listener_menu = globals.ngs.get_current_commtower_menu(unit);
	
	let buttonset = match buttons {
		93 => CommTowerMenu::Proposal,
		94 => CommTowerMenu::Listening,
		95 => CommTowerMenu::Orders,
		90 => CommTowerMenu::PickTargetForProposal,
		190 => CommTowerMenu::General,//psi disruptor/commtower id
		//should not happen with commands
		218 => CommTowerMenu::PickTargetForListening,
		91 => CommTowerMenu::PickTargetForOrders,		
		//should not matter for AttackTo and for Don't AttackTo
		//message details, pending messages - add later
		_=> {
			bw_print!("Incorrect buttonset {}",buttons);
			return;
		},
	};
	let previous = globals.ngs.get_listener_menu(unit);
	let target_player_id = player_button_list[(globals.ngs.get_target_player_id(unit)-1) as usize] as u8;
	//bw_print!("Previous: {:?}",previous);
	match previous {
	CommTowerMenu::PickTargetForListening=>{
		let action = match button_number {
			1=>ListeningButton::Start,
			2=>ListeningButton::Stop,	
			_=>{
				return;
			},
		};
		if action==ListeningButton::Start{
			//bw_print!("Start...{}",target_player_id);
			for i in 0..=7 {
				globals.ngs.diplomacy[player as usize].listening[i as usize] = false;
			} 
			globals.ngs.diplomacy[player as usize].listening[target_player_id as usize] = true;
		}
		else {
			//bw_print!("Stop...{}",target_player_id);
			globals.ngs.diplomacy[player as usize].listening[target_player_id as usize] = false;
		}	
	},
	CommTowerMenu::PickTargetForOrders=>{
		//bw_print!("Reset stuff");	
		globals.ngs.diplomacy[target_player_id as usize].set_attack_preference(None);
	},
	CommTowerMenu::PickTargetForProposal=>{
		
		match buttonset {
			CommTowerMenu::Proposal=>{
				//bw_print!("Proposal! {}",button_number);
				//1 - minerals
				//2 - gas
				//3 - alliance
				//4 - cease fire
				//5 - demand surrender
				let action = match button_number {
					1=>ActionButton::SendMinerals,
					2=>ActionButton::SendGas,
					3=>ActionButton::Ally,
					4=>ActionButton::CeaseFire,
					5=>ActionButton::Surrender,
					7=>ActionButton::AdjustResourcesSent,
					_=>{
						bw_print!("Incorrect button id {}",button_number);
						return;
					},
				};
				
				let resource_size = globals.ngs.get_resource_stack_size(unit);
				
				let faction = globals.ngs.diplomacy[target_player_id as usize].faction;
				let color = globals.ngs.factions.values().find(|x| faction == x.faction_id)
					.map(|x| x.color_index).unwrap();
				let name = globals.ngs.factions.values().find(|x| faction == x.faction_id)
					.map(|x| x.name.clone()).unwrap();	
				match action {
					ActionButton::SendMinerals|ActionButton::SendGas=>{
						let amount = cmp::min(resource_size, player_res(action,game,player));		
						if amount!=0 {
							if !is_ai(target_player_id){
//								bw_print!("Not ai");
								send_resources(action,amount,game,player,target_player_id);
								return;
							}															
							if globals.ngs.diplomacy[target_player_id as usize].alliance_status[player as usize] == true {
//								bw_print!("Allied");
								send_resources(action,amount,game,player,target_player_id);
								return;							
							}
							let event = ai_offer_event(game,&mut globals,player,target_player_id,action);
							if event {	
								accept_msg(color,name.clone(),gtc);
								send_resources(action,amount,game,player,target_player_id);
//								globals.ngs.adjust_disposition(player,target_player_id,10);
								let mut disp_value = 10.0 * (amount as f64 / 500.0);
								globals.ngs.adjust_disposition(player,target_player_id,disp_value as u32);
								let disposition = globals.ngs.diplomacy[player as usize]
									.disposition[target_player_id as usize];
								if game.allied(player,target_player_id){
									let diff = (150u32.saturating_sub(disposition))*50;
									desire_msg(color,name.clone(),gtc,true,diff);
								}
								else if cease_fire_conditions_acceptable(game,player,target_player_id) {
									let diff = 85u32.saturating_sub(disposition)*50;
									desire_msg(color,name.clone(),gtc,false,diff);
								}
									
								
								//bw_print!("Disposition value: {}",disp_value);
							}
							else {
								reject_msg(color,name.clone(),gtc);
							}
						}
					},
					ActionButton::Ally=>{
						//ALLY OPTION MUST BE REMOVED FOR NON-AI PLAYERS
						if !is_ai(target_player_id){
							//bw_print!("Not ai");
							return;
						}					
						let event = ai_offer_event(game,&mut globals,player,target_player_id,action);
						if event {
							accept_msg(color,name.clone(),gtc);
							globals.ngs.adjust_disposition(player,target_player_id,100);
							game.set_alliance(player,target_player_id,2);
							game.set_alliance(target_player_id,player,2);
							let _ = bw::modify_alliance_status(player as u32);
							let _ = bw::set_alliance();
							for i in 0..8 {
								if i != target_player_id && i!=player {
									(*game.0).alliances[target_player_id as usize][i as usize] = (*game.0).alliances[player as usize][i as usize];
									(*game.0).alliances[i as usize][target_player_id as usize] = (*game.0).alliances[i as usize][player as usize];																		
								}
							}
							bw::visions[player as usize] |= 1<<target_player_id;
							bw::visions[target_player_id as usize] |= 1<<player;
							globals.ngs.diplomacy[player as usize].alliance_status[target_player_id as usize] = true;
							globals.ngs.diplomacy[target_player_id as usize].alliance_status[player as usize] = true;						
						}
						else {
							reject_msg(color,name.clone(),gtc);
						}
					},
					ActionButton::CeaseFire=>{
						//CEASE FIRE OPTION MUST BE REMOVED FOR NON-AI PLAYERS
						if !is_ai(target_player_id){
							//bw_print!("Not ai");
							return;
						}					
						
						let event = ai_offer_event(game,&mut globals,player,target_player_id,action);					
						if event {
							accept_msg(color,name.clone(),gtc);	
							globals.ngs.adjust_disposition(player,target_player_id,50);
							game.set_alliance(player,target_player_id,2);
							game.set_alliance(target_player_id,player,2);
							let _ = bw::modify_alliance_status(player as u32);
							let _ = bw::set_alliance();							
						}
						else {
							if !cease_fire_conditions_acceptable(game,player,target_player_id){
								reject_msg_ceasefire(color,name.clone(),gtc);
							}
							else {
								reject_msg(color,name.clone(),gtc);
							}
							
						}
					},
					ActionButton::Surrender=>{//demand surrender
						//SURRENDER OPTION MUST BE REMOVED FOR NON-AI PLAYERS
						if !is_ai(target_player_id){
							//bw_print!("Not ai");
							return;
						}		
						let event = ai_offer_event(game,&mut globals,player,target_player_id,action);
						if event {
							accept_msg(color,name.clone(),gtc);			
							//
							//add later effect
							//
							bw::victory_status[target_player_id as usize] = 2;
							bw::victory_status2[target_player_id as usize] = 2;
							bw::visions[player as usize] |= 1<<target_player_id;
							bw::visions[target_player_id as usize] |= 1<<player;
							drop(globals);
							bw::neutralize(target_player_id as u32);
						}
						else {
							reject_msg(color,name.clone(),gtc);
						}						
						
						
					},
					ActionButton::AdjustResourcesSent=>{
						#[derive(Copy, Clone)]
						enum AdjOp {
							Increase,
							Decrease,
						}
						//bw_print!("Adjust");
						let keys = bw::get_modifier_key_states();
						/*if keys & 0x3 !=0 {
							bw_print!("Shift detected");
						}*/
						let shift = keys & 0x3;
						let op = match globals.ngs.set_right_click_status[player as usize]{
							false=>{
								
								//bw_print!("No rclick");
								AdjOp::Increase
							},
							true=>{
								//bw_print!("Rclick");
								AdjOp::Decrease
							},
						};
						let size = match shift {
							0=>100,
							_=>500,
						};
						match op {
							AdjOp::Increase=>{					
								globals.ngs.add_to_stack(unit,size);
							},
							AdjOp::Decrease=>{
								globals.ngs.add_to_stack(unit,-size);
								
							},
						}
						bw::update_ui();
					},
				}			
			},
			_=>{},
		}	
	
	},
	_=>{},
	}

	
}

unsafe fn gamecmd_shaman(data: *const u8){
	enum Medstims {
		Activate,
		Deactivate,
	}
	
	let action = *data.offset(2 as isize) as u8;
	let player = *data.offset(3 as isize) as u8;
	let action = match action {
		0=>Medstims::Activate,
		1=>Medstims::Deactivate,
		_=>{return;},
	};
	for i in 0..12 {
//		let unit = bw::client_selection[i];
		let unit = bw::player_selections[player as usize][i as usize]; 
		if unit != null_mut(){
			if (*unit).unit_id == 15 {//shaman
			match action {
					Medstims::Activate => {
						/*(*unit)._dc132 |= 0x8;
						(*unit)._dc133 = 24;
						bw::update_speed(unit);
						bw::reduce_energy(bw_dat::tech::RESTORATION.energy_cost()<<8, unit);*/
						medstim_on(unit);
						bw::update_ui();
					},
					Medstims::Deactivate => {
						/*(*unit)._dc132 &= !0x8;//remove medstim flag
						bw::update_speed(unit);*/
						//bw_print!("deactivate");
						medstim_off(unit);
						bw::update_ui();					
					},
				}
			}
		}
	}
	
}

fn is_cullen(id: u16)->bool{
	match id {
		20|22|23|25|29|98=>true,
		_=>false,
	}
}
pub unsafe fn anywhere_rect()->bw::Rect{
	let area = bw::location(63);
	let rect = bw::Rect {
		left: area.area.left as i16,
		top: area.area.top as i16,
		right: area.area.right as i16,
		bottom: area.area.bottom as i16,
	};	
	rect
}
unsafe fn gamecmd_bostoncreme(data: *const u8){
	let player = *data.offset(3 as isize) as u8;
	let action = *data.offset(2 as isize) as u8;
	let unit = bw::player_selections[player as usize][0];
	if action == 0 && unit != null_mut() {
		let mut globals = Globals::get("boston creme");
		let sound = globals.rng.synced_rand(0..2)+750;
		drop(globals);		
		bw::playsound_unit(1,0,sound,unit);
	}
}
unsafe fn gamecmd_dadwish(data: *const u8){
	let player = *data.offset(3 as isize) as u8;
	let action = *data.offset(2 as isize) as u8;
	let unit = bw::player_selections[player as usize][0];
	if action == 0 && unit != null_mut() {
		let mut globals = Globals::get("dadwish");
		let sound = globals.rng.synced_rand(0..2)+658;
		drop(globals);		
		bw::playsound_unit(1,0,sound,unit);
	}	
}
unsafe fn gamecmd_morning_glory(data: *const u8){
	let player = *data.offset(3 as isize) as u8;
	for i in 0..12 {
		let unit = bw::player_selections[player as usize][i as usize];
		let game = Game::get();
		if unit != null_mut(){
			bw::reduce_energy((bw::energy_cost_old[35] as u32) <<8,unit);
			let rect = bw::Rect {
				left: (*unit).position.x.saturating_sub(96) as i16,
				top: (*unit).position.y.saturating_sub(96) as i16,
				right: (*unit).position.x.saturating_add(96) as i16,
				bottom: (*unit).position.y.saturating_add(96) as i16,
			};
			let search = UnitSearch::from_bw();
			let targets = search
				.search_iter(&rect)
				.filter(|x| !game.allied((*unit).player,x.player()) && !x.is_invincible() && !x.id().is_building())
				.collect::<Vec<_>>();
				
			for i in targets{
				let img;
				if i.id().has_medium_overlay(){
					img = 983;
				}
				else if i.id().has_large_overlay(){
					img = 984;
				}
				else {
					img = 982;
				}
				bw::add_top_overlay(0,0,0,i.sprite().unwrap(),img);
				(*i.0).maelstrom_timer = 5*3;
				bw::disable_unit(i.0);
				bw::update_speed(i.0);
				let mut globals = Globals::get("morning glory");
				let sound = globals.rng.synced_rand(0..2)+746;
				drop(globals);
				bw::playsound_unit(1,0,sound,unit);
			}
		}
	}
}


unsafe fn gamecmd_oritemple_recall(data: *const u8){
	let player = *data.offset(3 as isize) as u8;
	for i in 0..12 {
		let unit = bw::player_selections[player as usize][i as usize];
		if unit != null_mut(){	
			if (*unit).unit_id == 168 {//oritemple
				let rect = anywhere_rect();			
				let search = UnitSearch::from_bw();
				let cullen = search
					.search_iter(&rect)
					.find(|x| x.player()==player && is_cullen(x.id().0));
				
				if let Some(cullen)=cullen{
					let previous_pos = cullen.position();
					bw::move_unit2(cullen.0,(*unit).position.x as u32,(*unit).position.y as u32);
					let mut new_pos = (*unit).position;
					bw::fix_target_location(cullen.id().0 as u32,&mut new_pos as *mut bw::Point);
					
					let recall = bw::create_lone_sprite(379,previous_pos.x as u32,0,previous_pos.y as u32);
					if recall != null_mut() {
						let sprite = (*recall).sprite;
						(*sprite).elevation_level = (*sprite).elevation_level + 1;
						bw::update_ls_visibility(recall);
					}
					bw::hide_unit_partial(cullen.0, 0);
					(*cullen.0).target = null_mut();				
					bw::move_unit(cullen.0,new_pos.x as u32,new_pos.y as u32);
					bw::finish_move_unit(cullen.0);
					cullen.issue_order(bw_dat::order::STOP, cullen.position(), None);
					let mut globals = Globals::get("oritemple recall");
					let sound = globals.rng.synced_rand(0..2)+550;
					drop(globals);
					bw::playsound_unit(1,0,sound,cullen.0);
					let recall = bw::create_lone_sprite(379,cullen.position().x as u32,0,cullen.position().y as u32);
					if recall != null_mut() {
						let sprite = (*recall).sprite;
						(*sprite).elevation_level = (*sprite).elevation_level + 1;
						bw::update_ls_visibility(recall);
					}					
					bw::reduce_energy((bw::energy_cost_old[0x21] as u32) <<8,unit); 			
				}
			}
		}
	}
}

use byteorder::ReadBytesExt;
unsafe fn gamecmd_custom_stim(data: *const u8){
	let player = *data.offset(2 as isize) as u8;
	let game = Game::get();
	
	for i in 0..12 {	
		let unit = bw::player_selections[player as usize][i as usize];
		//bw_print!("custom...{} {} {:p}",player,i,unit);
		if unit != null_mut(){
			//bw_print!("c");
			custom_stim(game, unit);										
			bw::update_speed(unit);
		}
	}
	bw::update_ui();
}


unsafe fn gamecmd_carrier_prebuilt(data: *const u8){
	let count = *data.offset(2 as isize) as u8;
	let player = *data.offset(3 as isize) as u8;
	let mut array = [0 as u8; 0x10];
	for i in 0..0x10 {
		array[i] = *data.offset(i as isize) as u8;
	}
	let carrier = (&array[0x4..]).read_u32::<LE>().unwrap() as *mut bw::Unit;
	if carrier != null_mut(){

		bw::add_to_hangar(carrier,count as u32);
	}
}

unsafe fn gamecmd_ued_createunit(data: *const u8){
	let mut array = [0 as u8; 0x10];
	for i in 0..0x10 {
		array[i] = *data.offset(i as isize) as u8;
	}
	let position_x = *data.offset(2 as isize) as u16;
	let position_y = *data.offset(4 as isize) as u16;
	let player = *data.offset(6 as isize) as u8;
	let unit_id = *data.offset(7 as isize) as u16;
	let source_unit = (&array[0x9..]).read_u32::<LE>().unwrap() as *mut bw::Unit;
	let mut position = bw::Point{ x: position_x as i16, y: position_y as i16};
	bw::fix_target_location(unit_id as u32, &mut position as *mut bw::Point);
	let unit_deployed = bw::create_unit(position.y as u32, player as u32,
										    position.x as u32, unit_id as u32) as *mut bw::Unit;
	bw_print!("UED create unit: x{} y{} {} {} {:p}",position_x,position_y,player,unit_id,source_unit);
	if unit_deployed == null_mut(){
		bw_print!("UED - unable to create unit");
		bw::show_last_error(player as u32);
	}			
	else {
		bw_print!("UED - finish creating unit");
		bw::finish_unit_pre(unit_deployed);
		bw::finish_unit(unit_deployed);
		if (*source_unit).ai != null_mut(){
			bw::inherit_ai2(source_unit, unit_deployed);
		}
		bw::rally_unit(source_unit,unit_deployed);
	}
	
}
unsafe fn planetcracker_action(unit: *mut bw::Unit, globals: &mut Globals){
	let starcaller = Unit::from_ptr(unit).unwrap();
	starcaller.issue_order(bw_dat::order::STOP, starcaller.position(), None);
	bw::reduce_energy((bw::energy_cost_old[37] as u32) <<8,unit);
	globals.ngs.set_generic_value(starcaller,1,1);
	globals.ngs.set_buildup_timer(starcaller,5*24);
	debug!("play planetcracker iscript anim unused1, unit id {}",(*unit).unit_id);
	bw::play_iscript_anim(0x04, 1, (*unit).sprite);
	(*unit).flags |= 0x1000;
}
unsafe fn gamecmd_planetcracker(data: *const u8, must_check_value: bool){
	let player = *data.offset(3 as isize) as u8;
	debug!("found");
	//bw_print!("found");
	for i in 0..12 {	
		let unit = bw::player_selections[player as usize][i as usize];
		if unit != null_mut(){
			debug!("unit found");
			//bw_print!("unit found");
			let mut globals = Globals::get("planet cracker");
			let value = globals.ngs.get_generic_value(unit,1);
			//bw_print!("planetcracker start: {}", value); 
			if value != 1 || !must_check_value {
				planetcracker_action(unit,&mut globals);

				
			}
		}
	}
}
#[derive(Copy, Clone)]
pub enum DeployWarpPrism {
	On,
	Off,
}

pub unsafe fn aperture(unit: *mut bw::Unit, action: DeployWarpPrism){
	let mut globals = Globals::get("aperture");
	match action {	
		DeployWarpPrism::On => {		
			if !((*unit).flingy_flags & 0x2 !=0 ){				
				globals.ngs.set_aperture_status(Unit::from_ptr(unit).unwrap(), true);
				drop(globals);
				bw::halt(unit as *mut bw::Flingy);
				bw::reduce_energy((bw::energy_cost_old[36] as u32) <<8,unit);
				(*unit).flags |= 0x1000;						
				bw::play_iscript_anim(0x04, 1, (*unit).sprite);					
			}
			else {
				drop(globals);
			}
	
		},
		DeployWarpPrism::Off => {
			globals.ngs.set_aperture_status(Unit::from_ptr(unit).unwrap(), false);
			drop(globals);
			
			debug!("play iscript anim prism off, unit id {}",(*unit).unit_id);
			bw::play_iscript_anim(0x0A, 1, (*unit).sprite);	
		},
	}		
	//bw::update_speed(unit);
	
}
pub unsafe fn phaselink(unit: *mut bw::Unit, action: PhaseLink){
	let mut globals = Globals::get("phaselink");
	let clarion = Unit::from_ptr(unit).unwrap();
	match action {	
		PhaseLink::On => {				
//			if (*unit).energy >= 256 {
				globals.ngs.set_phaselink_status(clarion, true);					
//			}
		},
		PhaseLink::Off => {
			globals.ngs.set_phaselink_status(clarion, false);
			globals.ngs.clear_clarion_links(clarion);
		},
	}		
	drop(globals);	
}

unsafe fn gamecmd_aperture(data: *const u8){
	let action = *data.offset(2 as isize) as u8;
	let player = *data.offset(3 as isize) as u8;
	let action = match action {
		0=>DeployWarpPrism::On,
		1=>DeployWarpPrism::Off,
		_=>{return;},
	};		
	for i in 0..12 {	
		let unit = bw::player_selections[player as usize][i as usize];
		if unit != null_mut(){
			if (*unit).unit_id == 29 {//warp relay 
				aperture(unit,action);
				bw::update_ui();		
			}			
		}
	}
}
unsafe fn gamecmd_phaselink(data: *const u8){
	let action = *data.offset(2 as isize) as u8;
	let player = *data.offset(3 as isize) as u8;
	let action = match action {
		0=>PhaseLink::On,
		1=>PhaseLink::Off,
		_=>{return;},
	};		
	for i in 0..12 {	
		let unit = bw::player_selections[player as usize][i as usize];
		if unit != null_mut(){
			if (*unit).unit_id == 81 {//clarion 
				phaselink(unit,action);		
				bw::update_ui();	
			}			
		}
	}
}

#[derive(Copy, Clone)]
pub enum Turbomode {
	Start,
	Stop,
}
#[derive(Copy, Clone)]
pub enum ExpelWaste {
	Start,
	Stop,
}
unsafe fn expelwaste(unit: *mut bw::Unit, mode: ExpelWaste){
	let mut globals = Globals::get("gamecmd_expel_waste");
	match mode {
		ExpelWaste::Start=>{
			globals.ngs.set_expel_status(Unit::from_ptr(unit).unwrap(),1);
		},
		ExpelWaste::Stop=>{
			globals.ngs.set_expel_status(Unit::from_ptr(unit).unwrap(),0);
		},
	}
	drop(globals);
	bw::update_speed(unit);
	bw::update_ui();	
}

unsafe fn turbomode(unit: *mut bw::Unit, action: Turbomode){
	let mut globals = Globals::get("gamecmd_turbomode");
	let unit = Unit::from_ptr(unit).unwrap();
	match action {
		Turbomode::Start=>{
			globals.ngs.set_generic_value(unit, 5, 1);
		},
		Turbomode::Stop=>{
			globals.ngs.set_generic_value(unit, 5, 0);
		},
	}
	drop(globals);
	bw::update_speed(unit.0);
	bw::update_ui();	
}

unsafe fn generic_mode(unit: *mut bw::Unit, state: u32, value_id: u32){
	let unit = Unit::from_ptr(unit).unwrap();
	let mut globals = Globals::get("gamecmd_generic_mode");
	globals.ngs.set_generic_value(unit, value_id, state as i32);
	drop(globals);
}

unsafe fn observance(unit: *mut bw::Unit){
	//from first version
	let mut globals = Globals::get("gamecmd_generic_mode");
	let unit = Unit::from_ptr(unit).unwrap();
	let game=Game::get();
	if unit.energy() as u32 >= (bw::energy_cost_old[39] as u32)<<8 {
		let rect = bw::Rect {
			left: unit.position().x.saturating_sub(128),
			top: unit.position().y.saturating_sub(128),
			right: unit.position().x.saturating_add(128),
			bottom: unit.position().y.saturating_add(128),
		};
		let unit_search = UnitSearch::from_bw();
		let units = unit_search
			.search_iter(&rect)
			.filter(|u| *u!=unit && !game.allied(u.player(),unit.player()) && !u.is_invincible() && !u.id().is_building())
			.collect::<Vec<_>>();
		for u in units {
			globals.ngs.set_generic_timer(u,21,1+(5*24));//timer
			let rend = globals.ngs.get_generic_value(u.0, 22);
			if rend<5 {
				globals.ngs.add_generic_value(u,22,1);//armor rend
			}
		}
		drop(globals);
		//bw::reduce_energy((bw::energy_cost_old[39] as u32) <<8,unit.0);
	}
	bw::update_speed(unit.0);
	bw::update_ui();	
}

unsafe fn gamecmd_observance(data: *const u8){
	let action = *data.offset(2 as isize) as u8;
	let player = *data.offset(3 as isize) as u8;
	let value = *data.offset(4 as isize) as u8;
	for i in 0..12 {
		let unit = bw::player_selections[player as usize][i as usize];
		if unit != null_mut(){
			observance(unit);
		}
	}	
}
pub unsafe fn antiair_mode_action(unit: *mut bw::Unit){
//clarion, exemplar, anti-air
	if unit != null_mut() && (*unit).sprite!=null_mut(){
		let id = (*unit).unit_id;
		//39 (Ultralisk) is Basilisk unit in Overlord
		//155 (CTFCop2) is Bombardment order in Overlord
		let mut correct_id = false;
		let config = config::config();
		if config.overlord && id==39 {
			correct_id = true;
		}/*
		if config.hydra_misc && (id==94 || id==81 || id==21){
			correct_id = true;
		}*/
		if correct_id && (*unit).order!=155 {
/*				bw::targeted_order(1, 0, 155, null_mut(), 
				*bw::unused_order_type_4_position_xy, *bw::unused_order_type_4_target, (*unit).position.x as u32, 
							(*unit).position.y as u32, 228, unit);    */	
			bw::issue_order_targ_nothing(unit,155);		
			(*unit).order_state = 0;
			(*unit).order_target_pos = (*(*unit).sprite).position;
		}		
	}
	
}
unsafe fn gamecmd_bombardment(data: *const u8){
	let player = *data.offset(2 as isize) as u8;
	let config = config::config();
	let mut clarion = null_mut();
	for i in 0..12 {
		let unit = bw::player_selections[player as usize][i as usize];
		antiair_mode_action(unit);
		if unit != null_mut() && (*unit).unit_id==81 {
			clarion = unit;
		}
	}	
	if clarion != null_mut(){
		let mut globals = Globals::get("gamecmd_bombardment");
		globals.ngs.set_generic_value(Unit::from_ptr(clarion).unwrap(), 109, 1);
		drop(globals);
	}
}
unsafe fn gamecmd_generic_mode(data: *const u8){
	let action = *data.offset(2 as isize) as u8;
	let player = *data.offset(3 as isize) as u8;
	let value = *data.offset(4 as isize) as u8;
	for i in 0..12 {
		let unit = bw::player_selections[player as usize][i as usize];
		if unit != null_mut(){
			generic_mode(unit,action as u32,value as u32);
		}
	}	
}
unsafe fn gamecmd_turbomode(data: *const u8){
	
	let action = *data.offset(2 as isize) as u8;
	let player = *data.offset(3 as isize) as u8;
	let action = match action {
		0=>Turbomode::Start,
		1=>Turbomode::Stop,
		_=>{return;},
	};	
	for i in 0..12 {
		let unit = bw::player_selections[player as usize][i as usize];
		if unit != null_mut(){
			if (*unit).unit_id == 79 {//mustang
				turbomode(unit,action);
			}
		}
	}	
}

unsafe fn gamecmd_expel_waste(data: *const u8){
	
	let action = *data.offset(2 as isize) as u8;
	let player = *data.offset(3 as isize) as u8;
	let action = match action {
		0=>ExpelWaste::Start,
		1=>ExpelWaste::Stop,
		_=>{return;},
	};	
	for i in 0..12 {
		let unit = bw::player_selections[player as usize][i as usize];
		if unit != null_mut(){
			if (*unit).unit_id == 47 {//explorer				
				expelwaste(unit,action);				
			}
		}
	}
}
unsafe fn gamecmd_cyprian(data: *const u8){
	enum Safety {
		Off,
		On,
	}
	let action = *data.offset(2 as isize) as u8;
	let player = *data.offset(3 as isize) as u8;
	let action = match action {
		0=>Safety::Off,
		1=>Safety::On,
		_=>{return;},
	};
	
	for i in 0..12 {
		let unit = bw::player_selections[player as usize][i as usize];
		if unit != null_mut(){
			if (*unit).unit_id == 92 {//cyprian 
				match action {
					Safety::Off => {
						bw::send_aise_command_func(1,gptpid_to_u32(GptpId::SetCyprianStatus),50000,unit,0,null_mut(),0);//2=SetCyprianStatus
						
					},
					Safety::On => {
						bw::send_aise_command_func(0,gptpid_to_u32(GptpId::SetCyprianStatus),50000,unit,0,null_mut(),0);
					},
				}				
				bw::update_speed(unit);
				bw::update_ui();				
			}
			else if (*unit).unit_id == 63 {//dark archon 
				//cancel mind control
				let mut globals = Globals::get("cancel_mind_control");
				globals.ngs.free_all(Unit::from_ptr(unit).unwrap());
				drop(globals);
				bw::update_ui();
			}
		}
	}
}


unsafe fn khaydarin_control(unit: *mut bw::Unit, action: ExemplarMode){
	let mut globals = Globals::get("exemplar switch");
	match action {
		ExemplarMode::On => {
		//	bw_print!("On");
			globals.ngs.set_generic_value(Unit::from_ptr(unit).unwrap(), 3, 1);
			
		},
		ExemplarMode::Off => {
		//	bw_print!("Off");
			globals.ngs.set_generic_value(Unit::from_ptr(unit).unwrap(), 3, 0);
		},
	}
	drop(globals);
	bw::update_speed(unit);
	
}



unsafe fn gamecmd_exemplar_switch(data: *const u8){
	let action = *data.offset(2 as isize) as u8;
	let player = *data.offset(3 as isize) as u8;
	let action = match action {
		0=>ExemplarMode::On,
		1=>ExemplarMode::Off,
		_=>{return;},
	};
	for i in 0..12 {	
		let unit = bw::player_selections[player as usize][i as usize];
		if unit != null_mut(){
			if (*unit).unit_id == 94 {
				khaydarin_control(unit, action);
				bw::update_ui();	
		
			}
		}
	}
}
pub unsafe extern fn comm_tower_hooks(
    data: *const u8,
    len: u32,
    _arg3: u32,
    _arg4: u32,
    orig: unsafe extern fn(*const u8, u32),
) {
	let config = config::config();
	if config.comm_tower_struct {
		gamecmd_commtower(data);
	}
	orig(data,len);
}


pub unsafe extern fn proc_test(
    data: *const u8,
    len: u32,
    _arg3: u32,
    _arg4: u32,
    orig: unsafe extern fn(*const u8, u32),
) {
	//bw_print!("Opcode 99");
	//0x99
	
//	bw_print!("Proc test: {} {} {}",*data,*data.offset(1 as isize),*data.offset(2 as isize));
	let config = config::config();
	
    let opcode = cmd_to_opcode(*data.offset(1 as isize) as u32);
    let mut save_to_replay = true;
    match opcode {
        GameCmd::GenericSwitchAbility=>{
            gamecmd_generic_mode(data);
        },
        GameCmd::BombardmentMode=>{
            gamecmd_bombardment(data);
        },
        _=> {
            save_to_replay = false;
        },
    }
    if (save_to_replay) {
        bw::add_to_replay_data(*bw::lobby_command_user, *bw::replay_data, len, data as *mut libc::c_void);
    }

    orig(data, len);
}
	
/*	
pub unsafe fn process_commands_hook(
	buflen: u32,
	replay_processframe: bool,
	buf: *const u8,
	orig: &dyn Fn(u32,bool,*const u8),){
		let mut old = false;
		if *buf != 0x37  && *buf!=0x36
		{
			bw_print!("Command: {} {} b {}",*buf,buflen,replay_processframe);
		}		
		if *buf != 0x15 {
			old = true;
		}
		else {
			if !(buf.offset(1 as isize) as u8 == 0xff && buf.offset(2 as isize) as u8==0xff) {
				old=true;
			}
		}

		
		if old {
			orig(buflen,replay_processframe,buf);
			return;
		}
		bw_print!("Nonstandard opcode");
		let mut total_len = buflen;
		while total_len > 0 {
			let command = buf.offset(1 as isize) as u8;//offset, 0 is stimpack
			
			let opcode = cmd_to_opcode(command);
			
			bw_print!("Receive: {}",command);
			let length = command_length(command as u32);
			bw_print!("Length: {} {}",length,total_len);
			total_len -= length;
			if total_len<0 || length<=0 {
				bw_print!("Invalid command opcode {}",command);
			}
			//replay stuff is not required, there are no replay commands
			match opcode {
			GameCmd::ShamanAction => {
				gamecmd_shaman(buf);
			},
			GameCmd::CyprianAction => {
				gamecmd_shaman(buf);
			},
			_=>{},
			}
			bw::add_to_replay_data(*bw::lobby_command_user,*bw::replay_data,length,buf as *mut libc::c_void);
		}
		
		

	}
	*/

	
//use fxhash::FxHashMap;
use std::collections::hash_map::Entry;
use fxhash::FxHashMap;

pub unsafe fn ngs_unit_hook(
    globals: &mut Globals,
    picked_unit: Unit,
) {
		let config=config::config();
		let mut single_effects: FxHashMap<HydraEffect,Unit> = FxHashMap::default();
		let game = Game::get();
		if picked_unit.id().0==83 {//reaver
			if config.reaver_autobuild && !globals.idle_tactics.in_reaver_micro(picked_unit){			
				if picked_unit.can_train_hangar(game) && has_resources(game,picked_unit.player(),&ai::unit_cost(UnitId(85))){
					(*picked_unit.0).build_queue[(*picked_unit.0).current_build_slot as usize] = 85;
					picked_unit.issue_secondary_order(OrderId(0x3f));
				}
				else {
					if (*picked_unit.0).secondary_order==0x3f {
						picked_unit.issue_secondary_order(OrderId(0x3a));//idle reaver
					}
				}
			}
		}
		match globals.ngs.new_units.entry(HashableUnit(picked_unit)) {
		   Entry::Vacant(_e) => {},
		   Entry::Occupied(entry) => {			
				let mut n = entry.into_mut();
				if n.blind_timer > 0 {
					n.blind_timer -= 1;
					if n.blind_timer == 0 {
						(*picked_unit.0).is_blind = 0;
						(*picked_unit.0)._dc132 |= 0x20;
						if ((*picked_unit.0).order==8 || (*picked_unit.0).order==10) && (*picked_unit.0).target!=null_mut(){
							if game.allied((*(*picked_unit.0).target).player, (*picked_unit.0).player) {
								single_effects.insert(HydraEffect::Stop,picked_unit);
							}
						}
					}
				}	
				/*
				for i in 0..8 {
					if n.parasite_timers[i] > 0 {
						n.parasite_timers[i] -= 1;
						if n.parasite_timers[i]==0 {
							(*picked_unit.0).parasited_by_players &= !(1<<i);
							(*picked_unit.0)._dc132 |= 0x20;
						}
					}						
				}
				if picked_unit.id().0==129 {		
					if config.remove_creeper {
						if !picked_unit.burrowed() && (((*picked_unit.0)._dc132 & 0x1)!=0) {
							(*picked_unit.0)._dc132 &= !0x1;
							if let Some(creeper) = n.creeper {
								(*creeper.0).order_flags |= 0x4;
								single_effects.insert(HydraEffect::RemoveUnit,creeper);
								n.creeper = None;
							}
						}			
					}			
				}*/
				for t in &mut n.madrigal_timers {
					*t = t.saturating_sub(1);
					if *t == 0 {
						single_effects.insert(HydraEffect::Impale,picked_unit);
					}
				}
				n.madrigal_timers.swap_retain(|x| *x > 0 );
				/*
				if config.moderator_behavior {
					if n.moderation_failsafe_timer > 0 {
						n.moderation_failsafe_timer -= 1;
						if n.moderation_failsafe_timer==0 {
							single_effects.insert(HydraEffect::UpdateSpeed,picked_unit);
							
						}
					}
					if n.goodboy_timer > 0 {
						n.goodboy_timer = n.goodboy_timer.saturating_sub(1);
						if n.goodboy_timer == 0 {
							single_effects.insert(HydraEffect::UpdateSpeed,picked_unit);
						}
					}
					
					
					if n.frosted_timer > 0 {
						n.frosted_timer = n.frosted_timer.saturating_sub(1);
						if n.frosted_timer == 0 {
							single_effects.insert(HydraEffect::UpdateSpeed,picked_unit);
						}
					}				
				}
				if n.void_timer > 0 {
					n.void_timer = n.void_timer.saturating_sub(1);
					if n.void_timer == 0 {			
						if n.safe_void {//hydra
							single_effects.insert(HydraEffect::WarpFlash,picked_unit);
						}
						else {//todborne
							single_effects.insert(HydraEffect::RemoveUnit,picked_unit);
						}
					}
				}*/
				if n.planetcracker_buildup > 0 {
					n.planetcracker_buildup = n.planetcracker_buildup.saturating_sub(1);
					if n.planetcracker_buildup == 0 {
						single_effects.insert(HydraEffect::PlanetcrackerBuildup,picked_unit);
					}
				}		
				if n.planetcracker_timer > 0 {
					n.planetcracker_timer = n.planetcracker_timer.saturating_sub(1);
					if n.planetcracker_timer % 12 == 0 {
						single_effects.insert(HydraEffect::PlanetcrackerBurst,picked_unit);
					}
					if n.planetcracker_timer == 0 {
						single_effects.insert(HydraEffect::PlanetcrackerOff,picked_unit);
						
					}
				}	
				for (_k,v) in &mut n.generic_timer {
					*v = v.saturating_sub(1); 
				}
				for (_u,t) in &mut n.sublime_shepherd_casters {
					*t = t.saturating_sub(1);
				}
				n.sublime_shepherd_casters.swap_retain(|(_u,t)| *t>0);
				/*
				for t in &mut n.moderation {
					*t = t.saturating_sub(1);
				}
				
				let old_stacks = n.moderation.len();
				n.moderation.swap_retain(|x| *x > 0);
				
				if config.overlord {
					if n.meskalloid.0 > 0 {
						n.meskalloid.0 -= 1;
					}
					if let Some(ref mut vermin) = n.vermin {
						if *vermin==0 {
							*vermin = 48;
							single_effects.insert(HydraEffect::VerminPoolEffect,picked_unit);
						}
						else {
							*vermin = *vermin-1;
						}	
						(*picked_unit.0).energy = (*picked_unit.0).energy.saturating_sub(21);
						if picked_unit.energy()==0{
							n.expelling_waste = None;
							n.vermin = None;
						}					
					} 	
				}
				if n.moderation.len()!=old_stacks {
					n.moderation_failsafe_timer=8;
					if n.moderation.len()==0 {
						n.can_overload=false;
					}
				}*/
			},
		}
		for (key,value) in single_effects {
			globals.ngs.add_single_effect(value.0,key);
		}
}	
pub unsafe fn ngs_hook(config: &Config, hydra: &mut NewGameState) {
	let single_effects: Vec<(HydraEffect,Unit)> = Vec::new();
	if config.listening_system {
		for i in 0..8 {
			for j in 0..8 {
				let ref mut timer = hydra.diplomacy[i as usize].listening_system[j as usize].generic_timer;
				*timer = timer.saturating_sub(1);
			}
		}
	}
	
	for u in &mut hydra.unit_structs{
		let mut timercheck = false;


		if config.hierophant_timer {
			for t in &mut u.hierophant_signify {
				if t.timer > 0 {
					timercheck = true;
				}
				t.timer = t.timer.saturating_sub(1);
			}		
		}
		if config.legionnaire_timer {
			for t in &mut u.legionnaire {
				t.timer = t.timer.saturating_sub(1);
			}		
		}

		if config.hierophant_timer {
			if u.hierophant_signify.len()>0 {
				(*u.unit.0)._dc132 |= 0x10;
			}
			let hierophant_check = u.hierophant_signify.len()!=0;
			u.hierophant_signify.swap_retain(|x| x.timer > 0);
			if u.hierophant_signify.len()==0 {
				if timercheck {
					u.signify_slowdown = false;
					u.malice_count = 0;
				}
				if hierophant_check {
					clear_signify_flags(u.unit);
				}

			}		
		}
		if config.legionnaire_timer {
			u.legionnaire.swap_retain(|x| x.timer > 0);
		}
	}
	for (ef,u) in single_effects {
		hydra.add_single_effect(u.0,ef);
	}
}
pub unsafe fn clear_signify_flags(u: Unit){
	(*u.0)._dc132 |= 0x10;
	(*u.0).unused8c |= 0x2;
}

pub unsafe fn queues_buildings_frame_hook(game: Game, unit_search: &UnitSearch){
	let globals = Globals::get("queues_buildings");
	if *bw::elapsed_frames % 30 == 0 && globals.queues.upgrade_queue.len()>0 {	
		
		let mut upgrades = globals.upgrades.clone();
		let mut queues = globals.queues.clone();
		drop(globals);
		for queue in &mut queues.upgrade_queue {
			let mut units = unit_search
				.search_iter(&anywhere_rect())
				.filter(|x| queue.can_upgrade(*x, get_new_upgrade_level_upg(game,&mut upgrades,queue.upgrade_id,queue.player), &mut upgrades))
				.collect::<Vec<_>>();	
			
			let cost = get_next_upgrade_cost(game, &mut upgrades, queue.upgrade_id, queue.player);
			for u in &mut units {
				
				if has_resources(game, u.player(), &cost){
					if bw::begin_upgrade(queue.upgrade_id as u32, u.0)!=0 {
						bw::issue_order_targ_nothing(u.0,bw_dat::order::UPGRADE.0 as u32);
					}
					//(*u.0).unit_specific[0x9] = queue.upgrade_id as u8;
					//bw::issue_order_targ_nothing(u.0,0x4c as u32);
				}
			}
		}		
	}

}
pub fn is_merger(id: UnitId)->bool {
	let config = config::config();
	if config.overlord {
		return false;
	}
	use bw_dat::unit::{ARCHON, DARK_ARCHON};
	let result = match id {
		//ARCHON | DARK_ARCHON => true,
		_=>false,
	};
	result
}
pub unsafe fn queues_frame_hook(queues: &mut Queues, upgrades: &mut Upgrades, ngs: &mut NewGameState, unit_search: &UnitSearch, game: Game) {
    use bw_dat::order::{ARCHON_WARP, DARK_ARCHON_MELD, TRAIN, UNIT_MORPH};
    use bw_dat::unit::{LARVA, DRONE};
    queues.queue.swap_retain(|x| x.current_quantity > 0);
	queues.upgrade_queue
		.swap_retain(|x| x.level > get_new_upgrade_level_upg(game,upgrades,x.upgrade_id,x.player));		
   
    for queue in &mut queues.queue {	
        let player = queue.player;
        let player_ai = ai::PlayerAi::get(player);

        if (*player_ai.0).request_count > 0 {
            let first_request = (*player_ai.0).requests[0];
            if queue.priority <= first_request.priority {
                continue;
            }
        }
        
        let mut q = 1;
        if queue.unit_id.two_in_egg(){
            q = 2;
        }

        let mut units = unit_search
            .search_iter(&queue.pos)
            .filter(|unit| queue.can_train(*unit) && bw::check_unit_dat_requirements(unit.player() as u32, queue.unit_id.0 as u32, unit.0) == 1 && unit.player() == player)
            .collect::<Vec<_>>();

        for unit in &mut units {
            if player_ai.has_resources(game, bw::players(), &ai::unit_cost(queue.unit_id)) {		
				let worker = queue.unit_id.is_worker();
				if worker {
                    // Morphing from worker
                    match unit.worker_ai() {
                        Some(ai) => {
                            let town = (*ai).town;
                            if town != null_mut() {
                                if bw::prepare_build_unit(queue.unit_id.0 as u32,unit.0)!=0 {
                                    queue.current_quantity = queue.current_quantity.saturating_sub(q);
                                    unit.issue_order(UNIT_MORPH, unit.position(), None);
                                }
                            }
                        },
                        None => {},
                    }

                    // Training/Morphing from larva
                    match unit.building_ai() {
                        Some(ai) => {
                            let town = (*ai).town;
                            if town != null_mut() {
                                if (*town).worker_count < (*town).worker_limit {
                                    if bw::prepare_build_unit(queue.unit_id.0 as u32,unit.0)!=0 {
                                        queue.current_quantity = queue.current_quantity.saturating_sub(q);
                                        if unit.id() == LARVA {
                                            unit.issue_order(UNIT_MORPH, unit.position(), None);
                                        } else {
                                            unit.issue_secondary_order(TRAIN);					
                                        }			
                                    }
                                }
                            }
                        },
                        None => {},
                    }
				} else {
					match unit.id().is_building() {
						true=>{
							let ai_region = bw::get_ai_region(unit.0);
							
							if bw::AiTrainUnit(queue.unit_id.0 as u32, 0x1, ai_region as *mut libc::c_void, unit.0) != 0 {
                                queue.current_quantity = queue.current_quantity.saturating_sub(q);
								unit.issue_secondary_order(TRAIN);
							}
						},
						false => {
                            let ai_region = bw::get_ai_region(unit.0);
							if unit.id() == LARVA {
							    let ai_region = bw::get_ai_region(unit.0);
								if bw::AiTrainUnit(queue.unit_id.0 as u32, 0x1, ai_region as *mut libc::c_void, unit.0) != 0 {	
                                    queue.current_quantity = queue.current_quantity.saturating_sub(q);
									unit.issue_order(UNIT_MORPH, unit.position(), None);
								}										
							} else {
                                if bw::prepare_build_unit(queue.unit_id.0 as u32,unit.0) != 0 {
                                    if unit.order() != UNIT_MORPH {
                                        queue.current_quantity = queue.current_quantity.saturating_sub(q);
                                        let ai_region = bw::get_ai_region(unit.0);
                                        bw::remove_unit_ai(unit.0, 0);
                                        bw::add_military_ai(true, ai_region, unit.0);
                                        debug!("Issue military  train to {}, current orders: {} {}",unit.id().0,unit.order().0,unit.secondary_order().0);
                                        unit.issue_order(UNIT_MORPH, unit.position(), None);										
                                    }
                                }
							}
						}
					}
					if queue.current_quantity == 0 {
						break;
					}		
				}
            }
        }
    }
}

pub unsafe fn lift_land_hook(lift_lands: &mut LiftLand, search: &UnitSearch, game: Game) {
    use bw_dat::order::{BUILDING_LAND, LIFTOFF, MOVE};
    lift_lands
        .structures
        .swap_retain(|x| x.stage() != LiftLandStage::End);
		
	
    for lift_land in &mut lift_lands.structures {
        if lift_land.state.is_none() {
            let unit = search
                .search_iter(&lift_land.src)
                .find(|x| x.id() == lift_land.unit_id && (lift_land.town_src.has_building(*x) || x.id()==UnitId(130)));
            if let Some(unit) = unit {
                lift_land.init_state(unit);			
            }
        }

        if let Some(ref mut state) = lift_land.state {
            let unit = state.unit;
            let target_area = match state.is_returning {
                false => lift_land.tgt,
                true => lift_land.src,
            };
            match state.stage {
                LiftLandStage::LiftOff_Start => {
                    if unit.hp_percent() >= lift_land.return_hp_percent as i32 {
                        unit.issue_order_ground(LIFTOFF, unit.position());
                        state.stage = LiftLandStage::LiftOff_End;
						//bw_print!("LIFTLAND: Start!");
                    }
                }
                LiftLandStage::LiftOff_End => {
                    if unit.order() != LIFTOFF {
                        state.stage = LiftLandStage::Fly;
                        state.is_returning = false;
                    }
                }
                LiftLandStage::Fly => {
                    if unit.hp_percent() < lift_land.return_hp_percent as i32 {
                        state.is_returning = true;
                    }
                    if unit_in_area(unit, target_area) {
                        state.stage = LiftLandStage::FindLocation;
                    } else {
                        unit.issue_order_ground(MOVE, bw::point_from_rect(target_area));
                    }
                }
                LiftLandStage::FindLocation => {
                    let rect_x = target_area.right / 32;
                    let rect_y = target_area.bottom / 32;
                    let pos_x = target_area.left / 32;
                    let pos_y = target_area.top / 32;
                    let offset_x = target_area.left - (pos_x * 32);
                    let offset_y = target_area.top - (pos_y * 32);
                    'outer: for i in pos_x..rect_x + 1 {
                        for j in pos_y..rect_y + 1 {
                            if check_placement(game, search, unit, i, j, unit.id(),false,bw::Point{x: (i*32)+offset_x,y: (j*32)+offset_y}) {
                                state.target = bw::Point {
                                    x: (i * 32) + offset_x,
                                    y: (j * 32) + offset_y,
                                };
                                unit.issue_order_ground(MOVE, state.target);
                                state.stage = LiftLandStage::Land;
                                break 'outer;
                            }
                        }
                    }
					
                }
                LiftLandStage::Land => {
                    if unit_in_area(unit, bw::Rect::from_point(state.target)) {						
                        if unit.is_air() {
                            let placement_ok = check_placement(
                                game,
                                search,
                                unit,
                                state.target.x / 32,
                                state.target.y / 32,
                                unit.id(),
								false,
								state.target,
                            );
                            if placement_ok {
								
                                if unit.order() != BUILDING_LAND {
                                    unit.issue_order_ground(BUILDING_LAND, state.target);
									//bw_print!("LIFTLAND: Land!");
                                }
                            } else {
                                state.stage = LiftLandStage::FindLocation;
                            }
                        } else {
                            match state.is_returning {
                                false => {
                                    let old_town = lift_land.town_src.0;
                                    let new_town = lift_land.town_tgt.0;
                                    if old_town != new_town {
                                        if unit.building_ai().is_some() {
                                            bw::remove_unit_ai(unit.0, 0);
                                            bw::add_town_unit_ai(unit.0, new_town);
                                        }
                                    }
                                    state.stage = LiftLandStage::End;
									//bw_print!("LIFTLAND: End!");
                                }
                                true => state.stage = LiftLandStage::LiftOff_Start,
                            }
                        }
                    }
					else if unit.is_air() && unit.order() != BUILDING_LAND {
						unit.issue_order_ground(MOVE, state.target);
						//bw_print!("LIFTLAND: Move!");
					}
                }
                _ => {}
            }
        }
    }
}

impl bw::Rect {
    pub fn from_point(point: bw::Point) -> bw::Rect {
        let x = point.x;
        let y = point.y;
        bw::Rect {
            left: x,
            right: x.saturating_add(1),
            top: y,
            bottom: y.saturating_add(1),
        }
    }
}

pub unsafe fn bunker_fill_hook(
    bunker_states: &mut BunkerCondition,
    picked_unit: Unit,
    search: &UnitSearch,
) {
    use bw_dat::order::*;
    if bunker_states.in_list(picked_unit) {
        return;
    }

    match picked_unit.order() {
        ENTER_TRANSPORT | AI_ATTACK_MOVE => {
            return;
        }
        _ => {}
    }
    for (decl, state) in &mut bunker_states.bunker_states {
        if decl.unit_id.matches(&picked_unit) {
            let units = search.search_iter(&decl.pos).filter(|u| {
                u.player() == decl.player &&
                    decl.bunker_id.matches(u) &&
                    u.player() == (*picked_unit.0).player
            });
            let mut used_bunkers = 0;
            for bunker in units {
                let cargo_capacity = bunker.id().cargo_space_provided();
                let cargo_amount = bunker.cargo_count();
                let units_targeted = state.count_associated_units(bunker).unwrap_or(0);
                let free_slots =
                    cargo_capacity.saturating_sub(u32::from(cargo_amount + units_targeted));

                let full = u32::from(cargo_amount) == cargo_capacity;
                if units_targeted > 0 || (full && units_targeted == 0) {
                    used_bunkers += 1;
                    if used_bunkers >= decl.bunker_quantity {
                        return;
                    }
                }
                if free_slots > 0 && decl.quantity > 0 {
                    picked_unit.issue_order_unit(ENTER_TRANSPORT, bunker);
                    state.add_targeter(picked_unit, bunker);
                    debug!(
                        "Add Targeter: uid {} at {} {} to pos {} {}",
                        bunker.id().0,
                        picked_unit.position().x,
                        picked_unit.position().y,
                        bunker.position().x,
                        bunker.position().y
                    );
                    return;
                }
            }
        }
    }
}

pub unsafe fn under_attack_frame_hook(globals: &mut Globals) {
    for (player, mode) in globals.under_attack_mode.iter().cloned().enumerate() {
        match mode {
            Some(true) => {

                (*bw::player_ai(player as u32)).previous_building_hit_second =
                    bw::elapsed_seconds().wrapping_sub(1);
            }
            Some(false) => {
                (*bw::player_ai(player as u32)).previous_building_hit_second = 0;
            }
            None => (),
        }
    }
}

#[derive(Serialize, Deserialize, Copy, Clone)]
pub struct AiMode {
    pub wait_for_resources: bool,
    pub build_gas: bool,
    pub retaliation: bool,
    pub focus_disabled_units: bool,
	pub move_from_baselayout: bool,								
}

impl Default for AiMode {
    fn default() -> AiMode {
        AiMode {
            wait_for_resources: true,
            build_gas: true,
            retaliation: true,
            focus_disabled_units: true,
//			move_from_baselayout: false,							   
			move_from_baselayout: true,
        }
    }
}

pub unsafe extern fn aicontrol(script: *mut bw::AiScript) {
    let mut read = ScriptData::new(script);
    let mode = read.read_u8();
    if feature_disabled("aicontrol") {
        return;
    }
    let player = (*script).player as usize;
    let mut globals = Globals::get("ais aicontrol");
    let out = &mut globals.ai_mode[player];

    match mode {
        0 => out.wait_for_resources = true,
        1 => out.wait_for_resources = false,
        2 => out.build_gas = true,
        3 => out.build_gas = false,
        4 => out.retaliation = true,
        5 => out.retaliation = false,
        6 => out.focus_disabled_units = true,
        7 => out.focus_disabled_units = false,
		8 => out.move_from_baselayout = true,
		9 => out.move_from_baselayout = false,									   
        _ => panic!("Invalid aicontrol {:x}", mode),
    };
}

pub unsafe extern fn notowns_jump(script: *mut bw::AiScript) {
	let mut read = ScriptData::new(script);
	let unit_id = UnitId(read.read_u16());
	let dest = read.read_jump_pos();
	let game = Game::get();
	if (*Game::completed_units_count())[unit_id.0 as usize][(*script).player as usize] == 0 {//original function used all_units_count
		(*script).pos = dest;
	}
}

pub unsafe extern fn goto(script: *mut bw::AiScript) {
    let mut read = ScriptData::new(script);
    (*script).pos = read.read_jump_pos();
}

pub unsafe extern fn call(script: *mut bw::AiScript) {
    let mut read = ScriptData::new(script);
    let dest = read.read_u16() as u32;
    if feature_disabled("everything_else") {
        return;	
    }
    let ret = (*script).pos;
    (*script).pos = dest;
    (*Script::ptr_from_bw(script)).call_stack.push(ret);
}

pub unsafe extern fn ret(script: *mut bw::AiScript) {
    let script = Script::ptr_from_bw(script);
    match (*script).call_stack.pop() {
        Some(s) => {
            (*script).bw.pos = s;
        }
        None => {
            bw_print!(
                "Script {} used return without call",
                (*script).debug_string()
            );
			debug!("Script {} used return without call",(*script).debug_string());
            (*script).bw.wait = !1;
            (*script).bw.pos -= 1;
        }
    }
}

pub unsafe extern fn do_morph(script: *mut bw::AiScript) {
    let mut read = ScriptData::new(script);
    let amount = read.read_u8();
    let unit_id = UnitId(read.read_u16());
    let globals = Globals::get("ais do_morph");
    let unit_id = globals.unit_replace.replace_check(unit_id); // replace_requests
	
	
    let player = (*script).player as u8;
	//globals.trains.push(TrainSequence{player: player, unit_id: unit_id, count: amount});
	
    if ai::count_units(player, unit_id, Game::get()) < u32::from(amount) {
        let ai = ai::PlayerAi::get(player);
        (*ai.0).train_unit_id = unit_id.0 + 1;
    }
}

pub unsafe extern fn train(script: *mut bw::AiScript) {
    let mut read = ScriptData::new(script);
    let amount = read.read_u8();
    let unit_id = UnitId(read.read_u16());
	let player = (*script).player as u8;
    let globals = Globals::get("ais train");
    let unit_id = globals.unit_replace.replace_check(unit_id); // replace_requests
    if ai::count_units(player, unit_id, Game::get()) < u32::from(amount) {
        let ai = ai::PlayerAi::get(player);
        (*ai.0).train_unit_id = unit_id.0 + 1;
        (*script).pos -= 4;
        (*script).wait = 30;
    }
}

#[derive(Default, Debug, Clone, Eq, PartialEq, Hash, Serialize, Deserialize)]
pub struct UnitMatch {
    pub units: Vec<UnitId>,
}
fn idmatch(id1: UnitId, id2: UnitId)->bool {
	match id2 {
		bw_dat::unit::ANY_UNIT => true,
		
		bw_dat::unit::GROUP_MEN => id1.group_flags() & 0x8 != 0,
		bw_dat::unit::GROUP_BUILDINGS => id1.group_flags() & 0x10 != 0,
		bw_dat::unit::GROUP_FACTORIES => id1.group_flags() & 0x20 != 0,
		id2 => id1 == id2,
	}
}
impl UnitMatch {
    pub fn iter_flatten_groups<'a>(&'a mut self) -> impl Iterator<Item = UnitId> + 'a {
        // Ineffective but w/e, simpler than ignoring duplicates
        if self.units.iter().any(|&x| x == unit::id::ANY_UNIT) {
            self.units = (0..unit::id::NONE.0).map(UnitId).collect();
        } else {
            let mut group_flags = 0;
            for &id in &self.units {
                group_flags |= match id {
                    unit::id::GROUP_MEN => 0x8,
                    unit::id::GROUP_BUILDINGS => 0x10,
                    unit::id::GROUP_FACTORIES => 0x20,
                    _ => 0x0,
                };
            }
            if group_flags != 0 {
                self.units.retain(|&unit_id| match unit_id {
                    x if x.0 >= unit::id::NONE.0 => false,
                    x => x.group_flags() & group_flags == 0,
                });
                let new_units = (0..unit::id::NONE.0)
                    .map(UnitId)
                    .filter(|x| x.group_flags() & group_flags != 0);
                self.units.extend(new_units);
            }
        }
        self.units.iter().cloned()
    }

    pub fn matches(&self, unit: &Unit) -> bool {
        self.units.iter().any(|&x| unit.matches_id(x))
    }
    pub fn matches_unit_id(&self, id: &UnitId) -> bool {
        self.units.iter().any(|&x| idmatch(*id,x))
    }
	
    pub fn get_one(&self) -> UnitId {
        self.units
            .iter()
            .cloned()
            .filter(|x| x.0 < unit::id::NONE.0)
            .next()
            .unwrap_or(UnitId(0))
    }
    fn replace_ids(&mut self, unit_replace: &mut UnitReplace) {
        for id in &mut self.units {
            *id = unit_replace.replace_check(*id);
        }
    }
}

#[derive(Default, Debug, Clone, Eq, PartialEq, Copy, Serialize, Deserialize)]
pub struct PlayerMatch {
    pub players: [bool; 12],
}

impl PlayerMatch {
    pub fn matches(&self, player: u8) -> bool {
        self.players.get(player as usize).cloned().unwrap_or(false)
    }
	
	
	pub fn match_with_script(&self, second: PlayerMatch) -> bool {
		for i in 0..12 {
			if !self.players[i] && second.players[i] {
				return false;
			}
		}
		true
	}
	pub fn match_bothdir(&self, second: PlayerMatch) -> bool {
		for i in 0..12 {
			if self.players[i]!=second.players[i] {
				return false;
			}
		}
		true
	}
    pub fn players<'a>(&'a self) -> impl Iterator<Item = u8> + 'a {
        self.players
            .iter()
            .enumerate()
            .filter(|x| *x.1 == true)
            .map(|x| x.0 as u8)
    }
}

pub unsafe extern fn supply(script: *mut bw::AiScript) {
    #[derive(Eq, PartialEq, Copy, Clone, Debug)]
    enum Race {
        Zerg,
        Terran,
        Protoss,
        Any,
    }

    #[derive(Eq, PartialEq, Copy, Clone, Debug)]
    enum SupplyType {
        Used,
        Provided,
        Max,
        InUnits,
    }

    #[derive(Eq, PartialEq, Copy, Clone, Debug)]
    enum Mode {
        Sum,
        Max,
    }
    let old_pos = (*script).pos - 1;
    let mut globals = Globals::get("ais supply");
    let game = Game::get();
    let mut read = ScriptData::new(script);
    let players = read.read_player_match(game);
    let modifier = read.read_modifier();
    let amount = (read.read_u16() as u32) * 2;
    let supply_type = read.read_u8();
    let units = read.read_unit_match();
    let race = read.read_u8();
    let dest = read.read_jump_pos();
    if feature_disabled("supply") {
        return;
    }
    let supply_type = match supply_type {
        0 => SupplyType::Provided,
        1 => SupplyType::Used,
        2 => SupplyType::Max,
        3 => SupplyType::InUnits,
        x => {
            bw_print!("Unsupported supply type in supply: {:x}", x);
            return;
        }
    };

    let (race, mode) = match race {
        0 => (Race::Zerg, Mode::Sum),
        1 => (Race::Terran, Mode::Sum),
        2 => (Race::Protoss, Mode::Sum),
        16 => (Race::Any, Mode::Sum),
        17 => (Race::Any, Mode::Max),
        x => {
            bw_print!("Unsupported race in supply: {:x}", x);
            return;
        }
    };

    match modifier.ty {
        ModifierType::Read(read) => {
            let mut sum = 0;
            match supply_type {
                SupplyType::InUnits => {
                    for unit in unit::active_units().chain(unit::hidden_units()) {
                        if players.matches(unit.player()) && units.matches(&unit) {
                            sum += unit.id().supply_cost();
                        }
                    }
                }
                _ => {
                    for race_id in 0..3 {
                        let race_matches = match race {
                            Race::Zerg => race_id == 0,
                            Race::Terran => race_id == 1,
                            Race::Protoss => race_id == 2,
                            Race::Any => true,
                        };
                        if race_matches {
                            for player in players.players() {
                                let supplies = &((*game.0).supplies[1]);
                                let amount = match supply_type {
                                    SupplyType::Provided => supplies.provided[player as usize],
                                    SupplyType::Used => supplies.used[player as usize],
                                    SupplyType::Max => supplies.max[player as usize],
                                    _ => 0,
                                };
                                if mode == Mode::Max {
                                    sum = sum.max(amount);
                                } else {
                                    sum += amount;
                                }
                            }
                        }
                    }
                }
            }
            read.compare_and_act(sum, amount, script, dest, old_pos);
        }
        ModifierType::Write(write) => {
            let race_i = match race {
                Race::Zerg => 1,
                Race::Terran => 1,
                Race::Protoss => 1,
                Race::Any => {
                    bw_print!("Only specific race supply can be modified.");
                    return;
                }
            };
            if supply_type == SupplyType::Max {
                for player in players.players() {
                    let supply_val = (*game.0).supplies[race_i].max.get_mut(player as usize);
                    if let Some(supply_val) = supply_val {
                        *supply_val = write.apply(*supply_val, amount, &mut globals.rng);
                    }
                }
            } else {
                bw_print!("Only Max supply can be modified.");
                return;
            }
        }
    }
}
#[derive(Copy, Clone)]
pub enum Resource {
        Ore,
        Gas,
}
pub unsafe extern fn container_jump(script: *mut bw::AiScript) {
    
   
    let old_pos = (*script).pos - 1;
    let game = Game::get();
    let mut read = ScriptData::new(script);
	let res = read.read_u8();
    let modifier = read.read_modifier();
    let amount = read.read_u32();
	let mut src = read.read_position();
    let radius = read.read_u16();
    src.extend_area(radius as i16);	
    let dest = read.read_jump_pos();
    if feature_disabled("container_jump") {
        return;
    }
    let resources_to_check: &[_] = match res {
        // Matching trigger conditions
        0 => &[Resource::Ore],
        1 => &[Resource::Gas],
        2 => &[Resource::Ore, Resource::Gas],
        x => {
            bw_print!("Unsupported resource type in resources: {:x}", x);
            return;
        }
    };
	
    let read = match modifier.ty {
        ModifierType::Read(r) => r,
        ModifierType::Write(w) => {
            bw_print!("Used writing modifier {:?} in resources", w);
            return;
        }
    };
    let search = aiscript_unit_search(game);
    let mut count = 0;
	let units = search
        .search_iter(&src.area)
        .filter(|u| u.id().is_resource_container());
	for u in units {
		for &res in resources_to_check {
			match res {
				Resource::Ore => {
					if u.id().0>=176 && u.id().0<=178 {
						count += u.resources();
					}
				},
				Resource::Gas => {
					if u.id() == unit::id::REFINERY || u.id()==unit::id::ASSIMILATOR ||
						u.id()==unit::id::EXTRACTOR || u.id()==unit::id::VESPENE_GEYSER {
						count += u.resources();
					}
				},
			};
        }
	}	
    read.compare_and_act(count as u32, amount, script, dest, old_pos);
	
}

pub unsafe extern fn resources_command(script: *mut bw::AiScript) {
    #[derive(Copy, Clone)]
    enum Resource {
        Ore,
        Gas,
    }
    let old_pos = (*script).pos - 1;
    let game = Game::get();
    let mut globals = Globals::get("ais resources");
    let mut read = ScriptData::new(script);
    let players = read.read_player_match(game);
    let modifier = read.read_modifier();
    let res = read.read_u8();
    let amount = read.read_u32();
    let dest = read.read_jump_pos();
    if feature_disabled("resources") {
        return;
    }
    let resources_to_check: &[_] = match res {
        // Matching trigger conditions
        0 => &[Resource::Ore],
        1 => &[Resource::Gas],
        2 => &[Resource::Ore, Resource::Gas],
        x => {
            bw_print!("Unsupported resource type in resources: {:x}", x);
            return;
        }
    };

    match modifier.ty {
        ModifierType::Read(read) => {
            let jump = players.players().any(|player| {
                resources_to_check.iter().any(|&res| {
                    let resvalue = match res {
                        Resource::Ore => game.minerals(player),
                        Resource::Gas => game.gas(player),
                    };
                    read.compare(resvalue, amount)
                })
            });
            let read_req = read.action.get_read_req();
            if jump == read_req {
                read.action.do_action(script, dest, old_pos);
            }
        }
        ModifierType::Write(write) => {
            for player in players.players() {
                for &res in resources_to_check {
                    let resources = match res {
                        Resource::Ore => (*game.0).minerals.get_mut(player as usize),
                        Resource::Gas => (*game.0).gas.get_mut(player as usize),
                    };
                    if let Some(resources) = resources {
                        *resources = write.apply(*resources, amount, &mut globals.rng);
                    }
                }
            }
        }
    }
}

pub unsafe fn reveal_vision_hook(globals: &mut Globals, game: Game) {
    for rev in &mut globals.reveal_states {
        if rev.time != 0 {
            rev.time -= 1;
            if rev.time == 0 {
                reveal(game, rev.pos, rev.players, false);
            }
        }
    }
    globals.reveal_states.swap_retain(|x| x.time > 0);
}

unsafe extern fn reveal(game: Game, area: bw::Rect, players: PlayerMatch, reveal: bool) {
    let tile_x = area.left / 32;
    let tile_y = area.top / 32;
    let limit_x = area.right / 32;
    let limit_y = area.bottom / 32;
    let map_width = (*game.0).map_width_tiles;
    for player in players.players().filter(|&x| x < 8) {
        for i in tile_x..=limit_x {
            for j in tile_y..=limit_y {
                let tile_flag =
                    (*bw::tile_flags).offset(i as isize + (map_width * j as u16) as isize);
                if reveal {
                    *tile_flag &= 0x1 << player;
                } else {
                    *tile_flag |= 0x100 << player;
                    *tile_flag |= 0x1 << player;
                }
            }
        }
    }
}

pub unsafe extern fn reveal_area(script: *mut bw::AiScript) {
    if bw::is_scr() {
        bw_print!("reveal_area is not supported in SCR");
        return;
    }
    let mut read = ScriptData::new(script);
    let game = Game::get();
    let mut globals = Globals::get("ais reveal_area");
    let players = read.read_player_match(game);
    let mut src = read.read_position();
    let radius = read.read_u16();
    src.extend_area(radius as i16);
    let time = read.read_u16();
    let flag = read.read_u8();
    if feature_disabled("reveal_area") {
        return;
    }
    let reveal_type = match flag {
        0 => RevealType::RevealFog,
        x => {
            bw_print!("Unsupported flag modifier: {:x}", x);
            return;
        }
    };
    if time != 0 {
        let reveal_state = RevealState {
            pos: src.area,
            time,
            reveal_type,
            players,
        };
        globals.reveal_states.push(reveal_state);
    }
    reveal(game, src.area, players, true);
}

fn get_bank_path(name: &str) -> Option<PathBuf> {
    let name = Path::new(name);
    let bad_path = name.components().any(|x| match x {
        Component::Prefix(..) | Component::RootDir | Component::ParentDir => true,
        Component::CurDir | Component::Normal(..) => false,
    });
    if bad_path {
        return None;
    }
    let root = if bw::is_scr() {
        UserDirs::new()
            .and_then(|user_dirs| user_dirs.document_dir().map(|s| s.join("Starcraft")))
            .unwrap_or_else(|| ".".into())
    } else {
        ".".into()
    };
    Some(root.join("save").join(name))
}

pub unsafe extern fn save_bank(script: *mut bw::AiScript) {
    let mut read = ScriptData::new(script);
    let name = read.read_string();
    let globals = Globals::get("ais save_bank");
    let name = String::from_utf8_lossy(name).to_string();
    let path = match get_bank_path(&name) {
        Some(s) => s,
        None => return,
    };
    let folder = Path::parent(&path);
    if let Some(s) = folder {
        let _ = fs::create_dir(s);
    }
    let mut file = match File::create(&path) {
        Ok(o) => o,
        Err(e) => {
            bw_print!("Bank load error: {}", e);
            return;
        }
    };
    if let Err(e) = bincode::serialize_into(&mut file, &globals.bank) {
        bw_print!("Bank save error: {}", e);
    }
}

pub unsafe extern fn load_bank(script: *mut bw::AiScript) {
    let mut read = ScriptData::new(script);
    let name = read.read_string();
    if feature_disabled("load_bank") {
        return;
    }
    let mut globals = Globals::get("ais load_bank");
    globals.bank.reset();
    let name = String::from_utf8_lossy(name);
    let path = match get_bank_path(&name) {
        Some(s) => s,
        None => return,
    };
    if path.exists() {
        let mut file = match File::open(path) {
            Ok(o) => o,
            Err(e) => {
                bw_print!("Bank load error: {}", e);
                return;
            }
        };
        globals.bank = match bincode::deserialize_from(&mut file) {
            Ok(o) => o,
            Err(e) => {
                bw_print!("Bank load error: {}", e);
                return;
            }
        };
    }
}

unsafe fn add_bank_data(
    script: *mut bw::AiScript,
    old_pos: u32,
    modifier: TriggerModifier,
    key: BankKey,
    amount: u32,
    dest: u32,
) {
    let mut globals = Globals::get("ais bank_data");
    let globals = &mut *globals;
    match modifier.ty {
        ModifierType::Read(read) => {
            let value = globals.bank.get(&key);
            read.compare_and_act(value, amount, script, dest, old_pos);
        }
        ModifierType::Write(write) => {
            let rng = &mut globals.rng;
            globals
                .bank
                .update(key, |val| write.apply(val, amount, rng));
        }
    }
}

pub unsafe extern fn bank_data_old(script: *mut bw::AiScript) {
    let old_pos = (*script).pos - 1;
    let mut read = ScriptData::new(script);
    let modifier = read.read_modifier();
    let category = read.read_string();
    let label = read.read_string();
    let amount = read.read_u32();
    let dest = read.read_jump_pos();
    let key = BankKey {
        label: String::from_utf8_lossy(label).to_string(),
        category: String::from_utf8_lossy(category).to_string(),
    };
    add_bank_data(script, old_pos, modifier, key, amount, dest);
}

pub unsafe extern fn bank_data(script: *mut bw::AiScript) {
    let old_pos = (*script).pos - 1;
    let mut read = ScriptData::new(script);
    let modifier = read.read_modifier();
    let amount = read.read_u32();
    let category = read.read_string();
    let label = read.read_string();
    let dest = read.read_jump_pos();
    let key = BankKey {
        label: String::from_utf8_lossy(label).to_string(),
        category: String::from_utf8_lossy(category).to_string(),
    };
    add_bank_data(script, old_pos, modifier, key, amount, dest);
}

pub unsafe extern fn remove_creep(script: *mut bw::AiScript) {
    if bw::is_scr() {
        bw_print!("remove_creep is not supported in SCR");
        return;
    }
    let mut read = ScriptData::new(script);
    let mut src = read.read_position();
    let radius = read.read_u16();
    if feature_disabled("remove_creep") {
        return;
    }
    src.extend_area(radius as i16);
    let rect_x = src.area.right / 32;
    let rect_y = src.area.bottom / 32;
    let pos_x = src.area.left / 32;
    let pos_y = src.area.top / 32;
	debug!("Remove creep: ({} {})~{}",src.center.x,src.center.y,radius,);
    for x_tile in pos_x..(rect_x + 1) {
        for y_tile in pos_y..(rect_y + 1) {
            let left = x_tile as u32 * 32;
            let top = y_tile as u32 * 32;
			//005153FC = func *GetRightClickOrder[0x7](), arg 1 y, arg 2 Unit **target, arg 3 unit_id, ecx Unit *unit, edx x

			
            unsafe extern "stdcall" fn nop(
                _x_tile: u32,
                _y_tile: u32,
                _area: *mut bw::Rect32,
            ) -> u32 {
                0
            }
            bw::remove_creep_at_unit(left, top, unit::id::EGG.0 as u32, nop);
        }
    }
}

pub unsafe extern fn time_command(script: *mut bw::AiScript) {
    enum TimeType {
        Frames,
        Minutes,
    }
    let old_pos = (*script).pos - 1;
    let game = Game::get();
    let mut read = ScriptData::new(script);
    let modifier = read.read_modifier();
    let amount = read.read_u32();
    let time_mod = read.read_u8();
    let dest = read.read_jump_pos();
    if feature_disabled("time") {
        return;
    }
    let time_mod = match time_mod {
        // Matching trigger conditions
        0 => TimeType::Frames,
        1 => TimeType::Minutes,
        x => {
            bw_print!("Unsupported time modifier in time: {:x}", x);
            return;
        }
    };
    let amount = match time_mod {
        TimeType::Minutes => amount * 960,
        TimeType::Frames => amount,
    };
    let time = game.frame_count();
    let read = match modifier.ty {
        ModifierType::Read(r) => r,
        ModifierType::Write(w) => {
            bw_print!("Used writing modifier {:?} in time", w);
            return;
        }
    };
    read.compare_and_act(time, amount, script, dest, old_pos);
}



pub unsafe extern fn attacking(script: *mut bw::AiScript) {
    let old_pos = (*script).pos - 1;
    let mut read = ScriptData::new(script);
    let modifier = read.read_bool_modifier();
    let dest = read.read_jump_pos();
    if feature_disabled("attacking") {
        return;
    }
    let ai = bw::player_ai((*script).player);
    let r_compare = ((*ai).attack_grouping_region != 0) == modifier.value;
    if r_compare == modifier.action.get_read_req() {
        modifier.action.do_action(script, dest, old_pos);
    }
}

fn unit_in_area(source: Unit, area: bw::Rect) -> bool {
    source.collision_rect().overlaps(&area)
}

/*
pub unsafe fn return_guard_hook(
	dont_issue_order: u32,
	unit: *mut bw::Unit,
	orig: &dyn Fn(u32,*mut bw::Unit)->u32,)->u32{
	
	let unit = match Unit::from_ptr(unit) {
		Some(s) => s,
		None => {
			return orig(dont_issue_order, unit);
		},
	};			
	let result = orig(dont_issue_order,unit.0);
	/*if unit.id().0==129 || unit.id().0==128 {
		bw_print!("Zerg morph Return Guard: arg1 {} result {}",dont_issue_order,result);
	}
	if unit.id().0==68 {
		bw_print!("Archon Return Guard: arg1 {} result {}",dont_issue_order,result);
	}	
	if unit.id().0==83 {
		bw_print!("Reaver Return Guard: arg1 {} result {}",dont_issue_order,result);
	}
	if unit.id().0==67 {
		bw_print!("High templar Return Guard: arg1 {} result {}",dont_issue_order,result);
	}*/
	result
}
*/

/*
pub unsafe fn order_computerguard(
	unit: *mut bw::Unit,
	orig: &dyn Fn(*mut bw::Unit),){
	
	let unit = match Unit::from_ptr(unit) {
		Some(s) => s,
		None => {
			orig(unit);
			return;
		},
	};
	/*
	if unit.id().0==129 || unit.id().0==128 {
		bw_print!("Zerg morph Guard");
	}
	if unit.id().0==68 {
		bw_print!("Archon Guard");
	}	*/
	if unit.id().0==83 {
		bw_print!("Reaver Guard");
	}
	/*
	if unit.id().0==67 {
		bw_print!("High templar Guard");
	}*/
	orig(unit.0);
}
*/

	//00488BF0 = ModifyUnitCounters(), arg 1 unit_count_diff, eax Unit *unit
	
pub unsafe fn can_load_unit_hook(
	unit: *mut bw::Unit,
	transport: *mut bw::Unit,
	orig: &dyn Fn(*mut bw::Unit,*mut bw::Unit)->u32)->u32 {
		let config = config::config();
		if !config.reliquary_hero_loading {
			return orig(unit,transport);
		}
		
		let unit = match Unit::from_ptr(unit) {
			Some(s) => s,
			None => {
				return orig(unit, transport);
			},
		};
		let transport = match Unit::from_ptr(transport) {
			Some(s) => s,
			None => {
				return orig(unit.0, transport);
			},
		};
		if !transport.is_completed() || transport.is_disabled() || unit.is_burrowed() || unit.player()!=transport.player() {
			return 0;
		}
		if transport.id().is_building(){
			/*
			if unit.id().is_terran(){
				return false;
			}
			*/
			if unit.id().cargo_space_required() > 1 && transport.id().0 != 175 {//reliquary check
				return 0;
			}		
			if transport.id().0 == 175 {
				if !unit.id().is_hero() {
					return 0;
				}			
				if transport.cargo_count()>1 {
					return 0;
				}
			}		
		}
		let cargo_capacity = transport.id().cargo_space_provided();
		let mut cargo_weight: u32 = 0;
		for i in 0..8 {
			let ptr = bw::get_loaded_unit(transport.0, i);
			if ptr != null_mut(){
				let uid = UnitId((*ptr).unit_id);
				cargo_weight = cargo_weight.saturating_add(uid.cargo_space_required());
			}
		}
		//bw_print!("Weight: {}, Free Space: {} Required: {}", cargo_weight, cargo_capacity.saturating_sub(cargo_weight), unit.id().cargo_space_required());
		if cargo_capacity.saturating_sub(cargo_weight) < unit.id().cargo_space_required(){
			return 0;
		}
		return 1;
		//return orig(unit.0,transport.0);
	}
	
	
	

	
	/*
pub unsafe fn gen_minitile_walkability_arr(
	orig: &dyn Fn()->u32)->u32{
		let mut count = *bw::megatile_count;
		*bw::megatile_count = 65535;
		let result = orig();
		*bw::megatile_count = count;
		result
	}
	*/
	
/*pub unsafe fn init_game_network_hook(
	orig: &dyn Fn()->u32,)->u32{
		let result = orig();
		/*
		for i in 0..8 {
			if (bw::player_list[i as usize].ty == 6 || bw::player_list[i as usize].ty==0){
				bw::player_list[i as usize].ty = 1;
				debug!("N Set player {} as computer",i);
			}
		}*/
		result
	}*/
	
	
pub unsafe fn command_startgame_hook(
	player: u32,
	orig: &dyn Fn(u32),){
		
		orig(player);
		
		for i in 0..8 {
			if bw::player_list[i as usize].ty == 6 || bw::player_list[i as usize].ty==0 {
				bw::player_list[i as usize].ty = 1;
				debug!("P Set player {} as computer",i);
			}
		}
	}



	

	
	/*
pub unsafe fn minimap64(
	orig: &dyn Fn(),){
		//orig();
		let game = Game::get();
		let mut mi_width = *bw::minimap_width;
		mi_width /= 2;
		let mut minimap_width = *bw::minimap_image_width;
		if mi_width == 0 {
			return;
		}		
		let minimap_image_height = *bw::minimap_height;
		minimap_width *= 2;
		let dif = minimap_width-minimap_image_height;
		
		//test ebx,ebx
		while mi_width != 0 {
		
			let mut mi_height = minimap_image_height/2;
			let mut minimap_image_offset = 0;	
			
			
			while mi_height!=0 {
				let mut minitile_data_offset = 0;
				let mut megatile_offset = 0;
				let mut megatile = *(*bw::megatiles).offset(megatile_offset as isize);
				minimap_image_offset += 2;
				megatile_offset += 1;//should be word/dword? originally 2
				megatile &= 0x7fffffff;//previously 0x7fff
				//megatile *= 32;//64? offset not required not
				let mut mtv = (*(*bw::megatile_data).offset(megatile as isize)).minitiles[0];
				mtv &= 0xfffffffe;//vx4
				//mtv *= 64;//vx4, originally 32 offset not required not
				let mini = (*(*bw::minitile_data).offset(mtv as isize)).colors[0x37];
				let color = bw::terrain_to_minimap_color[mini as usize];
				*(*bw::minimap_colors).offset((minimap_image_offset-2) as isize) = color;
				megatile = *(*bw::megatiles).offset(megatile_offset as isize);
				megatile &= 0x7fffffff;
				//megatile *= 32;//64? offset not required not
				let mut mtv = (*(*bw::megatile_data).offset(megatile as isize)).minitiles[4];//4 is 8/2 originally
				mtv &= 0xfffffffe;
				//mtv *= 64;//originally 32, shl not required in the hook
				let mini = (*(*bw::minitile_data).offset(mtv as isize)).colors[0x37];
				let color = bw::terrain_to_minimap_color[mini as usize];
				*(*bw::minimap_colors).offset((minimap_image_offset+minimap_image_height-2) as isize)=color;
				
				megatile = *(*bw::megatiles).offset((megatile_offset-1) as isize);//originally 1 offset
				megatile &= 0x7fffffff;
				//shl edx,5 -> shl edx,6, should be handled by arrays
				let mut mtv = (*(*bw::megatile_data).offset(megatile as isize)).minitiles[0];
				mtv &= 0xfffffffe;
				let mini = (*(*bw::minitile_data).offset(mtv as isize)).colors[0x37];
				let color = bw::terrain_to_minimap_color[mini as usize];	
				*(*bw::minimap_colors).offset((minimap_image_offset-1) as isize)=color;
				megatile = *(*bw::megatiles).offset((megatile_offset-1) as isize);
				megatile &= 0x7fffffff;
				//shl edx,5
				let mut mtv = (*(*bw::megatile_data).offset(megatile as isize)).minitiles[5];
				mtv &= 0xfffffffe;
				//shl edx,5
				let mini = (*(*bw::minitile_data).offset(mtv as isize)).colors[0x37];
				let color = bw::terrain_to_minimap_color[mini as usize];
				
				
				*(*bw::minimap_colors).offset((minimap_image_offset+minimap_image_height-1) as isize)=color;
				mi_height -= 1;
			}
			mi_height = minimap_image_height/2;
			minimap_image_offset += dif;
			mi_width -=1;
		}
		//edx is color [dl]
		//esi is megatile data (must be vx4ex)
		//ebx is  minimap height/2 
		//edi is minimap height
		//ecx is megatiles pointer
		//eax is *minimap image pointer+offsets+dif
		//[ebp-10] is (minimap width*2 - minimap height/2) 
		//[ebp-c] is minimap height/2 
		//[ebp-8] is width
		//[ebp-4] is *(minimap height/2)
		
	}
	*/
	
pub unsafe fn unit_counters1_hook(
	diff: i32,
	unit: *mut bw::Unit,
	orig: &dyn Fn(i32,*mut bw::Unit),){
		let unit = match Unit::from_ptr(unit) {
			Some(s) => s,
			None => {
				orig(diff, unit);
				return;
			},
		};		
		if unit.is_hallucination(){
			return;
		}
		if unit.id().is_subunit(){
			return;
		}
		let game = Game::get();
		if unit.player() > 11 {
			bw_print!("Error: Unit with unit player {} was passed to unit counters1 function",unit.player());
			return;
		}
		let unit_count = game.unit_count(unit.player(),unit.id());
		let new_count = unit_count+diff;
		(*Game::all_units_count())[unit.id().0 as usize][unit.player() as usize] = new_count as u32;
		//use bw_dat::unit::{EGG, LURKER_EGG, COCOON};
		let config = config::config();
		if unit.id().is_zerg(){
			let egg_jump = config.eggs.single_list.clone().iter_mut().find(|x| x.0 == unit.id().0).is_some();
			let mut supply = unit.id().supply_cost() as i32;
			if egg_jump {
				if !unit.build_queue_empty(){
					let train_id = unit.current_train_id();
					supply = train_id.supply_cost() as i32;
					if train_id.two_in_egg() {
						supply *= 2;	
					}			
				}
				else {
					if unit.id().two_in_egg() && !unit.is_completed() {
						supply *= 2;	
					}							
				}
			}
			
			supply *= diff;
			let used = ((*game.0).supplies[0].used[unit.player() as usize] as i32) + supply;				
			(*game.0).supplies[0].used[unit.player() as usize] = used as u32;		
		}
		else if unit.id().is_terran(){
			let mut supply = unit.id().supply_cost() as i32;
			supply *= diff;
			let used = ((*game.0).supplies[1].used[unit.player() as usize] as i32) + supply;
			(*game.0).supplies[1].used[unit.player() as usize] = used as u32;			
		}
		else if unit.id().is_protoss(){
			let mut supply = unit.id().supply_cost() as i32;
			supply *= diff;
			let used = ((*game.0).supplies[2].used[unit.player() as usize] as i32) + supply;
			(*game.0).supplies[2].used[unit.player() as usize] = used as u32;			
		}		
		if unit.id().is_factory(){
			let score = ((*game.0).factories_built_score[unit.player() as usize] as i32) + diff;
			(*game.0).factories_built_score[unit.player() as usize] = score as u32;
		}		
		if unit.id().men_flag(){
			let score = ((*game.0).unit_count_score[unit.player() as usize] as i32) + diff;
			(*game.0).unit_count_score[unit.player() as usize] = score as u32;			
		}
		else if unit.id().is_building(){
			let score = (bw::score_structures_total[unit.player() as usize] as i32) + diff;
			bw::score_structures_total[unit.player() as usize] = score as u32;			
		}
		else {		
			if config.eggs.single_list.clone().iter_mut().find(|x| x.0 == unit.id().0).is_some(){
				let score = ((*game.0).unit_count_score[unit.player() as usize] as i32) + diff;
				(*game.0).unit_count_score[unit.player() as usize] = score as u32;								
			}
		}
		if game.unit_count(unit.player(),unit.id())<=0{
			(*Game::all_units_count())[unit.id().0 as usize][unit.player() as usize] = 0;
		}	
	}

//0x00488D50 => modify_unit_counters2(i32, @edi *mut Unit, @ecx i32);//hook
pub unsafe fn unit_counters2_hook(
	increment_scores: i32,
	unit: *mut bw::Unit,
	diff: i32,//
	orig: &dyn Fn(i32,*mut bw::Unit,i32),){
	
		
		let unit = match Unit::from_ptr(unit) {
			Some(s) => s,
			None => {
				orig(increment_scores,unit,diff);
				return;
			},
		};		
		if unit.is_hallucination(){
			return;
		}
		if unit.id().is_subunit(){
			return;
		}
		let game = Game::get();
		let completed_count = game.completed_count(unit.player(), unit.id());
		let new_count = (completed_count as i32).saturating_add(diff);
		(*Game::completed_units_count())[unit.id().0 as usize][unit.player() as usize] = new_count as u32;
		let race = unit.id().race();
		
		let mut supply = unit.id().supply_provided() as i32;
		//bw_print!("Supply: {}, diff: {}",supply, diff);
		let config = config::config();
		if config.overlord
		{
			let mut globals = Globals::get("unit_counters2_hook");
			
			if unit.id().0==174 {
				let scrap = globals.ngs.get_scrap(unit);
				supply += (scrap*2) as i32;
				//bw_print!("After scrap: {}, scrap: {}",supply, scrap);
				if diff < 0 {
					globals.ngs.persist_unit_structs.swap_retain(|x| x.unit.0 != unit.0);
				}
			}
		}
		
		//debug!("Supply: {}, diff: {}, mul {}",supply, diff,supply*diff);
		let mut supplies = (*game.0).supplies[race as usize].provided[unit.player() as usize] as i32;
		supplies = supplies.wrapping_add(supply*diff);
		(*game.0).supplies[race as usize].provided[unit.player() as usize] = supplies as u32;		
		if unit.id().is_factory(){
			let score = (bw::score_factories_owned[unit.player() as usize] as i32).saturating_add(diff);
			bw::score_factories_owned[unit.player() as usize] = score as u32;
		}
		//......
		if unit.id().men_flag(){
			let score_units_produced = (bw::score_units_produced[unit.player() as usize] as i32).saturating_add(diff);
			bw::score_units_produced[unit.player() as usize] = score_units_produced as u32;
		}
		if unit.id().is_building(){
			let score = (bw::score_structures_constructed[unit.player() as usize] as i32).saturating_add(diff);
			bw::score_structures_constructed[unit.player() as usize] = score as u32;
		}
		if increment_scores == 1 && unit.player()!=11 {
			let config=config::config();
			if unit.id().men_flag() {								
				match config.unit_morph_prerequisites.id_list.clone().iter_mut()
									.find(|x| x.to.0==unit.id().0){
					Some(s)=>{},
					None=>{
						let score_men_owned = (diff*unit.id().build_score() as i32).saturating_add(diff);
						bw::score_men_owned[unit.player() as usize] = score_men_owned as u32;
					},
				};		
				let score_men_total = (diff*unit.id().build_score() as i32).saturating_add(diff);
				bw::score_men_total[unit.player() as usize] = score_men_total as u32;	
			} else if unit.id().is_building() {
				let morph = config.zerg_buildmorphs.single_list.iter().find(|x| x.0 == unit.id().0);
				if morph.is_none(){
					let score_str_owned = (bw::score_structures_owned[unit.player() as usize] as i32).saturating_mul(diff);
					bw::score_structures_owned[unit.player() as usize] = score_str_owned as u32;				
				}
				let score_build_total = (bw::score_buildings_total[unit.player() as usize] as i32).saturating_mul(diff);
				bw::score_buildings_total[unit.player() as usize] = score_build_total as u32;
			}
		}
		if (game.completed_count(unit.player(),unit.id()) as i32) < 0 {
			(*Game::completed_units_count())[unit.id().0 as usize][unit.player() as usize] = 0;
		}
		//orig(unk1,unit,diff);
	}

/*pub unsafe fn unknown_gas_func_hook(
	ecx: u32,
	edi: u32,
	arg1: u32,
	arg2: u32,
	arg3: u32,
	arg4: u32,
	orig: &dyn Fn(u32,u32,u32,u32,u32,u32)->u32,)->u32 {
		if *bw::first_active_fow_sprite!=null_mut() {
			if (*(*bw::first_active_fow_sprite)).sprite==null_mut(){
				//debug!("DEBUG: Fow sprite without sprite, id: {}", (*(*bw::first_active_fow_sprite)).value);
			}
		}
		let result = orig(ecx,edi,arg1,arg2,arg3,arg4);
		result
	}*/

pub unsafe fn ai_count_economy_units(
	player: u32,
	unit_id: u32,
	orig: &dyn Fn(u32,u32)->u32,)->u32{
		let mut result = 0;
		/*
		if unit_id == 153 {
			result = bw::ai_count_economy_units_func(player,unit::id::HYDRALISK_DEN.0 as u32);
		}*/
		let config = config::config();
		match config.equivalent_prerequisites.equivalent_list.clone().iter_mut()
								.find(|x| x.listing[1].0==unit_id as u16){
			Some(s)=>{
			
				result=bw::ai_count_economy_units_func(player,s.listing[0].0 as u32); 
			},
			None=>{},
		};
		result += orig(player,unit_id);
		result
	}
	

	//0048BEC0 = InitializeBullet(), arg 1 Bullet *bullet, arg 2 weapon_id, arg 3 x, arg 4 direction, ecx player, edx y, eax Unit *parent
	
//0x0048B770 => do_missile_dmg(*mut Bullet);	
	
	/*
pub unsafe fn missile_dmg_hook(
	bullet: *mut bw::Bullet,
	orig: &dyn Fn(*mut bw::Bullet),){
		orig(bullet);
	}
	*/
	
pub unsafe fn initialize_bullet_hook(
	bullet: *mut bw::Bullet,
	weapon_id: u32,
	x: u32,
	direction: u32,
	player: u32,
	y: u32,
	unit: *mut bw::Unit,
	orig: &dyn Fn(*mut bw::Bullet,u32,u32,u32,u32,u32,*mut bw::Unit)->u32,)->u32{
		
		let result = orig(bullet,weapon_id,x,direction,player,y,unit);
		
		let config = config::config();
		if config.bullet_bounces.bounce_list.len()==0 {
			return result;
		}
		let unit = match Unit::from_ptr(unit) {
			Some(s) => s,
			None => {
				return result;
			},
		};
		
	/*	if bullet!=null_mut() && config.hydra_misc && (unit.id().0==43 || unit.id().0==55) {//mutalisk or kuk
			let mut globals = Globals::get("mutalisk 4th bounce");
			if globals.upgrades.get_upgrade_level(117,unit.player() as u32)>0 {
				(*bullet).remaining_bounces += 1;//lithe organelle
			}
			drop(globals);
		}*/
		let bounces = match config.bullet_bounces.bounce_list.clone().iter_mut().find(|x| x.unit_id==unit.id()){
			Some(s)=>s.bounces,
			None=>return result,
		};
		
		let target = match Unit::from_ptr((*unit.0).target){
			Some(s) => s,
			None => {
				return result;
			},
		};
		if bullet == null_mut(){
			return result;
		}
		
		let distance = bw::distance(unit.position(),target.position());
		let mut globals = Globals::get("basilisk bounce upgrade");
		//option 1
		if (distance>=192) || !config.hydra_caustic_fission {
			//bw_print!("Set bounce behavior");
			(*bullet).remaining_bounces = bounces as u8;
			(*bullet).order = 3;//bounce behavior
		}
		
		drop(globals);		
		result
	}
	
	
pub unsafe fn ai_set_finished_unit_ai(
	unit: *mut bw::Unit,
	parent: *mut bw::Unit,
	orig: &dyn Fn(*mut bw::Unit,*mut bw::Unit),){
		let unit = match Unit::from_ptr(unit) {
			Some(s) => s,
			None => {
				orig(unit,parent);
				return;
			},
		};
		let config = config::config();
		if config.morph_train_ai.equivalent_list.clone().iter_mut().find(|x| x.listing[1].0==(*unit.0).unit_id).is_some(){
			return;
		}
		orig(unit.0,parent);
	}

/*pub unsafe fn does_provide_creep_hook(
	y: u32,
	completed: bool,
	unit_id: u32,
	x: u32,
	orig: &dyn Fn(u32,bool,u32,u32)->u32,
	)->u32{	
		let mut id = unit_id;
		let mut basilisk = false;
		if id == 180 {
			id = 143;
			basilisk = true;
		}
		let result = orig(y,completed,id,x);
		if basilisk {
			id = 180;
		}
		
		result
	}*/
//0x004765B0 => get_miss_chance(@ecx *mut Unit, @eax *mut Unit)->u32;
	
/*pub unsafe fn get_miss_chance(
	attacker: *mut bw::Unit,
	target: *mut bw::Unit,
	orig: &dyn Fn(*mut bw::Unit,*mut bw::Unit)->u32)->u32{
		
		orig(attacker,target)
	}*/
	
pub unsafe fn modify_speed_hook(
	unit: *mut bw::Unit,
	base_speed: u32,
	orig: &dyn Fn(*mut bw::Unit,u32)->u32,)->u32{
		
		let mut result = orig(unit,base_speed);
		let config = config::config();
		let unit = match Unit::from_ptr(unit) {
			Some(s) => s,
			None => {
						return result;
					},
		};
		let mut globals = Globals::get("modify_speed");
		
		if config.hydra_signify_cowardice {
			result = globals.ngs.hierophant_movespeed_proc(result, unit);	
		}
		
		drop(globals);	
		result
	}

pub unsafe fn attack_cooldown_hook(
	unit: *mut bw::Unit,
	weapon_id: u32,
	orig: &dyn Fn(*mut bw::Unit,u32)->u32,)->u32{
		let mut result = orig(unit,weapon_id);
		let unit = match Unit::from_ptr(unit) {
			Some(s) => s,
			None => {
						return result;
					},
		};
		let config = config::config();
		if config.hydra_signify_malice {
			let mut globals = Globals::get("attack_cooldown");
			result = globals.ngs.hierophant_attackspeed_proc(result, unit);
			drop(globals);	
		}
		result
	}

pub unsafe fn bullet_state_init_hook(
	bullet: *mut bw::Bullet,
	orig: &dyn Fn(*mut bw::Bullet)->u32,)->u32{
		
		if bullet!=null_mut(){
		
			let target = match Unit::from_ptr((*bullet).target) {
				Some(s) => s,
				None => {
							return orig(bullet);
				},
			};
			let config = config::config();/*
			if config.parasite_timers && (*bullet).weapon_type==56 {
				let mut globals = Globals::get("bullet_state");
				globals.ngs.add_parasite_timer((*bullet).player as u32,(*bullet).source_unit,target);
				drop(globals);
			}*/
			if config.hydra_misc && (*bullet).weapon_type==107 {
				let mut globals = Globals::get("bullet_state2");
				//bw_print!("Set blind");
				globals.ngs.add_blind_timer(target);
				drop(globals);
			}
			/*
			if config.full_toilet && (*bullet).weapon_type==76 {//blue lasers
				let sprite = (*bullet).sprite;
				let head_image = (*sprite).first_image;
				let ptr = (*bw::color_shift_data).offset(0x3 as isize);
				let address = (*ptr).data;
				bw_print!("remap: {:p} {:p}",address,(*head_image).drawfunc_param);
				//(*head_image).drawfunc_param = (*bw::color_shift_data).offset(0x3 as isize);		
			}*/
		}
		let result = orig(bullet);
		result
	}

pub unsafe fn reaver_check_movement_distance_hook(
	reaver: *mut bw::Unit,
	range: u32,
	orig: &dyn Fn(*mut bw::Unit,u32)->u32,)->u32
	{
	
		let config = config::config();
		if !config.relocator_reaver_pathfinding{
			return orig(reaver,range);
		}
		let reaver = match Unit::from_ptr(reaver) {
			Some(s) => s,
			None => {
				return orig(reaver,range);
			},
		};
		let mut result = orig(reaver.0,range);
		let mut globals = Globals::get("reaver movement distance");
		//let up_lev = globals.upgrades.get_upgrade_level(57,reaver.player() as u32);
		drop(globals);
		//if up_lev>0 {
			result = 1;
		//}
		result
	}
	
	/*
pub unsafe fn parse_upgrades(
	orig: &dyn Fn(),){
		//0046D5A0+3
		
		let game = Game::get();
		if game.frame_count() == 0 {
			debug!("Error - Orig parse");
			orig();
			return;
		}
		let mut globals = Globals::get("parse_upgrades");
		if globals.upgrades.upgrades_firegraft_offsets.len() < 61 {
			debug!("Error - Orig parse 2");
			orig();
			return;
		}
		let mut length = globals.upgrades.length as u16;	
		for i in 0..length {
			let offset = globals.upgrades.upgrades_firegraft_offsets[i as usize] as u16;
			bw::upgrades_dat_req[offset as usize] = globals.upgrades.upgrades_firegraft_offsets[i as usize] as u16;
			if offset == 0xffff {
				match i {
					0...61 => { bw::upgrades_dat_req[offset as usize] = 0; },
					_ => { globals.upgrades.restriction_flags[offset as usize] = 0},
				}
			}			
		}
//		orig();
	}
*/

pub unsafe fn escalate(game: Game, globals: &mut Globals, player: u8, upgrade: u16){
	let old_uid = globals.ngs.get_escalation_from_upgrade(upgrade,player as u32);
	globals.ngs.switch_escalation_status(upgrade,player as u32);
	let new_uid = globals.ngs.get_escalation_from_upgrade(upgrade,player as u32);
	//bw_print!("Ids: {} {}",old_uid.0,new_uid.0);
	if old_uid==UnitId(228) || new_uid==UnitId(228) {
		bw_print!("Wrong escalation calculation! - no escalation values");
		return;
	}
	if old_uid == new_uid {
		bw_print!("Incorrect escalation - unit id match");
		return;
	}
	game.set_unit_availability(player, new_uid, true);
	game.set_unit_availability(player, old_uid, false);
	let config = config::config();
	match upgrade {
		0..=45 => {
			game.set_upgrade_level(player, UpgradeId(upgrade), 0 as u8);
		},
		_=>{
			if config.extended_upgrades {
				globals.upgrades.set_upgrade_level(upgrade as u32,player as u32,0);
			}
			else {
				game.set_upgrade_level(player, UpgradeId(upgrade), 0 as u8);
			}
		},
	}
}

pub unsafe fn order_tech(
	unit: *mut bw::Unit,
	orig: &dyn Fn(*mut bw::Unit),){
	let unit = match Unit::from_ptr(unit) {
		Some(s) => s,
		None => {
					orig(unit);
					return;
				},
	};
	
	let config = config::config();
	if unit.upgrade_time() > 0 {
		let game = Game::get();
		let mut globals = Globals::get("order_tech");
		boost_research(unit,&config,game,&mut globals);
	}
	orig(unit.0);
}
unsafe fn boost_research(
	unit: Unit,
	config: &Config,
	game: Game,
	globals: &mut Globals,
){
	use std::cmp;
	if !config.hydra_misc && !config.overlord {
		return;
	}
	let mut boost = 15.0;//percent
	/*
	if config.hydra_misc {		
		match unit.id().0 {
			148|137|197|145|191|153|204=>{}, //gorgoleth apiary,guardian roost, devourer haunt, mutation pit, lurker den, basilisk den, vorval pond | TBD: keskath cavern
			_=>{
				boost = 0.0;
				let rect = bw::Rect {
					left: unit.position().x.saturating_sub(256),
					top: unit.position().y.saturating_sub(256),
					right: unit.position().x.saturating_add(256),
					bottom: unit.position().y.saturating_add(256),
				};
				let crucible = &globals.last_frame_search
					.search_iter(&rect)
					.any(|u| game.allied(u.player(),unit.player()) &&
					u.is_completed() && (*u.0).flags & 0x400 == 0
					&&
					u.id().0==201);
				if *crucible {
					 boost += 15.0;
				}
				if let Some(addon) = unit.addon(){
					if addon.id().0 == 192 {
						boost += 15.0;
					}
				}
				if boost == 0.0 {
					return;
				}
			},
		}
	}
	*/
	let value = globals.ngs.get_generic_value(unit.0,22);//enhanced research time delay
	let time = unit.upgrade_time();
	
	if config.overlord {//Overlord - Enhanced Research upgrade
		boost = 8.0*get_new_upgrade_level(game,globals,67,unit.player()) as f64;
	}
	let fraction = cmp::max(1,((100.0/boost) as i32).saturating_sub(1));
	if value == 0 {
		globals.ngs.set_generic_value(unit, 22, fraction);
		(&mut (*unit.0).unit_specific[0x6..]).write_u16::<LE>(time.saturating_sub(1)).unwrap();
	}
	else {
		globals.ngs.add_generic_value(unit,22,-1);
	}
}

/*

	0x004347E0 => delete_ai_town_if_needed(@eax *mut AiTown);
	0x00434330 => try_begin_delete_town(@esi *mut AiTown)->u32;
*/

pub unsafe fn delete_ai_town_if_needed(
	town: *mut bw::AiTown,
	orig: &dyn Fn(*mut bw::AiTown),){
		let globals = Globals::get("delete_ai_town_if_needed");
		for(t,val) in globals.town_states.states.iter(){
			if t.0 == town {
				if val.persisting {
					return;
				}
			}
		}
		drop(globals);
		orig(town);
	}

pub unsafe fn try_begin_delete_town(
	town: *mut bw::AiTown,
	orig: &dyn Fn(*mut bw::AiTown)->u32,)->u32{
		let globals = Globals::get("try_begin_delete_town");
		for(t,val) in globals.town_states.states.iter(){
			if t.0 == town {
				if val.persisting {
					return 0;
				}
			}
		}
		drop(globals);	
		return orig(town);
	}

pub unsafe fn order_upgrade(
	unit: *mut bw::Unit,
	orig: &dyn Fn(*mut bw::Unit),){
	
		let unit = match Unit::from_ptr(unit) {
			Some(s) => s,
			None => {
						orig(unit);
						return;
					},
		};
		
		let upgrade = (*unit.0).unit_specific[0x9] as u16;
		
		let config = config::config();
		if upgrade == 61 {
			orig(unit.0);
			return;
		}
		let level = (*unit.0).unit_specific[0xd] as u8;
		let game = Game::get();	
		let mut globals = Globals::get("order_upgrade1");
		
		if unit.upgrade_time() > 0 {			
			boost_research(unit,&config,game,&mut globals);
		}
		
		
		drop(globals);
		//replace later by future station
		
		orig(unit.0);
		let mut globals = Globals::get("order_upgrade2");
		let up_lev = get_new_upgrade_level(game,&mut globals,upgrade,unit.player());
		if level==up_lev {		
			//let escalation = config.escalations.esc_list.clone().iter_mut().find(|x| x.upg_id==upgrade as u32).is_some();
			let relic = config.relics.list.clone().iter_mut().find(|x| x.upg_id==upgrade as u32).is_some();
			if relic && config.relics_enabled {
				globals.ngs.load_relic_to_reliquary(upgrade,unit);
			}
			let switchable = config.switchables.iter().find(|x| **x==upgrade as u32).is_some();
			if switchable {
				for upg in config.switchables.clone().iter_mut(){
					if upgrade as u32 != *upg {
						//bw_print!("Set to 0 - {}",*upg);
						set_new_upgrade_level_current(game,&mut globals,*upg as u16,unit.player(),0);
					}
				}
			}

			if config.cullen_equipment {
				let mut v = vec![19,20,21,22,23];
				let equipment = v.iter_mut().find(|x| **x==upgrade).is_some();
				if equipment {
					//bw_print!("Is equipment");
					if !globals.ngs.class_unlocked(upgrade){
						//bw_print!("Unlock class: {}", upgrade);
						globals.ngs.unlock_class(upgrade);
						set_new_upgrade_level_current(game,&mut globals,upgrade,unit.player(),0);
					}
					else {
						//bw_print!("Equip class: {}",upgrade);
						//transform_unit
						let new_cullen_id = match upgrade {
							19=>25,
							20=>22,
							21=>23,
							22=>29,
							23=>98,
							_=>{
								bw_print!("Incorrect Cullen equipment class");
								return;
							},
						};
						let rect = anywhere_rect();
						let search = UnitSearch::from_bw();
						let cullen = search
							.search_iter(&rect)
							.find(|x| x.player()==unit.player() && is_cullen(x.id().0));
						if let Some(cullen)=cullen {
							globals.ngs.equip_class(upgrade);
							
							for e in v {
								if e != upgrade {
									set_new_upgrade_level_current(game,&mut globals,e,unit.player(),0);
								}
							}
							let cloak_status =  globals.ngs.get_generic_switch(cullen.0,10001);
							bw_print!("Switch - Cloak status: {}",cloak_status);
							if cloak_status != 0 {
								bw_print!("Switch - Cloak status !=0, switch the bool, decloak Cullen");
								cullen.issue_secondary_order(OrderId(23));//decloak
								globals.ngs.set_generic_switch(cullen,10001);
							}
							else {
								bw_print!("No cloak status");
							}
							drop(globals);
							let old_energy = (*cullen.0).energy;
							bw::transform_unit(new_cullen_id,cullen.0);
							(*cullen.0).energy = old_energy;

							return;
							
						}
						else {
							//bw_print!("No Cullen!");
							debug!("ERROR! Cullen not found!");
							set_new_upgrade_level_current(game,&mut globals,upgrade,unit.player(),0);
						}
//						
						
					}				
				}
			}
			else {
				//bw_print!("No cullen config");
			}
			drop(globals);
		}
	}
/*
pub unsafe fn progress_speed(
	flingy: *mut bw::Flingy,////Flingy type pointer originally
	orig: &dyn Fn(*mut bw::Flingy),){
		orig(flingy);
	}
	*/
pub unsafe fn progress_flingy_turning(
	flingy: *mut bw::Flingy,////Flingy type pointer originally
	orig: &dyn Fn(*mut bw::Flingy)->u32,)->u32{

/*	
		if ((*flingy).flingy_flags & 0x2 != 0) && !((*flingy).flingy_flags & 0x1 != 0) {
			return 0;
		}
		let mut turn_need = (*flingy).target_direction as i32 - (*flingy).facing_direction as i32;
		let flingy_turn_speed = (*flingy).flingy_turn_speed as i32;		
		let facing_direction = (*flingy).facing_direction as i32;
		
		if turn_need > 0x80 || turn_need < -0x80 {
			turn_need = -turn_need;
		}
		if turn_need > flingy_turn_speed {
			(*flingy).facing_direction = (facing_direction + flingy_turn_speed) as u8;
		}
		else if turn_need < -flingy_turn_speed {
			(*flingy).facing_direction = (facing_direction - flingy_turn_speed) as u8;
		}
		else {
			(*flingy).facing_direction = (*flingy).target_direction;
		}
		
		
		if (((*flingy).flingy_id >= 0x8d && (*flingy).flingy_id <= 0xab) || ((*flingy).flingy_id>=0xc9 && (*flingy).flingy_id<=0xce))==true {
			if (*flingy).flingy_turn_speed < 255 {
				(*flingy).flingy_turn_speed += 1;
			}			
		}	
*/		

		/*
		let config = config::config();
		let bullet = match config.bullet_hardcode.integer_list.clone().iter_mut().find(|x| x==&&((*flingy).flingy_id as u32)){
			Some(s)=>{
				if (*flingy).flingy_turn_speed < 255 {
					(*flingy).flingy_turn_speed += 1;
				}		
			},
			None=>{},
		};*/
		/*
		if (*flingy).target_direction==(*flingy).movement_direction && (*flingy).target_direction==(*flingy).facing_direction {
			(*flingy).flingy_flags &= !0x1;
		}		
		return 1;
		*/
		
		let old_id = (*flingy).flingy_id;
		if old_id == 204 {
			(*flingy).flingy_id = 88;
		}
		match old_id {
			203|205|202=>{
				(*flingy).flingy_id = 39;
			},
			202|203=>{
				(*flingy).flingy_id = 65;
			},
			_=>{},
		}
		
		let result = orig(flingy);
		(*flingy).flingy_id = old_id;
		
		result
	}
		
		
	
//	0x00492020 => can_target_spell(*mut Unit,u32,u32,*mut u32,@edx u32,@edi *mut Unit,@esi u32)->u32;
//00492020 = CanTargetSpell(), arg 1 Unit *unit, arg 2 x, arg 3 y, arg 4 *out_error, edx tech, edi Unit *target, esi fow_unit_id	
pub unsafe fn can_target_spell_fromgptp(
	unit: *mut bw::Unit,
	x: u32,
	mut y: u32,
	out_error: *mut u32,
	tech: u32,
	target: *mut bw::Unit,
	mut fow_unit_id: u32,
	orig: &dyn Fn(*mut bw::Unit,u32,u32,*mut u32,u32,*mut bw::Unit,u32)->u32,)->u32{
		#[derive(Copy, Clone)]
		#[allow(non_camel_case_types)]
		enum AiseId {
			NoOpcode,
			SendCyprianValue,
			RecvEscalation,
			GetRelicByIndex,
			HasRelic,
			UpgradesLength,
			HasMalice,
			IsSuicidingScout,
			IsBlowingUp,
			GenericSwitch,
			GenericValue,
			GetModerationStacks,
			SendWarpRelayValue,
			SendClarionValue,
			SendStarcallerValue,
			LinkedToWarpRelay,
			HasClarionLink,
			GenericTimer,
			SumMindControlCost,
			GetLegionnaireMarks,
			GetMatrixCasterPlayer,
			GetShepherds,
			GetFlingyData,
			GetSpriteData,
			GetImageData,
			ReadSamaseFile,
			ReadSamaseFile_Ptr,
			GenericUnit,
			GenericVariable,
			GetUnitData,
			GetFlingyPtr,
			//
			
			IsDeathwishing,
			IsBlowingUp_Todborne,
			GetSpatulaSwitch,
			HasGoodboyTimer,
			IsFrosted,
			DeathbyDoughSequence,
			//
			GetFactionIcon,
			GetFactionIconString,
			GetFactionButtonState,
			GetResourceStackSize,
			GetCurrentCommtowerMenu,
			GetFactSelectionButtonState,
			IsClassUnlocked,
			SendExpelWaste,
			GetMeskalloidStatus,
			GetCurrentFaction,
			GetMineralStatus,
			GetDisposition,
			GetCurrentCommtowerPlayer,
			//
		}
		/*if tech!=50000 {
			bw_print!("Recv Command: x {} y {} tech {} fow_unit_id {}, result {}",x,y,tech,fow_unit_id,result);
		}*/		
		if tech == 50000 {
			//bw_print!("Aise Opcode {} (Send) received",x);
			let mut result = 0;
			let opcode = match x {
				1=>AiseId::SendCyprianValue,
				2=>AiseId::RecvEscalation,
				3=>AiseId::GetRelicByIndex,
				4=>AiseId::HasRelic,
				5=>AiseId::UpgradesLength,
				6=>AiseId::HasMalice,
				7=>AiseId::IsSuicidingScout,
				8=>AiseId::IsBlowingUp,
				9=>AiseId::GenericSwitch,
				10=>AiseId::GenericValue,
				11=>AiseId::SendWarpRelayValue,
				12=>AiseId::SendClarionValue,
				13=>AiseId::SendStarcallerValue,
				14=>AiseId::LinkedToWarpRelay,
				15=>AiseId::HasClarionLink,
				16=>AiseId::GenericTimer,
				17=>AiseId::SumMindControlCost,
				18=>AiseId::GetLegionnaireMarks,
				19=>AiseId::GetMatrixCasterPlayer,
				20=>AiseId::GetShepherds,
				21=>AiseId::GetFlingyData,
				22=>AiseId::GetSpriteData,
				23=>AiseId::GetImageData,
				24=>AiseId::ReadSamaseFile,
				25=>AiseId::ReadSamaseFile_Ptr,
				26=>AiseId::GenericUnit,
				27=>AiseId::GenericVariable,
				28=>AiseId::GetUnitData,
				29=>AiseId::GetFlingyPtr,
				10000=>AiseId::GetModerationStacks,
				10001=>AiseId::IsDeathwishing,
				10002=>AiseId::IsBlowingUp_Todborne,
				10003=>AiseId::IsClassUnlocked,
				10004=>AiseId::GetSpatulaSwitch,
				10005=>AiseId::HasGoodboyTimer,
				10006=>AiseId::DeathbyDoughSequence,
				10007=>AiseId::IsFrosted,
				20000=>AiseId::GetFactionIcon,
				20001=>AiseId::GetFactionIconString,
				20002=>AiseId::GetFactionButtonState,
				20003=>AiseId::GetResourceStackSize,
				20004=>AiseId::GetCurrentCommtowerMenu,
				20005=>AiseId::GetFactSelectionButtonState,
				20006=>AiseId::SendExpelWaste,
				20007=>AiseId::GetMeskalloidStatus,
				20008=>AiseId::GetCurrentFaction,
				20009=>AiseId::GetMineralStatus,
				20010=>AiseId::GetDisposition,
				20011=>AiseId::GetCurrentCommtowerPlayer,
				_=>AiseId::NoOpcode,
			};			
			
			let mut globals = Globals::get("can_target_spell_send_aise_values");
			match opcode {
				AiseId::SendCyprianValue => {
					result = globals.ngs.get_cyprian_value(unit);
				},
				AiseId::RecvEscalation => {
					result = globals.ngs.get_escalation_value(y,UnitId(fow_unit_id as u16));
					//bw_print!("aise - recv escalation value: {} {} {}",result,y,fow_unit_id);
				},
				AiseId::GetRelicByIndex => {
					bw_print!("Get relic by index: {}",y);
					bw_print!("Reliquary unit id: {}",(*unit).unit_id);
					result = globals.ngs.get_relic_by_index(unit,y);
				},
				AiseId::HasRelic => {
					
					result = globals.ngs.has_relic(unit,y);
				},
				AiseId::UpgradesLength => {
					result = globals.upgrades.length as u32;
					if result == 0 {
						result = 61;
					}
				},
				AiseId::HasMalice => {
					result = globals.ngs.has_hierophant_malice(unit);
				},
				AiseId::IsSuicidingScout => {
					result = globals.ngs.is_executing_last_orders(unit);
				},
				AiseId::IsBlowingUp => {
					result = globals.ngs.is_blowing_up(unit);
					//bw_print!("Blow result: {}",result);
				},
				AiseId::SendWarpRelayValue=>{
					result = globals.ngs.relay_deployed(Unit::from_ptr(unit).unwrap()) as u32;
				},
				AiseId::SendClarionValue=>{
					result = globals.ngs.clarion_deployed(Unit::from_ptr(unit).unwrap()) as u32;
				},
				AiseId::SendStarcallerValue=>{//UNUSED
				
				},
				AiseId::LinkedToWarpRelay=>{
					result = globals.ngs.get_linked_relay_if_exist(Unit::from_ptr(unit).unwrap());
				},
				AiseId::HasClarionLink=>{
					result = globals.ngs.linked_to_clarion(Unit::from_ptr(unit).unwrap());
				},				
				AiseId::SumMindControlCost=>{
					result = globals.ngs.sum_mindcontrol_cost(Unit::from_ptr(unit).unwrap());
				},
				AiseId::GetLegionnaireMarks=>{
					result = globals.ngs.get_legionnaire_marks(Unit::from_ptr(unit).unwrap(),Unit::from_ptr(target).unwrap());
				},
				AiseId::GetMatrixCasterPlayer=>{
					result = globals.ngs.get_matrix_caster_player(Unit::from_ptr(unit).unwrap());
				},
				AiseId::GetShepherds=>{
					result = globals.ngs.get_sublime_shepherds(Unit::from_ptr(unit).unwrap());
				},
				AiseId::GetFlingyData=>{
					let config = config::config();
					let index = fow_unit_id as usize;
					//debug!("Get flingy data {} {}",fow_unit_id,y);
					if config.extended_data {
						result = data_extender::get_flingy_data(y,index);
					}
					else {
						result = match y {
							0=>bw::flingy_dat_top_speed[index].into(),
							1=>bw::flingy_dat_acceleration[index].into(),
							2=>bw::flingy_dat_turn_radius[index].into(),							
							3=>bw::flingy_dat_movement_type[index].into(),
							4=>bw::flingy_dat_sprite_id[index].into(),
							5=>bw::flingy_dat_halt_distance[index].into(),							
							6=>bw::flingy_dat_iscript_mask[index].into(),
							_=>0,
						};
					}
				},
				AiseId::GetSpriteData=>{
					let config = config::config();
					let index = fow_unit_id as usize;
					if config.extended_data {
						result = data_extender::get_sprite_data(y,index);
					}
					else {
						result = match y {
							0 => bw::sprites_dat_vision[index].into(),
							1 => bw::sprites_dat_as_visible[index].into(),
							2 => bw::sprites_dat_image_id[index].into(),
							3 => bw::sprites_dat_selection_circle[index].into(),
							4 => bw::sprites_dat_selection_imagepos[index].into(),
							5 => bw::sprites_dat_healthbar[index].into(),
							_=>0,
						};
					}		
				},
				AiseId::GetImageData=>{
					let config = config::config();
					let index = fow_unit_id as usize;
					//debug!("Get image data {} {}",fow_unit_id,y);
					if config.extended_data {
						result = data_extender::get_image_data(y,index);
					}
					else {
						result = match y {
							0 => bw::images_dat_grp_id[index].into(), 
							1 => bw::images_dat_graphic_turns[index].into(), 
							2 => bw::images_dat_clickable[index].into(), 
							3 => bw::images_dat_use_full_iscript[index].into(), 
							4 => bw::images_dat_draw_if_cloaked[index].into(), 
							5 => bw::images_dat_draw_func[index].into(), 
							6 => bw::images_dat_draw_remap[index].into(), 
							7 => bw::images_dat_iscript_id[index].into(), 
							8 => bw::images_dat_overlay_shield[index].into(), 
							9 => bw::images_dat_overlay_attack[index].into(), 
							10 => bw::images_dat_overlay_damage[index].into(), 
							11 => bw::images_dat_overlay_special[index].into(), 
							12 => bw::images_dat_overlay_landing[index].into(), 
							13 => bw::images_dat_overlay_liftoff[index].into(), 
							14 => bw::overlay_ptr_shield[index].into(), 
							15 => bw::overlay_ptr_attack[index].into(), 
							16 => bw::overlay_ptr_damage[index].into(), 
							17 => bw::overlay_ptr_special[index].into(), 
							18 => bw::overlay_ptr_landing[index].into(), 
							19 => bw::overlay_ptr_liftoff[index].into(), 							
							20 => bw::ptr_image[index].into(),
							21 => bw::building_overlay_state_max[index].into(),
							_=>0,
						};
					}
				},
				AiseId::ReadSamaseFile=>{
					//debug!("Start...");
					let cstr = CStr::from_ptr(target as *mut u32 as *const i8);
					//debug!("CStr done! {:?}",cstr);
					let string = cstr.to_str().unwrap();
					//debug!("Unwrap done!");
					let (data, len) = match create_file_state(string){
						Some(s)=>s,
						None=>{
							return 0;
						},
					};
					
					//debug!("Read done! {:?} {:?}",data,len);
					let attempt = CStr::from_ptr(data as *const i8);
					//debug!("Attempt: {:?}",attempt);
					//debug!("Ptrs send from gptp: {:x} {:x}",y,fow_unit_id);
					return len as u32;
				},
				AiseId::ReadSamaseFile_Ptr=>{
					return read_save_state_ptr();
				},
				AiseId::GenericUnit=>{
					let result_unit = globals.ngs.get_generic_unit(unit,y);
					if let Some(result_unit) = result_unit {
						return result_unit.0 as u32;
					}
					return 0;
				},
				AiseId::GenericVariable=>{
					return globals.ngs.get_generic_variable(y);
				},
				
				AiseId::GetUnitData=>{
					//bw_print!("Try get unit data");
					let config = config::config();
					let index = fow_unit_id as usize;
					if config.extended_data {
						//bw_print!("Extended: get unit data");
						if y == 0 {
							result = bw::gptp_flingyids(index as u32);
						}//entry is flingy
						else {
							result = data_extender::get_unit_data(y,index);
						}
						
					}
					else {
//						debug!("Non-extended: Get old data");
						result = match y {
							0 => bw::units_dat_flingy[index].into(),
							29 => bw::units_dat_snd_ready[index].into(),				
							30 => bw::units_dat_snd_what_begin[index].into(),
							31 => bw::units_dat_snd_what_end[index].into(),
							32 => bw::units_dat_snd_annoyed_begin[index].into(),
							33 => bw::units_dat_snd_annoyed_end[index].into(),
							34 => bw::units_dat_snd_yes_begin[index].into(),
							35 => bw::units_dat_snd_yes_end[index].into(),							
							_=>0,
						};
					}						
				},

				AiseId::GetFlingyPtr=>{
					let config = config::config();
					if config.extended_data {
						let ptr = data_extender::fakeid_flingy_ptr();
						return ptr;
					}
					else {
						windows::message_box("Aise","Critical error: GPTP wrappers were patched without extended_data config opcode");
						TerminateProcess(GetCurrentProcess(), 0x4230daef);
					}
					
					return 0;
				},
				//todborne
				AiseId::GetModerationStacks => {
					result = globals.ngs.get_moderation_stacks(Unit::from_ptr(unit).unwrap());
				},
				AiseId::IsDeathwishing=>{
					result = globals.ngs.is_deathwishing(unit);
				},
				AiseId::IsBlowingUp_Todborne=>{
					result = globals.ngs.is_blowing_up_todborne(unit);
				},
				AiseId::IsClassUnlocked=>{
					result = globals.ngs.class_unlocked(y as u16) as u32;
				},
				AiseId::HasGoodboyTimer=>{
					result = globals.ngs.has_goodboy_timer(unit);
				},
				AiseId::GetSpatulaSwitch=>{
					result = globals.ngs.get_spatula_status(Unit::from_ptr(unit).unwrap());
				},
				AiseId::DeathbyDoughSequence=>{
					result = globals.ngs.death_by_dough_status(Unit::from_ptr(unit).unwrap());
				},
				AiseId::IsFrosted=>{
					result = globals.ngs.frosted_status(unit);
				},
				AiseId::GenericSwitch=>{
					result = globals.ngs.get_generic_switch(unit,y);
				},
				AiseId::GenericValue=>{
					result = globals.ngs.get_generic_value(unit,y);
				},
				AiseId::GenericTimer=>{
					result = globals.ngs.get_generic_timer(unit,y);
				},
				//overlord
				AiseId::GetFactionIcon => {
					result = globals.ngs.get_faction_icon(y);
				},
				AiseId::GetFactionIconString=> {
					result = globals.ngs.get_registered_string_id(y);
				},
				AiseId::GetFactionButtonState=>{
					result = globals.ngs.get_button_state(y) as u32;
				},
				AiseId::GetResourceStackSize=>{			
					result = globals.ngs.get_resource_stack_size(unit);
				},
				AiseId::GetCurrentCommtowerMenu=>{
					
					result = parse_commtower_back(globals.ngs.get_current_commtower_menu(unit));
				},
				AiseId::GetFactSelectionButtonState=>{
					
					result = globals.ngs.get_faction_selection_button_state(y) as u32;
				},
				AiseId::SendExpelWaste=>{
					result = globals.ngs.get_waste_status(unit);
				},
				AiseId::GetMeskalloidStatus=>{
					result = globals.ngs.is_meskalloid((*unit).sprite);
				},
				AiseId::GetCurrentFaction=>{
					result = globals.ngs.get_current_faction(y);
				},
				AiseId::GetMineralStatus=>{
					result = globals.ngs.mineral_opcode((*unit).sprite);
				},
				AiseId::GetDisposition=>{
					result = globals.ngs.get_current_disposition(y,fow_unit_id);
				},
				AiseId::GetCurrentCommtowerPlayer=>{
					result = globals.ngs.get_target_player_id(unit) as u32;
				},
				//mapcrap 2019-28-12
				AiseId::NoOpcode => {},
			}
			drop(globals);
			return result;
		}	
		let result = orig(unit,x,y,out_error,tech,target,fow_unit_id);
		result
	}

    #[repr(C)]
    #[derive(Debug)]
    pub struct GptpBaseLayout {
        pub position: bw::Point,
        pub unit_id: u16,
        pub priority: u8,
        pub guard_rebuilds: u8,
        pub wait_build_start_count: u8,
        pub was_preplaced: u8,
        pub should_request: u8,
    }
	
#[derive(Copy, Clone)]
pub enum GptpId {
	NoOpcode,
	AddCyprianSwitch,
	SetCyprianStatus,
	SetEscalation,
	EmpowerHero,
	BlindJudgeTimedEffect,
	SetLastOrders,
	PrepareToExplode,
	ErasePersistingUnit,
	SetGenericSwitch,
	SetGenericValue,
	AddGenericValue,
	LinkWarpRelayToStructure,
	SetApertureStatus,
	PlaceScreamerPool,
	SetPhaselinkStatus,
	SetGenericTimer,
	
	
	PlaceClarionLinksForUnit,
	SpreadClarionDamage,
	FreeMindControlled,
	SetMindControlLink,
	SetMaelstromOffset,
	SetMatrixCaster,
	SetSublimeShepherd,
	
	SetGenericUnit,
	DirgeImpale,
	ClearClarionLinks,
	SetGenericVariable,
	SetSpriteData,

    SetGuard,
    SetBaseLayout,
	
	//todborne
	AddNSeverityStacks,
	SetDeathwish,
	PrepareToExplodeTodborne,
	ActivistBehavior,
	SwitchSpatulaSwitch,
	Void,
	Goodboy,
	SteelFreeze,
	DeathbyDoughSwitch,
	//overlord
	SetMeskalloidStatus,
	SetLastSuckedUnit,
	ResetListening,
	SetBasiliskGimmick,
	SetMineralChunkStatus,
	CustomStimAI,
	SetExpelWaste,
}

fn gptpid_to_u32(id: GptpId)->u32 {

	match id {
		GptpId::AddCyprianSwitch => 1,
		GptpId::SetCyprianStatus => 2,
		GptpId::SetEscalation => 3,
		GptpId::EmpowerHero => 4,
		GptpId::BlindJudgeTimedEffect => 5,
		GptpId::SetLastOrders=>6,
		GptpId::PrepareToExplode=>7,
		GptpId::ErasePersistingUnit=>8,
		GptpId::SetGenericSwitch=>9,
		GptpId::SetGenericValue=>10,
		GptpId::AddGenericValue=>11,
		GptpId::LinkWarpRelayToStructure=>12,
		GptpId::SetApertureStatus=>13,
		GptpId::PlaceScreamerPool=>14,
		GptpId::SetPhaselinkStatus=>15,
		GptpId::PlaceClarionLinksForUnit=>16,
		GptpId::SpreadClarionDamage=>17,
		GptpId::SetGenericTimer=>18,
		GptpId::FreeMindControlled=>19,
		GptpId::SetMindControlLink=>20,
		GptpId::SetMaelstromOffset=>21,
		GptpId::SetMatrixCaster=>22,
		GptpId::SetSublimeShepherd=>23,
		GptpId::SetGenericUnit=>24,
		GptpId::DirgeImpale=>25,
		GptpId::ClearClarionLinks=>26,
		GptpId::SetGenericVariable=>27,
		GptpId::SetSpriteData=>28,
        GptpId::SetGuard=>29,
        GptpId::SetBaseLayout=>30,
		GptpId::AddNSeverityStacks=>10000,
		GptpId::SetDeathwish=>10001,
		GptpId::PrepareToExplodeTodborne=>10002,
		GptpId::ActivistBehavior=>10003,
		GptpId::SwitchSpatulaSwitch=>10004,
		GptpId::Void=>10005,
		GptpId::Goodboy=>10006,
		GptpId::DeathbyDoughSwitch=>10007,
		GptpId::SteelFreeze=>10008,
		GptpId::SetMeskalloidStatus=>20000,
		GptpId::SetLastSuckedUnit=>20001,
		GptpId::ResetListening=>20002,
		GptpId::SetBasiliskGimmick=>20003,
		GptpId::SetMineralChunkStatus=>20004,
		GptpId::CustomStimAI=>20005,
		GptpId::SetExpelWaste=>20006,
		GptpId::NoOpcode => 0,
	}
}

#[derive(Copy, Clone)]
pub enum AiseSignal {
	None,
	GetTransportCargoCount,
	GetAiMain,
	GetAllUnitsCount,
	GetCompleteUnitsCount,
	GetKilledUnitsTable,
    GetDeathTable,
    GetUnitAvailabilityTable,
    GetUnitTbl,
    GetUnitReq,
    GetUpgradesReq,
    GetResearchReq,
}

pub fn aise_signal_to_u32(id: AiseSignal)->u32 {
	match id {
		AiseSignal::GetTransportCargoCount => 1,
		AiseSignal::GetAiMain => 2,
        AiseSignal::GetAllUnitsCount => 3,
        AiseSignal::GetCompleteUnitsCount => 4,
        AiseSignal::GetKilledUnitsTable => 5,
        AiseSignal::GetDeathTable => 6,
        AiseSignal::GetUnitAvailabilityTable => 7,
        AiseSignal::GetUnitTbl => 8,
        AiseSignal::GetUnitReq => 9,
        AiseSignal::GetUpgradesReq => 10,
        AiseSignal::GetResearchReq => 11,
		AiseSignal::None => 0,
	}
}

pub unsafe fn send_aise_command(unit1: *mut bw::structs::Unit, unit2: *mut bw::structs::Unit, signal_type: AiseSignal, v1: u32, v2: u32, v3: u32) -> u32 {
    return bw::send_aise_command_func(v1, aiscript::aise_signal_to_u32(signal_type), 33333, unit1, v2, unit2, v3);
}

pub unsafe fn get_new_upgrade_level(game: Game, globals: &mut Globals, upgrade: u16, player: u8)->u8{
	return get_new_upgrade_level_upg(game,&mut globals.upgrades,upgrade,player);
}

pub unsafe fn get_new_upgrade_level_upg(game: Game, upgrades: &mut Upgrades, upgrade: u16, player: u8)->u8{
	let config = config::config();
	let up_lev = match upgrade {
		0..=45=>{
			game.upgrade_level(player,UpgradeId(upgrade))
		},
		_=>{
			if config.extended_upgrades {
				upgrades.get_upgrade_level(upgrade as u32,player as u32)
			}
			else {
				game.upgrade_level(player,UpgradeId(upgrade))
			}			
		},
	};	
	up_lev
}

pub unsafe fn get_next_upgrade_cost(game: Game, upgrades: &mut Upgrades, upgrade: u16, player: u8)->ai::Cost{
	let config = config::config();
	match upgrade {
		0..=45 => {
			return ai::upgrade_cost(UpgradeId(upgrade),game.upgrade_level(player,UpgradeId(upgrade))+1);
		},
		_=> {
			if config.extended_upgrades {
				let level = upgrades.get_upgrade_level(upgrade as u32,player as u32);
				let cost = ai::Cost { 
					minerals: upgrades.mineral_cost(upgrade as u32)+(upgrades.mineral_cost_extra(upgrade as u32)*(level as u32+1)),
					gas: upgrades.gas_cost(upgrade as u32)+(upgrades.gas_cost_extra(upgrade as u32)*(level as u32+1)),
					supply: 0,
					races: RaceFlags::empty(),
				};			
				return cost;
			}
			else {
				return ai::upgrade_cost(UpgradeId(upgrade),game.upgrade_level(player,UpgradeId(upgrade))+1);
			}
		},
	}
}

pub unsafe fn get_new_max_upgrade_level(game: Game, globals: &mut Globals, upgrade: u16, player: u8)->u8{
	return get_upg_max_upgrade_level(game,&mut globals.upgrades,upgrade,player);
}
pub unsafe fn get_upg_max_upgrade_level(game: Game, upgrades: &mut Upgrades, upgrade: u16, player: u8)->u8{
	let config = config::config();
	let up_lev = match upgrade {
		0..=45=>{
			game.upgrade_max_level(player,UpgradeId(upgrade))
		},
		_=>{		
			//debug!("UDEBUG - gnul");
			if config.extended_upgrades {
				upgrades.get_max_upgrade_level(upgrade as u32,player as u32)
			}
			else {
				game.upgrade_max_level(player,UpgradeId(upgrade))
			}			
		},
	};	
	up_lev
}

pub unsafe fn set_new_upgrade_level_current(game: Game, globals: &mut Globals, upgrade: u16, player: u8, level: u8){
	let config = config::config();
	match upgrade {
		0..=45=>{
			game.set_upgrade_level(player,UpgradeId(upgrade),level);
		},
		_=>{
			if config.extended_upgrades {
				globals.upgrades.set_upgrade_level(upgrade as u32,player as u32,level as u32);
			}
			else {
				game.set_upgrade_level(player,UpgradeId(upgrade),level);
			}			
		},
	}
}

pub unsafe fn set_new_upgrade_level_max(game: Game, globals: &mut Globals, upgrade: u16, player: u8, level: u8){
	let config = config::config();
	match upgrade {
		0..=45=>{
			game.set_max_upgrade_level(player,UpgradeId(upgrade),level);
		},
		_=>{
			if config.extended_upgrades {
				globals.upgrades.set_max_upgrade_level(upgrade as u32,player as u32,level as u32);
			}
			else {
				game.set_max_upgrade_level(player,UpgradeId(upgrade),level);
			}			
		},
	}
}

//get values from gptp
	//004797B0 = DamageUnit(), arg 1 Unit *attacker, arg 2 player, arg 3 bool show_attacker, ecx Unit *target, eax damage

pub unsafe fn damage_unit_hook(
		attacker: *mut bw::Unit,
		attacking_player: u32,
		show_attacker: u32,
		target: *mut bw::Unit,
		damage: u32,
		orig: &dyn Fn(*mut bw::Unit,u32,u32,*mut bw::Unit,u32),){
			//comm tower required, set in lib.rs
			if target==null_mut(){
				orig(attacker,attacking_player,show_attacker,target,damage);
				return;
			}
			let target = Unit::from_ptr(target).unwrap();
			let game = Game::get();
			let config = config::config();
			if config.overlord && attacking_player<8 && target.player()<8 && attacker!=null_mut() 
				&& (*attacker).target==target.0 && *bw::local_nation_id==attacking_player
			{
				//possible desync warning
				if attacking_player as u8!=target.player() && game.allied(attacking_player as u8,target.player()) {
					let p1 = attacking_player as u8;
					let p2 = target.player() as u8;
					let mut globals = Globals::get("damage unit");
					if p1<8 && p2<8 && !globals.ngs.diplomacy[p1 as usize].alliance_status[p2 as usize] {
						//globals.ngs.adjust_disposition(p1,p2,d);
						let faction = globals.ngs.diplomacy[p2 as usize].faction;
						if globals.ngs.diplomacy[p1 as usize].disposition[p2 as usize] >= 100  
							 {
							//0 is zero faction
							globals.ngs.subtract_disposition(p1,p2,50);
						}
						else if faction!=OverlordFaction::NoFaction  {
							globals.ngs.subtract_disposition(p1,p2,50);
							bw::visions[p1 as usize] &= !(1<<p2);
							bw::visions[p2 as usize] &= !(1<<p1);
							game.set_alliance(p1,p2,0);
							let _ = bw::modify_alliance_status(p1 as u32);
							let _ = bw::set_alliance();
							let color = globals.ngs.factions.values().find(|x| faction == x.faction_id)
								.map(|x| x.color_index).unwrap();
							let name = globals.ngs.factions.values().find(|x| faction == x.faction_id)
								.map(|x| x.name.clone()).unwrap();	
							let gtc = *bw::next_frame_tick+7000;
							hostility_msg(color,name.clone(),gtc);
						}
					}
				}			
			}
			orig(attacker,attacking_player,show_attacker,target.0,damage);
		}
pub fn gptp_opcode(dmg_divide: u32)->GptpId{
	let opcode = match dmg_divide {
		1=>GptpId::AddCyprianSwitch,
		2=>GptpId::SetCyprianStatus,
		3=>GptpId::SetEscalation,
		4=>GptpId::EmpowerHero,
		5=>GptpId::BlindJudgeTimedEffect,
		6=>GptpId::SetLastOrders,
		7=>GptpId::PrepareToExplode,
		8=>GptpId::ErasePersistingUnit,
		9=>GptpId::SetGenericSwitch,
		10=>GptpId::SetGenericValue,
		11=>GptpId::AddGenericValue,
		12=>GptpId::LinkWarpRelayToStructure,
		13=>GptpId::SetApertureStatus,
		14=>GptpId::PlaceScreamerPool,
		15=>GptpId::SetPhaselinkStatus,
		16=>GptpId::PlaceClarionLinksForUnit,
		17=>GptpId::SpreadClarionDamage,
		18=>GptpId::SetGenericTimer,
		19=>GptpId::FreeMindControlled,
		20=>GptpId::SetMindControlLink,
		21=>GptpId::SetMaelstromOffset,
		22=>GptpId::SetMatrixCaster,
		23=>GptpId::SetSublimeShepherd,
		24=>GptpId::SetGenericUnit,
		25=>GptpId::DirgeImpale,
		26=>GptpId::ClearClarionLinks,
		27=>GptpId::SetGenericVariable,
		28=>GptpId::SetSpriteData,
		29=>GptpId::SetGuard,
		30=>GptpId::SetBaseLayout,
		10000=>GptpId::AddNSeverityStacks,
		10001=>GptpId::SetDeathwish,
		10002=>GptpId::PrepareToExplodeTodborne,
		10003=>GptpId::ActivistBehavior,
		10004=>GptpId::SwitchSpatulaSwitch,
		10005=>GptpId::Void,
		10006=>GptpId::Goodboy,
		10007=>GptpId::DeathbyDoughSwitch,
		10008=>GptpId::SteelFreeze,
		20000=>GptpId::SetMeskalloidStatus,
		20001=>GptpId::SetLastSuckedUnit,
		20002=>GptpId::ResetListening,
		20003=>GptpId::SetBasiliskGimmick,
		20004=>GptpId::SetMineralChunkStatus,
		20005=>GptpId::CustomStimAI,
		20006=>GptpId::SetExpelWaste,
		_=>GptpId::NoOpcode,
	};
	opcode
}
pub unsafe fn do_weapon_damage(
	weapon_id: u32,
	dmg_divide: u32,
	direction: u32,
	attacker: *mut bw::Unit,
	player: u32,
	target: *mut bw::Unit,
	base_damage: u32,
	orig: &dyn Fn(u32,u32,u32,*mut bw::Unit,u32,*mut bw::Unit,u32),){
		//special hook - write to globals
		let opcode = gptp_opcode(dmg_divide);
		let attacker = match Unit::from_ptr(attacker) {
			Some(s) => s,
			None => {

				if direction==50000 {
					let mut globals = Globals::get("set_gptp_value2");
					match opcode {
						GptpId::SetGenericVariable=>{
							globals.ngs.set_generic_variable(weapon_id,player as i32);
							return;
						},
						GptpId::SetSpriteData=>{
							let config = config::config();
							let index = player as usize;
							if config.extended_data {
								data_extender::set_sprite_data(weapon_id,player as usize,base_damage);			
							}
							else {
								match weapon_id {
									0 =>bw::sprites_dat_vision[index]=base_damage as u8,
									1 =>bw::sprites_dat_as_visible[index]=base_damage as u8,
									2 =>bw::sprites_dat_image_id[index]=base_damage as u16,
									3 =>bw::sprites_dat_selection_circle[index]=base_damage as u8,
									4 =>bw::sprites_dat_selection_imagepos[index]=base_damage as u8,
									5 =>bw::sprites_dat_healthbar[index]=base_damage as u8,
									_=>(),
								};
							}	
							return;
						},
						_=>(),
					}
					drop(globals);
				}
				orig(weapon_id,dmg_divide,direction,attacker,player,target,base_damage);
				return;
			},
		};
		if direction == 50000 {
			//bw_print!("Aise Opcode {} (Get) received",dmg_divide);
			let a1 = weapon_id;
			let a2 = player;
			let a3 = base_damage;/*
			let cmd = dmg_divide;*/
			
			let mut globals = Globals::get("weapon_dmg_get_gptp_values");
			match opcode {
				GptpId::AddCyprianSwitch => {
					globals.ngs.add_cyprian_variable(attacker);
				},
				GptpId::SetCyprianStatus => {			
					globals.ngs.set_cyprian_status(attacker,weapon_id);//dmg_divide is 0/1 switch
				},
				GptpId::SetEscalation => {
					globals.ngs.set_escalation(weapon_id,player);
				},
				GptpId::EmpowerHero=>{
					if weapon_id!=0{
						let hero = bw::get_loaded_unit(attacker.0, 0);
						let hero = match Unit::from_ptr(hero) {
							Some(s) => s,
							None => {
								orig(weapon_id,dmg_divide,direction,attacker.0,player,target,base_damage);
								return;
							},
						};
						globals.ngs.take_relic(attacker,weapon_id);
						//bw_print!("Equip {}",weapon_id);
						globals.ngs.equip_relic(hero,weapon_id);
						
					}
				},
				GptpId::SetGenericSwitch=>{
					globals.ngs.set_generic_switch(attacker,a1);
				},
				GptpId::SetGenericValue=>{
					globals.ngs.set_generic_value(attacker,a1,a2 as i32);
				},			
				GptpId::AddGenericValue=>{
					globals.ngs.add_generic_value(attacker,a1,a2 as i32);
				},
				GptpId::ResetListening=>{
					for i in 0..8 {
						globals.ngs.diplomacy[i as usize].listening_system[a1 as usize].generic_timer =
							globals.ngs.diplomacy[i as usize].listening_system[a1 as usize].generic_max_const;
					}
				},
				GptpId::BlindJudgeTimedEffect=>{
					bw_print!("Add Timed Effect [1]");
					globals.ngs.add_bj_effect(attacker.0,target);
				},
				GptpId::SetLastOrders=>{
					globals.ngs.set_last_orders(attacker);
				},
				GptpId::LinkWarpRelayToStructure=>{
//					bw_print!("Link");
					globals.ngs.link_relay_to_structure(attacker,Unit::from_ptr(target).unwrap());
				},
				GptpId::SetApertureStatus=>{
					globals.ngs.set_aperture_status(attacker,weapon_id != 0);
				},
				GptpId::PlaceScreamerPool=>{
					globals.ngs.place_screamer_pool(attacker,a1,a2,a3);
				},
				GptpId::SetPhaselinkStatus=>{
					globals.ngs.set_phaselink_status(attacker,weapon_id != 0);
				},			
				GptpId::PlaceClarionLinksForUnit=>{
					globals.ngs.place_clarion_links(attacker);
				},
				GptpId::SpreadClarionDamage=>{
					globals.ngs.spread_clarion_damage(attacker, a1, a2, a3, target);
				
				},
				GptpId::SetGenericTimer=>{
					globals.ngs.set_generic_timer(attacker,a1,a2);
				},
				GptpId::FreeMindControlled=>{
					globals.ngs.free_all(attacker);
				},
				GptpId::SetMindControlLink=>{
					globals.ngs.set_mind_control_link(attacker,Unit::from_ptr(target).unwrap());
				},
				GptpId::SetMaelstromOffset=>{
					globals.ngs.set_maelstrom_offset(attacker.0,a1);
				},
				GptpId::SetMatrixCaster=>{
					globals.ngs.set_matrix_caster(attacker,Unit::from_ptr(target).unwrap());
				},
				GptpId::SetSublimeShepherd=>{
					globals.ngs.set_sublime_shepherd(attacker,Unit::from_ptr(target).unwrap());
				},
				GptpId::SetGenericUnit=>{
					if target == null_mut(){
						globals.ngs.clear_generic_unit(attacker,a1);
						return;
					}
					globals.ngs.set_generic_unit(attacker,a1,Unit::from_ptr(target).unwrap());
				},
				GptpId::DirgeImpale=>{
					globals.ngs.add_madrigal_timer(attacker,a1);
				},
				GptpId::ClearClarionLinks=>{
					globals.ngs.clear_clarion_links(attacker);
				},
				GptpId::SetGenericVariable=>{},//handled elsewhere
				GptpId::SetSpriteData=>{},
                GptpId::SetGuard=>{
                    let base_layout = attacker.0 as *mut aiscript::GptpBaseLayout;
                    let town = Town::from_ptr(target as *mut bw::AiTown).unwrap();
                    let player = a1;

                    // Copy paste from guard_command
                    let unit_id = globals.unit_replace.replace_check(UnitId((*base_layout).unit_id as u16));
                    let guards = samase::guard_ais().add(player as usize);
                    let old_first_active = (*guards).first;
                    let new_ai = (*(*guards).array).first_free;
                    if !new_ai.is_null() {
                        (*new_ai) = bw::GuardAi {
                            next: (*new_ai).next,
                            prev: (*new_ai).prev,
                            ai_type: 1,
                            times_died: 0,
                            dca: [0, 0],
                            parent: null_mut(),
                            unit_id: unit_id.0,
                            home: (*base_layout).position,
                            other_home: (*base_layout).position,
                            padding1a: [0, 0],
                            previous_update: 0,
                        };
                        let new_first_free = (*new_ai).next;
                        (*(*guards).array).first_free = new_first_free;
                        if !new_first_free.is_null() {
                            (*new_first_free).prev = null_mut();
                        }
                        (*new_ai).next = old_first_active;
                        if !old_first_active.is_null() {
                            (*old_first_active).prev = new_ai;
                        }
                        (*guards).first = new_ai;
                        globals.guards.add(
                            (*(*guards).array).ais.as_mut_ptr(),
                            new_ai,
                            (*base_layout).guard_rebuilds,
                            (*base_layout).priority,
                        );
                    }
                },
                GptpId::SetBaseLayout=>{
                    let base_layout = attacker.0 as *mut aiscript::GptpBaseLayout;
                    aiscript::set_free_id_town(&mut globals, target as *mut bw::AiTown); // Set free id if we don't have any (required for base layouts...)
                    let town = Town::from_ptr(target as *mut bw::AiTown).unwrap();
                    let player = a1;
                    let unit_id = globals.unit_replace.replace_check(UnitId((*base_layout).unit_id as u16));
                    let layout = BaseLayout {
                        pos: Position::from_point((*base_layout).position.x, (*base_layout).position.y).area,
                        player: player as u8,
                        unit_id: unit_id,
                        amount: 1,
                        town_id: 255,
                        town: town,
                        priority: (*base_layout).priority,
                        unique_id: "".to_string(),
                        is_placeholder: (*base_layout).should_request != 0,
                    };
                    globals.base_layouts.try_add(layout);
                },
				//
				//
				GptpId::SetDeathwish=>{
					globals.ngs.set_deathwish(attacker);
				},				
				GptpId::PrepareToExplode=>{
//					bw_print!("Prepare to explode");
					globals.ngs.prepare_to_explode(attacker);
				},
				GptpId::PrepareToExplodeTodborne=>{
					globals.ngs.prepare_to_explode_todborne(attacker);
				},		
				GptpId::ActivistBehavior=>{
					globals.ngs.feeling_the_bern(attacker,(*target).sprite,a1,a2,0);
				},				
				GptpId::ErasePersistingUnit=>{
					globals.ngs.persist_unit_structs.swap_retain(|x| x.unit != attacker);
				},
				GptpId::AddNSeverityStacks=>{
					let game=Game::get();
					let up_lev = get_new_upgrade_level(game,&mut globals,25,attacker.player());
					//Todborne - Moderation upgrade-dependent, not removing
					globals.ngs.add_n_moderation_stacks(attacker,a1,up_lev as u32);
				},
				GptpId::SwitchSpatulaSwitch=>{
					globals.ngs.switch_spatula(attacker);
				},
				GptpId::Void=>{
					globals.ngs.set_void(attacker,a1);
				},
				GptpId::Goodboy=>{
					globals.ngs.set_goodboy_timer(attacker);
				},
				GptpId::DeathbyDoughSwitch=>{
					globals.ngs.iterate_death_by_dough(attacker);
				},
				GptpId::SteelFreeze=>{
					globals.ngs.set_frosted_timer(attacker);
				},			
				//overlord
				GptpId::SetMeskalloidStatus=>{
					globals.ngs.set_meskalloid_timer(attacker,a1);
				},
				GptpId::SetLastSuckedUnit=>{
					globals.set_sucking_data(attacker,target,a1,a2);
				},
				GptpId::SetBasiliskGimmick=>{
					globals.ngs.set_basilisk_gimmick(attacker.sprite().unwrap(),a1);
				},
				GptpId::SetMineralChunkStatus=>{
					globals.ngs.set_mineral_chunk_status(attacker.sprite().unwrap(),a1);
				},
				GptpId::CustomStimAI=>{
					globals.ngs.add_single_effect(attacker.0,HydraEffect::CustomStim);
				},
				GptpId::SetExpelWaste=>{
					globals.ngs.set_expel_status(attacker,a1);
				},
				GptpId::NoOpcode => {},
				
			}
			drop(globals);
			return;
		}
		
		let target = match Unit::from_ptr(target) {
			Some(s) => s,
			None => {
						orig(weapon_id,dmg_divide,direction,attacker.0,player,target,base_damage);
						return;
					},
		};
		let mut dmg = base_damage;
		let mut globals = Globals::get("weapon_dmg");	
		globals.events.register_combat_event(attacker,target,weapon_id,24);

		let game = Game::get();
		let config = config::config();
		match attacker.id().0 {
			43 => {
				//todborne moderator
				if config.moderator_behavior {
					let up_lev = get_new_upgrade_level(game,&mut globals,25,attacker.player());
					//bw_print!("attack upgrade level: {}",up_lev);
					
//					bw_print!("config is good, add stack");
					if up_lev > 0 {
						let overload = match up_lev {
							1=>0,
							2=>1,
							_=>0,
						};
						/*if overload==1 {
							bw_print!("Can overload");
						}*/
						let stacks = globals.ngs.add_moderation_stack(target,overload);
						match stacks {
							10|20=>{
								//bw_print!("Can overload: {}",globals.ngs.can_overload(target));
								if overload!=0{
									globals.ngs.add_single_effect_misc(target.0, HydraEffect::SeverityOverload, stacks/2);
								}					
							},
							_=>{},
						}
						globals.ngs.add_single_effect(target.0,HydraEffect::UpdateSpeed);					
					}

				}
				
			},
			76 => {
				//hierophant
//				let up_lev = game.upgrade_level(attacker.player(), UpgradeId(55));
				//debug!("Read Hier");
				//let up_lev = globals.upgrades.get_upgrade_level(55,attacker.player() as u32);
				//debug!("UDEBUG hier");				
				if config.hierophant_timer {
					//let up_lev = get_new_upgrade_level(game,&mut globals,55,attacker.player());
					//globals.ngs.add_hierophanted_unit(attacker,target,up_lev as u32);
					globals.ngs.add_hierophanted_unit(attacker,target,1);
					if !target.id().is_building() {
						(*target.0).unused8c |= 0x1;
						(*target.0)._dc132 |= 0x10;
					}				
				}
			},
			74 => {
				//legionnaire
				if config.legionnaire_timer {
					let mut bonus = globals.ngs.get_legionnaire_marks(attacker,target);
//					let up_lev = get_new_upgrade_level(game,&mut globals,56,attacker.player());//old battle harmony code
					let max = 2;
					/*if up_lev>0 {
						max = 4;
					}*/
					if bonus>max {
						bonus=max;
					}/*
					if bonus > 0 {
						dmg += 256*2*bonus;
					}*/
					globals.ngs.add_legionnaire_unittarget(attacker,target);				
				}
			},
			
			_=>{},
		}	
		
		/*
		if target.id().0==76 {
			let up_lev = game.upgrade_level(attacker.player(), UpgradeId(55));//hierophant upgrade
			if up_lev>0 {
				globals.ngs.hierophant_proc(attacker,target);
			}
		}*/
		drop(globals);	
		orig(weapon_id,dmg_divide,direction,attacker.0,player,target.0,dmg);
	}
	
/*
0045991C   .  FF56 08       CALL DWORD PTR DS:[ESI+8]
call button->action
0x8     func *Action(), ecx var, edx shift_down*/

pub unsafe fn finish_unit_hook(
	unit: *mut bw::Unit,
	orig: &dyn Fn(*mut bw::Unit),){
		let config = config::config();
		if config.overlord {
			let mut globals = Globals::get("finish_unit");
			match (*unit).unit_id {
				174|190=>{//scrap depot, commtower
					let u_str = globals.ngs.default_unit_struct(Unit::from_ptr(unit).unwrap());
					globals.ngs.persist_unit_structs.push(u_str);
				},
				_=>{},
			}
			
		}
		orig(unit);
	}
pub unsafe fn cmdbtn_handler_hook(
	control: *mut bw::Control,
	event: *mut bw::Event,
	orig: &dyn Fn(*mut bw::Control,*mut bw::Event)->u32,)->u32{
		let config=config::config();
		//
		let button = (*control).val as *mut bw::Button;
		let mut globals = Globals::get("cmdbtn_handler_start");
		if button!=null_mut(){
			if (*button).action as u32 == 0x00459AF0 && (*button).act_var == 93 || (*button).act_var==94 || (*button).act_var==95 {
				let player = *bw::active_player_id;
				globals.ngs.register_button(player as u8,button);								
			}
		}
		drop(globals);
		//
		match (*event).extended_command_type {
			0x2 => {
				let mut globals = Globals::get("cmdbtn_handler2");
				globals.ngs.last_button = (*button).pos as u8;		
				if globals.ngs.global_right_click_status {
					//bw_print!("Global flag detected, execute custom code2");
					globals.ngs.set_right_click_status[*bw::active_player_id as usize] = true;
					let func = (*button).action;
					func((*button).act_var as u32,*bw::shift_down as u32);				
					return 1;
				}
				else {
					globals.ngs.set_right_click_status[*bw::active_player_id as usize] = false;
				}
				
				drop(globals);
			},
			_=>{},
		}	
		if config.comm_tower_struct {
			let mut globals = Globals::get("cmdbtn_handler1");
			//globals.ngs.global_right_click_status = false;
			//let mut right = false;
			match (*event).event_type {
				0x7|0x9=>{
					//bw_print!("button right");
					if (*button).action as u32 == 0x00423F70 && (*button).act_var == 26 && (*button).pos==7 {
//						bw_print!("rclickable");
						
						bw::register_event_handler(0x3,control);											
						bw::register_event_handler(0x8,control);
						bw::update_button_state(event,control);
						return 1;				
					}
					else {
//						bw_print!("not rclickable");
						return 0;
					}
				},
				0x8=>{
					//let mut globals = Globals::get("cmdbtn_handler2");
//					bw_print!("button up, ext event {}",(*event).extended_command_type);
					globals.ngs.global_right_click_status = true;		
					//right = true;
					drop(globals);
					bw::button_up(control);
					let mut globals = Globals::get("cmdbtn_handler2");
					globals.ngs.global_right_click_status = false;
//					bw_print!("button up");
					return 1;
				},
				_=>{},
			}			

			let player = *bw::active_player_id;
			let unit = bw::player_selections[player as usize][0];
			let current_buttonset = *bw::current_button_set;
/*			
			if button!=null_mut(){
				if (*button).action as u32 == 0x00459AF0 && (*button).act_var == 93 {
					globals.ngs.register_button(player as u8,button);								
				}
			}
			*/
			if unit!=null_mut(){
				if (*unit).unit_id==190 && button != null_mut() {
					globals.ngs.comm_tower_setup(unit);
					match (*event).extended_command_type {
						0x2 => {
							if (*button).action as u32 == 0x00459AF0 {//Change Displayed Buttons
								//let buttons = (*unit).buttons;
								let next = (*button).act_var;
								//globals.ngs.set_previous(unit,buttons);
//								bw_print!("Change buttonset: {} -> {}",current_buttonset,next);
								match current_buttonset {
									90|91|218|103|95=>{
										globals.ngs.set_previous(unit,current_buttonset);
										
									},
									_=>{},
								}
								match next {	
									93=>{
										globals.ngs.set_button_n(unit,button);
										//bw_print!("Current: {}",curren
										match current_buttonset {
											218=>{
												(*button).act_var = 94;
											},
											91=>{
												(*button).act_var = 95;
											},
											_=>{},
										}
									},
									
									_=>{},
								}
								globals.ngs.set_current_menu(unit,(*button).act_var);
							}										
						},
						_=>{},
					}
			
				}			
			}
		}
		let result = orig(control,event);
		result
	}

	
pub unsafe fn playframe_hook(
	frame: u32,
	img: *mut bw::Image,
	orig: &dyn Fn(u32,*mut bw::Image),){
		if img!=null_mut(){
			let config = config::config();
			if config.overlord {
				match (*img).image_id {
					347|349|351=>{
						let mut globals = Globals::get("playframe");					
						if globals.ngs.is_meskalloid((*img).parent)!=0 {
							//orig(frame+4,img);
							orig(frame+16,img);
							return;								
						}		
						//bw_print!("playfram {}", globals.ngs.mineral_opcode((*img).parent));
						if globals.ngs.mineral_opcode((*img).parent)>1{
							let opcode = globals.ngs.mineral_opcode((*img).parent)-1;
							//bw_print!("opcode {} offset {}",opcode,opcode*4);
							orig(frame+(opcode*4),img);
							return;
							//if 2
						}
					},
					397=>{
						let mut globals = Globals::get("playframe");
						let res_id = globals.ngs.get_mineral_chunk_status((*img).parent);
						
						match res_id {
							2=>{//connorite
								orig(frame+0x22,img);
								return;
							},
							3=>{//tricorvid
								orig(frame+0x11,img);
								return;
							},
							1|4=>{
								orig(frame+0x33,img);
								return;							
							},
							_=>{},
						}
					},
					50|51=>{//basilisk
						let mut globals = Globals::get("playframe");
						if globals.ngs.basilisk_gimmick((*img).parent)!=0 {
							orig(frame+0x99,img);
							return;
						}
					},
					_=>{},
				}
			}
			match (*img).image_id {
				982..=984=>{
					let mut globals = Globals::get("playframe_maelstrom");
					let offset = globals.ngs.maelstrom_offset((*img).parent);
//					bw_print!("Playfram {}, off: {}",frame,offset);
					orig(frame+offset,img);
					return;	
				},
			
				_=>{},
			}
			if config.full_toilet {
				match (*img).image_id {
					158|167|179|189|174|199|164|195=>{//assimilator,forge,nexus,pylon,cybcore,stargate,citadel,battery
						if (*(*img).parent).player==3 {//nerazim
							let offset = 1;
							orig(frame+offset,img);
							return;
						}
					},
					965|985=>{//argosy,starfield
						if (*(*img).parent).player==3 {//nerazim
							let offset = 1;
							orig(frame+offset,img);
							return;
						}
					},	
					947|934=>{//warp anchor, principality
						if (*(*img).parent).player==3 {//nerazim
							let offset = 6;
							orig(frame+offset,img);
							return;
						}					
					},
					176=>{//cybcore overlay
						if (*(*img).parent).player==3 {//nerazim
							let offset = 2;
							orig(frame+offset,img);
							return;
						}					
					},
					168|161|204|208|155=>{//forge overlay, observatory, robo bay,fleet beacon,archives
						if (*(*img).parent).player==3 {
							let offset = 3;
							orig(frame+offset,img);
							return;
						}	
					},
				
					211=>{//anchor
					
						if (*img).parent!=null_mut() && (*(*img).parent).player==3 {
//							let ptr = (*bw::remap_data).offset(0x3 as isize);
//							(*img).drawfunc_param = (*bw::remap_data).offset(0x1 as isize) as *mut libc::c_void;	
							//bw_print!("Compare: {:?} {:?}",(*(*bw::remap_data)).offset(0x3 as isize).data,(*img).drawfunc_param);
							//(*img).drawfunc_param = (*bw::remap_data.offset(0x1 as isize)).data as *mut libc::c_void;
							//let ptr = (*bw::remap_data.offset(0x3 as isize)).data;
							//debug!("{:?}",(*img).drawfunc_param);
						//	debug!("{:?}",ptr);

//							bw_print!("Compare: {:?} {:?}",ptr,(*img).drawfunc_param);
//							bw_print!("Compare: {:?} {:?}",bw::color_shift_data[0x3].data,(*img).drawfunc_param);
							(*img).drawfunc_param = bw::color_shift_data[0x1].data;
							orig(frame,img);
							return;
						}						
					}
					183=>{//photon cannon overlay
						if (*(*img).parent).player==3 {
							let offset = 4;
							orig(frame+offset,img);
							return;
						}						
					},
					171=>{//gateway		
						if (*(*img).parent).player==3 {//nerazim
							let offset = 6;
							orig(frame+offset,img);
							return;
						}					
					},
					192=>{//robo facility
						if (*(*img).parent).player==3 {//nerazim
							let offset = 13;
							orig(frame+offset,img);
							return;
						}						
					},
					_=>{},
				}
			}

		}
		orig(frame,img);
	}
	
	
pub unsafe fn unselectable_hook(
	unit_id: u32,
	orig: &dyn Fn(u32)->u32,
	)->u32{
	let result = orig(unit_id);
	/*
	if unit_id==180 {
		return 1;
	}*/
	let config = config::config();
	if config.unselectable.single_list.clone().iter_mut().find(|x| x.0 == unit_id as u16).is_some(){
		return 1;			
	}
	if config.selectable.single_list.clone().iter_mut().find(|x| x.0 == unit_id as u16).is_some(){
		return 0;	
	}
	/*
	if unit_id==85 {
		return 0;
	}*/
	result
}
/*
pub unsafe fn creep_recede_hook(
	unit: *mut bw::Unit,
	orig: &dyn Fn(*mut bw::Unit)->u32,
	)->u32{
		let mut unit = match Unit::from_ptr(unit) {
			Some(s) => s,
			None => return orig(unit),
		};
		let mut basilisk =false;
		if unit.id().0 == 180 {
			(*unit.0).unit_id = 143;
			basilisk = true;
		}
		let result = orig(unit.0);
		if basilisk {
			(*unit.0).unit_id = 180;
		}
		result
	}

pub unsafe fn progress_creep_disappearance(
	orig: &dyn Fn(),){
		orig();
	}
	*/
	/*
	//incorrect hook, must be fixed
pub unsafe fn set_megatiles_and_flags(
	orig: &dyn Fn(),){
		//orig();
		let game = Game::get();
		let width = (*game.0).map_width_tiles as u32;
		let height = (*game.0).map_height_tiles as u32;
		let mul = width*height;
		if mul==0 {
			return;
		}
		for i in 0..mul {
			let id = (*(*bw::tile_ids).offset(i as isize)) as u32;
			let tile_group = (id>>0x4)&0xfff;//old - 7ff
			let index = id & 0xf;//get megatile number in tile group
			let megatile_id = (*(*bw::tile_groups).offset(tile_group as isize)).megatiles[index as usize];
			*(*bw::megatiles).offset(i as isize) = megatile_id;
			*(*bw::tile_flags).offset(i as isize) &= 0xffff0000;//clear 2 bytes - player vision/fog
			let mut flags = (*(*bw::tile_groups).offset(tile_group as isize)).flags as u32;
			let unk_flag = (*(bw::unk_flags).offset(i as isize));
			flags = (flags & 0xffffd8fa) << 0x10;
			*(*bw::megatiles).offset(i as isize) &= 0x7fffffff;//horizontal flip flag, 7fff originally
			flags |= unk_flag as u32;
			*(*bw::tile_flags).offset(i as isize) |= flags;
			if (*(*bw::tile_ids).offset(i as isize)) & 0x8000 != 0 {
				*(*bw::megatiles).offset(i as isize) |= 0x8000_0000;
				*(*bw::tile_flags).offset(i as isize) |= 0x40_0000;
			}		
		}
		
	}
*/
	

unsafe fn good_military_ai(
	unit: Unit,
	)->bool {
		if let Some(ai) = unit.military_ai() {
            let state = (*(*ai).region).state;
			if state==1 || state==4 || state==5 {
				return true;
			}
			return true;
        }
		false
	}
	
unsafe fn has_guard_ai(
	unit: Unit,
	)->bool {
		if unit.guard_ai().is_some(){
			return true;
		}
		false
	}
	
pub unsafe fn progress_military_ai_hook(
	unit: *mut bw::Unit,
	move_order: u32,
	x: u32,
	y: u32,
	orig: &dyn Fn(*mut bw::Unit,u32,u32,u32),){
		let unit = match Unit::from_ptr(unit) {
			Some(s) => s,
			None => {
						orig(unit,move_order,x,y);
						return;
					},
		};
		orig(unit.0,move_order,x,y);
	}

pub unsafe fn btnsact_placeaddon_hook(
	id: u16,
	orig: &dyn Fn(u16),){
	
		//orig(id);
		let config = config::config();
		if !config.addon_autoplacing {
			orig(id);
			return;
		}
		let unit = match Unit::from_ptr(*bw::active_sel) {
			Some(s) => s,
			None => { 
				  orig(id);
				  return;
			},
		};
		let game = Game::get();
		let unit_id = id;
		let player = unit.player();
		let mineral_cost = unit.id().mineral_cost();
		let gas_cost = unit.id().gas_cost();
		bw::player_build_minecost[player as usize] = mineral_cost;
		bw::player_build_gascost[player as usize] = gas_cost;
		let result = bw::check_supply_for_building(player as u32,unit_id as u32,1);
		if result==0 {
			return;
		}
		let race = *bw::game_player_race;
		if game.minerals(player) < bw::player_build_minecost[player as usize] {	
			let message = 0x352;
			let sound = 0x93+race;
			bw::show_info_msg(player as u32,message,sound as u32);
			return;			
		}
		//block1
		if game.gas(player) < bw::player_build_gascost[player as usize] {
			let message = 0x353;
			let sound = 0x96+race;
			bw::show_info_msg(player as u32,message,sound as u32);
			return;
		}
		//issue placebuilding order
		unit.issue_order_ground(OrderId(0x24), unit.position()); 
        (&mut (*unit.0).unit_specific[4..]).write_u16::<LE>(unit_id).unwrap();
		//bw::issue_placebuilding_order(unit_id, 0x24);
	}

	
	
	/*
pub unsafe fn ss_drawarmor_hook(
	status_index: u32,
	ctrl: *mut bw::Control,
	orig: &dyn Fn(u32,*mut bw::Control),){
		//orig(status_index,ctrl);
	}
	*/
pub unsafe fn find_child(
	ctrl: *mut bw::Control, ctype: u16)->*mut bw::Control{
		let mut first_child = (&(*ctrl).control_specific[0xc..]).read_u32::<LE>().unwrap() as *mut bw::Control;
		while first_child != null_mut(){
			if (*first_child).control_type==ctype {
				return first_child;
			}
			first_child = (*first_child).next;
		}	
		return null_mut();
}

use std::io::Write;
/*pub unsafe fn increment_kills(
	unit: *mut bw::Unit,
	orig: &dyn Fn(*mut bw::Unit),){
	orig(unit);
}*/


/*pub unsafe fn ss_drawkills(
	ctrl: *mut bw::Control,
	orig: &dyn Fn(*mut bw::Control),){
		//orig(ctrl);
		//orig(ctrl);
		
		
		let active_unit = match Unit::from_ptr(*bw::active_sel) {
			Some(s) => s,
			None => { 
			  orig(ctrl);
			  return;
			},
		};
		let id = active_unit.id().0;
		if id==47 || id==50 || active_unit.is_hallucination() || (!active_unit.has_way_of_attacking() && (*active_unit.0).kills!=0){
			let find = find_child(ctrl,0xffeb);
			if find!=null_mut(){
				bw::hide_control(find);
			}		
		}
		else {			
			let buf_len = 0x20;
			let mut buf_ptr = 0x006CAC78 as *mut u8;
			let mut slice = std::slice::from_raw_parts_mut(buf_ptr, buf_len);
			let _ = write!(slice,"{:?}{}\0",CString::from_raw(bw::get_name(764) as *mut i8),(*active_unit.0).kills);
			bw::set_label(buf_ptr as *const u8, 0xffeb, ctrl);
		}
	}*/

	
pub unsafe fn ss_drawsupplyunitstatus(
	ctrl: *mut bw::Control,
	orig: &dyn Fn(*mut bw::Control),){
		let active_unit = match Unit::from_ptr(*bw::active_sel) {
			Some(s) => s,
			None => { 
			  orig(ctrl);
			  return;
			},
		};
		if *bw::is_replay!=0 || active_unit.player()!=*bw::local_nation_id as u8 {
			bw::draw_generic_unit_status();
			return;
		}
		bw::unk_func246d0();//sets ui_death_timer (supply used if building)
		if *bw::status_screen_func != 0xc {
			bw::hide_optional_screen_ctrl(ctrl);
			bw::show_control_group(ctrl,-0xa,-0xd);
			*bw::status_screen_func = 0xc;
		}
		let offset = active_unit.id().race() as u32;
		let game=Game::get();
		let supply_used = (*game.0).supplies[offset as usize].used[active_unit.player() as usize]/2;
		let supply_provided = (*game.0).supplies[offset as usize].provided[active_unit.player() as usize]/2;
		let supply_max = (*game.0).supplies[offset as usize].max[active_unit.player() as usize]/2;
		let mut supply_this = active_unit.id().supply_provided()/2;
		let config = config::config();
		if config.overlord && active_unit.id().0==174 {
			let mut globals = Globals::get("ss_drawsupplyunitstatus");
			let scrap = globals.ngs.get_scrap(active_unit);
			supply_this += scrap;
		}
		let buf_len = 0x20;
		let used_ptr = 0x006CA9F8 as *mut u8;
		let provided_ptr = 0x006CAC78 as *mut u8;
		let total_ptr = 0x006CA668 as *mut u8;
		let max_ptr = 0x006CAB08 as *mut u8;
		let header = match offset {
			0=>"Control",
			2=>"Psi",
			_=>"Supplies",
		};
		let mut out = slice::from_raw_parts_mut(used_ptr, buf_len as usize);
		let _ = write!(out,"{} Used: {}\0",header,supply_used);	
		bw::set_label(used_ptr as *const u8,-0xa,ctrl);
						
		let mut out = slice::from_raw_parts_mut(provided_ptr, buf_len as usize);
		if config.overlord && active_unit.id().0==174 {
			let _ = write!(out,"{} Provided: {}/{}\0",header,supply_this,
								(active_unit.id().supply_provided()/2)+6);	
		}
		else {
			let _ = write!(out,"{} Provided: {}\0",header,supply_this);	
		}
		bw::set_label(provided_ptr as *const u8,-0xb,ctrl);

		let mut out = slice::from_raw_parts_mut(total_ptr, buf_len as usize);
		let _ = write!(out,"Total {}: {}\0",header,supply_provided);	
		bw::set_label(total_ptr as *const u8,-0xc,ctrl);

		let mut out = slice::from_raw_parts_mut(max_ptr, buf_len as usize);
		let _ = write!(out,"{} Max: {}\0",header,supply_max);	
		bw::set_label(max_ptr as *const u8,-0xd,ctrl);
		
		if active_unit.id().has_shields(){
			bw::ss_draw_shield_upg(0,ctrl);
			return;
		}
		if active_unit.id().0==0x2a || active_unit.id().0==0x39 {
			bw::ss_draw_armor(0,ctrl);
			return;
		}
		else {
			let control = ctrl;
			let find = find_child(control,0x9);
			if find!=null_mut(){
				bw::hide_control(find);
			}	
		}
//		orig(ctrl);
	}

	
pub unsafe fn ss_draw_energy_hook(
	ctrl: *mut bw::Control,
	orig: &dyn Fn(*mut bw::Control),){
		//bugged - EDX is not preserved
		let config = config::config();
		if !config.new_energy_ui {
			orig(ctrl);
			return;
		}
		let unit = match Unit::from_ptr(*bw::active_sel) {
			Some(s) => s,
			None => { 
					  orig(ctrl);
					  return;
					},
		};
		
		
		let mut not_player = false;
		let player_id = (*unit.0).player;
		if *bw::local_nation_id != unit.player() as u32 {			
			let game = Game::get();
			if game.allied(unit.player(),*bw::local_nation_id as u8){
				not_player = true;			
				(*unit.0).player = *bw::local_nation_id as u8;
			}
			else {
				let search = aiscript_unit_search(game);
				let area = bw::location(63);
				let rect = bw::Rect {
					left: area.area.left as i16,
					top: area.area.top as i16,
					right: area.area.right as i16,
					bottom: area.area.bottom as i16,
				};
				
				let detector = search
					.search_iter(&rect)
					.find(|u| (u.player() as u32 == *bw::local_nation_id || game.allied(u.player(),*bw::local_nation_id as u8)) &&
								u.id().detector() && bw::distance(u.position(),unit.position())<=32+(bw::get_sight_range(u.0,1)*32));
				if detector.is_some(){
					not_player = true;			
					(*unit.0).player = *bw::local_nation_id as u8;				
				}
			}
		}
		orig(ctrl);
		
		if not_player {
			(*unit.0).player = player_id;
		}
	}
	
pub unsafe fn add_guard_ai(
	unit: *mut bw::Unit,
	orig: &dyn Fn(*mut bw::Unit),){
		if unit!=null_mut(){
			//debug!("Add guard ai: id {}, p {}",(*unit).unit_id,(*unit).player);
		}
		
		orig(unit);
	}

pub unsafe fn pre_create_guard_ai(
	unit_id: u32,
	x: u32,
	y: u32,
	player: u32,
	orig: &dyn Fn(u32,u32,u32,u32),){
		//debug!("Preplace guard u{}, x{} y{} p{}",unit_id,x,y,player);
		orig(unit_id,x,y,player);
	}

	

pub unsafe fn are_same_race_hook(unit: *mut bw::Unit, unit_id: u32,
								_orig: &dyn Fn(*mut bw::Unit,u32)->u32)->u32{
	return 1;
	//orig(unit,unit_id)
}


pub unsafe fn ai_finish_unit_hook(unit: *mut bw::Unit, orig: &dyn Fn(*mut bw::Unit),){
	debug!("AiPtr: {:p}",unit);
	orig(unit);
}

pub unsafe fn try_progress_spending_queue_hook(unit: *mut bw::Unit, orig: &dyn Fn(*mut bw::Unit),){
	if unit==null_mut(){
		debug!("BUG: Null pointer passed to try_progress_spending_queue_hook");
		return;
	}
	else if unit as u32 <= 0x100000 {
		debug!("BUG: Corrupted pointer {:p} passed to try_progress_spending_queue_hook",unit);
		return;	
	
	}
	orig(unit);
}

//has mistake, must be fixed
/*pub unsafe fn ai_spendreq_buildmorph(unit: *mut bw::Unit, ai: *mut bw::BuildingAi, player_id: u32,
								orig: &dyn Fn(*mut bw::Unit, *mut bw::BuildingAi,u32)->u32,)->u32{							
	let player = bw::player_ai(player_id);
	let req = (*player).requests[0 as usize];
	let request_id = req.id;
	let config = config::config();
	let unit = match Unit::from_ptr(unit) {
		Some(s) => s,
		None => { 
				  return orig(unit,ai,player_id);
		},
	};
	
	if request_id==0x92 && (*unit.0).player==4 {
		bw_print!("req sunken0");
	}
	
	if bw::Ai_DoesHaveResourcesForUnit(player_id,request_id as u32) == 0 {
		if request_id==0x92 && (*unit.0).player==4 {
			bw_print!("req sunken - no resources");
		}	
		return 0;
	}
	
	if request_id==0x92 && (*unit.0).player==4 {
		bw_print!("req sunken1");
	}	
	if request_id == 42 {//overlord
		if unit.id().0 != 35 {//larva
			return 0;
		}
		let town_ptr = (*ai).town;
		if bw::AiTrainUnit(request_id as u32, req.ty as u32, town_ptr as *mut libc::c_void, unit.0) == 0 {		
			return 0;
		}
		bw::issue_order_targ_nothing(unit.0, 0x2a); //unit morph 0x2a
		bw::ai_popspendingreq_resourceneeds(1, player_id);
		bw::pop_spending_request(player_id);
		return 0;
	}
	
	let morph = config.zerg_buildmorphs.single_list.iter().find(|x| x.0 == request_id);
	if morph.is_none(){
		if request_id==0x92 && (*unit.0).player==4 {
			bw_print!("not valid morph!");
		}	
		return 0;
	}
	let val = req.val as *mut bw::AiTown;
	if (*ai).town != val {
		if request_id==0x92 && (*unit.0).player==4 {
			bw_print!("not valid town!");
		}	
		return 0;
	}
	if !(bw::are_same_race(unit.0, req.id as u32)==0 || 
		bw::check_unit_dat_requirements(player_id,req.id as u32,unit.0) != 1) {
		if bw::prepare_build_unit(req.id as u32,unit.0)==0 {
			if request_id==0x92 && (*unit.0).player==4 {
				bw_print!("not valid morph!");
			}
			return 0;
		}
		bw::issue_order_targ_nothing(unit.0, 0x2b);//building morph 
		bw::ai_popspendingreq_resourceneeds(1, player_id);
		bw::pop_spending_request(player_id);
		return 1;
	}
	else {
		if request_id==0x92 && (*unit.0).player==4 {
			bw_print!("not same race or unit dat requirements are bad! {} ",bw::are_same_race(unit.0,req.id as u32));
		}	
	}
	
	let err = *bw::last_error;
	match err {
		0x2|0x17|0x15|0x8|0x7|0xd => {
			bw::ai_popspendingreq_resourceneeds(0,player_id);
			bw::pop_spending_request(player_id);
//			debug!("tryspend {:p}",unit.0);		
			bw::ai_tryprogress_spendingqueue(unit.0);
		},
		_=>{},
	}
	return 0;
}*/

pub unsafe fn kill_unit_hook(
	unit: *mut bw::Unit,
	orig: &dyn Fn(*mut bw::Unit),){
	/*if unit != null_mut(){
		debug!("K: {} {} {:?}",(*unit).unit_id,(*unit).player,(*unit).ai);
	}*/
	
	let unit = match Unit::from_ptr(unit) {
			Some(s) => s,
			None => {return;},
	};
	let unit_id = unit.id();
	if !unit_id.is_building() || !unit.is_landed_building(){
		orig(unit.0);
		return;
	}
	let mut globals = Globals::get("kill_unit_hook");
	let mesk = globals.ngs.meskalloid(unit);
	let mineral_id = globals.rng.synced_rand(0..3)+176;
	
	drop(globals);
	let position = unit.position();
	orig(unit.0);
	if let Some(mesk) = mesk {
		let unit_mineral = bw::create_unit(position.y as u32, 11,
										    position.x as u32, mineral_id as u32) as *mut bw::Unit;					
		if unit_mineral != null_mut(){
			bw::finish_unit_pre(unit_mineral);
			bw::finish_unit(unit_mineral);
			(&mut (*unit_mineral).unit_specific2[..]).write_u16::<LE>(unit_id.mineral_cost() as u16).unwrap();		
			let mut globals = Globals::get("kill_unit_hook2");
			globals.ngs.set_meskalloid_sprite((*unit_mineral).sprite);
			drop(globals);
			bw::update_mineral_amount_animation(unit_mineral);
			let game = Game::get();
			let mut x_tile = ((*unit_mineral).position.x/32) as u16;
			x_tile = x_tile.saturating_sub(1);
			let y_tile = ((*unit_mineral).position.y/32) as u16;
			let map_width = (*game.0).map_width_tiles;
			for i in 0..=1 {
				let offset = x_tile+i+(map_width * y_tile);
				*(*bw::tile_flags).offset(offset as isize) |= 0x08000000;//building blocking
				*(*bw::tile_flags).offset(offset as isize) |= 0x00800000;//unbuildable
			}
			
			
			
			//position, not tile. function will divide it by 32
		}
	}
		
}
pub unsafe fn is_resource_container_for_blocking(
	unit: *mut bw::Unit,
	orig: &dyn Fn(*mut bw::Unit)->u32,)->u32 {
		match (*unit).unit_id {
			176..=178=>{
				let mut globals = Globals::get("is_resource_container_for_blocking");
				if globals.ngs.is_meskalloid((*unit).sprite)!=0{
					return 0;
				}
			},
			_=>{},
		}
		
		let result = orig(unit);
		result
	}
unsafe fn konvilisk_condition(
	request_id: u16,
	unit: Unit,
	game: Game) -> bool {
		if request_id == 217 {//konvilisk 
			let map_width = (*game.0).map_width_tiles;
			let tile = *(*bw::tile_flags).offset(
				(unit.position().x as u16 / 32) as isize +
					(map_width * (unit.position().y as u16 / 32)) as isize,
			);
			if tile & 0x0001_0000 == 0 {
				return false;
			}
		}
		return true;
	}
pub unsafe fn ai_spending_req_trainunit_hook(
	player: u32,
	unit: *mut bw::Unit,
	no_retry: u32,
	orig: &dyn Fn(u32,*mut bw::Unit,u32)->u32,)->u32{	
		let unit = match Unit::from_ptr(unit) {
			Some(s) => s,
			None => return orig(player,unit,no_retry),
		};
		let ai = ai::PlayerAi::get(player as u8);
		let game = Game::get();
		let search = aiscript_unit_search(game);
		let req: bw::AiSpendingRequest;
		match ai.first_request() {
			Some(s) => {
				req = s as bw::AiSpendingRequest;
			},
			None => {
				return 0;
			},
		}
		/*
		let target_u_id = match req.id 
		{
			128 => unit::id::ZERGLING,
			129=> unit::id::HYDRALISK,
			_=>{return orig(player,unit.0,no_retry);},
		};*/
		let config = config::config();
		
		/*let mut target_u_id = UnitId(228);
		for i in config.morph_train_ai.equivalent_list {
			if i.listing[1].0==req.id {
				target_u_id = i.listing[0].0;
			}
		}
		if target_u_id == unit::NONE {
			return orig(player,unit.0,no_retry);
		}
		*/
		
		let target_u_id = match config.morph_train_ai.equivalent_list.clone().iter_mut().find(|x| x.listing[1].0==req.id){
			Some(s)=>s.listing[0],
			None=>return orig(player,unit.0,no_retry),
		};
	
		let area = bw::location(63);
		let rect = bw::Rect {
			left: area.area.left as i16,
			top: area.area.top as i16,
			right: area.area.right as i16,
			bottom: area.area.bottom as i16,
		};
		let parent = search
			.search_iter(&rect)
			.find(|u| u.player() as u32 == player && u.id()==target_u_id
					&& u.order()!=order::id::DIE && (*u.0).sprite!=null_mut() &&
					u.order().0!=158 &&//new
					konvilisk_condition(req.id, *u, game) &&
					(*u.0).target==null_mut() && (*u.0).previous_attacker==null_mut()
					&& (*u.0).ai!=null_mut())
			.filter(|u| has_guard_ai(*u) ||
						good_military_ai(*u));
		
		if let Some(parent)=parent {	
			
			let mut good=false;
			if req.id==129 {
				let den = search
						.search_iter(&rect)
						.find(|u| u.player() as u32 == player && u.id()==UnitId(153));
				if den.is_some(){
					good=true;
				}
			}/*
			if(bw::check_unit_dat_requirements(player, req.id as u32,parent.0) == 0){
				//bw_print!("Vorv/hydra failure: unit dat not met");
			}*/
			if good || bw::check_unit_dat_requirements(player, req.id as u32,parent.0) == 1 {
				//
				if bw::Ai_DoesHaveResourcesForUnit(player,req.id as u32)==0 {
					ai.pop_request();
					//bw_print!("Vorv/hydra failure: no res");
					return 0;
				}
				//
				if ai.is_at_limit(UnitId(req.id),game)
				{
					ai.pop_request();
					//bw_print!("Vorv/hydra failure: unit at limit");
					return 0;
				}
				if parent==unit {		
					if bw::AiTrainUnit(req.id as u32,req.ty as u32,req.val,parent.0)==0 {
						//bw_print!("Vorv/hydra failure: can't train (shouldn't appear at all)");
						return 0;
					}
				}
				else 
				{
					 //req.id
					  if bw::prepare_build_unit(req.id as u32,parent.0)==0{
						//bw_print!("Vorv/hydra failure: failed to prepare build unit");
						return 0;
					  }
					 bw::remove_unit_ai(parent.0,0);
					 if req.ty == 2 {
                        assert_eq!(req.ty, 2);
						let ai = req.val as *mut bw::GuardAi;
						if ai == null_mut() {
							return 0;
						}
						let par = parent.0;
						if par == null_mut() {
							return 0;
						}
                        (*ai).home = (*ai).other_home;
                        (*par).ai = ai as *mut _;
                        (*ai).parent = par;
					 }
					 else {
						assert!(req.ty==1);
						let val = req.val as *mut bw::AiRegion;
						if val == null_mut() {
							return 0;
						}
						let par = parent.0;
						if par == null_mut() {
							return 0;
						}
						bw::add_military_ai(true, val, par);
					 }				
							 
				}
				ai.pop_request();
			/*	if req.id==128 {
					bw_print!("Morph to vorvaling!");
				}	*/
				parent.issue_order_ground(order::id::UNIT_MORPH, parent.position());
				return 1;
			
			
			}
			else {
			
				let error = *bw::last_error;
				//bw_print!("Vorv/hydra failure: error code {}",error);
				match error {
					0x2|0x7|0x8|0xd|0x15|0x17=>{
						ai.pop_request();
					}
					_=>{},
				}			
				return 0;
			}
		}
		else{
			ai.pop_request();
			return 0;
			/*
			//bw_print!("Vorv/basilisk failure: no unit found {} {}, source {}",req.id,req.ty,target_u_id.0);
			let rect2 = bw::Rect {
				left: 5 as i16,
				top: 5 as i16,
				right: 2000 as i16,
				bottom: 2000 as i16,
			};
			let mut units = search
				.search_iter(&rect2)
				.collect::<Vec<_>>();
			for un in &mut units {
				if un.id() == unit::id::ZERGLING {
					//bw_print!("Zergling detected");
					if un.order()==order::id::DIE {
						//bw_print!("Unit is dying!");
					}
					if un.order()==order::id::DIE {
						//bw_print!("Player mismatch: {} {}",player,un.player());
					}
					if (*un.0).sprite==null_mut() {
						//bw_print!("Unit's sprite is gone!");
					}
					if (*un.0).target!=null_mut() {
						//bw_print!("Unit is targeting something");
					}
					if (*un.0).previous_attacker!=null_mut() {
						//bw_print!("Unit has previous attacker value");
					}
					if (*un.0).ai==null_mut() {
						//bw_print!("Unit ai is gone!");
					}
					if let Some(uai) = un.military_ai() {
						let state = (*(*uai).region).state;
						if !(state==1 || state==4 || state==5) {
							//bw_print!("Unit is military ai, but state is {}",state);
						}
					}
				}
				
			}
			if units.len()==0 {
				//bw_print!("No units of required id: {}",target_u_id.0);
			}*/
			
		}
//		return 0;
//		orig(player,unit.0,no_retry)
	}
/*
pub unsafe fn does_spread_creep_hook(
	completed: bool,
	unit_id: u16,
	orig: &dyn Fn(bool,u16)->u32,
	)->u32{	
		
		
		match UnitId(unit_id) {
			unit::id::HATCHERY|unit::id::CREEP_COLONY|UnitId(180)=>{return completed as u32;},
			unit::id::SUNKEN_COLONY|unit::id::SPORE_COLONY|unit::id::LAIR|unit::id::HIVE=>{return 1},
			_=>{return 0},
		}
		
		/*
		let result = orig(completed,unit_id);
		result*/
	}


pub unsafe fn creep_spread_hook(
	x: u32,
	y: u32,
	completed: bool,
	tile_area: *mut bw::Rect32,
	unit_id: u32,
	orig: &dyn Fn(u32,u32,bool,*mut bw::Rect32,u32)->u32,
	)->u32{

		let result = orig(x,y,completed,tile_area,unit_id);
		
		result
	}

*/
pub unsafe fn buildtown_hook(
	unit: *mut bw::Unit,
	town: *mut bw::AiTown,
	orig: &dyn Fn(*mut bw::Unit, *mut bw::AiTown),){
		let unit = match Unit::from_ptr(unit) {
			Some(s) => s,
			None => return orig(unit,town),
		};
		//notice for the future: it has hook in teippi
		let mut town_hall = false;
		let mut hall = 0;
		if unit.id().0 == 131 || unit.id().0==200 {
			town_hall=true;
			hall = unit.id().0;
			(*unit.0).unit_id = 106;
		}
		orig(unit.0,town);
		if town_hall {
			(*unit.0).unit_id = hall;
		}
	}


pub unsafe fn supply_requests(
	town: *mut bw::AiTown,
	orig: &dyn Fn(*mut bw::AiTown),){
		orig(town);
	}
	
pub unsafe fn add_spending_request_hook(
    priority: u32,
    c: *mut libc::c_void,
    unit_id: u16,
    ai_type: u32,
    player: u8,
    orig: &dyn Fn(u32, *mut libc::c_void, u16, u32, u8),
) {
    let mut globals = Globals::get("add_spending_request");
    let unit_id = UnitId(unit_id);
    for maxbuild in &mut globals.build_max.buildmax {
        if let Some(town) = maxbuild.town {
            if maxbuild.unit_id == unit_id {
                let units_in_town = ai::count_town_units(town, unit_id, false);
                if units_in_town >= maxbuild.quantity as u32 {				
                    return;
                }
            }
        }
    }
    drop(globals);
    orig(priority, c, unit_id.0, ai_type, player);
}

pub unsafe fn ai_attack_focus_hook(
    unit: *mut bw::Unit,
    func_param: u32,
    orig: &dyn Fn(*mut bw::Unit, u32) -> u32,
) -> u32 {
    if (*unit).player < 8 {
        let globals = Globals::get("ai focus unit hook");
        let ai_mode = globals.ai_mode[(*unit).player as usize];
        if !ai_mode.focus_disabled_units {
            return 0;
        }
    }
    orig(unit, func_param)
}


pub unsafe fn unit_name_hook(unit_id: u32, orig: &dyn Fn(u32) -> *const u8) -> *const u8 {
    let unit = bw::client_selection[0];
    let unit = match Unit::from_ptr(unit) {
        Some(s) => s,
        None => return orig(unit_id),
    };
	
    let mut globals = Globals::get("unit name hook");
	let config = config::config();
	if config.overlord {
		match unit_id {
			176..=178=>{
				if globals.ngs.is_meskalloid((*unit.0).sprite)!=0{
					return bw::get_stat_txt_str_func(1079);
				}
				if globals.ngs.mineral_opcode((*unit.0).sprite)>0{
					return bw::get_stat_txt_str_func(1078+globals.ngs.mineral_opcode((*unit.0).sprite));
				}
			},
			_=>(),
		}
	}
    let name_match = globals
        .renamed_units
        .states
        .iter()
        .find(|x| {
            unit_id == x.unit_id.0 as u32 &&
                x.players.matches(unit.player()) &&
                (unit_in_area(unit, x.area))
        })
        .map(|x| &x.name);
	
	/*
	let config=config::config();
	if config.high_yield {
		if unit_id>=176 && unit_id<=178 {
			let current_player = *bw::active_player_id;
			let unit = bw::player_selections[current_player as usize][0]; 
			let unit = match Unit::from_ptr(unit) {
				Some(s) => s,
				None => {
					return orig(unit_id);
				},
			};
			let mut c_str = CString::new("High<32>Yield<32>Mineral<32>Field").unwrap();
			if globals.ngs.is_high_yield(unit)!=0{
				return c_str.as_ptr() as *const u8;
			}
		}	
	}*/

    if let Some(name_match) = name_match {
        name_match.as_ptr() as *const u8
    } else {
        orig(unit_id)
    }
}

pub unsafe extern fn unit_name(script: *mut bw::AiScript) {
    enum NameStatus {
        Enable,
        Disable,
    }
    let game = Game::get();
    let mut read = ScriptData::new(script);
    let players = read.read_player_match(game);
    let unit = read.read_u16();
    let mut src = read.read_position();
    let radius = read.read_u16();
    src.extend_area(radius as i16);
    let replacement_string = read.read_string();
    let flag = read.read_u8();
    if feature_disabled("unit_name") {
        return;
    }
    let flag = match flag {
        0 => NameStatus::Enable,
        1 => NameStatus::Disable,
        x => {
            bw_print!("Unsupported flag modifier in unit_name: {:x}", x);
            return;
        }
    };
    if bw::is_scr() {
        bw_print!("unit_name is not supported in SCR");
        return;
    }
    let mut globals = Globals::get("ais unit_name");

    let rename_status = RenameStatus {
        area: src.area,
        unit_id: UnitId(unit),
        name: CString::new(replacement_string).unwrap(),
        players,
    };
    match flag {
        NameStatus::Enable => globals.renamed_units.try_add(rename_status),
        NameStatus::Disable => globals.renamed_units.try_remove(&rename_status),
    }
}
pub unsafe extern fn wait_build(script: *mut bw::AiScript) {
    let old_pos = (*script).pos - 1;
						   
    let mut read = ScriptData::new(script);
    let amount = read.read_u8();
    let mut unit_id = UnitId(read.read_u16());
    let town = Town::from_ptr((*script).town);
    let globals = Globals::get("ais wait_build");
    unit_id = globals.unit_replace.replace_check(unit_id);
	
    if let Some(town) = town {
        let units_in_town = ai::count_town_units(town, unit_id, true);
		//bw_print!("unit {:?} waitbuild comparison: town - {}, script - {}",unit_id,units_in_town,amount);
        if units_in_town < amount as u32 {
            (*script).pos = old_pos;
            (*script).wait = 30;
        }
    }
}



pub unsafe extern fn wait_buildstart(script: *mut bw::AiScript) {
    let old_pos = (*script).pos - 1;
						   
    let mut read = ScriptData::new(script);
    let amount = read.read_u8();
    let mut unit_id = UnitId(read.read_u16());
    let town = Town::from_ptr((*script).town);
    let globals = Globals::get("ais wait_buildstart");
    unit_id = globals.unit_replace.replace_check(unit_id);
    if let Some(town) = town {
        let units_in_town = ai::count_town_units(town, unit_id, false);
		//bw_print!("unit {:?} waitbuildstart comparison: town - {}, script - {}",unit_id,units_in_town,amount);
	
        if units_in_town < amount as u32 {
            (*script).pos = old_pos;
            (*script).wait = 30;
        }
    }
}

pub unsafe extern fn deaths(script: *mut bw::AiScript) {
    let old_pos = (*script).pos - 1;
    let game = Game::get();
    // deaths(player, modifier, amount, unit, dest)
    let mut read = ScriptData::new(script);
    let players = read.read_player_match(game);
    let modifier = read.read_modifier();
    let amount = read.read_u32();
    let mut units = read.read_unit_match();
    let dest = read.read_jump_pos();
    if feature_disabled("deaths") {
        return;
    }

    let mut globals = Globals::get("ais deaths");
    match modifier.ty {
        ModifierType::Read(read) => {
            let sum = units
                .iter_flatten_groups()
                .map(|unit_id| {
                    players
                        .players()
                        .map(|player| {
                            (*Game::deaths())
                                .get_mut(unit_id.0 as usize)
                                .and_then(|x| x.get_mut(player as usize))
                                .cloned()
                                .unwrap_or(0)
                        })
                        .sum::<u32>()
                })
                .sum::<u32>();
            read.compare_and_act(sum, amount, script, dest, old_pos);
        }
        ModifierType::Write(write) => {
            for unit_id in units.iter_flatten_groups() {
                for player in players.players() {
                    let deaths = (*Game::deaths())
                        .get_mut(unit_id.0 as usize)
                        .and_then(|x| x.get_mut(player as usize));
                    if let Some(deaths) = deaths {
                        *deaths = write.apply(*deaths, amount, &mut globals.rng);
                    }
                }
            }
        }
    }
}

pub unsafe extern fn wait_rand(script: *mut bw::AiScript) {
    let mut read = ScriptData::new(script);
    let mut r1 = read.read_u32();
    let mut r2 = read.read_u32();
    let mut globals = Globals::get("ais wait_rand");
    if r1 > r2 {
        mem::swap(&mut r1, &mut r2);
    }
    (*script).wait = globals.rng.synced_rand(r1..r2 + 1);
}

pub unsafe extern fn kills_command(script: *mut bw::AiScript) {
    let old_pos = (*script).pos - 1;
    let game = Game::get();
    // kills(player1, player2, modifier, amount, unit, dest)
    let mut read = ScriptData::new(script);
    let player1 = read.read_player_match(game);
    let player2 = read.read_player_match(game);
    let modifier = read.read_modifier();
    let amount = read.read_u32();
    let mut units = read.read_unit_match();
    let dest = read.read_jump_pos();
    if feature_disabled("kills_command") {
        return;
    }
    let mut globals = Globals::get("ais kills");

    match modifier.ty {
        ModifierType::Read(read) => {
            let sum = units
                .iter_flatten_groups()
                .map(|unit_id| {
                    player1
                        .players()
                        .map(|p1| {
                            player2
                                .players()
                                .map(|p2| globals.kills_table.count_kills(p1, p2, unit_id.0))
                                .sum::<u32>()
                        })
                        .sum::<u32>()
                })
                .sum::<u32>();
            read.compare_and_act(sum, amount, script, dest, old_pos);
        }
        ModifierType::Write(write) => {
            for unit_id in units.iter_flatten_groups() {
                for p1 in player1.players() {
                    for p2 in player2.players() {
                        let kpos = globals::KCPos::new(p1, p2, unit_id.0);
                        match write {
                            WriteModifier::Set => globals.kills_table.try_set(kpos, amount),
                            WriteModifier::Add => globals.kills_table.try_add(kpos, amount),
                            WriteModifier::Subtract => globals.kills_table.try_sub(kpos, amount),
                            WriteModifier::Randomize => {
                                if amount != 0 {
                                    let random = globals.rng.synced_rand(0..amount);
                                    globals.kills_table.try_set(kpos, random);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

pub unsafe fn increment_deaths(
    target: *mut bw::Unit,
    attacker_p_id: u8,
    orig: &dyn Fn(*mut bw::Unit, u8),
) {
    let unit_id = (*target).unit_id;
    let amount = 1;
    let player = (*target).player;
    {
        let mut globals = Globals::get("increment deaths hook");
        let kpos = globals::KCPos::new(attacker_p_id, player, unit_id);
        globals.kills_table.try_add(kpos, amount);
		let config = config::config();
		let game = Game::get();
		if config.overlord && game.completed_count(attacker_p_id,UnitId(174))>0 && UnitId((*target).unit_id).is_building()
						   && (*target).flags & 0x1 !=0 && attacker_p_id!=(*target).player {//is_completed
			//scrap depots
			//bw_print!("fill scrap depots");
			globals.ngs.fill_scrap_depots(attacker_p_id);
		}
    }
    orig(target, attacker_p_id);
}

pub unsafe extern fn print_command(script: *mut bw::AiScript) {
    let mut read = ScriptData::new(script);
    let msg = read.read_string();
    // The null isn't included in returned slice, but it still is always there for ptr.	
    samase::print_text(msg.as_ptr());
}


pub unsafe extern fn local_jump(script: *mut bw::AiScript) {
    // local_jump(townId, operator, quantity, unitId, block)
    let old_pos = (*script).pos - 1;
    let mut read = ScriptData::new(script);
    let town_id = read.read_u8();
    let modifier = read.read_modifier();
    let amount = read.read_u32();
    let unit_id = read.read_unit_match();
    let dest = read.read_jump_pos();    
    if feature_disabled("local_jump") {
        return;
    }
    
    let read = match modifier.ty {
        ModifierType::Read(r) => r,
        ModifierType::Write(w) => {
            bw_print!("Used writing modifier {:?} in local_jump", w);
            return;
        }
    };
    
	let mut globals = Globals::get("local_jump hook");
	let mut total_count = 0;
	let town = town_from_id(script, &mut globals, town_id);
	if let Some(town) = town {
		let mut workers = (*town.0).workers;    
		while workers != null_mut() {          
			let current = (*workers).parent;
			if let Some(current) = Unit::from_ptr(current) {
				if unit_id.matches(&current) {
					total_count+=1;
				}
			}
			workers = (*workers).next;
		}
		let mut buildings = (*town.0).buildings;    
		while buildings != null_mut() {          
			let current = (*buildings).parent;
			if let Some(current) = Unit::from_ptr(current) {
				if unit_id.matches(&current) {
					total_count+=1;
				}  
			}
			buildings = (*buildings).next;
		}
		read.compare_and_act(total_count, amount, script, dest, old_pos);
	}
}

pub unsafe extern fn resarea_jump(script: *mut bw::AiScript) {
    // resarea_jump(townId, operator, resarea, resourceType, quantity, block)
	#[derive(Copy, Clone)]
    enum Resource {
        Ore,
        Gas,
    }
	let old_pos = (*script).pos - 1;
    let mut read = ScriptData::new(script);
    let town_id = read.read_u8();
    let modifier = read.read_modifier();
	let script_resarea = read.read_u8();
	let res = read.read_u8();
	let amount = read.read_u32();
    let dest = read.read_jump_pos();
    if feature_disabled("resarea_jump") {
        return;
    }
	let mut globals = Globals::get("resarea_jump hook");
	let town = town_from_id(script, &mut globals, town_id);
	let resarea = &bw::resource_areas;
	if let Some(town) = town {
		let resarea_id = (*town.0).resource_area;
		//bw_print!("Script town pointer: {:p}, current script town: {:p}",town.0,(*script).town);
		//bw_print!("Resareas: script {}, town resarea {}, town {}",script_resarea,resarea_id,town_id);
		if /*town_id != 255 &&*/ script_resarea != 255 && script_resarea != resarea_id {
			//bw_print!("Bad resarea match");
			return;
		}
		let resources_to_check: &[_] = match res {
			// Matching trigger conditions
			0 => &[Resource::Ore],
			1 => &[Resource::Gas],
			2 => &[Resource::Ore, Resource::Gas],
			x => {
				bw_print!("Unsupported resource type in resarea_jump: {:x}", x);
				return;
			}
		};
		match modifier.ty {
			ModifierType::Read(read) => {
				let jump = resources_to_check.iter().any(|&res| {
                        let resvalue = match res {
                            Resource::Ore => resarea.areas[resarea_id as usize].total_mineral_count,
                            Resource::Gas => resarea.areas[resarea_id as usize].total_gas_count,
                        };
						//bw_print!("Compare: resarea {}, value in resarea {}, value in script {}",resarea_id,resvalue,amount);
						//bw_print!("Read resvalue: {}",resvalue);
                        read.compare(resvalue, amount)
                });
				let read_req = read.action.get_read_req();
				if jump == read_req {
					read.action.do_action(script, dest, old_pos);
				}	
			}
			ModifierType::Write(w) => {
				bw_print!("Used writing modifier {:?} in resarea_jump", w);
				return;
			}
		}
	}
	else {
		//bw_print!("resarea_jump: town is not detected");
	}
}

pub unsafe extern fn aise_debug(script: *mut bw::AiScript) {
    let mut read = ScriptData::new(script);
    let msg = read.read_string();
	debug!("{}", String::from_utf8_lossy(msg));
}
pub unsafe extern fn ping(script: *mut bw::AiScript) {
    let mut read = ScriptData::new(script);
    let x = read.read_u16();
    let y = read.read_u16();
    let color = read.read_u8();
    if bw::is_scr() {
        bw_print!("ping is not supported in SCR");
        return;
    }
    bw::ping_minimap(x as u32, y as u32, color);
}

pub unsafe extern fn player_jump(script: *mut bw::AiScript) {
    let mut read = ScriptData::new(script);
    let player = read.read_string();
    let dest = read.read_jump_pos();
    if feature_disabled("player_jump") {
        return;
    }
    if bw::is_scr() {
        bw_print!("player_jump is not supported in SCR");
        return;
    }
    if *bw::is_multiplayer != 0 {
        // Not doing this since it'd desync
        return;
    }
    let player_name = {
        let len = bw::player_name
            .iter()
            .position(|&x| x == 0)
            .unwrap_or_else(|| bw::player_name.len());
        &bw::player_name[..len]
    };
    if player_name.eq_ignore_ascii_case(&player) {
        (*script).pos = dest;
    }
}

pub unsafe extern fn upgrade_jump(script: *mut bw::AiScript) {
    let old_pos = (*script).pos - 1;
    let game = Game::get();
    let mut read = ScriptData::new(script);
    let players = read.read_player_match(game);
    let modifier = read.read_modifier();
    let upgrade = UpgradeId(read.read_u16());
    let level = read.read_u8();
    let dest = read.read_jump_pos();
    if feature_disabled("upgrade_jump") {
        return;
    }
	let mut globals = Globals::get("ais upgrade_jump");
	let config = config::config();
	//hydra/guild wars -> ext upgrades added 
	//debug!("Read u_j");
    match modifier.ty {
        ModifierType::Read(r) => {
            let read_req = r.action.get_read_req();
            let jump = players.players().any(|player| {
                //let up_lev = game.upgrade_level(player, upgrade);
				
				//debug!("UDEBUG - readujump");
				let up_lev = match upgrade.0 {
					0..=45=>{
						game.upgrade_level(player,upgrade)
					},
					_=>{
						if config.extended_upgrades {
							globals.upgrades.get_upgrade_level(upgrade.0 as u32,(*script).player)
						}
						else {
							game.upgrade_level(player,upgrade)
						}
					},
				};
                r.compare(u32::from(up_lev), u32::from(level))
            });

            if jump == read_req {
                r.action.do_action(script, dest, old_pos);
            }
        }
        ModifierType::Write(w) => {
            for player in players.players() {
				let old = get_new_upgrade_level(game,&mut globals,upgrade.0,player);
                let new = w.apply(u32::from(old), u32::from(level), &mut globals.rng);
				set_new_upgrade_level_current(game,&mut globals,upgrade.0,player,new as u8);
            }
        }
    };
	//debug!("done");
}

pub unsafe extern fn load_bunkers(script: *mut bw::AiScript) {
    // load_bunkers(area, load_unit, quantity, bunker_unit, bunker_quantity, priority)
    let mut globals = Globals::get("ais load_bunkers");
    let mut read = ScriptData::new(script);
    let player = (*script).player;
    let mut src = read.read_position();
    let radius = read.read_u16();
    src.extend_area(radius as i16);
    let mut unit_id = read.read_unit_match();
    let quantity = read.read_u8();
    let mut bunker_id = read.read_unit_match();
    let bunker_quantity = read.read_u8();
    let priority = read.read_u8();
    unit_id.replace_ids(&mut globals.unit_replace);
    bunker_id.replace_ids(&mut globals.unit_replace);


    if feature_disabled("load_bunkers") {
        return;
    }
    let decl = BunkerDecl {
        pos: src.area,
        unit_id: unit_id,
        bunker_id: bunker_id,
        quantity: quantity,
        player: player as u8,
        priority: priority,
        bunker_quantity: bunker_quantity,
    };

    globals.bunker_states.add(decl);
}

pub unsafe extern fn unit_avail(script: *mut bw::AiScript) {
    let old_pos = (*script).pos - 1;
    let game = Game::get();
    let mut globals = Globals::get("ais unit_avail");
    let mut read = ScriptData::new(script);
    let players = read.read_player_match(game);
    let modifier = read.read_modifier();
    let avail_modifier = read.read_u8();
    let unit = UnitId(read.read_u16());
    let dest = read.read_jump_pos();
    if feature_disabled("unit_avail") {
        return;
    }
    if avail_modifier >= 2 {
        bw_print!("Invalid modifier in unit_avail");
        return;
    }
    match modifier.ty {
        ModifierType::Read(r) => {
            let read_req = r.action.get_read_req();
            let jump = players.players().any(|player| {
                let avail = game.unit_available(player, unit);
                r.compare(avail as u32, u32::from(avail_modifier))
            });
            if jump == read_req {
                r.action.do_action(script, dest, old_pos);
            }
        }
        ModifierType::Write(w) => {
            for player in players.players() {
                let new_value = match w {
                    WriteModifier::Add | WriteModifier::Subtract => {
                        bw_print!("Add/subtract modifier is not supported in unit_avail");
                        return;
                    }
                    WriteModifier::Set => avail_modifier,
                    WriteModifier::Randomize => globals.rng.synced_rand(0..2) as u8,
                };
                game.set_unit_availability(player, unit, new_value != 0);
            }
        }
    };
}

pub unsafe extern fn tech_jump(script: *mut bw::AiScript) {
    let old_pos = (*script).pos - 1;
    let game = Game::get();
    let mut read = ScriptData::new(script);
    let players = read.read_player_match(game);
    let modifier = read.read_modifier();
    let tech = TechId(read.read_u16());
    let level = read.read_u8();
    let dest = read.read_jump_pos();
    if feature_disabled("tech_jump") {
        return;
    }
    match modifier.ty {
        ModifierType::Read(r) => {
            let read_req = r.action.get_read_req();
            let jump = players.players().any(|player| {
                let up_lev = game.tech_researched(player, tech);
                r.compare(u32::from(up_lev), u32::from(level))
            });
            if jump == read_req {
                r.action.do_action(script, dest, old_pos);
            }
        }
        ModifierType::Write(w) => {
            let mut globals = Globals::get("ais tech_jump");
            for player in players.players() {
                let old = game.tech_researched(player, tech);
                let new = w.apply(u32::from(old), u32::from(level), &mut globals.rng);
                game.set_tech_level(player, tech, new as u8);
            }
        }
    };
}

pub unsafe extern fn tech_avail(script: *mut bw::AiScript) {
    let old_pos = (*script).pos - 1;
    let game = Game::get();
    let mut read = ScriptData::new(script);
    let players = read.read_player_match(game);
    let modifier = read.read_modifier();
    let tech = TechId(read.read_u16());
    let level = read.read_u8();
    let dest = read.read_jump_pos();
    if feature_disabled("tech_avail") {
        return;
    }
    match modifier.ty {
        ModifierType::Read(r) => {
            let read_req = r.action.get_read_req();
            let jump = players.players().any(|player| {
                let up_lev = game.tech_available(player, tech);
                r.compare(u32::from(up_lev), u32::from(level))
            });
            if jump == read_req {
                r.action.do_action(script, dest, old_pos);
            }
        }
        ModifierType::Write(w) => {
            let mut globals = Globals::get("ais tech_avail");
            for player in players.players() {
                let old = game.tech_available(player, tech);
                let new = w.apply(u32::from(old), u32::from(level), &mut globals.rng);
                game.set_tech_availability(player, tech, new as u8);
            }
        }
    };
}

pub unsafe extern fn random_call(script: *mut bw::AiScript) {
    let mut read = ScriptData::new(script);
    let chance = read.read_u8();
    let dest = read.read_jump_pos();
    if feature_disabled("random_call") {
        return;
    }

    let mut globals = Globals::get("ais random_call");
    let random = globals.rng.synced_rand(0..256);
    if u32::from(chance) > random {
        let ret = (*script).pos;
        (*script).pos = dest;
        (*Script::ptr_from_bw(script)).call_stack.push(ret);
    }
}
pub unsafe extern fn attack_rand(script: *mut bw::AiScript) {
    let mut read = ScriptData::new(script);
    let mut r1 = read.read_u8() as u32;
    let mut r2 = read.read_u8() as u32;
    let unit = read.read_u16();
    if feature_disabled("attack_rand") {
        return;
    }
    if r1 > r2 {
        mem::swap(&mut r1, &mut r2);
    }
    let mut globals = Globals::get("ais attack_rand");
    let random = globals.rng.synced_rand(r1..r2 + 1);
    add_to_attack_force(&mut globals, (*script).player as u8, UnitId(unit), random);
}

#[allow(dead_code)]
pub unsafe extern fn send_suicide(script: *mut bw::AiScript){
	let mut read = ScriptData::new(script);
	let suicide_type = read.read_u8();
	let player = (*script).player;
	let ai = bw::player_ai(player);
	let mut globals = Globals::get("send_suicide");
	let config = config::config();
	if config.extended_attacks {
		//globals.attack_extender[(*script).player as usize].unit_ids.clear();
		let len = bw::gptp_get_attack_length();
		for i in 0..len {
			bw::gptp_set_attack_unit(0,i,player);
		}
	/*
		for i in globals.attack_extender.unit_ids.iter_mut(){
			*i=0;
		}		*/	
	}
	else {
		for i in (*ai).attack_force.iter_mut(){
			*i=0;
		}	
	}
	drop(globals);
	(*ai).last_attack_second = 0;
	bw::ai_attack_clear(player);
	bw::ai_send_suicide(suicide_type, player);
	(*ai).last_attack_second = bw::elapsed_seconds().wrapping_sub(175);
}

#[allow(dead_code)]
pub unsafe extern fn attack_clear(script: *mut bw::AiScript){
	let player = (*script).player;
	let ai = bw::player_ai(player);
	let mut globals = Globals::get("attack_clear");
	let config = config::config();
	if config.extended_attacks {
		let len = bw::gptp_get_attack_length();
		for i in 0..len {
			bw::gptp_set_attack_unit(0,i,player);
		}
		
		/*for i in globals.attack_extender[player as usize].unit_ids.iter_mut(){
			*i=0;
		}		*/
	}
	else {
		for i in (*ai).attack_force.iter_mut(){
			*i=0;
		}	
	}
	drop(globals);
	(*ai).attack_grouping_region = 0;
	(*ai).last_attack_second = 0;
	let _ = bw::ai_attack_clear(player);
}

pub unsafe extern fn attack_add(script: *mut bw::AiScript) {
    let mut read = ScriptData::new(script);
    let amount = read.read_u8() as u32;
    let unit = read.read_u16();
    let mut globals = Globals::get("ais attack_rand");
    add_to_attack_force(&mut globals, (*script).player as u8, UnitId(unit), amount);
}
pub unsafe fn add_to_attack_force(globals: &mut Globals, player: u8, unit: UnitId, amount: u32) {						   
    let unit = globals.unit_replace.replace_check(unit);
    let ai = ai::PlayerAi::get(player);
    // Reuse slots that may have been deleted during the attack
	let config = config::config();
	if config.extended_attacks {
	
/*		for _ in 0..amount {
			globals.attack_extender[player as usize].unit_ids.push(unit.0+1);
		}*/
		let len = bw::gptp_get_attack_length();
		let mut count = amount;
		for i in 0..len {
			if count == 0 {
				break;
			}
			let id = bw::gptp_get_attack_unit(player as u32, i);
			if id == 0 || id == 0xe4 + 1 {
				bw::gptp_set_attack_unit(unit.0 as u32,i,player as u32);
				count -= 1;
			}
		}	
	}
	else {
		let attack_force = &mut (*ai.0).attack_force[..];
		let free_slots = attack_force
			.iter_mut()
			.filter(|&&mut x| x == 0 || x == unit::id::NONE.0 + 1)
			.take(amount as usize);
		for out in free_slots {
			*out = unit.0 + 1;
		}
		
	}

}

pub unsafe extern fn bring_jump(script: *mut bw::AiScript) {
    let old_pos = (*script).pos - 1;
    let game = Game::get();
    let mut read = ScriptData::new(script);
    let players = read.read_player_match(game);
    let modifier = read.read_modifier();
    let amount = read.read_u32();
    let unit_id = read.read_unit_match();
    let mut src = read.read_position();
    let radius = read.read_u16();
    src.extend_area(radius as i16);
    let dest = read.read_jump_pos();
    if feature_disabled("bring_jump") {
        return;
    }
    let read = match modifier.ty {
        ModifierType::Read(r) => r,
        ModifierType::Write(w) => {
            bw_print!("Used writing modifier {:?} in bring_jump", w);
            return;
        }
    };
    let search = aiscript_unit_search(game);
    let count = search
        .search_iter(&src.area)
        .filter(|u| players.matches(u.player()) && unit_id.matches(u))
        .count() as u32;
		
	let mut globals = Globals::get("bring_jump");
	read.compare_and_act_global(count,amount,script, dest, old_pos,&mut globals);
}

pub unsafe fn ai_region(player: u32, region: u16) -> *mut bw::AiRegion {
    bw::ai_regions(player).offset(region as isize)
}

//#[derive(Clone, Eq, PartialEq)]
#[derive(Debug, Serialize, Deserialize, Clone, Eq, PartialEq)]
pub struct Position {
    pub center: bw::Point,
    pub area: bw::Rect,
}

#[repr(C)]
#[derive(Copy, Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct Rect32 {
    pub left: i32,
    pub top: i32,
    pub right: i32,
    pub bottom: i32,
}


impl Position {
    pub fn from_point(x: i16, y: i16) -> Position {
        Position {
            center: bw::Point {
                x,
                y,
            },
            area: bw::Rect {
                left: x,
                right: x.saturating_add(1),
                top: y,
                bottom: y.saturating_add(1),
            },
        }
    }
	
    pub fn from_rect32(rect: &bw::Rect32) -> Position {
        Position {
            center: bw::Point {
                x: (rect.left + (rect.right - rect.left) / 2) as i16,
                y: (rect.top + (rect.bottom - rect.top) / 2) as i16,
            },
            area: bw::Rect {
                left: rect.left as i16,
                right: rect.right as i16,
                top: rect.top as i16,
                bottom: rect.bottom as i16,
            },
        }
    }
    pub fn extend_area(&mut self, amt: i16) {
        self.area.left = self.area.left.saturating_sub(amt);
        self.area.right = self.area.right.saturating_add(amt);
        self.area.top = self.area.top.saturating_sub(amt);
        self.area.bottom = self.area.bottom.saturating_add(amt);
    }
}

impl fmt::Display for Position {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let bw::Rect {
            left,
            right,
            top,
            bottom,
        } = self.area;

        if left == right - 1 && top == bottom - 1 {
            write!(f, "{}, {}", left, right)
        } else {
            write!(f, "{}, {}, {}, {}", left, top, right, bottom)
        }
    }
}

#[derive(Copy, Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub enum ReadModifierType {
    AtLeast,
    AtMost,
    Exactly,
	Greater,
	Less,
}

impl ReadModifierType {
    pub fn compare(&self, value: u32, constant: u32) -> bool {
        match self {
            ReadModifierType::AtLeast => value >= constant,
            ReadModifierType::AtMost => value <= constant,
            ReadModifierType::Exactly => value == constant,
			ReadModifierType::Greater=> value > constant,
			ReadModifierType::Less=> value < constant,
        }
    }
	pub fn compare_ext(&self, value: i32, constant: i32) -> bool {
	match self {
            ReadModifierType::AtLeast => value >= constant,
            ReadModifierType::AtMost => value <= constant,
            ReadModifierType::Exactly => value == constant,
			ReadModifierType::Greater=> value > constant,
			ReadModifierType::Less=> value < constant,			
			
        }
    }
}

impl ReadModifier {
    pub fn compare(&self, value: u32, constant: u32) -> bool {
        self.ty.compare(value, constant)
    }
	/*
    pub fn compare_ext(&self, value: i32, constant: i32) -> bool {
        self.ty.compare_ext(value, constant)
    }*/
    pub unsafe fn compare_and_act(
        &self,
        value: u32,
        constant: u32,
        script: *mut bw::AiScript,
        dest: u32,
        old_pos: u32,
    ) {
        let read_req = self.action.get_read_req();
        if self.ty.compare(value, constant) == read_req {
            self.action.do_action(script, dest, old_pos);
        }
    }
    pub unsafe fn compare_and_act_global(
        &self,
        value: u32,
        constant: u32,
        script: *mut bw::AiScript,
        dest: u32,
        old_pos: u32,
		globals: &mut Globals
    ) {	
        let read_req = self.action.get_read_req();
        if self.ty.compare(value, constant) == read_req {
            self.action.do_action_global(script, dest, old_pos, globals);
        }		
	}
	
	
	pub unsafe fn compare_and_act_ext(
	&self,
	value: i32,
	constant: i32,
	script: *mut bw::AiScript,
	dest: u32,
	old_pos: u32,
	globals: &mut Globals,
	new_action: ModifierAction,
    ) {
        //let read_req = self.action.get_read_req();
		let read_req = match new_action {
            ModifierAction::Jump | ModifierAction::Call | ModifierAction::Ignore | ModifierAction::Multirun => true,
            ModifierAction::Wait => false,		
		};
        if self.ty.compare_ext(value, constant) == read_req {
            self.action.do_action_ext(script, dest, old_pos, globals, new_action);
        }
    }
}

//#[derive(Copy, Clone, Debug, PartialEq)]
#[derive(Debug, Serialize, Deserialize, Copy, Clone, Eq, PartialEq)]//buildq
pub enum WriteModifier {
    Set,
    Add,
    Subtract,
    Randomize,
}

impl WriteModifier {
    pub fn apply(self, old: u32, operand: u32, rng: &mut Rng) -> u32 {
        match self {
            WriteModifier::Add => old.saturating_add(operand),
            WriteModifier::Subtract => old.saturating_sub(operand),
            WriteModifier::Set => operand,
            WriteModifier::Randomize => {
                if operand != 0 {
                    rng.synced_rand(0..operand)
                } else {
                    bw_print!("Cannot randomize with 0 cases");
                    !0
                }
            }
        }
    }
}
#[derive(Copy, Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
pub struct ReadModifier {
    pub ty: ReadModifierType,
    pub action: ModifierAction,
}

enum ModifierType {
    Read(ReadModifier),
    Write(WriteModifier),
}

struct TriggerModifier {
    ty: ModifierType,
}

//#[derive(Copy, Clone, Debug, PartialEq)]
#[derive(Debug, Serialize, Deserialize, Copy, Clone, Eq, PartialEq)]//buildq
pub enum ModifierAction {
    Jump,
    Call,
    Wait,
	Multirun,
	
	Ignore,
}

impl ModifierAction {
    /// Whether the action should be done on comparision being false or true.
    pub fn get_read_req(&self) -> bool {
        match self {
            ModifierAction::Jump | ModifierAction::Call | ModifierAction::Ignore | ModifierAction::Multirun => true,
            ModifierAction::Wait => false,
        }
    }
    pub unsafe fn do_action(&self, script: *mut bw::AiScript, dest: u32, old_pos: u32) {
        match self {
            ModifierAction::Jump => {
                (*script).pos = dest;
            }
            ModifierAction::Call => {
                let ret = (*script).pos;
                (*script).pos = dest;
                (*Script::ptr_from_bw(script)).call_stack.push(ret);
            }
            ModifierAction::Wait => {
                (*script).pos = old_pos;
                (*script).wait = 30;
            }
			_=>{},
        }
    }
    pub unsafe fn do_action_global(&self, script: *mut bw::AiScript, dest: u32, old_pos: u32,globals: &mut Globals) {
        match self {
           ModifierAction::Jump => {
                (*script).pos = dest;
            }
            ModifierAction::Call => {
                let ret = (*script).pos;
                (*script).pos = dest;
                (*Script::ptr_from_bw(script)).call_stack.push(ret);
            }
            ModifierAction::Wait => {
                (*script).pos = old_pos;
                (*script).wait = 30;
            }
			ModifierAction::Multirun => {
				let first_ai_script = bw::first_ai_script();
				let script = globals.ai_scripts.alloc(Script {
					bw: bw::AiScript {
						next: first_ai_script,
						prev: null_mut(),
						pos: dest,
						wait: 0,
						player: (*script).player,
						area: (*script).area,
						center: (*script).center,
						town: (*script).town,
						flags: (*script).flags,
					},
					delete_mark: false,
					call_stack: Vec::new(),
					wait_for: 0,
					waitfor_sequence: 0,
				});
				(*first_ai_script).prev = &mut (*script).bw;
				bw::set_first_ai_script(&mut (*script).bw);			
			}
			_=>{},
        }
    }
	pub unsafe fn do_action_ext(&self, script: *mut bw::AiScript, dest: u32, old_pos: u32, globals: &mut Globals, new_action: ModifierAction){
        match new_action {
            ModifierAction::Jump => {
                (*script).pos = dest;
            }
            ModifierAction::Call => {
                let ret = (*script).pos;
                (*script).pos = dest;
                (*Script::ptr_from_bw(script)).call_stack.push(ret);
            }
            ModifierAction::Wait => {
                (*script).pos = old_pos;
                (*script).wait = 30;
            }
			ModifierAction::Multirun => {
//				let flags = ((*script).flags & 1) | (((*script).resarea as u32) << 3);
				debug!("multirun");
				let first_ai_script = bw::first_ai_script();
				let script = globals.ai_scripts.alloc(Script {
					bw: bw::AiScript {
						next: first_ai_script,
						prev: null_mut(),
						pos: dest,
						wait: 0,
						player: (*script).player,
						area: (*script).area,
						center: (*script).center,
						town: (*script).town,
						flags: (*script).flags,
					},
					delete_mark: false,
					call_stack: Vec::new(),
					wait_for: 0,
					waitfor_sequence: 0,
				});
				(*first_ai_script).prev = &mut (*script).bw;
				bw::set_first_ai_script(&mut (*script).bw);			
			}
			_=>{},
        }	
	}
}


#[derive(Debug, Serialize, Deserialize, Copy, Clone, Eq, PartialEq)]//buildq
pub struct BoolModifier {
    pub value: bool,
    pub action: ModifierAction,
}

// For reading aiscript.bin or bwscript.bin (or some other?) bytes
pub struct ScriptData {
    start: *const u8,
    pos: *const u8,
    script: *mut bw::AiScript,
}

impl ScriptData {
    pub unsafe fn new(script: *mut bw::AiScript) -> ScriptData {
        let script_bytes = match (*script).flags & 0x1 != 0 {
            false => bw::aiscript_bin(),
            true => bw::bwscript_bin(),
        };
        ScriptData {
            start: script_bytes,
            pos: script_bytes.offset((*script).pos as isize) as *const u8,
            script,
        }
    }

    pub unsafe fn read_player_match(&mut self, game: Game) -> PlayerMatch {
        let mut cont = true;
        let mut result = PlayerMatch {
            players: [false; 12],
        };
        let current_player = (*self.script).player as u8;
        while cont {
            let byte = self.read_u8();
            cont = byte & 0x80 != 0;
            let player = byte & 0x7f;
            match player {
                x @ 0..=11 => result.players[x as usize] = true,
                13 => result.players[current_player as usize] = true,
                // Foes, allies
                14 | 15 => {
                    let allies = player == 15;
                    let players = (0..12)
                        .filter(|&x| x != current_player)
                        .filter(|&x| game.allied(current_player, x) == allies);
                    for player in players {
                        result.players[player as usize] = true;
                    }
                }
                // All players
                17 => result.players = [true; 12],
                // Forces
                18 | 19 | 20 | 21 => {
                    // Forces are 1-based
                    let force = 1 + player - 18;
                    for player in (0..8).filter(|&x| (*game.0).player_forces[x as usize] == force) {
                        result.players[player as usize] = true;
                    }
                }
                x => {
                    bw_print!("Unsupported player: {:x}", x);
                }
            };
        }
        result
    }

    fn read_bool_modifier(&mut self) -> BoolModifier {
        let val = self.read_u8();
        let action = match val >> 1 {
            0x20 => ModifierAction::Wait,
            0x40 => ModifierAction::Call,
            0x0 => ModifierAction::Jump,
            _ => {
                bw_print!("Unsupported modifier: {:x}", val);
                ModifierAction::Jump
            }
        };

        BoolModifier {
            action,
            value: val & 1 != 0,
        }
    }

    fn read_modifier(&mut self) -> TriggerModifier {
        let val = self.read_u8();
//        let action = match val & 0xc0 {
		let action = match val & 0xe0 {
            0x40 => ModifierAction::Wait,
            0x80 => ModifierAction::Call,
			0x20 => ModifierAction::Multirun,
            0x0 => ModifierAction::Jump,
            _ => {
                bw_print!("Unsupported modifier: {:x}", val);
                ModifierAction::Jump
            }
        };
        let read_modifier = |ty, action| ReadModifier {
            ty,
            action,
        };
        TriggerModifier {
            ty: match val & 0x1f {
                // Matching triggers in chk
                0 => ModifierType::Read(read_modifier(ReadModifierType::AtLeast, action)),
                1 => ModifierType::Read(read_modifier(ReadModifierType::AtMost, action)),
                7 => ModifierType::Write(WriteModifier::Set),
                8 => ModifierType::Write(WriteModifier::Add),
                9 => ModifierType::Write(WriteModifier::Subtract),
                10 => ModifierType::Read(read_modifier(ReadModifierType::Exactly, action)),
                11 => ModifierType::Write(WriteModifier::Randomize),
                x => {
                    bw_print!("Unsupported modifier: {:x}", x);
                    ModifierType::Read(read_modifier(ReadModifierType::AtLeast, action))
                }
            },
        }
    }

    pub fn read_unit_match(&mut self) -> UnitMatch {
        let val = self.read_u16();
        if val > 0xff00 {
            let repeat = val & 0xff;
            let units = (0..repeat).map(|_| UnitId(self.read_u16())).collect();
            UnitMatch {
                units,
            }
        } else if val < 0x1000 {
            UnitMatch {
                units: vec![UnitId(val)],
            }
        } else {
            bw_print!("Invalid script encoding: unit match {:x}", val);
            UnitMatch {
                units: vec![],
            }
        }
    }

    fn read_position(&mut self) -> Position {
        let x = self.read_u16();
        let y = self.read_u16();
        if x == !0 {
            assert!(y < 255);
            let location = if y >= 255 {
                bw_print!("Invalid location id 0x{:x} used", y);
                bw::location(63)
            } else {
                bw::location(y as u8)
            };
            Position::from_rect32(&location.area)
        } else {
            // !1, !1 is used for inherit position in create_script
            Position::from_point(x as i16, y as i16)
        }
    }

    pub fn read_jump_pos(&mut self) -> u32 {
        let long_jumps = unsafe { (self.start as *const u32).read_unaligned() >= 0x10000 };
        if long_jumps {
            self.read_u32()
        } else {
            self.read_u16().into()
        }
    }

    pub fn read_u8(&mut self) -> u8 {
        self.read()
    }

    pub fn read_u16(&mut self) -> u16 {
        self.read()
    }

    pub fn read_u32(&mut self) -> u32 {
        self.read()
    }

    // Maybe not a good idea to inline(never) but saves 1k in binary size
    #[inline(never)]
    pub fn read<T: Copy>(&mut self) -> T {
        unsafe {
            let size = mem::size_of::<T>();
            let val = (self.pos as *const T).read_unaligned();
            self.pos = self.pos.add(size);
            (*self.script).pos = (*self.script).pos.wrapping_add(size as u32);
            val
        }
    }

    pub unsafe fn read_string(&mut self) -> &'static [u8] {
        let length = (0usize..).position(|x| *self.pos.add(x) == 0).unwrap_or(0);
        let val = slice::from_raw_parts(self.pos, length);
        self.pos = self.pos.add(length + 1);
        (*self.script).pos += length as u32 + 1;
        val
    }
}

#[derive(Serialize, Deserialize)]
#[repr(C)]
pub struct Script {
    #[serde(serialize_with = "serialize_bw_script")]
    #[serde(deserialize_with = "deserialize_bw_script")]
    bw: bw::AiScript,
    delete_mark: bool,
    call_stack: Vec<u32>,
	pub wait_for: u32,
	pub waitfor_sequence: u32,
}

#[derive(Serialize, Deserialize)]
struct SerializeBwScript {
    pos: u32,
    wait: u32,
    player: u32,
    area: bw::Rect32,
    center: bw::Point32,
    town: Option<Town>,
    flags: u32,
}

fn serialize_bw_script<S: Serializer>(script: &bw::AiScript, s: S) -> Result<S::Ok, S::Error> {
    let bw::AiScript {
        next: _,
        prev: _,
        pos,
        wait,
        player,
        area,
        center,
        town,
        flags,
    } = *script;
    SerializeBwScript {
        pos,
        wait,
        player,
        area,
        center,
        town: Town::from_ptr(town),
        flags,
    }
    .serialize(s)
}

// The deserialization of next/prev is handled in the container deserialization.
fn deserialize_bw_script<'de, D: Deserializer<'de>>(d: D) -> Result<bw::AiScript, D::Error> {
    let result: SerializeBwScript = SerializeBwScript::deserialize(d)?;
    Ok(bw::AiScript {
        next: null_mut(),
        prev: null_mut(),
        pos: result.pos,
        wait: result.wait,
        player: result.player,
        area: result.area,
        center: result.center,
        town: result.town.map(|x| x.0).unwrap_or_else(null_mut),
        flags: result.flags,
    })
}

pub fn serialize_scripts<S: Serializer>(
    scripts: &BlockAllocSet<Script>,
    s: S,
) -> Result<S::Ok, S::Error> {
    use serde::ser::SerializeSeq;

    let mut state = globals::save_state("serialize_scripts");
    let state = state
        .as_mut()
        .expect("Serializing AI scripts without state init");
    debug!("Serializing {} scripts", scripts.len());
    let mut s = s.serialize_seq(Some(scripts.len()))?;
    let mut script = state.first_ai_script.0;
    while !script.is_null() {
        unsafe {
            s.serialize_element(&*Script::ptr_from_bw(script))?;
            script = (*script).next;
        }
    }
    s.end()
}

pub fn deserialize_scripts<'de, D: Deserializer<'de>>(
    d: D,
) -> Result<BlockAllocSet<Script>, D::Error> {
    struct Visitor;
    impl<'de> serde::de::Visitor<'de> for Visitor {
        type Value = (*mut bw::AiScript, BlockAllocSet<Script>);
        fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
            write!(formatter, "a sequence of AI scripts")
        }

        fn visit_seq<A>(self, mut seq: A) -> Result<Self::Value, A::Error>
        where
            A: serde::de::SeqAccess<'de>,
        {
            let mut first_script: *mut bw::AiScript = null_mut();
            let mut latest = null_mut();
            let mut container = BlockAllocSet::new();
            while let Some(script) = seq.next_element::<Script>()? {
                let ptr = container.alloc(script);
                unsafe {
                    if first_script.is_null() {
                        first_script = &mut (*ptr).bw;
                    }
                    (*ptr).bw.prev = latest;
                    if !latest.is_null() {
                        (*latest).next = &mut (*ptr).bw;
                    }
                    latest = &mut (*ptr).bw;
                }
            }
            Ok((first_script, container))
        }
    }
    let (first_script, container) = d.deserialize_seq(Visitor)?;
    debug!(
        "Deserialized {} scripts, first {:?}",
        container.len(),
        first_script
    );
    bw::set_first_ai_script(first_script);
    Ok(container)
}

impl Script {
    pub fn ptr_from_bw(bw: *mut bw::AiScript) -> *mut Script {
        bw as *mut Script
    }
	
    fn debug_string(&self) -> String {
        unsafe {
            format!(
                "Player {}, pos {}, {}, script position {}",
                self.bw.player, self.bw.center.x, self.bw.center.y,self.bw.pos
            )
        }
    }
	pub fn waitfor_action(&mut self, wait: u32){
		unsafe {
		if self.wait_for == 0 || wait!=self.waitfor_sequence {
			self.wait_for = *bw::elapsed_frames+wait;
			self.waitfor_sequence = wait;
		}
		}
	}
	pub fn waitfor_check(&mut self)->bool{	
		unsafe {
		if self.wait_for != 0 {
			//bw_print!("E: to {} {}",*bw::elapsed_frames,self.wait_for);
			if *bw::elapsed_frames >= self.wait_for {
				self.wait_for = 0;
				//bw_print!("ret true");
				return true;
			}
		}
		}
		false
		
	}
}

const AISCRIPT_LIMIT: usize = 8192;

pub fn claim_bw_allocated_scripts(globals: &mut Globals) {
    unsafe {
        let first = bw::first_ai_script();
        let first_free = bw::first_free_ai_script();

        if globals.ai_scripts.len() >= AISCRIPT_LIMIT {
            let (_, new_first_free) = clean_free_scripts(&globals.ai_scripts, first_free, !0);
            if let Some(x) = new_first_free {
                bw::set_first_free_ai_script(x);
            }
            return;
        }

        if first_free.is_null() {
            bw_print!(
                "Warning: ran out of AI scripts for the frame, some of the scripts \
                 may not have started.",
            );
        }
        let (new_first, mut new_first_free) =
            take_bw_allocated_scripts(&mut globals.ai_scripts, first, first_free);
        if new_first != first {
            bw::set_first_ai_script(new_first);
        }
        let (deleted, even_newer_first_free) =
            clean_free_scripts(&globals.ai_scripts, new_first_free, !0);
        if deleted != 0 {
            if let Some(x) = even_newer_first_free {
                new_first_free = x;
            }
			let delete_count = clear_deleted_scripts(&mut globals.ai_scripts, new_first);
            assert_eq!(delete_count, deleted);
        }
        if new_first_free != first_free {
            bw::set_first_free_ai_script(new_first_free);
        }
    }
}

// Remove aise objects from bw's free list
unsafe fn clean_free_scripts(
    scripts: &BlockAllocSet<Script>,
    free_list_pos: *mut bw::AiScript,
    delete_count: usize,
) -> (usize, Option<*mut bw::AiScript>) {
    let mut script = free_list_pos;
    let mut new_first_free = None;
    let mut deleted = 0;
    while !script.is_null() && delete_count != deleted {
        if scripts.contains(Script::ptr_from_bw(script)) {
            if !(*script).prev.is_null() {
                (*(*script).prev).next = (*script).next;
            } else {
                new_first_free = Some((*script).next);
            }
            if !(*script).next.is_null() {
                (*(*script).next).prev = (*script).prev;
            }
            deleted += 1;
        }
        script = (*script).next;
    }
    (deleted, new_first_free)
}

// return delete count
unsafe fn clear_deleted_scripts(
    scripts: &mut BlockAllocSet<Script>,
    bw_list: *mut bw::AiScript,
) -> usize {
    for script in scripts.iter() {
        (*script).delete_mark = true;
    }
    let mut script = bw_list;
    let mut count = 0;
    while script != null_mut() {
        (*Script::ptr_from_bw(script)).delete_mark = false;
        script = (*script).next;
        count += 1;
    }
    let delete_count = scripts.len() - count;
    if delete_count != 0 {
        scripts.retain(|x| !(*x).delete_mark);
    }
    delete_count
}

unsafe fn take_bw_allocated_scripts(
    scripts: &mut BlockAllocSet<Script>,
    first: *mut bw::AiScript,
    first_free: *mut bw::AiScript,
) -> (*mut bw::AiScript, *mut bw::AiScript) {
    let mut script = first;
    let mut last_new_free: Option<*mut bw::AiScript> = None;
    let mut first_new_free = None;
    let mut first_new: Option<*mut bw::AiScript> = None;
    let mut prev: Option<*mut bw::AiScript> = None;
    while !script.is_null() {
        if scripts.len() == AISCRIPT_LIMIT {
            bw_print!("AI script limit reached.");
            break;
        }
        if !scripts.contains(Script::ptr_from_bw(script)) {
            let taken = scripts.alloc(Script {
                bw: *script,
                delete_mark: false,
                call_stack: Vec::new(),
				wait_for: 0,
				waitfor_sequence: 0,
            });
            if let Some(prev) = prev {
                (*prev).next = &mut (*taken).bw;
            }
            (*taken).bw.prev = prev.unwrap_or_else(null_mut);
            if first_new.is_none() {
                first_new = Some(&mut (*taken).bw);
            }
            if first_new_free.is_none() {
                first_new_free = Some(script);
            }
            prev = Some(&mut (*taken).bw);
            if let Some(last_new_free) = last_new_free {
                (*last_new_free).next = script;
                (*script).prev = last_new_free;
            } else {
                (*script).prev = null_mut();
            }
            last_new_free = Some(script);
        } else {
            if first_new.is_none() {
                first_new = Some(script);
            }
            if let Some(prev) = prev {
                (*prev).next = script;
            }
            (*script).prev = prev.unwrap_or_else(null_mut);
            prev = Some(script);
        }
        script = (*script).next;
    }
    if let Some(prev) = prev {
        (*prev).next = script;
        if !script.is_null() {
            (*script).prev = prev;
        }
    }
    if let Some(new_free) = last_new_free {
        (*new_free).next = first_free;
        if !first_free.is_null() {
            (*first_free).prev = new_free;
        }
    }
    (
        first_new.unwrap_or(first),
        first_new_free.unwrap_or(first_free),
    )
}


/*
pub unsafe fn clear_tiles_hook(
	x_tile: u32,
	y_tile: u32,
	w_tile: u32,
	h_tile: u32,
	flags: u32,
	orig: &dyn Fn(u32,u32,u32,u32,u32),){
		
		let mut max_x = x_tile+w_tile;
		let mut max_y = y_tile+h_tile;
		let game = Game::get();
		let map_width = (*game.0).map_width_tiles as u32;
		let map_height = (*game.0).map_height_tiles as u32;
		if max_x>map_width {
			max_x = map_width;
		}
		if max_y>map_height {
			max_y = map_height;
		}
		for i in x_tile..max_x {
			for j in y_tile..max_y {
				let mut tile = *(*bw::tile_flags).offset(
					(i + (map_width * j)) as isize,
				);
//				tile &= !flags;
				*(*bw::tile_flags).offset(
					(i + (map_width * j)) as isize,
				) &= !flags;
			}
		}
		
		//orig(x_tile,y_tile,w_tile,h_tile,flags);
	}
*/


/*
pub unsafe fn find_blocking_resource(
	x_tile: u32,
	y_tile: u32,
	radius: u32,
	orig: &dyn Fn(u32,u32,u32)->*mut bw::Sprite)->*mut bw::Sprite {
		bw_print!("{} {} {} {:p}",x_tile,y_tile,radius,*bw::first_active_fow_sprite);
	
		return null_mut();
		let x_tile = x_tile as i16;
		let y_tile = y_tile as i16;
		let radius = radius as i16;
		let mut sprite = *bw::first_active_fow_sprite as *mut bw::Sprite;
		
		let mut globals = Globals::get("find_blocking_resource");
		bw_print!("N: {:p}",sprite);
		while sprite != null_mut(){
			let unit_id = (*sprite).index;
			let is_resource = match unit_id {
				176|177|178=>{
					let config = config::config();				
					bw_print!("N: {}",globals.ngs.is_meskalloid(sprite)!=0);
					if config.overlord && globals.ngs.is_meskalloid(sprite)!=0 {
						false
					}
					else {
						true
					}
				},
				188=>true,
				_=>false,
			};
			
			if is_resource {
				let id = UnitId(unit_id);
				let w = (id.placement().width/32) as i16;
				let h = (id.placement().height/32) as i16;
				let x = (*sprite).position.x/32-(w/2);
				let y = (*sprite).position.y/32-(h/2);
				if x-radius <= x_tile && x+w+radius > x_tile {
					if y-radius <= y_tile && y+h+radius > y_tile {
						return sprite;
					}
				}
			}
			sprite = (*sprite).next;
		}	
		return null_mut();
		
		//drop(globals);
		//return orig(x_tile,y_tile,radius);
	}
	*/
	

pub unsafe fn irradiate_hook(
	attacker: *mut bw::Unit,
	attacking_player: u32,
	target: *mut bw::Unit,
	orig: &dyn Fn(*mut bw::Unit,u32,*mut bw::Unit),){
		
		let config = config::config();
		if !config.creeper_link {
			orig(attacker,attacking_player,target); 
			return; 		
		}
		
		
		let attacker = match Unit::from_ptr(attacker) {
			Some(s) => s,
			None => {
				orig(attacker,attacking_player,target); 
				return; 
			},
		};	
		let target = match Unit::from_ptr(target) {
			Some(s) => s,
			None => {
				orig(attacker.0,attacking_player,target); 
				return; 
			},
		};			
		
		match attacking_player {
			50000 => {
				//bw_print!("Run aise creeper code!");
				let mut globals = Globals::get("fake irradiate read hook");
				globals.ngs.add_creeper_link(attacker,target);
				return;
			},
			_=>(),
		}
		orig(attacker.0,attacking_player,target.0);
	}
	
	
//y = max/current upgrade read
pub unsafe fn get_psi_statement_hook(
	x: u32,
	y: u32,
	unit_id: u32,
	player: u32,
	orig: &dyn Fn(u32,u32,u32,u32)->u32,)->u32{
		if x==50000 {
			let mut globals = Globals::get("fake global read/psi hook");
			let game = Game::get();
			let result: u8;		
		//	debug!("Read ext data: {} upgr id{} player{}",y,unit_id,player);
			
			result = match y {
				0=>{
					if globals.upgrades.upgrade_level.len()==0 {
						if unit_id < 46 {
							game.upgrade_level(player as u8,UpgradeId(unit_id as u16))
						}
						else {
							0
						}
					}
					else {
						globals.upgrades.get_upgrade_level(unit_id,player)
					}					
				},
				1=>{
					if globals.upgrades.upgrade_level.len()==0 || unit_id<46 {
						if unit_id < 46 {
							game.upgrade_max_level(player as u8,UpgradeId(unit_id as u16))
						}
						else {
							0
						}
					}
					else {
						globals.upgrades.get_max_upgrade_level(unit_id,player)
					}
				},				
				_=>0,
			};
			//debug!("Finish reading ext data");
			return result as u32;
		}
		else {
			orig(x,y,unit_id,player)
		}
	}

pub unsafe fn ai_spellcast_hook(
    revenge: bool,
    unit: *mut bw::Unit,
    orig: &dyn Fn(bool, *mut bw::Unit) -> u32,
) -> u32 {
    let globals = Globals::get("ai spellcast hook");
//    let ai_mode = &globals.ai_mode[(*unit).player as usize];
	if (*unit).player >= 8 {
		return orig(revenge,unit);
	}
	let retaliation = globals.ai_mode[(*unit).player as usize].retaliation;
	drop(globals);
    if !retaliation && revenge {
        0
    } else {
        orig(revenge, unit)
    }
}



pub unsafe fn check_upgrade_req_hook(
	unit: *mut bw::Unit,
	upgrade_id: u16,
	player: u8,
	orig: &dyn Fn(*mut bw::Unit,u16,u8)->u32,)->u32{
		//return 1;
		//return 0xffffffff;		
//		let config = config::config();
//		debug!("Check Upgrade Req: {} {}",upgrade_id,player);
		//if !config.debug_upgrade_req {
//		return orig(unit,upgrade_id,player);
		//}
		if upgrade_id >= 62 {			
			let unit = match Unit::from_ptr(unit) {
				Some(s) => s,
				None => {return orig(unit,upgrade_id,player);},
			};
			let game = Game::get();
			let mut globals = Globals::get("check_upgrade_req_hook");
			
			if player != unit.player() {
				*bw::last_error = 1;
				
				return 0;
			}
			if !unit.is_completed(){
				*bw::last_error = 0x14;
				
				return 0;
			}
			if unit.is_disabled(){
				*bw::last_error = 0xa;
				/*if unit.id().0==131 {
					bw_print!("bad evaluate: disabled");
				}*/
				return 0;
			}
			
			if get_new_upgrade_level(game,&mut globals,upgrade_id,player)>=
				get_new_max_upgrade_level(game,&mut globals,upgrade_id,player){
				*bw::last_error = 0xe;
				return 0;
			}
			if bw::upgrade_in_progress(upgrade_id as u32,player as u32)!=0{
				*bw::last_error = 0xc;
				
				return 0;
			}
			if globals.upgrades.req_flags(upgrade_id as u32) == 0 {
				*bw::last_error = 0x17;
				
				return 0;
			}
			if globals.upgrades.req_flags(upgrade_id as u32) == 0xffff {
				let graft = config::grafting_data();
				
				let result = graft.evaluate_unit(unit,unit.player(),upgrade_id,game,&mut globals);	
						/*	
				if upgrade_id == 93 {
					bw_print!("evaluate: {}, result = {}, error = 0x{:x}",upgrade_id,result,*bw::last_error);
				}*/
				if !result {
					if *bw::unk_unused == 1 && *bw::last_error == 0x8 {
						return 0xffffffff;//-1
					}
					return 0;
				}				
			}
			//check dat requirements(upgradeId, player, 0x005145C0, req offset (req flags of unit id i.e. ffff),unit)  
			//0x005145C0 (Upgrade Reqs data) - probably is replaced by firegraft array
			return 1;
			//replace later by check, this is only placeholder
		}
		else {
			return orig(unit,upgrade_id,player);
		}
		
	}

pub unsafe fn zergbuildself_hook(
	unit: *mut bw::Unit,
	orig: &dyn Fn(*mut bw::Unit),){
		
		orig(unit);
		let unit = match Unit::from_ptr(unit) {
			Some(s) => s,
			None => {return;},
		};
		if unit.id().0==134{//nydus canal
			if unit.has_nydus_exit() && unit.is_completed(){
//				bw_print!("Exit built!");
				let mut globals = Globals::get("zergbuildself");	
				let nydus_target = unit.nydus_exit().unwrap();
				globals.waygating.add_nydus(unit);
				globals.waygating.add_nydus(nydus_target);

				if unit.player() < 8 {
					bw::move_military_to_defend(bw::get_ai_region(unit.0));
				}
			}		
		}
	}
	/*
pub unsafe fn order_ai_pickup(
	unit: *mut bw::Unit,
	orig: &dyn Fn(*mut bw::Unit),){
		let unit = match Unit::from_ptr(unit) {
			Some(s) => s,
			None => { 
				orig(unit); 
				return; 
			},
		};	
		let mut globals = Globals::get("order_ai_pickup"); 
		let result = globals.ngs.is_nydus_pickup(unit);
		//drop(globals);
		
		if result {
			bw_print!("nydus pickup");
			if !unit.in_transport() && bw::ai_update_attack_target(0,0,1,unit.0)==0{
				let mut jump_to_E7C2B = false;
				let mut jump_to_E7C37 = false;
				let mut jump_to_E7C44 = false;
				let destination = globals.ngs.get_nydus_area(unit);
				let target = globals.waygating.find_good_nydus(unit,destination.x as u16,destination.y as u16);
				if let Some(target) = target {
					//unit.issue_order_unit(OrderId(0x2f),target);
					bw::prepend_order_targeting_unit(0x2f,unit.0,target.0);
					//bw::do_next_queued_order(unit);
//	0x00438E70 => are_on_connected_regions(u32,u32,@eax *mut Unit)->u32;
					
				}
			}
		
		}
		else {
			orig(unit);
		//}
	}*/
pub unsafe fn return_resources_hook(
	unit: *mut bw::Unit,
	orig: &dyn Fn(*mut bw::Unit),){
		let config=config::config();
		if config.cullen_resource_micro {
			let unit = match Unit::from_ptr(unit) {
				Some(s) => s,
				None => {return orig(unit);},
			};		
			let game = Game::get();
			for p in 0..8 {
				if (game.unit_count(p,UnitId(20))>0 ||
					game.unit_count(p,UnitId(22))>0 ||
					game.unit_count(p,UnitId(23))>0 ||
					game.unit_count(p,UnitId(25))>0 ||
					game.unit_count(p,UnitId(29))>0 ||
					game.unit_count(p,UnitId(98))>0 ) && game.allied(unit.player(),p) {
					//
					//x=10
					//x=10*
					//(10+(10/2))*x=2.5;
					
					
					//
					let mut resource = unit.resources_carried()/4;
					if resource > 2 {
						resource -= 1;
					}
					
					let res_type = unit.powerup_flags();
					if res_type == 2 {//minerals
						(*game.0).minerals[p as usize] += resource as u32;						
					}
					else if res_type == 1 {//gas
						(*game.0).gas[p as usize] += resource as u32;
					}	
					bw::update_ui();
				}
			}
		}
		
	
		orig(unit);
	}
	
	/*
//causes update flickering bug
pub unsafe fn get_portrait_hook(
	unit_id: u32,
	orig: &dyn Fn(u32)->u32)->u32{
	
		let mut result = orig(unit_id);
		let config = config::config();
		if config.full_toilet {
			if UnitId(unit_id as u16).is_building(){
				let player = *bw::active_player_id;
				let unit = bw::player_selections[player as usize][0];
				if unit != null_mut(){
					match (*unit).player {
						3=>{//nerazim
							result = 96;//raszagal
						},
						_=>{},
					}
				}
			}
			
		}
		result
	}
	*/
/*pub unsafe fn has_rally_hook(
	unit: *mut bw::Unit,
	orig: &dyn Fn(*mut bw::Unit)->u32)->u32{
		orig(unit)	
	}*/
	
pub unsafe fn preparemovingto_hook(
	unit: *mut bw::Unit,
	x: u32,
	y: u32,
	orig: &dyn Fn(*mut bw::Unit,u32,u32)->u32)->u32{
	
	
		let unit = match Unit::from_ptr(unit) {
			Some(s) => s,
			None => {return orig(unit,x,y);},
		};		
		if unit.order().0==0x60 {//pickup4
			return 1;
		}
		if unit.is_air(){
			return 0;
		}
		if unit.id().is_subunit(){
			return 0;
		}
		let sprite = unit.sprite().unwrap();
		let position = (*sprite).position;
		let target_position = bw::Point { x: x as i16, y: y as i16};
		if position.x == x as i16 && position.y == y as i16 {
			return 0;
		}
		if !((*unit.0).flags & 0x20000 != 0) {//reaction flag	
			return 0;
		}
		let config = config::config();
		let mut is_terran_worker = false;
		if unit.id().0 == 7 {//scv
			is_terran_worker = true;
		}
		if config.overlord {
			if unit.id().0==77 || unit.id().0==41 || unit.id().0==77 {
				is_terran_worker = true;
			}
		}
		
		if is_terran_worker || unit.id()==unit::id::MEDIC {
			if let Some(target) = unit.target(){
				if target.is_air(){
					return 0;
				}
			}
		}
		
		let target_region = bw::get_region(target_position);
		if target_region.is_none(){
			return orig(unit.0,x,y);
		}
		let target_region = target_region.unwrap();
		let pathing = bw::pathing();
		if (*pathing).regions[target_region as usize].unk == 0x1ffd {//unwalkable
			return 0;
		}
		
		//CUSTOM NYDUS CODE STARTS HERE
		let mut globals = Globals::get("moving_to_nydus_hook1");
		if (*unit.0).order == 0x2f { // already moving to nydus, don't do anything
			//bw_print!("Move unit {} to nydus", unit.id().0);
			return 0; // Needs to be 0 for nyduses to work
		}
		if let Some(nydus) = globals.waygating.find_best_nydus(unit.player(), unit, position, target_position) {
			if (*unit.0).order_queue_begin == null_mut(){
				bw::append_order_simple2((*unit.0).order as u32, (*unit.0).order_target_pos, (*unit.0).target, 0, unit.0);
			}
			else {
				bw::insert_order_before((*unit.0).order_target_pos,(*unit.0).target,0xe4,(*unit.0).order_queue_begin,
											(*unit.0).order as u32,unit.0);		
			}
			//bw_print!("Issue unit {} to nydus", unit.id().0);
			//bw_print!("Issue move {:?} {:?} {:?} -> {:?}", unit, unit.id(), position, target_position);
			//bw::ping_minimap(position.x as u32, position.y as u32, 7);
			//bw::ping_minimap(nydus.position().x as u32, nydus.position().y as u32, 8);

			unit.issue_order_unit(OrderId(0x2f), nydus);//enter nydus canal
			if bw::issubunit((*unit.0).subunit)!=0 {
				bw::issue_order_targ_nothing((*unit.0).subunit,0x17);
			}

			return 1; // Seems to be fine as either 1 or 0, 1 is probably safer, while 0 could result in faster movement (not sure)
		}
		drop(globals);
		//CUSTOM NYDUS CODE ENDS HERE

		if bw::are_in_same_region_group(x, y, unit.0) != 0 {
			match unit.military_ai(){
				Some(m_ai)=>{
					let region = (*m_ai).region; //*region 
					let state = (*region).state;
					if state==0x1 || state==0x2 {
						return 0;
					}
				},
				None=>{},
			}

			if bw::are_on_connected_regions(x, y, unit.0) != 0 {
				// Normal units
				if unit.id() != unit::id::REAVER {
					return 0;
				}
				// Reavers
				if bw::distance(position, target_position) < 30 * 32 {
					return 0;
				}
			}
		}

		let mut out_unit_offset = null_mut();
		let out_unit = &mut out_unit_offset as *mut *mut bw::Unit;
		
		let find_result = bw::ai_findtransport(unit.0,out_unit);
		if find_result==0 {
			return 0;
		}
		if (*unit.0).order_queue_begin == null_mut(){
			bw::append_order_simple2((*unit.0).order as u32, (*unit.0).order_target_pos, (*unit.0).target, 0, unit.0);
		}
		else {
			bw::insert_order_before((*unit.0).order_target_pos,(*unit.0).target,0xe4,(*unit.0).order_queue_begin,
										(*unit.0).order as u32,unit.0);		
		}
		bw::insert_order_targeting_ground_before(x,y,unit.0,0x60,(*unit.0).order_queue_begin);//0x60 - Pickup4 (AI Pickup)
		if bw::issubunit((*unit.0).subunit)!=0 {
			bw::issue_order_targ_nothing((*unit.0).subunit,0x17);
		}
		bw::force_order_done(unit.0);
		bw::set_attackmove_holdpos_flag(unit.0);
		//bw_print!("Issue pickup: {:?} {:?}, targ {:p} order {:x}",unit.id(),unit.position(),(*unit.0).target,(*unit.0).order);
		return 1;
		
		
		//return 0;
		//let result = orig(unit,x,y);
		
		//result
	}
	
	
/*
pub unsafe fn check_dat_req_hook(
	value_id: u32,
	player: u32,
	requirement_table: *mut u16,
	requirement_offset: u32,
	unit: *mut bw::Unit,
	orig: &dyn Fn(u32,u32,*mut u16,u32,*mut bw::Unit)->u32,)->u32 {
	//	bw_print!("Dat req: {} {} {:?} {} {:?}",value_id,player,requirement_table,requirement_offset,unit);
		return orig(value_id,player,requirement_table,requirement_offset,unit);
	}
	*/
		/*
	00473010 = UpdateCreepBuildingPlacementState(), eax player, arg 1 x_tile, arg 2 y_tile, arg 3 size_wh,
    arg 4 placement_state_entry, arg 5 check_vision, arg 6 check_vision

	*/
	
	/*
	00473FB0 = UpdateBuildingPlacementState(), arg 1 Unit *builder, arg 2 player, arg 3 x_tile,
    arg 4 y_tile, arg 5 unit_id, arg 6 placement_state_entry, arg 7 check_vision,

	*/
	
/*00473720 = UpdateBuildingPlacementState_Units(), arg 1 Unit *builder, arg 2 x_tile, arg 3 player, arg 4 unit_id, arg 5 size_wh,
    arg 6 placement_state_entry, arg 7 dont_ignore_reacting, arg 8 also_invisible, arg 9 without_vision, eax y_tile
    arg 7 is 0 for addons
	*/		

//0043F320 = Ai_ReactToHit(), arg 1 bool main_target_reactions, arg 2 bool dont_call_help, ecx Unit *self, edx Unit *attacker
   /* Tbh Ai_UnitWasHit is more accurate name

	//0x0043F320 => ai_reacttohit(u32,u32,@ecx *mut Unit,@edx *mut Unit);
	*/
pub unsafe fn ai_reacttohit(
	main_target: u32,
	dont_call_help: u32,
	unit: *mut bw::Unit,
	attacker: *mut bw::Unit,
	orig: &dyn Fn(u32,u32,*mut bw::Unit,*mut bw::Unit),){
		let unit = match Unit::from_ptr(unit) {
			Some(s) => s,
			None => {
				orig(main_target,dont_call_help,unit,attacker);
				return;
			},
		};
		if let Some(b_ai) = unit.building_ai(){
			if (*b_ai).town == null_mut(){
				return;
				//hard fix
			}
		}
		orig(main_target,dont_call_help,unit.0,attacker);
	}
	
	
	
	

	

	

/*pub unsafe fn creep_maptileflag_placement_hook(
	player: u32,
	x_tile: u32,
	y_tile: u32,
	size_wh: u32,
	placement_state_entry: u32,
	check_vision: u32,//???
	unit_id: u32,
	orig: &dyn Fn(u32,u32,u32,u32,u32,u32,u32)->u32)->u32{
		if x_tile > 255 || y_tile > 255 {
			return orig(player,x_tile,y_tile,size_wh,placement_state_entry,check_vision,unit_id);
		}
		let id = UnitId(unit_id as u16);
		let width = size_wh & 0x0000ffff;
		let height = (size_wh & 0xffff0000)>>16;
		//bw_print!("Creepwh: {:x} {:x} {:x}, placement {}",width,height,size_wh,placement_state_entry);
		if height == 0 {
			return 0;
		}
		let game = Game::get();
		let map_width = (*game.0).map_width_tiles as u32;
		let player_offset = 1 << player;
		let pt = bw::player_list[player as usize].ty;
		let mut result = 0x0;
		if id.require_creep(){
			let require_visibility = !check_vision;
			for i in 0..width {
				for j in 0..height {
					let offset = (x_tile+i+((map_width*(y_tile+j)))) as u32;
					let state_offset = i+((0x30*placement_state_entry)+j*6);
					let tile = *(*bw::tile_flags).offset(offset as isize);
					let explored_flags = (tile & 0xff00)>>8;
					let visible_flags = tile & 0xff;
					bw::building_placement_state[state_offset as usize] = 0x0;
					if explored_flags & player_offset !=0 && pt==2 {
						bw::building_placement_state[state_offset as usize] = 0x10;
						result = 0x10;
					}
					if require_visibility!=0 || visible_flags & player_offset == 0 {
						if !(tile & 0x0040_0000) != 0 {
							bw::building_placement_state[state_offset as usize] = 0x4;
							result = 0x4;
						}
					}
				}
			}
		}	
		result
	}*/

/*	
pub unsafe fn update_placement_hook(
    builder: *mut bw::Unit,
    player: u8,
    x_tile: u32,
    y_tile: u32,
    unit_id: u16,
    placement_state_entry: u8,
    check_vision: u8,
    also_invisible: u8,
    without_vision: u8,
    orig: &dyn Fn(*mut bw::Unit, u8, u32, u32, u16, u8, u8, u8, u8) -> u32,
) -> u32 {
//	bw_print!("p{} c{} a{} w{}",placement_state_entry,check_vision,also_invisible,without_vision);
//	ext tile group happens only with check_vision == 0
	use bw_dat::unit::{SIEGE_TANK_TURRET};
	let mut id = UnitId(unit_id);
	match id {
		unit::id::EXTRACTOR|unit::id::ASSIMILATOR|unit::id::REFINERY=>{
			return orig(builder,player,x_tile,y_tile,unit_id,placement_state_entry,check_vision,also_invisible,without_vision);
		},
		_=>{},
	}
	
	let globals = Globals::get("update placement hook");
	if let Some(builder) = Unit::from_ptr(builder){
		if globals.placement_check_mode[builder.player() as usize]{
			id = SIEGE_TANK_TURRET;
		}
	}			
	//
	//
	/*
	let result = orig(
		builder,
		player,
		x_tile,
		y_tile,
		id.0,
		1,1,0,0);
		*/
	
	
	drop(globals);
    let result = orig(
        builder,
        player,
        x_tile,
        y_tile,
        id.0,
        placement_state_entry,
        check_vision,
        also_invisible,
        without_vision,
    );
    result
}*/

/*



//00473DB0 = GetGasBuildingPlacementState?(), arg 1 x_tile, arg 2 y_tile, arg 3 player, arg 4 check_vision
//132

//00473150 = UpdateNydusPlacementState(), eax player, arg 1 x_tile, arg 2 y_tile, arg 3 size_wh, arg 4 placement_state_entry,
    arg 5 check_vision
//114
	
//00473010 = UpdateCreepBuildingPlacementState(), eax player, arg 1 x_tile, arg 2 y_tile, arg 3 size_wh,
    arg 4 placement_state_entry, arg 5 check_vision, arg 6 check_vision
	//117
	
//00473A10 = UpdateBuildingPlacementState_MapTileFlags(), arg 1 x_tile, arg 2 y_tile, arg 3 player, arg 4 size_wh,
    arg 5 placement_state_entry, arg 6 check_vision, eax unit_id
    If arg 6 true, requires vision, but always requires at least fog
	//145
	
	
//00473720 = UpdateBuildingPlacementState_Units(), arg 1 Unit *builder, arg 2 x_tile, arg 3 player, arg 4 unit_id, arg 5 size_wh,
    arg 6 placement_state_entry, arg 7 dont_ignore_reacting, arg 8 also_invisible, arg 9 without_vision, eax y_tile
    arg 7 is 0 for addons
//158


static int GetBuildingPlacementError(Unit *builder, int player, x32 x_tile, y32 y_tile, UnitType unit_id, bool check_vision)
{
    int width = unit_id.PlacementBox().width / 32;
    int height = unit_id.PlacementBox().height / 32;
    // If units.dat placement box < 32, bw allows to place the unit anywhere
    // However, ai code calls this function with past-limit x/y, so we need to check both
    // x_tile < width && x_tile + w_tiles <= width
    int map_width = *bw::map_width_tiles;
    int map_height = *bw::map_height_tiles;
    if (x_tile < 0 || y_tile < 0 || x_tile >= map_width || y_tile >= map_height ||
            x_tile + width > map_width || y_tile + height > map_height)
    {
        return 0x3;
    }
    if (builder && !(builder->flags & (UnitStatus::Building | UnitStatus::Air)))
    {
        if (!bw::CanWalkHere(builder, x_tile * 32, y_tile * 32))
            return 0x7;
    }
    if (unit_id.IsGasBuilding())
        return bw::GetGasBuildingPlacementState(x_tile, y_tile, player, check_vision);
    if (unit_id.Flags() & UnitFlags::RequiresPsi)
        return bw::GetPsiPlacementState(x_tile, y_tile, unit_id.Raw(), player);
    return 0;
}  

int UpdateBuildingPlacementState(Unit *builder, int player, x32 x_tile, y32 y_tile, UnitType unit_id,
    int placement_state_entry, bool check_vision, bool also_invisible, bool without_vision)
{
    x_tile &= 0xffff;
    y_tile &= 0xffff;
    player &= 0xff;
    int error = GetBuildingPlacementError(builder, player, x_tile, y_tile, unit_id, check_vision);
    if (error || unit_id.IsGasBuilding())
    {
        uint8_t *placement_data = &*bw::building_placement_state + placement_state_entry * 0x30;
        memset(placement_data, error, 0x30);
        return error;
    }
    int width = unit_id.PlacementBox().width / 32;
    int height = unit_id.PlacementBox().height / 32;
    unsigned int size_wh = height << 16 | width;
    int specific_state;
    if (builder && builder->Type() == UnitId::NydusCanal)
    {
        specific_state = bw::UpdateNydusPlacementState(player,
                                                       x_tile,
                                                       y_tile,
                                                       size_wh,
                                                       placement_state_entry,
                                                       check_vision);
    }
    else if (unit_id.Flags() & UnitFlags::RequiresCreep)
    {
        specific_state = bw::UpdateCreepBuildingPlacementState(player,
                                                               x_tile,
                                                               y_tile,
                                                               size_wh,
                                                               placement_state_entry,
                                                               check_vision,
                                                               without_vision);
    }
    else
    {
        specific_state = bw::UpdateBuildingPlacementState_MapTileFlags(x_tile,
                                                                       y_tile,
                                                                       player,
                                                                       size_wh,
                                                                       placement_state_entry,
                                                                       check_vision,
                                                                       unit_id.Raw());
    }

    if (*bw::ai_building_placement_hack)
        builder = *bw::ai_building_placement_hack;
    int generic_state = bw::UpdateBuildingPlacementState_Units(builder,
                                                               x_tile,
                                                               y_tile,
                                                               player,
                                                               unit_id.Raw(),
                                                               size_wh,
                                                               placement_state_entry,
                                                               ~unit_id.Flags() & UnitFlags::Addon,
                                                               also_invisible,
                                                               without_vision);
    return max(generic_state, specific_state);
}*/

/*
pub unsafe fn can_attack_unit(
	check_detection: u32,
	attacker: *mut bw::Unit,
	target: *mut bw::Unit,
	orig: &dyn Fn(u32,*mut bw::Unit,*mut bw::Unit)->u32)->u32{
		let result = orig(check_detection,attacker,target);
		result
	}*/

pub unsafe fn start_building_hook(
	builder: *mut bw::Unit,
	orig: &dyn Fn(*mut bw::Unit)->u32,
) -> u32 {
	let result = orig(builder);
	let builder = match Unit::from_ptr(builder) {
        Some(s) => s,
        None => return result,
    };
	let game = Game::get();
	let globals = Globals::get("start building hook");
	let unit_search = &globals.last_frame_search;
	if builder.player()>=8 {
		return orig(builder.0);
	}/*
	if builder.id().0==0x07 {//scv
		if builder.current_train_id().0==0x6d {//supply depot
			bw_print!("Start supply depot, result of attempt is: {}",result);
		}
	}*/
	//for unburrow
	let area = bw::Rect {
		left: (builder.position().x).saturating_sub(32),
		top: (builder.position().y).saturating_sub(32),
		right: (builder.position().x) + (builder.id().placement().width as i16 / 2).saturating_add(32),
		bottom: (builder.position().y) + (builder.id().placement().height as i16 / 2).saturating_add(32),
	};	
	let units = unit_search
		.search_iter(&area)
		.filter(|u| u.burrowed() && *u!=builder);
	for unit in units {
		bw::unburrow(unit.0);
	}
	//new unit mover, old code disabled
	/*
	let assume_pushing_units = globals.ai_mode[builder.player() as usize].move_from_baselayout;
	if assume_pushing_units {
		let ok = check_placement(game, &unit_search, builder, 
			((*builder.0).order_target_pos.x as i16)/32, 
			((*builder.0).order_target_pos.y as i16)/32, 
			builder.id(),
			assume_pushing_units,
			(*builder.0).order_target_pos);	
		if ok 
		{
			return 1;
		}
	}*/
	result
}


pub unsafe fn start_zerg_building_hook(
	builder: *mut bw::Unit,
	orig: &dyn Fn(*mut bw::Unit)->u32,
) -> u32 {
	let builder = match Unit::from_ptr(builder) {
        Some(s) => s,
        None => return orig(builder),
    };
	let globals = Globals::get("start zerg building hook");
	let unit_search = &globals.last_frame_search;
	let area = bw::Rect {
		left: (builder.position().x).saturating_sub(32),
		top: (builder.position().y).saturating_sub(32),
		right: (builder.position().x) + (builder.id().placement().width as i16 / 2).saturating_add(32),
		bottom: (builder.position().y) + (builder.id().placement().height as i16 / 2).saturating_add(32),
	};	
	let units = unit_search
		.search_iter(&area)
		.filter(|u| u.burrowed() && *u!=builder);
	for unit in units {
		bw::unburrow(unit.0);
	}
	drop(globals);
	let result = orig(builder.0);
	result
}


pub unsafe fn drone_land_hook(
	builder: *mut bw::Unit,
	orig: &dyn Fn(*mut bw::Unit)->u32,
) -> u32 {
/*z
	let mut globals = Globals::get("drone land hook");
    let game = Game::get();
    let unit_search = UnitSearch::from_bw();
    let builder = match Unit::from_ptr(builder) {
        Some(s) => s,
        None => return orig(builder),
    };
    // Replicate order state 1 (Moving to build/check if can build) with aise's build check
    // if the ai has unit pushing active.
    if builder.current_train_id() != bw_dat::unit::EXTRACTOR {
        if (*builder.0).order_state == 1 && builder.position() == (*builder.0).move_target {
            let assume_pushing_units = globals.ai_mode[builder.player() as usize].move_from_baselayout;
            if assume_pushing_units {
                let ok = check_placement(
                    game,
                    &unit_search,
                    builder,
                    (builder.order_target_pos().x as i16) / 32,
                    (builder.order_target_pos().y as i16) / 32,
                    builder.current_train_id(),
                    true,
					builder.order_target_pos()
                );
                if ok {
                    // Probably not even necessary, the unit should already be there?
                   // 004EB9C0(unit.order_target_pos());
                    bw::change_movement_target_movetarget2(builder.0,builder.order_target_pos().x as u32,builder.order_target_pos().y as u32);
					globals.placement_check_mode[builder.player() as usize] = true;
                    if let Some(sprite) = builder.sprite() {
                        let ref mut last_image = *(*sprite).last_image;
                        // Shadow
                        if (*last_image).drawfunc == 0xa {
                            // Freeze y pos?
                            (*last_image).flags |= 0x4;
                        }
                    }
                    // Uninterruptable order
                    (*builder.0).flags |= 0x1000;
                    (*builder.0).order_state = 2;
					drop(globals);
                    return 1;
                }
            }
        }
    }
	drop(globals);
    orig(builder.0)
	
	*/

	let globals = Globals::get("drone land hook");
	let game = Game::get();
	let unit_search = UnitSearch::from_bw();
	let builder = match Unit::from_ptr(builder) {
        Some(s) => s,
        None => return orig(builder),
    };
	let player = builder.player();
	if player > 7 {
		let result = orig(builder.0);
		return result;
	}
	/*
	//new unit mover, old disabled
	let assume_pushing_units = globals.ai_mode[builder.player() as usize].move_from_baselayout;
	if assume_pushing_units {
		let ok = check_placement(game, &unit_search, builder, 
				(builder.order_target_pos().x as i16)/32, 
				(builder.order_target_pos().y as i16)/32, 
				builder.current_train_id(),
				true,
				builder.order_target_pos());
		if ok {
			globals.placement_check_mode[builder.player() as usize]=true;
		}
	}*/
	drop(globals);
	//unburrow all
	
	
	
	let result = orig(builder.0);
	
	let mut globals=Globals::get("drone land hook_2");
	globals.placement_check_mode[builder.player() as usize]=false;
	result
}  


pub unsafe fn choose_building_placement(
    unit_id: u32,
    position_xy: u32,
    out_pos: *mut bw::Point,
    area_tiles: u32,
    builder: *mut bw::Unit,
    orig: &dyn Fn(u32, u32, *mut bw::Point, u32, *mut bw::Unit) -> u32,
) -> u32 {
    let result = orig(unit_id, position_xy, out_pos, area_tiles, builder);
    if out_pos.is_null() {
		return result;
    }
    let builder = match Unit::from_ptr(builder) {
        Some(s) => s,
        None => { 
			return result;	
		},
    };
    let ai = match builder.worker_ai() {
        Some(s) => s,
        None => {
			return result;
		},
    };
    let town = (*ai).town;
    let unit_id = UnitId(unit_id as u16);
    let player = builder.player();
    let globals = Globals::get("place building hook");
    let unit_search = UnitSearch::from_bw();
    if let Some(town_id) = globals.town_ids.iter().find(|x| x.town.0 == town) {
        let game = Game::get();
        let layouts = globals
            .base_layouts
            .layouts
            .iter()
            .filter(|x| {
                x.unit_id == unit_id &&
                    (x.town_id == town_id.id || (x.town_id == 255 && x.town.0 == town)) &&
                    x.player == player
            })
            .filter(|layout| {
                let existing_count = unit_search
                    .search_iter(&layout.pos)
                    .filter(|u| u.player() == layout.player && u.id() == layout.unit_id)
                    .count();
                existing_count < usize::from(layout.amount)
            });
		let assume_pushing_units = globals.ai_mode[builder.player() as usize].move_from_baselayout;
        for layout in layouts {
            //uses tiles instead of pixels
            let rect_x = layout.pos.right / 32;
            let rect_y = layout.pos.bottom / 32;
            let pos_x = layout.pos.left / 32;
            let pos_y = layout.pos.top / 32;
            let offset_x = layout.pos.left - (pos_x * 32);
            let offset_y = layout.pos.top - (pos_y * 32);
            for i in pos_x..rect_x + 1 {
                for j in pos_y..rect_y + 1 {												 
                    let mut ok = check_placement(game, &unit_search, builder, i, j, unit_id,assume_pushing_units,
												bw::Point{x: (i*32)+offset_x,y: (j*32+offset_y)});								
                    if ok {
                        let mut workers = (*town).workers;
                        while workers != null_mut() {
                            let current = (*workers).parent;
                            if current != builder.0 {
                                if (*current).order_target_pos.x == i * 32 &&
                                    (*current).order_target_pos.y == j * 32
                                {
                                    ok = false;
                                    break;
                                }
                            }
                            workers = (*workers).next;
                        }
                    }
                    if ok {
						debug!(
                            "Placing {:x} to {}, {} for player {:x}",
                            unit_id.0, (i*32)+offset_x, (j*32)+offset_y, player
                        );						
                        // modify coordinates by offset for buildings (offset will be 0 is width/height has even number of tiles)
                        // to avoid placement of structures unaligned to tile grid
                        (*out_pos).x = (i * 32) + offset_x;
                        (*out_pos).y = (j * 32) + offset_y;						
                        return result;
                    }
                }
            }
        }
    }
    result
}



pub fn block_check(
	u: Unit,
	builder: Unit,
	assume_pushing_units: bool,
)->bool{
	use bw_dat::unit::{SPIDER_MINE,SIEGE_TANK_SIEGE,SUNKEN_COLONY};
	if u.id()==SPIDER_MINE {
		return true;
	}
	if assume_pushing_units && !u.id().is_building() {
		if (u.player()==builder.player() || u.player()==11) 
		&& !u.is_disabled() && u.id()!=SIEGE_TANK_SIEGE && u.id()!=SUNKEN_COLONY
		{
			
			return false;
		}
	}
	true
}	
													  
pub unsafe fn check_placement(
    game: Game,
    unit_search: &UnitSearch,
    builder: Unit,
    x_tile: i16,
    y_tile: i16,
    unit_id: UnitId,
	assume_pushing_units: bool,   
	position: bw::Point,
) -> bool {
    let map_width = (*game.0).map_width_tiles;
    let map_height = (*game.0).map_height_tiles;
    let zerg = unit_id.group_flags() & 0x1 != 0;
    let require_creep = unit_id.require_creep();
    let forbid_creep = !require_creep && !zerg;

    let placement = unit_id.placement();
	let x_off = ((placement.width as i16)/2).saturating_sub(position.x-(x_tile.saturating_mul(32)));
	let y_off = ((placement.height as i16)/2).saturating_sub(position.y-(y_tile.saturating_mul(32)));
    let area = bw::Rect {
        left: (x_tile * 32)-x_off,
        top: (y_tile * 32)-y_off,
        right: (x_tile * 32) + (placement.width as i16)-x_off,
        bottom: (y_tile * 32) + (placement.height as i16)-y_off,
    };
    let blocked = unit_search
        .search_iter(&area)
        .any(|u| u != builder && !u.is_air() && block_check(u,builder,assume_pushing_units));

    if blocked {
		//debug test
        return false;
    }
    let width_tiles = placement.width / 32;
    let height_tiles = placement.height / 32;
    if area.left as u16/32 + width_tiles as u16> map_width || area.top as u16/32 + height_tiles as u16 > map_height {
        return false;
    }

    if unit_id.is_town_hall() {
        let area = bw::Rect {
            left: position.x.saturating_sub(3 * 32) - (placement.width as i16 / 2),
            top: position.y.saturating_sub(3 * 32)-(placement.height as i16 / 2),
            right: position.x + (placement.width as i16)/2 + (3 * 32),
            bottom: position.y + (placement.height as i16)/2 + (3 * 32),
        };
        let res_units = unit_search
            .search_iter(&area)
            .any(|u| u.id().is_resource_container());
        if res_units {
            return false;
        }
    }

    for px in 0..width_tiles {
        for py in 0..height_tiles {
            let tile = *(*bw::tile_flags).offset(
                (px + (area.left as u16/32)) as isize + (map_width * (py + (area.top as u16/32))) as isize,
            );
            if tile & 0x0080_0000 != 0 {
                //unbuildable
					
                return false;
            }
            if tile & 0x1000_0000 != 0 {
                //creep disappearing
                if forbid_creep || require_creep {
						
                    return false;
                }
            }
			
            let creep_tile = tile & 0x0040_0000 != 0;
            if (!creep_tile && require_creep) || (creep_tile && forbid_creep) {
					if unit_id.0==172 {
		}
                return false;
            }
        }
    }
    if unit_id.require_psi() {
        let powered = bw::is_powered(
            (x_tile as u32) * 32,
            (y_tile as u32) * 32,
            builder.player(),
            unit_id.0 as u32,
        );
        if powered == 0 {		
            return false;
        }
		
    }
    true
}
pub unsafe extern fn add_layout(
    script: *mut bw::AiScript,
    unit_id: UnitId,
    layout_modifier: u8,
    src: Vec<Position>,
    amount: u8,
    town_id: u8,
    priority: u8,
	unique_id: String,
) {
    #[derive(Eq, PartialEq, Copy, Clone, Debug)]
    enum LayoutModifier {
        Set,
        Remove,
    }
    if feature_disabled("base_layout") {
        return;
    }
    let layout_modifier = match layout_modifier {
        0 => LayoutModifier::Set,
        1 => LayoutModifier::Remove,
        x => {
            bw_print!("Unsupported layout modifier in base_layout: {:x}", x);
            return;
        }
    };

    let mut globals = Globals::get("ais base_layout");
    let town = Town::from_ptr((*script).town);
	for pos in src {
		if let Some(town) = town {
			let layout = BaseLayout {
				pos: pos.area,
				player: (*script).player as u8,
				unit_id,
				amount,
				town_id,
				town,
				priority,
				unique_id: unique_id.clone(),
                is_placeholder: false,
			};

			match layout_modifier {
				LayoutModifier::Set => globals.base_layouts.try_add(layout),
				LayoutModifier::Remove => globals.base_layouts.try_remove(&layout),
			}
		}	
	}

}
 
pub unsafe fn town_from_id(script: *mut bw::AiScript, globals: &mut Globals, id: u8) -> Option<Town> {
    let town_src = match id {
        255 => Town::from_ptr((*script).town),
        _ => globals
            .town_ids
            .iter()
            .find(|x| id == x.id && u32::from((*x.town.0).player) == (*script).player)
            .map(|x| x.town),
    };
    town_src
}

pub unsafe extern fn max_build(script: *mut bw::AiScript) {
	let mut read = ScriptData::new(script);
    let mut globals = Globals::get("ais maxbuild");
	let town_id = read.read_u8();
	let unit_id = UnitId(read.read_u16());
	let quantity = read.read_u16();
	if feature_disabled("max_build") {
        return;
    }
	let id = town_from_id(script, &mut globals, town_id);
//	bw_print!("Max_build current town pointer: {:p}",(*script).town);
	let town = match id {
		None => {
			bw_print!("town id {:x} for max build do not exist", town_id);
			return;
		}
		Some(s) => Some(s),
	};
	let build = BuildMax {
        town,
        quantity,
        unit_id,
    };
	globals.build_max.add(build);
}

//#[derive(Eq, PartialEq, Copy, Clone, Debug)]
#[derive(Debug, Serialize, Deserialize, Copy, Clone, Eq, PartialEq)]//buildq
pub enum LocalModifier {
	Local,
	Global,
}

pub unsafe extern fn try_add_queue(script: *mut bw::AiScript, town_id: u8, quantity: u8, unit_id: UnitId, 
						factory_id: UnitMatch, modifier: LocalModifier, src: Position, priority: u8, globals: &mut Globals){
    let town = match modifier {
        LocalModifier::Global => None,
        LocalModifier::Local => {
            let id = town_from_id(script, globals, town_id);
            match id {
                None => {
                    bw_print!("town id {:x} for local queue do not exist", town_id);
                    return;
                }
                Some(s) => Some(s),
            }
        }
    };
    let queue = UnitQueue {
        player: (*script).player as u8,
        current_quantity: quantity,
        unit_id,
        factory_id,
        town,
        pos: src.area,
        priority,
    };
    globals.queues.add(queue);
}

pub unsafe extern fn queue(script: *mut bw::AiScript) {
    
    //quantity unit_id factory_id id modifier location priority
    let mut read = ScriptData::new(script);
    let mut globals = Globals::get("ais queue");
    let quantity = read.read_u8();
    let unit_id = UnitId(read.read_u16());
    let factory_id = UnitId(read.read_u16());
    let town_id = read.read_u8();
    let modifier = read.read_u8();
    let mut src = read.read_position();
    let radius = read.read_u16();
    src.extend_area(radius as i16);
    let priority = read.read_u8();
    if feature_disabled("queue") {
        return;
    }
    let modifier = match modifier {
        0 => LocalModifier::Local,
        1 => LocalModifier::Global,
        x => {
            bw_print!("Unsupported local modifier in queue: {:x}", x);
            return;
        }
    };
	try_add_queue(script,town_id,quantity,unit_id,UnitMatch { units: vec![factory_id] },modifier,src,priority,&mut globals);
	
	/*
    let town = match modifier {
        LocalModifier::Global => None,
        LocalModifier::Local => {
            let id = town_from_id(script, &mut globals, town_id);
            match id {
                None => {
                    bw_print!("town id {:x} for local queue do not exist", town_id);
                    return;
                }
                Some(s) => Some(s),
            }
        }
    };
    let queue = UnitQueue {
        player: (*script).player as u8,
        current_quantity: quantity,
        unit_id,
        factory_id,
        town,
        pos: src.area,
        priority,
    };
    globals.queues.add(queue);
	*/
}
pub unsafe extern fn defense_command(script: *mut bw::AiScript) {
	#[derive(Eq, PartialEq, Copy, Clone, Debug)]
	enum DefenseType {
        Build,
        Use,
    }
	#[derive(Eq, PartialEq, Copy, Clone, Debug)]
	enum DefenseUnit {
        Ground,
        Air,
    }
	//defense   amount unit
	let mut read = ScriptData::new(script);
	let amount = read.read_u16();
	let mut unit = UnitId(read.read_u16());
	let defense_type = read.read_u8();
	let first = read.read_u8();
	let second = read.read_u8();
	let ai_data = bw::player_ai((*script).player);
	let globals = Globals::get("ais defense");
	unit = globals.unit_replace.replace_check(unit);//for replace_unit
	let defense_type = match defense_type {
        0 => DefenseType::Build,
        1 => DefenseType::Use,
        x => {
            bw_print!("Unsupported defense type in defense command: {:x}", x);
            return;
        }
    };
	let first = match first {
        0 => DefenseUnit::Ground,
        1 => DefenseUnit::Air,
        x => {
            bw_print!("Unsupported defense direction in defense command: {:x}", x);
            return;
        }
    };
	let second = match second {
        0 => DefenseUnit::Ground,
        1 => DefenseUnit::Air,
        x => {
            bw_print!("Unsupported defense direction in defense command: {:x}", x);
            return;
        }
    };
	
	let defense_list: &mut [u16] = match defense_type {
		DefenseType::Build=>{
			if first==DefenseUnit::Ground && second==DefenseUnit::Ground {
				&mut (*ai_data).ground_vs_ground_build_def
			}
			else if first==DefenseUnit::Ground && second==DefenseUnit::Air {
				&mut (*ai_data).ground_vs_air_build_def
			}
			else if first==DefenseUnit::Air && second==DefenseUnit::Ground {
				&mut (*ai_data).air_vs_ground_build_def
			}
			else {
				&mut (*ai_data).air_vs_air_build_def
			}					
		},
		DefenseType::Use=>{
			if first==DefenseUnit::Ground && second==DefenseUnit::Ground {
				&mut (*ai_data).ground_vs_ground_use_def
			}
			else if first==DefenseUnit::Ground && second==DefenseUnit::Air {
				&mut (*ai_data).ground_vs_air_use_def
			}
			else if first==DefenseUnit::Air && second==DefenseUnit::Ground {
				&mut (*ai_data).air_vs_ground_use_def
			}
			else {
				&mut (*ai_data).air_vs_air_use_def
			}
		},
	};
	let free_slots = defense_list.iter_mut().skip_while(|x| **x != 0).take(amount as usize);
	for defense_entry in free_slots {
	  *defense_entry = unit.0 + 1;
	}
}
pub unsafe extern fn replace_requests(script: *mut bw::AiScript) {
	// Replacing guard, attack_add, defense, guard, do_morph, train, load_bunkers
    let mut read = ScriptData::new(script);
    let id_first = UnitId(read.read_u16());
	let id_second = UnitId(read.read_u16());
	//debug!("Replace unit {:x} with {:x}",id_first.0,id_second.0);
	let ai_data = bw::player_ai((*script).player);
	//replace build requests
	//unit_id
	let mut globals = Globals::get("ais replace_requests");
	globals.unit_replace.add(id_first,id_second);	
	let replace_defense_requests = |list: &mut [u16]| {
	  for x in list {
		if *x == id_first.0 + 1 {
		   *x = id_second.0 + 1;
		}
	  }
	};
	replace_defense_requests(&mut (*ai_data).ground_vs_ground_build_def);
	replace_defense_requests(&mut (*ai_data).ground_vs_air_build_def);
	replace_defense_requests(&mut (*ai_data).air_vs_ground_build_def);
	replace_defense_requests(&mut (*ai_data).air_vs_air_build_def);
	replace_defense_requests(&mut (*ai_data).ground_vs_ground_use_def);
	replace_defense_requests(&mut (*ai_data).ground_vs_air_use_def);
	replace_defense_requests(&mut (*ai_data).air_vs_ground_use_def);
	replace_defense_requests(&mut (*ai_data).air_vs_air_use_def);
	let mut guard = bw::guard_ais((*script).player as u8);
	while guard != null_mut(){
		if(*guard).unit_id==id_first.0{
			(*guard).unit_id = id_second.0;
		}
		guard=(*guard).next;
	}

}		 												   

pub unsafe extern fn lift_land(script: *mut bw::AiScript) {
    let mut read = ScriptData::new(script);
    //unitId quantity liftLocation landLocation lifttownid landtownid hpval
    let unit_id = UnitId(read.read_u16());
    let amount = read.read_u8();
    let mut src = read.read_position();
    let radius = read.read_u16();
    src.extend_area(radius as i16);
    let mut tgt = read.read_position();
    let radius_target = read.read_u16();
    tgt.extend_area(radius_target as i16);
    let id_source = read.read_u8();
    let id_target = read.read_u8();
    let return_hp_percent = read.read_u8();
    if feature_disabled("lift_land") {
        return;
    }
	
    let mut globals = Globals::get("ais lift_land");
    let town_src = town_from_id(script, &mut globals, id_source);
    let town_tgt = town_from_id(script, &mut globals, id_target);
	
	

	
    if let Some(town_src) = town_src {
		//bw_print!("Current town data: pointer {:p}, current script: {:p}",town_src.0,(*script).town);
        if let Some(town_tgt) = town_tgt {
            let lift_land = LiftLandBuilding {
                player: (*script).player as u8,
                unit_id: unit_id,
                src: src.area,
                tgt: tgt.area,
                town_src,
                town_tgt,
                return_hp_percent,
                state: Default::default(),
            };
            globals.lift_lands.add(lift_land, amount);
        } else {
            //debug!("No target town");
        }
    } else {
		//bw_print!("Current town data: no pointer found, current script: {:p}",(*script).town);
        //debug!("No src town");
    }
}

pub unsafe extern fn base_layout(script: *mut bw::AiScript) {
    // base_layout(unit, modifier, src_area, amount, town_id)
    let mut read = ScriptData::new(script);
    let unit = UnitId(read.read_u16());
    let layout_modifier = read.read_u8();
    let mut src = read.read_position();
    let radius = read.read_u16();
    src.extend_area(radius as i16);
    let amount = read.read_u8();
    let town_id = read.read_u8();
    let priority = read.read_u8();
    add_layout(
        script,
        unit,
        layout_modifier,
        vec![src],
        amount,
        town_id,
        priority, 
		"".to_string(),
    );
}
pub unsafe extern fn base_layout_old(script: *mut bw::AiScript) {
    // base_layout(unit, modifier, src_area, amount, town_id)
    let mut read = ScriptData::new(script);
    let unit = UnitId(read.read_u16());
    let layout_modifier = read.read_u8();
    let mut src = read.read_position();
    let radius = read.read_u16();
    src.extend_area(radius as i16);
    let amount = read.read_u8();
    let town_id = read.read_u8();
    add_layout(script, unit, layout_modifier, vec![src], amount, town_id, 50, "".to_string());
}

pub unsafe extern fn guard_command(script: *mut bw::AiScript) {
    let mut read = ScriptData::new(script);
    let mut unit = UnitId(read.read_u16());
    let target = read.read_position();
    let quantity = read.read_u8();
    let death_limit = read.read_u8();
    let priority = read.read_u8();
    if feature_disabled("guard") {
        return;
    }
    let mut globals = Globals::get("ais guard");
    unit = globals.unit_replace.replace_check(unit);
    for _n in 0..quantity {
        let guards = samase::guard_ais().add((*script).player as usize);
        let old_first_active = (*guards).first;
        let new_ai = (*(*guards).array).first_free;
		if new_ai.is_null(){
			//commit 021a1d7
			break;
		}
		(*new_ai) = bw::GuardAi {
			next: (*new_ai).next,
			prev: (*new_ai).prev,
			ai_type: 1,
			times_died: 0,
			dca: [0, 0],
			parent: null_mut(),
			unit_id: unit.0,
			home: target.center,
			other_home: target.center,
			padding1a: [0, 0],
			previous_update: 0,
		};
		let new_first_free = (*new_ai).next;
		(*(*guards).array).first_free = new_first_free;
		if !new_first_free.is_null() {
			(*new_first_free).prev = null_mut();
		}
		(*new_ai).next = old_first_active;
		if !old_first_active.is_null() {
			(*old_first_active).prev = new_ai;
		}
		(*guards).first = new_ai;
		globals.guards.add(
			(*(*guards).array).ais.as_mut_ptr(),
			new_ai,
			death_limit,
			priority,
		);
		
    }
}

pub unsafe extern fn create_script(script: *mut bw::AiScript) {
    // create_script(pos, player, area, town, resarea)
    let mut read = ScriptData::new(script);
    let pos = read.read_jump_pos();
    let player = match read.read_u8() {
        255 => (*script).player as u8,
        x => x,
    };
	let mut area = read.read_position();
    let radius = read.read_u16();
    area.extend_area(radius as i16);
    if area.center.x == -2 && area.center.y == -2 {
        area = Position::from_rect32(&(*script).area)
    }
    let town = read.read_u8();
    let resarea = match read.read_u8() {
        255 => (((*script).flags >> 3) & 0xff) as u8,
        x => x,
    };
    if feature_disabled("create_script") {
        return;
    }
    let town = match town {
        0 => null_mut(),
        255 => (*script).town,
        _ => {
            bw_print!("Invalid town in create_script");
            return;
        }
    };
    debug!("CreateScript Init");
    let mut globals = Globals::get("ais create_script");
    let flags = ((*script).flags & 1) | ((resarea as u32) << 3);
    let first_ai_script = bw::first_ai_script();
    let script = globals.ai_scripts.alloc(Script {
        bw: bw::AiScript {
            next: first_ai_script,
            prev: null_mut(),
            pos,
            wait: 0,
            player: player as u32,
            area: bw::Rect32 {
                left: i32::from(area.area.left),
                top: i32::from(area.area.top),
                right: i32::from(area.area.right),
                bottom: i32::from(area.area.bottom),
            },
            center: bw::Point32 {
                x: i32::from(area.center.x),
                y: i32::from(area.center.y),
            },
            town,
            flags,
        },
        delete_mark: false,
        call_stack: Vec::new(),
		wait_for: 0,
		waitfor_sequence: 0,
    });
    (*first_ai_script).prev = &mut (*script).bw;
    bw::set_first_ai_script(&mut (*script).bw);
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn take_scripts_none() {
        unsafe {
            let mut scripts = BlockAllocSet::new();
            let (new_first, first_free) =
                take_bw_allocated_scripts(&mut scripts, null_mut(), null_mut());
            assert!(new_first.is_null());
            assert!(first_free.is_null());
            assert_eq!(scripts.len(), 0);
            clear_deleted_scripts(&mut scripts, null_mut());
            assert_eq!(scripts.len(), 0);
        }
    }

    fn dummy_script(num: u32) -> bw::AiScript {
        let mut script: bw::AiScript = unsafe { mem::zeroed() };
        script.pos = num;
        script
    }

    unsafe fn dummy_list(amt: usize, base: u32) -> Vec<bw::AiScript> {
        let mut external = Vec::new();
        for i in 0..amt {
            external.push(dummy_script(base + i as u32));
        }
        let mut prev = null_mut();
        for i in 0..amt {
            external[i].prev = prev;
            if i != 0 {
                external[i - 1].next = &mut external[i];
            }
            prev = &mut external[i];
        }
        validate_links(&mut external[0], amt);
        external
    }

    unsafe fn validate_links(start: *mut bw::AiScript, expected_len: usize) {
        let mut len = 0;
        let mut script = start;
        assert!((*script).prev.is_null());
        while !script.is_null() {
            if !(*script).prev.is_null() {
                assert_eq!((*(*script).prev).next, script, "Script {} prevnext", len);
            }
            if !(*script).next.is_null() {
                assert_eq!((*(*script).next).prev, script, "Script {} nextprev", len);
            }
            script = (*script).next;
            len += 1;
        }
        assert_eq!(len, expected_len);
    }

    #[test]
    fn take_scripts() {
        unsafe {
            let mut scripts = BlockAllocSet::new();
            let mut external = dummy_list(10, 0);

            let mut lone_free = dummy_script(123);
            let (new_first, first_free) =
                take_bw_allocated_scripts(&mut scripts, &mut external[0], &mut lone_free);
            assert!(!new_first.is_null());
            assert!(external
                .iter_mut()
                .any(|x| (&mut *x) as *mut _ == first_free));
            assert_eq!(scripts.len(), 10);
            validate_links(new_first, 10);
            validate_links(first_free, 11);

            let mut external = dummy_list(20, 100);
            external[19].next = new_first;
            (*new_first).prev = &mut external[19];
            let (new_first, first_free) =
                take_bw_allocated_scripts(&mut scripts, &mut external[0], first_free);
            assert!(!new_first.is_null());
            assert!(external
                .iter_mut()
                .any(|x| (&mut *x) as *mut _ == first_free));
            assert_eq!(scripts.len(), 30);
            validate_links(new_first, 30);
            validate_links(first_free, 31);

            let mut script = new_first;
            let mut i = 0;
            while !script.is_null() {
                if i < 20 {
                    assert_eq!((*script).pos, 100 + i);
                } else {
                    assert_eq!((*script).pos, i - 20);
                }
                script = (*script).next;
                i += 1;
            }
        }
    }

    #[test]
    fn delete_scripts() {
        unsafe {
            let mut scripts = BlockAllocSet::new();
            let mut external = dummy_list(10, 0);

            let (new_first, first_free) =
                take_bw_allocated_scripts(&mut scripts, &mut external[0], null_mut());
            assert_eq!(scripts.len(), 10);
            validate_links(new_first, 10);
            validate_links(first_free, 10);

            let (new_first, first_free) =
                take_bw_allocated_scripts(&mut scripts, new_first, first_free);
            assert_eq!(scripts.len(), 10);
            validate_links(new_first, 10);
            validate_links(first_free, 10);

            // Remove the first script
            let second = (*new_first).next;
            (*new_first).next = first_free;
            (*first_free).prev = new_first;
            let first_free = new_first;
            let new_first = second;
            (*new_first).prev = null_mut();
            validate_links(new_first, 9);
            validate_links(first_free, 11);
            clear_deleted_scripts(&mut scripts, new_first);
            assert_eq!(scripts.len(), 9);

            // Remove the second and fourth script
            let second = (*new_first).next;
            let fourth = (*(*second).next).next;
            (*(*second).next).prev = (*second).prev;
            (*(*second).prev).next = (*second).next;
            (*(*fourth).next).prev = (*fourth).prev;
            (*(*fourth).prev).next = (*fourth).next;
            // Too lazy to add them to free list D:
            validate_links(new_first, 7);
            clear_deleted_scripts(&mut scripts, new_first);
            assert_eq!(scripts.len(), 7);

            // Remove the 4th, 6th and 7th scripts
            let second = (*new_first).next;
            let fourth = (*(*second).next).next;
            let fifth = (*fourth).next;
            (*(*fourth).next).prev = (*fourth).prev;
            (*(*fourth).prev).next = (*fourth).next;
            (*fifth).next = null_mut();
            validate_links(new_first, 4);
            clear_deleted_scripts(&mut scripts, new_first);
            assert_eq!(scripts.len(), 4);
        }
    }

    #[test]
    fn town_buildreq_removal() {
        // Always removes unit id 8
        unsafe fn check(mut town: bw::AiTown, remove_count: u8, remaining: &[bw::TownReq]) {
            let mut units = UnitMatch {
                units: vec![UnitId(8)],
            };
            println!("Remove count is: {:#?}", remove_count);

            remove_build_from_town(Town(&mut town), &mut units, remove_count);
            for i in 0..remaining.len() {
                println!(
                    "Flags and count is: {:#?}",
                    town.town_units[i].flags_and_count
                );
                assert_ne!(town.town_units[i].flags_and_count >> 3, 0);
            }
            assert_eq!(town.town_units[remaining.len()].flags_and_count >> 3, 0);
            for rem in remaining {
                let ok = (0..remaining.len()).any(|i| town.town_units[i] == *rem);
                if !ok {
                    panic!(
                        "(line {}) Couldn't find {:#x?} after removal, remaining were {:#x?}",
                        line!(),
                        rem,
                        &town.town_units[..remaining.len()],
                    );
                }
            }
        }
        unsafe {
            let req = |amt: u8, id: u16, flag| bw::TownReq {
                flags_and_count: if flag { 0x1 } else { 0x0 } | (amt << 3),
                priority: 50,
                id,
            };
            println!("Unsafe test");
            let mut town: bw::AiTown = mem::zeroed();
            town.town_units[0] = req(5, 8, false);
            town.town_units[1] = req(8, 8, false);
            // Removes one, reduces one
            let remaining = vec![req(2, 8, false)];
            check(town, 6, &remaining);

            let mut town: bw::AiTown = mem::zeroed();
            town.town_units[0] = req(5, 8, false);
            town.town_units[1] = req(9, 8, false);
            town.town_units[2] = req(10, 7, true);
            town.town_units[3] = req(5, 9, false);
            town.town_units[4] = req(8, 8, true);
            town.town_units[5] = req(5, 1, false);
            // Removes all of the unit 8
            let remaining = vec![town.town_units[2], town.town_units[3], town.town_units[5]];
            check(town, 11, &remaining);

            // Check that two of the unit 8 reqs stay
            let mut town: bw::AiTown = mem::zeroed();
            town.town_units[0] = req(5, 8, false);
            town.town_units[1] = req(9, 8, false);
            town.town_units[2] = req(5, 8, false);
            town.town_units[3] = req(10, 7, true);
            town.town_units[4] = req(5, 9, false);
            town.town_units[5] = req(8, 8, true);
            town.town_units[6] = req(1, 8, true);
            town.town_units[7] = req(5, 1, false);
            town.town_units[8] = req(1, 8, true);
            let remaining = vec![
                town.town_units[3],
                town.town_units[4],
                town.town_units[7],
                req(3, 8, false),
                req(2, 8, true),
            ];
            check(town, 6, &remaining);
        }
    }

    #[test]
    fn script_data_reading() {
        use byteorder::{WriteBytesExt, LE};

        let mut buf = vec![];
        buf.write_u32::<LE>(0x123).unwrap();
        buf.write_u32::<LE>(43242).unwrap();
        buf.write_u16::<LE>(12345).unwrap();
        for &c in b"test test \0".iter() {
            buf.push(c);
        }
        buf.write_u16::<LE>(941).unwrap();
        unsafe {
            let mut script: bw::AiScript = mem::zeroed();
            script.pos = 4;
            let mut read = ScriptData {
                start: buf.as_ptr(),
                pos: buf.as_ptr().add(4),
                script: &mut script,
            };
            assert_eq!(read.read_u32(), 43242);
            assert_eq!(read.read_u16(), 12345);
            assert_eq!(read.read_string(), b"test test ");
            assert_eq!(read.read_u16(), 941);
            assert_eq!(script.pos, buf.len() as u32);
        }
    }

    #[test]
    fn script_data_unit_match() {
        use byteorder::{WriteBytesExt, LE};

        let mut buf = vec![];
        buf.write_u32::<LE>(0x123).unwrap();
        buf.write_u16::<LE>(0x33).unwrap();
        buf.write_u16::<LE>(0xff04).unwrap();
        buf.write_u16::<LE>(0x123).unwrap();
        buf.write_u16::<LE>(0x110).unwrap();
        buf.write_u16::<LE>(0x30).unwrap();
        buf.write_u16::<LE>(0x70).unwrap();
        unsafe {
            let mut script: bw::AiScript = mem::zeroed();
            script.pos = 4;
            let mut read = ScriptData {
                start: buf.as_ptr(),
                pos: buf.as_ptr().add(4),
                script: &mut script,
            };
            assert_eq!(read.read_unit_match().units, vec![UnitId(0x33)]);
            let eq = vec![UnitId(0x123), UnitId(0x110), UnitId(0x30), UnitId(0x70)];
            assert_eq!(read.read_unit_match().units, eq);
            assert_eq!(script.pos, buf.len() as u32);
        }
    }

    #[test]
    fn script_data_long_jumps() {
        use byteorder::{WriteBytesExt, LE};

        let mut buf = vec![];
        buf.write_u32::<LE>(0x123456).unwrap();
        buf.write_u32::<LE>(0x12345678).unwrap();
        buf.write_u32::<LE>(0x33113322).unwrap();
        unsafe {
            let mut script: bw::AiScript = mem::zeroed();
            script.pos = 4;
            let mut read = ScriptData {
                start: buf.as_ptr(),
                pos: buf.as_ptr().add(4),
                script: &mut script,
            };
            assert_eq!(read.read_jump_pos(), 0x12345678);
            assert_eq!(read.read_jump_pos(), 0x33113322);
            assert_eq!(script.pos, buf.len() as u32);
        }
    }

    #[test]
    fn script_data_short_jumps() {
        use byteorder::{WriteBytesExt, LE};

        let mut buf = vec![];
        buf.write_u32::<LE>(0x1234).unwrap();
        buf.write_u32::<LE>(0x12345678).unwrap();
        buf.write_u32::<LE>(0x33113322).unwrap();
        unsafe {
            let mut script: bw::AiScript = mem::zeroed();
            script.pos = 4;
            let mut read = ScriptData {
                start: buf.as_ptr(),
                pos: buf.as_ptr().add(4),
                script: &mut script,
            };
            assert_eq!(read.read_jump_pos(), 0x5678);
            assert_eq!(read.read_jump_pos(), 0x1234);
            assert_eq!(read.read_jump_pos(), 0x3322);
            assert_eq!(read.read_jump_pos(), 0x3311);
            assert_eq!(script.pos, buf.len() as u32);
        }
    }
}
